import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatTooltipModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTabsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDividerModule,
    MatRadioModule
} from '@angular/material';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ButtonLoaderComponent } from '@Components/button-loader/button-loader.component';
import { ContactsFilterPipe } from '@Components/chat/pipes/contact-filter.pipe';

import { AvatarModule, AvatarSource } from 'ngx-avatar';
import { LoaderModule } from '@Components/loader/loader.module';
import { FooterModule } from './Components/footer/footer.module';
import { AvailableForDirective } from '@Directives/available-for.directive';
import { ForbiddenForDirective } from '@Directives/forbidden-for.directive';
import { MediaPreviewModule } from '@Components/media-preview/media-preview.module';
import { SafeUrlPipe } from './Pipes/safe-url.pipe';
import { SafeHtmlPipe } from './Pipes/safe-html.pipe';

const avatarSourcesOrder = [AvatarSource.CUSTOM, AvatarSource.INITIALS];

@NgModule({
    declarations: [
        ButtonLoaderComponent,
        ContactsFilterPipe,
        SafeUrlPipe,
        SafeHtmlPipe,
        AvailableForDirective,
        ForbiddenForDirective
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        MatFormFieldModule,
        MatTabsModule,
        MatRadioModule,
        MatCheckboxModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatTooltipModule,
        MatCardModule,
        MatDialogModule,

        AvatarModule.forRoot({
            sourcePriorityOrder: avatarSourcesOrder
        }),

        LoaderModule,
        MediaPreviewModule,
        FooterModule
    ],
    exports: [
        ButtonLoaderComponent,

        FormsModule,
        ReactiveFormsModule,

        MatFormFieldModule,
        MatTabsModule,
        MatRadioModule,
        MatCheckboxModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatTooltipModule,
        MatDialogModule,
        MatCardModule,
        MatInputModule,
        MatDividerModule,
        MatDatepickerModule,
        MatNativeDateModule,

        LoaderModule,
        SafeUrlPipe,
        SafeHtmlPipe,
        ContactsFilterPipe,
        AvailableForDirective,
        ForbiddenForDirective,
        AvatarModule,
        MediaPreviewModule,
        FooterModule
    ],
    providers: []
})
export class SharedModule {}
