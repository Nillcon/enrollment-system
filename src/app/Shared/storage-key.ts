export enum StorageKey {
    Access_Token                    = 'access_token',
    Debug_Host                      = 'debug_host',
    Local_User_Token                = 'local_user_token'
}
