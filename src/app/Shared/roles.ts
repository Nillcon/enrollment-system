import { IRole } from '@Interfaces/role.interface';

export enum Roles {
    Anonymous       = 0,
    InvitedUser     = 1,
    SuperUser       = 2,
    Staff           = 3,
    Support         = 4,
    Student         = 5
}

export const RolesList: { [key: number]: IRole } = {
    [Roles.Anonymous]: {
        id: Roles.Anonymous,
        name: 'Anonymous'
    },
    [Roles.InvitedUser]: {
        id: Roles.InvitedUser,
        name: 'Invited user'
    },
    [Roles.SuperUser]: {
        id: Roles.SuperUser,
        name: 'Super user',
        signInRedirectLink: 'Chat'
    },
    [Roles.Staff]: {
        id: Roles.Staff,
        name: 'Staff',
        signInRedirectLink: 'Appointments'
    },
    [Roles.Support]: {
        id: Roles.Support,
        name: 'Support',
        signInRedirectLink: 'Chat'
    },
    [Roles.Student]: {
        id: Roles.Student,
        name: 'Student',
        signInRedirectLink: 'Appointments'
    }
};

export function GetRolesListAsArray (): IRole[] {
    const result: IRole[] = [];

    for (const role of Object.values(RolesList)) {
        result.push(role);
    }

    return result;
}

export function GetRolesListAvailableForUserCreating (): IRole[] {
    const forbiddenRoles: Roles[] = [
        Roles.Anonymous,
        Roles.InvitedUser,
        Roles.SuperUser
    ];
    const result: IRole[] = GetRolesListAsArray();

    return result.filter(role => {
        return !forbiddenRoles.includes(role.id);
    });
}
