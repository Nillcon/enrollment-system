export enum ContentTypeEnum {
    Greeters = 1,
    Text,
    Html,
    Appointment,
    Video,
    Photo,
    Scheduler
}

export interface IContentType {
    id: ContentTypeEnum;
    name: string;
    iconClass: string;
}

export const ContentTypeList: { [key: number]: IContentType } = {
    [ContentTypeEnum.Greeters]: {
        id: ContentTypeEnum.Greeters,
        name: 'Greeters',
        iconClass: 'fas fa-user-graduate'
    },
    [ContentTypeEnum.Text]: {
        id: ContentTypeEnum.Text,
        name: 'Text',
        iconClass: 'fas fa-align-left'
    },
    [ContentTypeEnum.Html]: {
        id: ContentTypeEnum.Html,
        name: 'News',
        iconClass: 'fas fa-newspaper'
    },
    [ContentTypeEnum.Appointment]: {
        id: ContentTypeEnum.Appointment,
        name: 'Appointment',
        iconClass: 'fas fa-handshake'
    },
    [ContentTypeEnum.Video]: {
        id: ContentTypeEnum.Video,
        name: 'Video',
        iconClass: 'fas fa-video'
    },
    [ContentTypeEnum.Scheduler]: {
        id: ContentTypeEnum.Scheduler,
        name: 'Scheduler',
        iconClass: 'far fa-calendar-alt'
    }
};

export function getContentTypeListAsArray (): IContentType[] {
    return Object.values(ContentTypeList);
}
