export enum VariableAndConstantQueryTypes {
    TextContent = 1,
    VariablesTable = 2,
    JoeLionMessage = 3,
    ScenariosConditions = 4,
    ScenariosActionsTextboxes = 5,
    ScenariosActionsSelectboxes = 6,
    Mailing = 7,
    WelcomeMessageAndTitle = 8,
    ScenariosConditionsVariables = 9
}
