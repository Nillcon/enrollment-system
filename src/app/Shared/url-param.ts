export enum UrlParam {
    /** Param wich contains token of user for opening his personal page */
    PersonalLink = 'personalLink'
}
