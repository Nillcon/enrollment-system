export const CustomFontList: string[] = [
    'Arial, Helvetica, sans-serif',
    'Comic Sans MS, Comic Sans MS, cursive',
    'Courier New, Courier New, monospace',
    'Georgia, serif',
    'Impact, Charcoal, sans-serif',
    'Lucida Console, Monaco, monospace',
    'Lucida Sans Unicode, Lucida Grande, sans-serif',
    'Palatino Linotype, Book Antiqua, Palatino, serif',
    'Tahoma, Geneva, sans-serif',
    'Times New Roman, Times New Roman, Times, serif',
    'Trebuchet MS, Trebuchet MS, sans-serif',
    'Verdana, Verdana, Geneva, sans-serif',
    'MS Sans Serif, Geneva, sans-serif',
    'MS Serif, New York, serif'
];
