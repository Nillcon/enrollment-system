import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { NgModule } from '@angular/core';
import { MainLayoutComponent } from '@Layouts/main-layout/main-layout.component';
import { DashboardLayoutComponent } from '@Layouts/dashboard-layout/dashboard-layout.component';
import { ChatLayoutComponent } from '@Layouts/chat-layout/chat-layout.component';

export const routes: Routes = [
    {
        path: 'Dashboard',
        component: DashboardLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: '@Layouts/dashboard-layout/dashboard-layout.module#DashboardLayoutModule'
            }
        ]
    },
    {
        path: 'Chat',
        component: ChatLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: '@Layouts/chat-layout/chat-layout.module#ChatLayoutModule'
            }
        ]
    },
    {
        path: 'SignIn',
        loadChildren: '@Pages/sign-in-page/sign-in-page.module#SignInPageModule'
    },
    {
        path: '',
        component: MainLayoutComponent,
        children: [
            {
                path: 'Index',
                loadChildren: '@Layouts/main-layout/main-layout.module#MainLayoutModule'
            },
            {
                path: '',
                redirectTo: 'Index',
                pathMatch: 'full'
            },
            {
                path: ':personalLink',
                loadChildren: '@Layouts/main-layout/main-layout.module#MainLayoutModule'
            }
        ]
    },
    {
        path: '**',
        redirectTo: 'Index',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes,
            { preloadingStrategy: PreloadAllModules }
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
