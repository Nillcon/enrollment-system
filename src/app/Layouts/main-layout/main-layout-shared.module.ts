import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatModule } from '@Components/chat/chat.module';

@NgModule({
    imports: [
        CommonModule,

        ChatModule
    ],
    exports: [
        ChatModule
    ]
})
export class MainLayoutSharedModule {}
