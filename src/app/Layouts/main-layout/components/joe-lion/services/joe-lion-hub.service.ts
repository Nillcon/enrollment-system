import { Injectable } from '@angular/core';
import { HubConnectionBuilder, HubConnection } from "@aspnet/signalr";
import { HttpRequestService } from '@Services/http-request.service';
import { LocalStorageService } from '@Services/local-storage.service';
import { StorageKey } from '@Shared/storage-key';
import { ILeoInfo } from '../interfaces/leo-info.interface';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class JoeLionHubService {

    private stream: HubConnection;

    private _leoInfo$: BehaviorSubject<ILeoInfo> = new BehaviorSubject(null);

    private readonly hourInMilliseconds = 3600000;

    constructor (
        private httpService: HttpRequestService,
        private storageService: LocalStorageService,
    ) {
        this.connectToJoeLionHub();
    }

    private connectToJoeLionHub() {
        const host = this.httpService.getHost();
        const token = this.storageService.get<string>(StorageKey.Access_Token);

        this.stream = new HubConnectionBuilder()
            .withUrl(`${host}/leoHub`,
                {
                    accessTokenFactory: () => token
                }
            )
            .build();

        this.stream.start()
            .then(() => {
                console.log('Lion connection started');
                this.invokeInfo();
                this.infoObserver();
            })
            .catch(err => console.log('Error while starting connection: ' + err));

        this.stream.serverTimeoutInMilliseconds = this.hourInMilliseconds;
    }

    private infoObserver(): void {
        this.stream.on('Info',
            (res: ILeoInfo) => {
                this._leoInfo$.next(res);
            }
        );
    }

    public invokeInfo (): void {
        this.stream.invoke('GetInfo');
    }

    public async updateMessageForBlock(blockIds: number[]) {
        await this.stream.invoke('GetMessageForBlock', blockIds);
    }

    public get leoInfo$(): Observable<ILeoInfo> {
        return this._leoInfo$.asObservable();
    }
}
