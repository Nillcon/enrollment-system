import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-simple-message',
    templateUrl: './simple-message.component.html',
    styleUrls: ['./simple-message.component.scss']
})
export class SimpleMessageComponent implements OnInit {
    @Input() messageTitle: string;
    @Input() messageText: string;

    @Output() OnOpen: EventEmitter<boolean> = new EventEmitter();
    @Output() OnClose: EventEmitter<boolean> = new EventEmitter();

    public readonly maxLettersCount: number = 140;

    constructor () {}

    ngOnInit () {}

    public open() {
        this.OnOpen.next(true);
    }

    public close() {
        this.OnClose.next(true);
    }
}
