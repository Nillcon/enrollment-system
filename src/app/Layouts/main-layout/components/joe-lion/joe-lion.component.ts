import { Component, OnInit, OnDestroy } from '@angular/core';
import { JoeLionHubService } from './services/joe-lion-hub.service';
import { ILeoInfo } from './interfaces/leo-info.interface';
import { Observable, Subscription } from 'rxjs';
import { IUser } from '@Interfaces/users';
import { WindowedChatService } from '@Components/chat/services/windowed-chat.service';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { tap, filter, take } from 'rxjs/operators';
import { JoeLionFaceList, IJoeLionFace } from './shared/joe-lion-face';
import { UserService } from '@Services/user.service';
import { Roles } from '@Shared/roles';

@AutoUnsubscribe()
@Component({
    selector: 'app-joe-lion',
    templateUrl: './joe-lion.component.html',
    styleUrls: ['./joe-lion.component.scss']
})
export class JoeLionComponent implements OnInit, OnDestroy {

    public isChatOpened: boolean = false;

    public JoeLionFaceList: { [key: number]: IJoeLionFace } = JoeLionFaceList;

    public lionInfo$: Observable<ILeoInfo>;
    public userInfo$: Observable<IUser>;

    public isShowMessage: boolean = true;

    private blockId: number;

    private chatStateSubscription: Subscription;

    constructor (
        private joeLionService: JoeLionHubService,
        private windowedChatService: WindowedChatService,
        private userService: UserService
    ) {}

    ngOnInit () {
        this.initLionInfoUpdating();
        this.chatObserver();
    }

    ngOnDestroy () {}

    private initLionInfoUpdating (): void {
        this.lionInfo$ = this.joeLionService.leoInfo$
            .pipe(
                tap(data => {
                    if (data) {
                        this.blockId = data.blockId;
                    }
                })
            );
    }

    public openChat (): void {
        const isAdmin: boolean = this.userService.Data.role === Roles.SuperUser;

        this.showMessage();

        this.userService.ChatLink$
            .pipe(
                take(1)
            )
            .subscribe((chatLink) => {
                this.windowedChatService.open({
                    analiticsData: {
                        blockId: this.blockId,
                    },
                    chatLink: (isAdmin) ? null : chatLink,
                    isShowContacts: isAdmin
                });
            });
    }

    public showMessage() {
        this.isShowMessage = true;
    }

    public closeMessage() {
        this.isShowMessage = false;
    }

    public closeChat (): void {
        this.windowedChatService.close();
    }

    private chatObserver (): void {
        this.chatStateSubscription = this.windowedChatService.State$
            .subscribe(state => {
                this.isChatOpened = state;
            });
    }
}
