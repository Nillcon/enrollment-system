export enum JoeLionFaceEnum {
    Smiling     = 0,
    Smirking    = 1,
    Cool        = 2,
    Winking     = 3,
    Angel       = 4,
    Sleepy      = 5,
    Angry       = 6,
    Sick        = 7,
    Crying      = 8,
    Laughing    = 9,
    Loving      = 10
}

export interface IJoeLionFace {
    id: JoeLionFaceEnum;
    imageSrc: string;
    name: string;
}

export const JoeLionFaceList: { [key: number]: IJoeLionFace } = {
    [JoeLionFaceEnum.Smiling]: {
        id: JoeLionFaceEnum.Smiling,
        imageSrc: '/assets/images/leo_smile.png',
        name: 'Smiling'
    },
    [JoeLionFaceEnum.Smirking]: {
        id: JoeLionFaceEnum.Smirking,
        imageSrc: '/assets/images/leo_smirking.png',
        name: 'Smirking'
    },
    [JoeLionFaceEnum.Cool]: {
        id: JoeLionFaceEnum.Cool,
        imageSrc: '/assets/images/leo_cool.png',
        name: 'Cool'
    },
    [JoeLionFaceEnum.Winking]: {
        id: JoeLionFaceEnum.Winking,
        imageSrc: '/assets/images/leo_winking.png',
        name: 'Winking'
    },
    [JoeLionFaceEnum.Laughing]: {
        id: JoeLionFaceEnum.Laughing,
        imageSrc: '/assets/images/leo_laugh.png',
        name: 'Laughing'
    },
    [JoeLionFaceEnum.Angel]: {
        id: JoeLionFaceEnum.Angel,
        imageSrc: '/assets/images/leo_angel.png',
        name: 'Angel'
    },
    [JoeLionFaceEnum.Loving]: {
        id: JoeLionFaceEnum.Loving,
        imageSrc: '/assets/images/leo_love.png',
        name: 'Loving'
    },
    [JoeLionFaceEnum.Sleepy]: {
        id: JoeLionFaceEnum.Sleepy,
        imageSrc: '/assets/images/leo_sleepy.png',
        name: 'Sleepy'
    },
    [JoeLionFaceEnum.Angry]: {
        id: JoeLionFaceEnum.Angry,
        imageSrc: '/assets/images/leo_angry.png',
        name: 'Angry'
    },
    [JoeLionFaceEnum.Sick]: {
        id: JoeLionFaceEnum.Sick,
        imageSrc: '/assets/images/leo_sick.png',
        name: 'Sick'
    },
    [JoeLionFaceEnum.Crying]: {
        id: JoeLionFaceEnum.Crying,
        imageSrc: '/assets/images/leo_cry.png',
        name: 'Crying'
    }
};

export function getJoeLionFaceListAsArray (): IJoeLionFace[] {
    return Object.values(JoeLionFaceList);
}
