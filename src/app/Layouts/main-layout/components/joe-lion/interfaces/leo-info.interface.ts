import { MessageBlockTypesEnum } from "@Components/chat/shared/message-block-types";

export interface ILeoInfo {
    avatar: number;
    chatLink: string;
    countOfUnreadMessages: number;
    message: string;
    messageType: MessageBlockTypesEnum;
    blockId: number;
}
