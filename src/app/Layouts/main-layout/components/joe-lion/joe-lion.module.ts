import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JoeLionComponent } from './joe-lion.component';
import { MatButtonModule } from '@angular/material';
import { SimpleMessageComponent } from './components/simple-message/simple-message.component';
import { BubbleMessageComponent } from './components/bubble-message/bubble-message.component';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        JoeLionComponent,
        SimpleMessageComponent,
        BubbleMessageComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        MatButtonModule,
    ],
    exports: [
        JoeLionComponent
    ]
})
export class JoeLionModule { }
