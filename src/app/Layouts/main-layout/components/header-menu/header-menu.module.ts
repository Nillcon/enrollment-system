import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderMenuComponent } from './header-menu.component';
import { SharedModule } from '@App/app-shared.module';
import { RouterModule } from '@angular/router';
import { MatToolbarModule } from '@angular/material';

@NgModule({
    declarations: [
        HeaderMenuComponent
    ],
    imports: [
        CommonModule,

        RouterModule,
        SharedModule,

        MatToolbarModule
    ],
    exports: [
        HeaderMenuComponent
    ]
})
export class HeaderMenuModule { }
