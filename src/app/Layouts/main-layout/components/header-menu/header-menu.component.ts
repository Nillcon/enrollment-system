import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '@Services/user.service';
import { IUser } from '@Interfaces/users/user.interface';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AuthorizeService } from '@Services/authorize.service';
import { Roles } from '@Shared/roles';
import { LocalStorageService } from '@Services/local-storage.service';
import { StorageKey } from '@Shared/storage-key';

@AutoUnsubscribe()
@Component({
    selector: 'app-header-menu',
    templateUrl: './header-menu.component.html',
    styleUrls: ['./header-menu.component.scss']
})
export class HeaderMenuComponent implements OnInit, OnDestroy {
    public readonly Roles: typeof Roles = Roles;

    private userDataSubscription: Subscription;

    public isLocalUser: boolean = false;

    public userData: IUser;

    constructor (
        private userService: UserService,
        private storageService: LocalStorageService,
        private authorizeService: AuthorizeService
    ) {}

    ngOnInit () {
        this.userDataSubscription = this.userService.Data$
            .subscribe(data => {
                this.userData = data;
            });

        this.isLocalUser = Boolean(this.storageService.get(StorageKey.Local_User_Token));
    }

    ngOnDestroy () {}

    /** Removes "Local_User_Token" key from localStorage and changes current token on that one from the localStorage key */
    public exitFromLocalStatus (): void {
        this.storageService.set(StorageKey.Access_Token, this.storageService.get(StorageKey.Local_User_Token));
        this.storageService.remove(StorageKey.Local_User_Token);
        location.href = location.origin + '/Dashboard';
    }

    public exitFromAccount (): void {
        this.authorizeService.signOut();
    }
}
