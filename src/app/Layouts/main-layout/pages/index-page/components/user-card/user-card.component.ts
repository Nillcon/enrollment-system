import { Component, OnInit, Input } from '@angular/core';
import { IStaffUser } from '@Interfaces/users';
import { UserDetailsWindowService } from '@Components/user-details-window/services/user-details-window.service';
import { ActionResultEnum } from '@Components/user-details-window/shared/action-result-enum';
import { WindowedChatService } from '@Components/chat/services/windowed-chat.service';
import { filter } from 'rxjs/operators';
import { UserService } from '@Services/user.service';

@Component({
    selector: 'app-user-card',
    templateUrl: './user-card.component.html',
    styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
    @Input() public user: IStaffUser;

    constructor (
        private userDetailsWindowService: UserDetailsWindowService,
        private userService: UserService,
        private windowedChatService: WindowedChatService
    ) {}

    ngOnInit () {
        if (!this.user) {
            throw new Error('Component can\'t get data about user');
        }
    }

    public view () {
        const detailsWindow = this.userDetailsWindowService.open(this.user);

        detailsWindow.afterClosed()
            .pipe(
                filter(result => result !== undefined)
            )
            .subscribe((actionResult: ActionResultEnum) => {

                if (actionResult === ActionResultEnum.Meet) {
                    this.windowedChatService.open({
                        isShowContacts: false,
                        analiticsData: {
                            userId: this.user.id
                        }
                    });
                }

            });
    }
}
