import { Component, OnInit, Input } from '@angular/core';
import { IContentBlockModel } from '@Interfaces/content-block-model.interface';

@Component({
    selector: 'app-text-block',
    templateUrl: './text-block.component.html',
    styleUrls: ['./text-block.component.scss']
})
export class TextBlockComponent implements OnInit {

    @Input()
    public data: IContentBlockModel;

    constructor () {}

    public ngOnInit (): void {}

}
