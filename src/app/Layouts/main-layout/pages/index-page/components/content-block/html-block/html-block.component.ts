import { Component, OnInit, Input } from '@angular/core';
import { IContentBlockModel } from '@Interfaces/content-block-model.interface';

@Component({
    selector: 'app-html-block',
    templateUrl: './html-block.component.html',
    styleUrls: ['./html-block.component.scss']
})
export class HtmlBlockComponent implements OnInit {

    @Input()
    public data: IContentBlockModel;

    constructor () {}

    public ngOnInit (): void {}

}
