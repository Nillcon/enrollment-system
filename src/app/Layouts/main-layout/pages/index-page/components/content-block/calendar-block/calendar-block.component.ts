import { Component, OnInit, Input } from '@angular/core';
import { IContentBlockModel } from '@Interfaces/content-block-model.interface';
import { UserDetailsWindowService } from '@Components/user-details-window/services/user-details-window.service';
import { IStaffUser } from '@Interfaces/users';
import { IndexPageService } from '../../../services/index-page.service';

@Component({
    selector: 'app-calendar-block',
    templateUrl: './calendar-block.component.html',
    styleUrls: ['./calendar-block.component.scss']
})
export class CalendarBlockComponent implements OnInit {

    @Input()
    public data: IContentBlockModel;

    public currentDate: Date = new Date();

    constructor (
        private indexPageService: IndexPageService,
        private userDetailsWindowService: UserDetailsWindowService
    ) {}

    public ngOnInit (): void {}

    public onCurrentDateChanged (date: Date): void {
        this.currentDate = date;
        this.updateCalendarData(date);
    }

    public updateCalendarData (date: Date): void {
        this.indexPageService.getGreetersCalendarData(date)
            .subscribe((data) => {
                this.data.content = data;
            });
    }

    public openUserDetailsWindow (greeter: IStaffUser): void {
        this.userDetailsWindowService.open(greeter, { isMeetButtonVisible: true });
    }

}
