import { Component, OnInit, Input } from '@angular/core';
import { IContentBlockModel } from '@Interfaces/content-block-model.interface';

@Component({
    selector: 'app-greeters-block',
    templateUrl: './greeters-block.component.html',
    styleUrls: ['./greeters-block.component.scss']
})
export class GreetersBlockComponent implements OnInit {

    @Input()
    public data: IContentBlockModel;

    constructor () {}

    public ngOnInit (): void {}

}
