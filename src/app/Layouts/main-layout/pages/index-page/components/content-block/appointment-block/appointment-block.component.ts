import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { IContentBlockModel } from '@Interfaces/content-block-model.interface';
import { IAppointment } from '@Components/appointment/interfaces/appointment.interface';

@Component({
    selector: 'app-appointment-block',
    templateUrl: './appointment-block.component.html',
    styleUrls: ['./appointment-block.component.scss']
})
export class AppointmentBlockComponent implements OnInit {

    @Input()
    public data: IContentBlockModel;

    @Output()
    public OnAppointmentCancel: EventEmitter<IAppointment> = new EventEmitter();

    @Output()
    public OnAppointmentFeedback: EventEmitter<IAppointment> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
