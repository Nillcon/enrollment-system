import { Component, OnInit, Input } from '@angular/core';
import { IContentBlockModel } from '@Interfaces/content-block-model.interface';

@Component({
    selector: 'app-video-block',
    templateUrl: './video-block.component.html',
    styleUrls: ['./video-block.component.scss']
})
export class VideoBlockComponent implements OnInit {

    @Input()
    public data: IContentBlockModel;

    constructor () {}

    public ngOnInit (): void {}

}
