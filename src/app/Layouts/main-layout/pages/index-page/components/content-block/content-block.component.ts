import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IContentBlockModel } from '../../../../../../Interfaces/content-block-model.interface';
import { ContentTypeEnum, IContentType, ContentTypeList } from '@Shared/content-type';
import { WindowedChatService } from '@Components/chat/services/windowed-chat.service';
import { UserService } from '@Services/user.service';

@Component({
    selector: 'app-content-block',
    templateUrl: './content-block.component.html',
    styleUrls: ['./content-block.component.scss']
})
export class ContentBlockComponent implements OnInit {

    @Input()
    public data: IContentBlockModel;

    @Output()
    public OnSomeChanges: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public OnChatButtonClick: EventEmitter<IContentBlockModel> = new EventEmitter();

    @ViewChild('BlockItem')
    public BlockItem: any;

    public readonly ContentType: typeof ContentTypeEnum = ContentTypeEnum;
    public readonly ContentTypeList: { [key: number]: IContentType } = ContentTypeList;

    constructor () {}

    public ngOnInit (): void {}

}
