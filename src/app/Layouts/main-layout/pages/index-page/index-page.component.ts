import { Component, OnInit, AfterViewInit, ElementRef, OnDestroy, ViewChildren, QueryList } from '@angular/core';
import { ViewportService } from '@Services/viewport.service';
import { ScrollService } from '@Services/scroll.service';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { IndexPageService } from './services/index-page.service';
import { IBaseContentBlockModel, IContentBlockModel } from '@Interfaces/content-block-model.interface';
import { JoeLionHubService } from './../../components/joe-lion/services/joe-lion-hub.service';
import { LandingService } from './services/landing-hub.service';
import { ContentBlockComponent } from './components/content-block/content-block.component';
import { ContentTypeEnum } from '@Shared/content-type';
import { WindowedChatService } from '@Components/chat/services/windowed-chat.service';
import { UserService } from '@Services/user.service';
import { Roles } from '@Shared/roles';
import { take } from 'rxjs/operators';
import { CalendarBlockComponent } from './components/content-block/calendar-block/calendar-block.component';

@AutoUnsubscribe()
@Component({
    selector: 'app-index-page',
    templateUrl: './index-page.component.html',
    styleUrls: ['./index-page.component.scss'],
    providers: [
        IndexPageService,
        LandingService
    ]
})
export class IndexPageComponent implements OnInit, OnDestroy, AfterViewInit {
    @ViewChildren('ContentBlock')
    private ContentBlocksList: QueryList<ContentBlockComponent>;

    public contentBlocksData: IBaseContentBlockModel[];

    private contentBlocksAsElements: NodeList;

    private contentBlocksRenderingSubscription: Subscription;
    private scrollSubscription: Subscription;
    private landingUpdatingSubscription: Subscription;

    constructor (
        private indexPageService: IndexPageService,
        private pageElement: ElementRef,
        private viewportService: ViewportService,
        private scrollService: ScrollService,
        private joeLionHubService: JoeLionHubService,
        private landingService: LandingService,
        private windowedChatService: WindowedChatService,
        private userService: UserService
    ) {}

    public ngOnInit (): void {
        this.update();
        this.scrollObserver();
        this.landingUpdatingObserver();
    }

    public ngAfterViewInit (): void {
        this.contentBlocksRenderingObserver();
    }

    public ngOnDestroy (): void {}

    public update (): void {
        this.indexPageService.getContent()
            .subscribe(blocks => {
                this.contentBlocksData = blocks;
            });
    }

    public onChatButtonClick (blockData: IContentBlockModel): void {
        const isAdmin: boolean = this.userService.Data.role === Roles.SuperUser;

        this.userService.ChatLink$
            .pipe(
                take(1)
            )
            .subscribe((chatLink) => {
                this.windowedChatService.open({
                    analiticsData: {
                        blockId: blockData.id,
                        buttonName: blockData.chatButton.name
                    },
                    chatLink: (isAdmin) ? null : chatLink,
                    isShowContacts: isAdmin
                });
            });
    }

    private getOnScreenBlockIds (): number[] {
        if (!this.contentBlocksAsElements) {
            return;
        }

        const blockIdsOnScreen: number[] = [];

        this.contentBlocksAsElements.forEach((block) => {
            const currentElement: HTMLElement = <HTMLElement>block.firstChild;

            if (this.viewportService.isAnyPartOfElementInViewport(currentElement)) {
                const targetElement: HTMLElement = <HTMLElement>block.firstChild;
                const targetElementId: number    = +targetElement.getAttribute('data-tech-id');

                blockIdsOnScreen.push(targetElementId);
            }
        });

        return blockIdsOnScreen;
    }

    private contentBlocksRenderingObserver (): void {
        this.contentBlocksRenderingSubscription = this.ContentBlocksList.changes
            .subscribe(() => {
                this.contentBlocksAsElements = this.pageElement.nativeElement.querySelectorAll('.ContentBlock');
            });
    }

    private scrollObserver (): void {
        this.scrollSubscription = this.scrollService.LastScrollEventEnd$
            .subscribe(() => {
                this.emitOnScreenForJoeLion();
            });
    }

    private landingUpdatingObserver (): void {
        this.landingUpdatingSubscription = this.landingService.landingUpdate$
            .subscribe((blockIds) => {
                this.updateBlocksData(blockIds);
            });
    }

    private emitOnScreenForJoeLion (): void {
        const blockIds: number[] = this.getOnScreenBlockIds();
        this.joeLionHubService.updateMessageForBlock(blockIds);
    }

    private updateBlocksData (blockIds: number[]): void {
        this.ContentBlocksList.forEach((currBlock) => {
            const isCurrentBlockNeedUpdate = blockIds.includes(currBlock.data.id);

            if (isCurrentBlockNeedUpdate) {
                switch (currBlock.data.type) {
                    case ContentTypeEnum.Appointment:
                        this.updateAppointmentsBlockData(currBlock);
                        break;
                    case ContentTypeEnum.Scheduler:
                        this.updateGreetersCalendarBlockData(currBlock);
                        break;
                }
            }
        });
    }

    private updateAppointmentsBlockData (block: ContentBlockComponent): void {
        this.indexPageService.getAppointmentsData()
            .subscribe((appointments) => {
                block.data.content = appointments;
            });
    }

    private updateGreetersCalendarBlockData (block: ContentBlockComponent): void {
        const date = (block.BlockItem as CalendarBlockComponent).currentDate;

        this.indexPageService.getGreetersCalendarData(date)
            .subscribe((calendarInfo) => {
                block.data.content = calendarInfo;
            });
    }

}
