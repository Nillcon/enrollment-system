import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { LocalStorageService } from "@Services/local-storage.service";
import { StorageKey } from "@Shared/storage-key";
import { HubConnectionBuilder, HubConnection } from "@aspnet/signalr";
import { Observable, Subject } from "rxjs";

@Injectable()
export class LandingService {

    private stream: HubConnection;

    private readonly hourInMilliseconds = 3600000;

    private _landingUpdate$ = new Subject<number[]>();

    public get landingUpdate$ (): Observable<number[]> {
        return this._landingUpdate$.asObservable();
    }

    constructor (
        private httpService: HttpRequestService,
        private storageService: LocalStorageService
    ) {
        this.connectToChatHub();
    }

    public connectToChatHub(): void {
        const host = this.httpService.getHost();
        const token = this.storageService.get<string>(StorageKey.Access_Token);

        this.stream = new HubConnectionBuilder()
            .withUrl(`${host}/landinghub`,
                {
                    accessTokenFactory: () => token
                }
            )
            .build();

        this.stream.start()
            .then(() => {
                console.log('Landing connection started');

                this.landingUpdatingObserver();
            })
            .catch(err => console.log('Error while starting connection:', err));

        this.stream.serverTimeoutInMilliseconds = this.hourInMilliseconds;
    }

    /** Update landing page data */
    private landingUpdatingObserver (): void {
        this.stream.on(
            'Update',
            (blockIds: number[]) => this._landingUpdate$.next(blockIds)
        );
    }

}
