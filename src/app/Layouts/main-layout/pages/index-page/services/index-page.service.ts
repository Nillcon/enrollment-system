import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { IBaseContentBlockModel } from "@Interfaces/content-block-model.interface";
import { Observable } from "rxjs";
import { IAppointment } from "@Components/appointment/interfaces/appointment.interface";
import { IGreeterCalendarInfo } from "@Components/greeter-calendar/interface/info.interface";
import { DateMethod } from "@Methods/date.method";

@Injectable()
export class IndexPageService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getContent (): Observable<IBaseContentBlockModel[]> {
        return this.httpService.get<IBaseContentBlockModel[]>(`/Content/Landing`);
    }

    public getAppointmentsData (): Observable<IAppointment[]> {
        return this.httpService.get<IAppointment[]>(`/`);
    }

    public getGreetersCalendarData (dateFrom: Date = new Date()): Observable<IGreeterCalendarInfo> {
        const dateFromAsStr: string = DateMethod.getLocalJsonTimeString(dateFrom);

        return this.httpService.get<IGreeterCalendarInfo>(`/Content/Calendar?from=${dateFromAsStr}`);
    }

}
