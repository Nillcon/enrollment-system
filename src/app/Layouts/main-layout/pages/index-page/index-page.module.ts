import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexPageRoutingModule } from './index-page.routing';
import { SharedModule } from '@App/app-shared.module';
import { UserDetailsWindowModule } from '@Components/user-details-window/user-details-window.module';
import { ContentBlockComponent } from './components/content-block/content-block.component';
import { UserCardComponent } from './components/user-card/user-card.component';
import { IndexPageComponent } from './index-page.component';
import { AppointmentModule } from '@Components/appointment/appointment.module';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { MainLayoutSharedModule } from '@Layouts/main-layout/main-layout-shared.module';
import { BlindPeopleAdaptationModule } from '@Components/blind-people-adaptation/blind-people-adaptation.module';
import { GreeterCalendarModule } from '@Components/greeter-calendar/greeter-calendar.module';
import { GreetersBlockComponent } from './components/content-block/greeters-block/greeters-block.component';
import { TextBlockComponent } from './components/content-block/text-block/text-block.component';
import { HtmlBlockComponent } from './components/content-block/html-block/html-block.component';
import { VideoBlockComponent } from './components/content-block/video-block/video-block.component';
import { AppointmentBlockComponent } from './components/content-block/appointment-block/appointment-block.component';
import { CalendarBlockComponent } from './components/content-block/calendar-block/calendar-block.component';

@NgModule({
    declarations: [
        IndexPageComponent,
        UserCardComponent,
        ContentBlockComponent,
        GreetersBlockComponent,
        TextBlockComponent,
        HtmlBlockComponent,
        VideoBlockComponent,
        AppointmentBlockComponent,
        CalendarBlockComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        MainLayoutSharedModule,

        UserDetailsWindowModule,
        AppointmentModule,

        NgxYoutubePlayerModule,
        GreeterCalendarModule,

        IndexPageRoutingModule,
        BlindPeopleAdaptationModule
    ]
})
export class IndexPageModule {}
