import { Component, OnInit, OnDestroy } from '@angular/core';
import { JoeLionHubService } from './components/joe-lion/services/joe-lion-hub.service';
import { ActivatedRoute } from '@angular/router';
import { UrlParam } from '@Shared/url-param';
import { UserService } from '@Services/user.service';
import { LocalStorageService } from '@Services/local-storage.service';
import { StorageKey } from '@Shared/storage-key';
import { Subscription } from 'rxjs';
import { IUser } from '@Interfaces/users';
import { Roles } from '@Shared/roles';
import { NavigationService } from '@Services/navigation.service';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Title } from '@angular/platform-browser';
import { take, delayWhen, tap } from 'rxjs/operators';

@AutoUnsubscribe()
@Component({
    selector: 'app-main-layout',
    templateUrl: './main-layout.component.html',
    styleUrls: ['./main-layout.component.scss'],
    providers: [
        JoeLionHubService
    ]
})
export class MainLayoutComponent implements OnInit, OnDestroy {

    private userData: IUser;
    private userDataSubscription: Subscription;

    public backgroundColor: string;

    constructor (
        private navigationService: NavigationService,
        private activatedRoute: ActivatedRoute,
        private storageService: LocalStorageService,
        private userService: UserService,
        private titleService: Title
    ) {}

    public ngOnInit (): void {
        this.processingPersonalLink();
    }

    public ngOnDestroy (): void {}

    private processingPersonalLink (): void {
        let isPersonalLinkFound: boolean = false;

        while (this.activatedRoute.snapshot.firstChild) {
            if (
                this.activatedRoute.snapshot.params
                &&
                this.activatedRoute.snapshot.params[UrlParam.PersonalLink]
            ) {
                this.makeUserLocal();

                const oldUserToken: string = this.storageService.get(StorageKey.Access_Token);
                const newUserToken: string = this.activatedRoute.snapshot.params[UrlParam.PersonalLink];
                this.storageService.set(StorageKey.Access_Token, newUserToken);
                isPersonalLinkFound = true;

                if (oldUserToken !== newUserToken) {
                    location.href = location.origin;
                } else {
                    this.userService.update()
                        .pipe(
                            delayWhen(() => this.userService.updateChatLink())
                        )
                        .subscribe(() => {
                            this.userDataObserver();
                        });
                }
            }

            this.activatedRoute = this.activatedRoute.firstChild;
        }

        if (!isPersonalLinkFound) {
            this.userService.updateChatLink()
                .pipe(
                    tap(() => this.userDataObserver())
                )
                .subscribe();
        }
    }

    private processignAnonymousUser (): void {
        if (this.userData.role === Roles.Anonymous && !this.userData.isAllowAnonymous) {
            this.userService.ChatLink$
                .pipe(
                    take(1)
                )
                .subscribe(() => {
                    this.navigationService.navigate(`Chat/${this.userData.chatLink}`, false);
                });
        }
    }

    private makeUserLocal (): boolean {
        if (this.userService.Data.role === Roles.SuperUser) {
            this.storageService.set(StorageKey.Local_User_Token, this.storageService.get(StorageKey.Access_Token));
            return true;
        }

        return false;
    }

    private updateTitleText (): void {
        if (this.userData.browserTitleText) {
            this.titleService.setTitle(this.userData.browserTitleText);
        } else {
            this.titleService.setTitle('Enrollment_System');
        }
    }

    private userDataObserver (): void {
        this.userDataSubscription = this.userService.Data$
            .subscribe(data => {
                this.backgroundColor =  data.backgroundColor;
                this.userData = data;

                this.processignAnonymousUser();
                this.updateTitleText();
            });
    }

}
