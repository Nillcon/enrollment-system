import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainLayoutRoutingModule } from './main-layout.routing';
import { MainLayoutComponent } from './main-layout.component';
import { HeaderMenuModule } from '@Layouts/main-layout/components/header-menu/header-menu.module';
import { SharedModule } from '@App/app-shared.module';
import { IndexPageModule } from './pages/index-page/index-page.module';
import { JoeLionModule } from './components/joe-lion/joe-lion.module';
import { MainLayoutSharedModule } from './main-layout-shared.module';

@NgModule({
    declarations: [
        MainLayoutComponent
    ],
    imports: [
        CommonModule,

        MainLayoutSharedModule,
        MainLayoutRoutingModule,

        SharedModule,

        IndexPageModule,

        HeaderMenuModule,
        JoeLionModule
    ]
})
export class MainLayoutModule { }
