import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
  selector: 'app-index-page',
  templateUrl: './index-page.component.html',
  styleUrls: ['./index-page.component.scss']
})
export class IndexPageComponent implements OnInit, OnDestroy {

    public chatLink: string;

    private routeParamsSubscription: Subscription;

    constructor(
        private route: ActivatedRoute,
        private router: Router
    ) { }

    ngOnInit() {
        this.routeParamsSubscription = this.route.params
            .subscribe(params => this.chatLink = params['id']);
    }

    ngOnDestroy() {}

    public onSelectedChat(chatLink: string) {
        this.router.navigate(['Dashboard', 'Chat', chatLink]);
    }

    public onBackToContacts() {
        this.router.navigate(['Dashboard', 'Chat', '']);
    }

}
