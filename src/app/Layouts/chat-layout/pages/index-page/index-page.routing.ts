import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { IndexPageComponent } from './index-page.component';

export const routes: Routes = [
    {
        path: '',
        component: IndexPageComponent
    },
    {
        path: ':id',
        component: IndexPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class IndexPageRoutingModule {}
