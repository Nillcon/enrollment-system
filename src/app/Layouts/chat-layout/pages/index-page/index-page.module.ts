import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexPageComponent } from './index-page.component';
import { IndexPageRoutingModule } from './index-page.routing';
import { ChatModule } from '@Components/chat/chat.module';

@NgModule({
    declarations: [
        IndexPageComponent
    ],
    imports: [
        CommonModule,

        IndexPageRoutingModule,

        ChatModule
    ]
})
export class IndexPageModule { }
