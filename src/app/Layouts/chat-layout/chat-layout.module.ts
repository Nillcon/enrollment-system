import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatLayoutComponent } from './chat-layout.component';
import { ChatLayoutRoutingModule } from './chat-layout.routing';
import { IndexPageModule } from './pages/index-page/index-page.module';

@NgModule({
    declarations: [
        ChatLayoutComponent
    ],
    imports: [
        CommonModule,
        ChatLayoutRoutingModule,

        IndexPageModule
    ]
})
export class ChatLayoutModule { }
