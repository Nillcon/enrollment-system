import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardLayoutComponent } from './dashboard-layout.component';
import { IndexPageModule } from './pages/index-page/index-page.module';
import { SharedModule } from '@App/app-shared.module';
import { DashboardLayoutRoutingModule } from './dashboard-layout.routing';
import { StaffPageModule } from './pages/staff-page/staff-page.module';
import { MenuComponent } from './components/menu/menu.component';
import { UsersPageModule } from './pages/users-page/users-page.module';
import { HeaderComponent } from './components/header/header.component';
import { CampaignsPageModule } from './pages/campaigns-page/campaigns-page.module';
import { ChatPageModule } from './pages/chat-page/chat-page.module';
import { AppointmentsPageModule } from './pages/appointments-page/appointments-page.module';
import { LogsPageModule } from './pages/logs-page/logs-page.module';
import { ReportsPageModule } from './pages/reports-page/reports-page.module';
import { DashboardLayoutSharedModule } from './dashboard-layout-shared.module';

@NgModule({
    declarations: [
        DashboardLayoutComponent,
        MenuComponent,
        HeaderComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule,

        DashboardLayoutRoutingModule,

        IndexPageModule,
        CampaignsPageModule,
        StaffPageModule,
        UsersPageModule,
        ChatPageModule,
        AppointmentsPageModule,
        LogsPageModule,
        ReportsPageModule
    ]
})
export class DashboardLayoutModule { }
