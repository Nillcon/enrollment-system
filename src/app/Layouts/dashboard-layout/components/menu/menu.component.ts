import { Component, OnInit, OnDestroy } from '@angular/core';
import { IUser } from '@Interfaces/users/user.interface';
import { UserService } from '@Services/user.service';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { IMenuSection } from '@Interfaces/menu-section';
import { MenuSectionsList } from '@Layouts/dashboard-layout/data/menu-sections-list';

@AutoUnsubscribe()
@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit, OnDestroy {
    private userDataSubscription: Subscription;

    public userData: IUser;
    public sections: IMenuSection[] = MenuSectionsList;

    constructor (
        private userService: UserService
    ) {}

    ngOnInit () {
        this.userDataSubscription = this.userService.Data$
            .subscribe(data => {
                this.userData = data;
            });
    }

    ngOnDestroy () {}
}
