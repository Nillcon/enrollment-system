import { Component, OnInit, Injector, AfterViewInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { LogsService } from '../../services/logs.service';
import { IInvitedUser } from '@Interfaces/users';
import { ILog } from '../../interfaces/log.interface';
import { IDateRange } from '@Components/range-date/interfaces/date-range.interface';
import { CampaignsService } from '@Layouts/dashboard-layout/pages/campaigns-page/services/campaigns.service';
import { ICampaign } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/campaign.interface';
import { DateMethod } from '@Methods/date.method';

@Component({
    selector: 'app-logs-table',
    templateUrl: './logs-table.component.html',
    styleUrls: ['./logs-table.component.scss'],
    providers: [
        CampaignsService
    ]
})
export class LogsTableComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort)      private sort: MatSort;

    @Input() public openedInvitedUser: IInvitedUser;
    @Input() public isShowCampaigns: boolean = false;

    public from: Date = DateMethod.minusDays(new Date(), 7);
    public to: Date   = new Date();
    public selectedCampaignId: number;

    public campaignsList: ICampaign[];

    public readonly DisplayedColumns: Array<string> = [
        'info', 'userPrimaryKey', 'ip', 'date'
    ];

    public dataSource: MatTableDataSource<ILog> = new MatTableDataSource();

    public isLoading: boolean = false;

    constructor (
        public injector: Injector,
        private logsService: LogsService,
        private campaignsService: CampaignsService
    ) {}

    ngOnInit () {
        this.update();
        this.updateCampaignsList();
    }

    ngAfterViewInit () {
        this.dataSource.paginator   = this.paginator;
        this.dataSource.sort        = this.sort;
    }

    public update (): void {
        this.isLoading = true;

        this.logsService.getList(
            (this.openedInvitedUser) ? this.openedInvitedUser.id : null,
            {
                from: this.from,
                to: this.to,
                campaignId: this.selectedCampaignId
            }
        )
            .subscribe(logs => {
                this.dataSource.data = logs;
                this.isLoading       = false;
            });
    }

    public updateCampaignsList (): void {
        if (this.isShowCampaigns) {
            this.campaignsService.getList()
                .subscribe(campaigns => {
                    this.campaignsList = campaigns;
                });
        }
    }

    public search (phrase: string): void {
        this.dataSource.filter = phrase.trim().toLowerCase();
    }

    public changeDates (dates: IDateRange): void {
        this.from = dates.from;
        this.to   = dates.to;

        this.update();
    }

    public clearSelectedCampaign (): void {
        this.selectedCampaignId = 0;
        this.update();
    }
}
