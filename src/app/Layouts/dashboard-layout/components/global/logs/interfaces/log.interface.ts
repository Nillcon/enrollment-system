export interface ILog {
    id: number;
    userId: number;
    date: string;
    info: string;
    ip: string;
    userPrimaryKey: string;
}
