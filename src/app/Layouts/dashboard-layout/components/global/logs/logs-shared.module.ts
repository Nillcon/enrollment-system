import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsTableComponent } from './components/logs-table/logs-table.component';
import { LogsService } from './services/logs.service';
import { SharedModule } from '@App/app-shared.module';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { RangeDateModule } from '@Components/range-date/range-date.module';

@NgModule({
    declarations: [
        LogsTableComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule,
        RangeDateModule
    ],
    exports: [
        LogsTableComponent
    ],
    providers: [
        LogsService
    ]
})
export class LogsSharedModule { }
