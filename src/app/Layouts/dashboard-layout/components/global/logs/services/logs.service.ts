import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ILog } from "../interfaces/log.interface";
import { HttpRequestService } from "@Services/http-request.service";

@Injectable()
export class LogsService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getList (
        userId: number,
        {
            from       = new Date(),
            to         = new Date(),
            campaignId = 0
        } = {}
    ): Observable<ILog[]> {
        if (userId) {
            return this.httpService.get(`/Users/${userId}/Logs`);
        } else {
            return this.httpService.get(
                `/Logs?campaignId=${campaignId}&from=${from.toJSON()}&to=${to.toJSON()}`
            );
        }
    }
}
