import { Component, OnInit } from '@angular/core';
import { AuthorizeService } from '@Services/authorize.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    constructor (
        private authorizeService: AuthorizeService
    ) {}

    ngOnInit () {}

    public exitFromAccount (): void {
        this.authorizeService.signOut({
            redirectUrl: location.origin + '/SignIn',
            isNeedAuthorizeAfterRedirect: false
        });
    }
}
