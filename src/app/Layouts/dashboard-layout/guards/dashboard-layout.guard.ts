import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { tap, map } from 'rxjs/operators';
import { UserService } from '@Services/user.service';
import { NavigationService } from '@Services/navigation.service';
import { Roles } from '@Shared/roles';

@Injectable({
    providedIn: 'root'
})
export class DashboardLayoutGuard implements CanActivate {
    private readonly availableRoles: Roles[] = [
        Roles.Staff, Roles.Student,
        Roles.SuperUser, Roles.Support
    ];

    constructor (
        private userService: UserService,
        private navigationService: NavigationService
    ) {}

    canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
        return this.userService.Data$
        .pipe(
            tap(userData => {
                if (!this.availableRoles.includes(userData.role)) {
                    this.navigationService.navigate('/SignIn');
                }
            }),
            map(() => true)
        );
    }
}
