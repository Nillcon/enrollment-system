import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { HttpInterceptorService } from '@Services/http-interceptor.service';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { NotificationService } from '@Services/notification.service';

@Component({
    selector: 'app-dashboard-layout',
    templateUrl: './dashboard-layout.component.html',
    styleUrls: ['./dashboard-layout.component.scss']
})
export class DashboardLayoutComponent implements OnInit {

    public httpErrorSubscription: Subscription;

    constructor (
        private httpInterceptorService: HttpInterceptorService,
        private notificationService: NotificationService,
        private titleService: Title
    ) {}

    ngOnInit () {
        this.httpErrorObserver();
        this.updateTitle();
    }

    private updateTitle (): void {
        this.titleService.setTitle('Dashboard');
    }

    private httpErrorObserver (): void {
        const errorNotifyDuration: number = 7000;

        this.httpErrorSubscription = this.httpInterceptorService.OnError$
            .pipe(
                tap((error) => {
                    if (error.status === 500) {
                        this.notificationService.error(
                            'Error',
                            error.error || error.message,
                            errorNotifyDuration
                        );
                    } else if (error.status === 422) {
                        this.notificationService.warning(
                            'Warning',
                            error.error || error.message,
                            errorNotifyDuration
                        );
                    }
                })
            )
            .subscribe();
    }

}
