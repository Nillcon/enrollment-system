import { IMenuSection } from '@Interfaces/menu-section';
import { Roles } from '@Shared/roles';

export const MenuSectionsList: IMenuSection[] = [
    {
        text: 'Chat',
        href: 'Chat',
        permissions: [
            Roles.Support
        ],
        iconClass: 'fas fa-comment'
    },
    {
        text: 'Campaigns',
        href: 'Campaigns',
        permissions: [
            Roles.Support
        ],
        iconClass: 'fas fa-users'
    },
    {
        text: 'Appointments',
        href: 'Appointments',
        permissions: [
            Roles.Student,
            Roles.Staff,
            Roles.Support
        ],
        iconClass: 'fas fa-handshake'
    },
    {
        text: 'Scheduler',
        href: 'Scheduler',
        permissions: [],
        iconClass: 'far fa-calendar-alt'
    },
    {
        text: 'Users',
        href: 'Users',
        permissions: [
            Roles.Support
        ],
        iconClass: 'fas fa-user-graduate'
    },
    {
        text: 'Visitors',
        href: 'Visitors',
        permissions: [
            Roles.Support
        ],
        iconClass: 'fas fa-user-clock'
    },
    {
        text: 'Logs',
        href: 'Logs',
        permissions: [
            Roles.Support
        ],
        iconClass: 'fas fa-table'
    },
    {
        text: 'Reports',
        href: 'Reports',
        permissions: [
            Roles.Support
        ],
        iconClass: 'fas fa-file-alt'
    },
    {
        text: 'Audio description',
        href: 'AudioDescription',
        permissions: [
            Roles.Support
        ],
        iconClass: 'fas fa-audio-description'
    }
];
