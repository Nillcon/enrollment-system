import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexPageComponent } from './index-page.component';
import { IndexPageRoutingModule } from './index-page.routing';

@NgModule({
    declarations: [
        IndexPageComponent
    ],
    imports: [
        CommonModule,

        IndexPageRoutingModule
    ]
})
export class IndexPageModule { }
