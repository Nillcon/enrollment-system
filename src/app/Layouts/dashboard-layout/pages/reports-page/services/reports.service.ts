import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";

@Injectable()
export class ReportsService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public downloadUsersReport (campaignId: number, withChanges: boolean): Observable<Blob> {
        return this.httpService.downloadFile(
            `/Reports/Users?campaignId=${campaignId}&withChanges=${withChanges}`,
            {
                name: `Users report`,
                type: '.csv'
            }
        );
    }

    public downloadLogsReport (campaignId: number, from: Date, to: Date): Observable<Blob> {
        return this.httpService.downloadFile(
            `/Reports/Logs?campaignId=${campaignId}&from=${from.toJSON()}&to=${to.toJSON()}`,
            {
                name: `Logs report`,
                type: '.csv'
            }
        );
    }

    public downloadAppointmentsReport (campaignId: number, from: Date, to: Date): Observable<Blob> {
        return this.httpService.downloadFile(
            `/Reports/Appointments?campaignId=${campaignId}&from=${from.toJSON()}&to=${to.toJSON()}`,
            {
                name: `Appointments report`,
                type: '.csv'
            }
        );
    }
}
