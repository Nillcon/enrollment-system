import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ReportsPageComponent } from './reports-page.component';

export const routes: Routes = [
    {
        path: '',
        component: ReportsPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ReportsPageRoutingModule {}
