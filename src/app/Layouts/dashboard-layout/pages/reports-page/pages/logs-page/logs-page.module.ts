import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsPageComponent } from './logs-page.component';
import { SharedModule } from '@App/app-shared.module';
import { RangeDateModule } from '@Components/range-date/range-date.module';

@NgModule({
    declarations: [
        LogsPageComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        RangeDateModule
    ],
    exports: [
        LogsPageComponent
    ]
})
export class LogsPageModule { }
