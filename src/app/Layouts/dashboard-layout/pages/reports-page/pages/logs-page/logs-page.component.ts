import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ICampaign } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/campaign.interface';
import { CampaignsService } from '@Layouts/dashboard-layout/pages/campaigns-page/services/campaigns.service';
import { ReportsService } from '../../services/reports.service';
import { IDateRange } from '@Components/range-date/interfaces/date-range.interface';
import { DateMethod } from '@Methods/date.method';

@Component({
    selector: 'app-logs-page',
    templateUrl: './logs-page.component.html',
    styleUrls: ['./logs-page.component.scss']
})
export class LogsPageComponent implements OnInit {
    public logsReportFormGroup: FormGroup;
    public campaignIdFormControl: FormControl;
    public fromFormControl: FormControl;
    public toFormControl: FormControl;

    public campaignList: ICampaign[];

    public isDownload: boolean = false;

    constructor (
        private campaignsService: CampaignsService,
        private reportsService: ReportsService
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.campaignsService.getList()
            .subscribe(campaigns => {
                this.campaignList = campaigns;
                this.initForm();
            });
    }

    public download (): void {
        const campaignId: number = this.campaignIdFormControl.value;
        const from: Date         = this.fromFormControl.value;
        const to: Date           = this.toFormControl.value;
        this.isDownload          = true;

        this.reportsService.downloadLogsReport(campaignId, from, to)
            .subscribe(
                () => {
                    this.isDownload = false;
                },
                () => {
                    this.isDownload = false;
                }
            );
    }

    public changeDateRange (event: IDateRange): void {
        this.fromFormControl.setValue(event.from);
        this.toFormControl.setValue(event.to);
    }

    public clearCampaignSelection (): void {
        this.campaignIdFormControl.setValue(0);
    }

    private initForm (): void {
        this.campaignIdFormControl  = new FormControl(0);
        this.fromFormControl        = new FormControl(DateMethod.minusDays(new Date(), 14));
        this.toFormControl          = new FormControl(new Date());

        this.logsReportFormGroup   = new FormGroup({
            campaignId: this.campaignIdFormControl,
            from: this.fromFormControl,
            to: this.toFormControl
        });
    }
}
