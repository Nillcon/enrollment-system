import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentsPageComponent } from './appointments-page.component';
import { SharedModule } from '@App/app-shared.module';
import { RangeDateModule } from '@Components/range-date/range-date.module';

@NgModule({
    declarations: [
        AppointmentsPageComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        RangeDateModule
    ],
    exports: [
        AppointmentsPageComponent
    ]
})
export class AppointmentsPageModule { }
