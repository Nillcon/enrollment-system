import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersPageComponent } from './users-page.component';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        UsersPageComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    exports: [
        UsersPageComponent
    ]
})
export class UsersPageModule { }
