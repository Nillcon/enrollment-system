import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ICampaign } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/campaign.interface';
import { CampaignsService } from '@Layouts/dashboard-layout/pages/campaigns-page/services/campaigns.service';
import { ReportsService } from '../../services/reports.service';
import { NotificationService } from '@Services/notification.service';

@Component({
    selector: 'app-users-page',
    templateUrl: './users-page.component.html',
    styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {
    public usersReportFormGroup: FormGroup;
    public campaignIdFormControl: FormControl;
    public withChangesFormControl: FormControl;

    public campaignList: ICampaign[];

    public isDownload: boolean = false;

    constructor (
        private campaignsService: CampaignsService,
        private reportsService: ReportsService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.campaignsService.getList()
            .subscribe(campaigns => {
                this.campaignList = campaigns;
                this.initForm();
            });
    }

    public download (): void {
        if (this.usersReportFormGroup.valid) {
            const campaignId: number = this.campaignIdFormControl.value;
            const withChanges: boolean = this.withChangesFormControl.value;
            this.isDownload = true;

            this.reportsService.downloadUsersReport(campaignId, withChanges)
                .subscribe(
                    () => {
                        this.isDownload = false;
                    },
                    () => {
                        this.isDownload = false;
                    }
                );
        } else {
            this.notificationService.error('Oops', 'Campaign is required! Please select campaign before save report.');
        }
    }

    public clearCampaignSelection (): void {
        this.campaignIdFormControl.setValue(0);
    }

    private initForm (): void {
        this.campaignIdFormControl  = new FormControl(null, [
            Validators.required
        ]);
        this.withChangesFormControl = new FormControl(false);

        this.usersReportFormGroup   = new FormGroup({
            campaignId: this.campaignIdFormControl,
            withChanges: this.withChangesFormControl
        });
    }
}
