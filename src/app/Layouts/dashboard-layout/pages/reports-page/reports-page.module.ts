import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsPageComponent } from './reports-page.component';
import { ReportsPageRoutingModule } from './reports-page.routing';
import { UsersPageModule } from './pages/users-page/users-page.module';
import { LogsPageModule } from './pages/logs-page/logs-page.module';
import { AppointmentsPageModule } from './pages/appointments-page/appointments-page.module';
import { SharedModule } from '@App/app-shared.module';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { ReportsService } from './services/reports.service';

@NgModule({
    declarations: [
        ReportsPageComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule,
        ReportsPageRoutingModule,

        UsersPageModule,
        LogsPageModule,
        AppointmentsPageModule
    ],
    providers: [
        ReportsService
    ]
})
export class ReportsPageModule { }
