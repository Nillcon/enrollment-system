import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ICampaign } from "../interfaces/campaign.interface";
import { HttpRequestService } from "@Services/http-request.service";
import { SaveMode } from "@Types/save-mode.type";
import { IBriefValueModel } from "@Interfaces/brief-value-model.interface";
import { IVariable } from "@Interfaces/variable.interface";
import { IStyling } from "../interfaces/styling.interface";

@Injectable()
export class CampaignsService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getList (): Observable<ICampaign[]> {
        return this.httpService.get(`/Campaigns`);
    }

    public get (campaignId: number): Observable<ICampaign> {
        return this.httpService.get(`/Campaigns/${campaignId}`);
    }

    public create (campaign: ICampaign): Observable<any> {
        return this.httpService.post(`/Campaigns`, campaign);
    }

    public edit (campaign: ICampaign): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaign.id}`, campaign);
    }

    public save (campaign: ICampaign, mode: SaveMode): Observable<any> {
        return (mode === 'create') ? this.create(campaign) : this.edit(campaign);
    }

    public delete (campaignId: number): Observable<any> {
        return this.httpService.delete(`/Campaigns/${campaignId}`);
    }

    public getConstantsForMailing(campaignId: number) {
        return this.httpService.get<IVariable[]>(`/Campaigns/${campaignId}/ConstantsForMailing`);
    }

    public getMailing(campaignId: number): Observable<IBriefValueModel[]> {
        return this.httpService.get<IBriefValueModel[]>(`/Campaigns/${campaignId}/Mailing`);
    }

    public saveMailing(campaignId: number, mailingData: IBriefValueModel[]): Observable<any> {
        const data = {
            value: JSON.stringify(mailingData)
        };

        return this.httpService.put<any>(`/Campaigns/${campaignId}/Mailing`, data);
    }

    public getStyling(campaignId: number) {
        return this.httpService.get<IStyling>(`/Campaigns/${campaignId}/Styling`);
    }

    public saveStyling(campaignId: number, data: IStyling) {
        return this.httpService.put<IStyling>(`/Campaigns/${campaignId}/Styling`, data);
    }
}
