import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CampaignsPageComponent } from './campaigns-page.component';

export const routes: Routes = [
    {
        path: '',
        component: CampaignsPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class CampaignsPageRoutingModule {}
