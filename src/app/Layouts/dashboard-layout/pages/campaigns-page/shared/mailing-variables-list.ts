import { IBriefModel } from "@Interfaces/brief-model.interface";

export const MailingVariablesList: IBriefModel[] = [
    {
        id: 1,
        name: 'NAME'
    },
    {
        id: 2,
        name: 'SALARY'
    },
    {
        id: 3,
        name: 'YEAR'
    }
];
