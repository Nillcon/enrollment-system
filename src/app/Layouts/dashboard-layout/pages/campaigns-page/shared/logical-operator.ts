export enum LogicalOperatorsEnum {
    Contains,
    NotContains,
    LessThan,
    MoreThan,
    NotEqual,
    Equal
}
interface ILogicalOperatorData {
    name: string;
}

export const LogicaOperatorsList: { [key: number]: ILogicalOperatorData } = {
    [LogicalOperatorsEnum.Equal]: {
        name: 'Equal'
    },
    [LogicalOperatorsEnum.NotEqual]: {
        name: 'NotEqual'
    },
    [LogicalOperatorsEnum.LessThan]: {
        name: 'LessThan'
    },
    [LogicalOperatorsEnum.MoreThan]: {
        name: 'MoreThan'
    },
    [LogicalOperatorsEnum.Contains]: {
        name: 'Contains'
    },
    [LogicalOperatorsEnum.NotContains]: {
        name: 'NotContains'
    }
};
