export enum ValueTypeEnum {
    Text = 0,
    Variable = 1,
    Const = 2,
}
