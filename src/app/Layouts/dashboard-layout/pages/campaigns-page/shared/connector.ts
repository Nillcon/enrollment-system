export enum ConnectorsEnum {
    If = 0,
    And,
    Or
}

interface IConnectorData {
    name: string;
}

export const ConnectorsList: { [key: number]: IConnectorData } = {
    [ConnectorsEnum.And]: {
        name: 'And'
    },
    [ConnectorsEnum.Or]: {
        name: 'Or'
    }
};
