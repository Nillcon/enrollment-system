export enum ActionTypeEnum {
    Text = 0,
    Image = 1,
    Poll = 2,
    Form = 3,
    Assignment = 4,
    Appointment = 5,
    CallSupport = 6,
    Check = 7
}

export interface IActionType {
    id: ActionTypeEnum;
    name: string;
    type: actionReturnType;
}

export type actionReturnType = 'message' | 'question' |  'logic';

export const ActionTypeList: { [key: number]: IActionType } = {
    [ActionTypeEnum.Text]: {
        id: ActionTypeEnum.Text,
        name: 'Text',
        type: 'message'
    },
    [ActionTypeEnum.Poll]: {
        id: ActionTypeEnum.Poll,
        name: 'Poll',
        type: 'question'
    },
    [ActionTypeEnum.Form]: {
        id: ActionTypeEnum.Form,
        name: 'Form',
        type: 'question'
    },
    [ActionTypeEnum.Assignment]: {
        id: ActionTypeEnum.Assignment,
        name: 'Assignment',
        type: 'logic'
    },
    [ActionTypeEnum.Appointment]: {
        id: ActionTypeEnum.Appointment,
        name: 'Appointment',
        type: 'logic'
    },
    [ActionTypeEnum.CallSupport]: {
        id: ActionTypeEnum.CallSupport,
        name: 'Call support',
        type: 'logic'
    },
    [ActionTypeEnum.Check]: {
        id: ActionTypeEnum.Check,
        name: 'Checkbox',
        type: 'question'
    }
};

export interface IMyAction {
    actionId: number;
    conditionId?: number;
    id?: number;
    isConditionMet?: boolean;
}
