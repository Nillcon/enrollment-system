import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ICampaign } from '../../interfaces/campaign.interface';
import { FormGroup, FormControl } from '@angular/forms';
import { CampaignsService } from '../../services/campaigns.service';
import { NotificationService } from '@Services/notification.service';
import { WindowMode } from '@Types/window-mode.type';
import { SaveMode } from '@Types/save-mode.type';
import { IVariable } from '@Interfaces/variable.interface';
import { CampaignsVariablesService } from '../../pages/variables-page/services/campaigns-variables.service';
import { VariableAndConstantQueryTypes } from '@Shared/variable-and-constant-query-types';

interface IData {
    mode: WindowMode;
    campaign?: Partial<ICampaign>;
}

@Component({
    selector: 'app-campaign-window',
    templateUrl: './campaign-window.component.html',
    styleUrls: ['./campaign-window.component.scss'],
    providers: [
        CampaignsVariablesService
    ]
})
export class CampaignWindowComponent implements OnInit {
    public campaignFormGroup: FormGroup;

    public idFormControl: FormControl;
    public nameFormControl: FormControl;
    public welcomeMessageFormControl: FormControl;
    public browserTitleTextFormControl: FormControl;
    public hostFormControl: FormControl;
    public allowAnonymousFormControl: FormControl;
    public durationFormControl: FormControl;
    public capacityFormControl: FormControl;

    public variablesList: IVariable[];

    public isLoading: boolean = false;

    constructor (
        private dialogRef: MatDialogRef<CampaignWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IData,
        private campaignService: CampaignsService,
        private notificationService: NotificationService,
        private variablesService: CampaignsVariablesService
    ) {}

    ngOnInit () {
        if (!this.data) {
            throw new Error('Component can\'t resolve required @Input parameters!');
        }

        this.updateVariables();
        this.initForm();
    }

    public updateVariables (): void {
        if (this.data.mode === 'editing') {
            this.variablesService.getList(this.data.campaign.id, VariableAndConstantQueryTypes.WelcomeMessageAndTitle)
                .subscribe(data => {
                    this.variablesList = data;
                });
        }
    }

    public save (): void {
        if (this.campaignFormGroup.valid) {
            const savingMode: SaveMode = (this.data.mode === 'addition') ? 'create' : 'edit';
            this.isLoading = true;

            this.campaignService.save(this.campaignFormGroup.value, savingMode)
                .subscribe(
                    () => {
                        this.isLoading = false;
                        this.close(true);
                    },
                    () => {
                        this.isLoading = false;
                    }
                );
        } else {
            this.notificationService.error('Oops', 'An error occurred while trying to save the campaign.');
        }
    }

    public close (result: boolean = false): void {
        this.dialogRef.close(result);
    }

    private initForm (): void {
        if (!this.data.campaign) {
            this.data.campaign = {};
        }

        this.idFormControl = new FormControl(this.data.campaign.id || 0);
        this.nameFormControl = new FormControl(this.data.campaign.name || '');
        this.welcomeMessageFormControl = new FormControl(this.data.campaign.welcomeMessage || '');
        this.browserTitleTextFormControl = new FormControl(this.data.campaign.browserTitleText || '');
        this.hostFormControl = new FormControl(this.data.campaign.host || '');
        this.allowAnonymousFormControl = new FormControl(this.data.campaign.isAllowAnonymous || false);
        this.durationFormControl = new FormControl(this.data.campaign.appointmentDuration || 30);
        this.capacityFormControl = new FormControl(this.data.campaign.appointmentCapacity || 1);

        this.campaignFormGroup = new FormGroup({
            id: this.idFormControl,
            name: this.nameFormControl,
            welcomeMessage: this.welcomeMessageFormControl,
            browserTitleText: this.browserTitleTextFormControl,
            host: this.hostFormControl,
            isAllowAnonymous: this.allowAnonymousFormControl,
            appointmentDuration: this.durationFormControl,
            appointmentCapacity: this.capacityFormControl
        });
    }
}
