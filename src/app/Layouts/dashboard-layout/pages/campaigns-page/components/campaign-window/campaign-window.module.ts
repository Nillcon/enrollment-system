import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignWindowComponent } from './campaign-window.component';
import { SharedModule } from '@App/app-shared.module';
import { InputWithVariablesModule } from '@Components/input-with-variables/input-with-variables.module';

@NgModule({
    declarations: [
        CampaignWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        InputWithVariablesModule
    ],
    exports: [
        CampaignWindowComponent
    ],
    entryComponents: [
        CampaignWindowComponent
    ]
})
export class CampaignWindowModule { }
