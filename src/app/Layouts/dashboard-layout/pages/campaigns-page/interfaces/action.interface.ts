import { MessageBlockTypesEnum } from "@Components/chat/shared/message-block-types";
import { IFormMessage, IImageMessage, IPollMessage, IRadioMessage, IHiddenButton } from "@Components/chat/interfaces/message.interface";
import { IAppointmentAction } from "./actions-type.interface";

export interface IBaseAction {
    id?: number;
    type: MessageBlockTypesEnum;
    name: string;
    delay?: number;
    parentId?: number;
    text?: string;
    data?: Partial<IImageMessage | IFormMessage | IPollMessage | IRadioMessage | IAppointmentAction>;
    variableId?: number;
    skipButton?: IHiddenButton;
    callSupportButton?: IHiddenButton;
    submitButton: string;
}

