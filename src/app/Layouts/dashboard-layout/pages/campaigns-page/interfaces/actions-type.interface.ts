import { IVariable } from "@Interfaces/variable.interface";

export interface ILogicAction {
    variable: number;
    value: string;
}

export interface IAppointmentAction {
    selectDateText: string;
    selectTimeText: string;
    selectGreeterText: string;
}