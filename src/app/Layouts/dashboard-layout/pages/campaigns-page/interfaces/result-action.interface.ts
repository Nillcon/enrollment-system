import { ResultActionTypeEnum } from "../shared/result-action-type";

export interface IResultAction {
    id: number;
    actionId: number;
    name: string;
    type: ResultActionTypeEnum;
}
