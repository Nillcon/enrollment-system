import { IStipulation } from "./stipulation.interface";
import { IBaseAction } from "./action.interface";

export interface IBaseCondition {
    id?: number;
    name?: string;
    conditionText?: string;
    value?: string;
    position?: number;
    isActive?: boolean;

    parentId?: number;

    conditions: IStipulation[];
    resultActions: IBaseAction[];
}
