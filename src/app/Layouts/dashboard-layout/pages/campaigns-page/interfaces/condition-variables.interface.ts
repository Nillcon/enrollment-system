import { ValueTypeEnum } from "../shared/value-type";

export interface IConditionVariables {
    id: number;
    name: string;
    type: ValueTypeEnum;
}
