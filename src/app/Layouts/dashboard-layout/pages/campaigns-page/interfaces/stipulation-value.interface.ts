import { ValueTypeEnum } from "../shared/value-type";

export interface IStupulationValue {
    type?: ValueTypeEnum;
    data: ITextValue | IVariableValue | IConstValue;
}

interface ITextValue {
    value: string;
}

interface IVariableValue {
    id: number;
    name: string;
    value: string;
}

interface IConstValue {
    id: number;
    name: string;
    value: string;
}
