import { ConnectorsEnum } from "../shared/connector";
import { IVariable } from "@Interfaces/variable.interface";
import { LogicalOperatorsEnum } from "../shared/logical-operator";

export interface IStipulation {
    id?: number;
    value?: string;
    variable?: number;
    connectorType?: ConnectorsEnum;
    condition?: LogicalOperatorsEnum;
}
