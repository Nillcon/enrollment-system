export interface ICampaign {
    id: number;
    name: string;
    host: string;
    isAllowAnonymous: boolean;
    appointmentDuration: number;
    appointmentCapacity: number;
    welcomeMessage: string;
    browserTitleText: string;
}
