import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StylingPageComponent } from './styling-page.component';
import { MatExpansionModule } from '@angular/material';
import { BackgroundStylingComponent } from './components/background-styling/background-styling.component';
import { ColorSketchModule } from 'ngx-color/sketch';
import { SharedModule } from '@App/app-shared.module';
import { DxColorBoxModule } from 'devextreme-angular';

@NgModule({
    declarations: [
        StylingPageComponent,
        BackgroundStylingComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MatExpansionModule,
        ColorSketchModule,

        DxColorBoxModule
    ],
    exports: [
        StylingPageComponent
    ]
})
export class StylingPageModule { }
