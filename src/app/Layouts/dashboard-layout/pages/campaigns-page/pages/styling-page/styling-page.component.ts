import { Component, OnInit, Input } from '@angular/core';
import { IStyling } from '../../interfaces/styling.interface';
import { CampaignsService } from '../../services/campaigns.service';
import { NotificationService } from '@Services/notification.service';

@Component({
    selector: 'app-styling-page',
    templateUrl: './styling-page.component.html',
    styleUrls: ['./styling-page.component.scss']
})
export class StylingPageComponent implements OnInit {

    @Input() public campaignId: number;
    public isLoading: boolean;

    public styling: IStyling;

    constructor(
        private campaignsService: CampaignsService,
        private notifyService: NotificationService
    ) { }

    ngOnInit() {
        this.updateStyling();
    }

    private updateStyling() {
        this.campaignsService.getStyling(this.campaignId)
            .subscribe(res => {
                this.styling = res;
            });
    }

    public save() {
        this.startLoading();

        this.campaignsService.saveStyling(this.campaignId, this.styling)
            .subscribe(
                () => {
                    this.notifyService.success();
                    this.stopLoading();
                },
                () => this.stopLoading()
            );
    }

    public startLoading() {
        this.isLoading = true;
    }

    public stopLoading() {
        this.isLoading = false;
    }

}
