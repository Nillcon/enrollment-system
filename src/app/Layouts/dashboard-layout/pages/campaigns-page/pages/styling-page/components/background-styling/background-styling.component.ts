import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { IStyling } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/styling.interface';
import { DxColorBoxComponent } from 'devextreme-angular';

@Component({
    selector: 'app-background-styling',
    templateUrl: './background-styling.component.html',
    styleUrls: ['./background-styling.component.scss']
})
export class BackgroundStylingComponent implements OnInit {

    @Input() public campaignId: number;
    @Input() public styling: IStyling;

    @Output() public OnChange: EventEmitter<IStyling> = new EventEmitter();

    @ViewChild('colorBox') colorBox: DxColorBoxComponent;

    public color: string = null;

    constructor() { }

    ngOnInit() {
    }

    public changeComplete (event) {
        console.log(this.styling);
        // this.styling.backgroundColor = ;
    }

    public toDefault() {
        this.styling.backgroundColor = '';
        this.colorBox.value = '';
    }

}
