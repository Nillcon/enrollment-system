import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MailingPageComponent } from './mailing-page.component';
import { SharedModule } from '@App/app-shared.module';
import { MatTableModule } from '@angular/material';
import { InputWithVariablesModule } from '@Components/input-with-variables/input-with-variables.module';

@NgModule({
    declarations: [
        MailingPageComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MatTableModule,
        InputWithVariablesModule
    ],
    exports: [
        MailingPageComponent
    ]
})
export class MailingPageModule { }
