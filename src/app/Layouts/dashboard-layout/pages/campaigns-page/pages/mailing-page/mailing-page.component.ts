import { Component, OnInit, Input } from '@angular/core';
import { MailingVariablesList } from './../../shared/mailing-variables-list';
import { IBriefValueModel } from '@Interfaces/brief-value-model.interface';
import { CampaignsService } from '../../services/campaigns.service';
import { NotificationService } from '@Services/notification.service';
import { IVariable } from '@Interfaces/variable.interface';
import { CampaignsVariablesService } from '../variables-page/services/campaigns-variables.service';
import { VariableAndConstantQueryTypes } from '@Shared/variable-and-constant-query-types';

@Component({
    selector: 'app-mailing-page',
    templateUrl: './mailing-page.component.html',
    styleUrls: ['./mailing-page.component.scss']
})
export class MailingPageComponent implements OnInit {

    public MailingVariablesList = MailingVariablesList;
    public readonly displayedColumns: string[] = ['name', 'value'];

    public MailingVariablesValue: IBriefValueModel[];
    public mailingData: IBriefValueModel[];

    public constantsList: IVariable[];

    public isLoading: boolean = false;

    @Input() campaignId: number;

    constructor(
        private campaingsService: CampaignsService,
        private campaignsVariablesService: CampaignsVariablesService, 
        private notifyService: NotificationService
    ) { }

    ngOnInit() {
        this.updateConstantsList();
        this.updateMailingData();
    }

    private updateMailingData() {
        this.campaingsService.getMailing(this.campaignId)
            .subscribe(res => this.mailingData = res);
    }

    public saveMailing() {
        this.startLoading();

        this.campaingsService.saveMailing(this.campaignId, this.mailingData)
            .subscribe(
                res => {
                    this.notifyService.success('Save');
                    this.finishLoading();
                },
                error =>  this.finishLoading()
            );
    }

    public updateConstantsList() {
        this.campaignsVariablesService.getList(this.campaignId, VariableAndConstantQueryTypes.Mailing)
            .subscribe(res => this.constantsList = res);
    }

    public startLoading() {
        this.isLoading = true;
    }

    public finishLoading() {
        this.isLoading = false;
    }
}
