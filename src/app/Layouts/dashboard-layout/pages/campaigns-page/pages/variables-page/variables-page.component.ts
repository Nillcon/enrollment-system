import { Component, OnInit, Input } from '@angular/core';
import { CampaignsVariablesService } from './services/campaigns-variables.service';
import { IVariable } from '@Interfaces/variable.interface';
import { VariableAndConstantQueryTypes } from '@Shared/variable-and-constant-query-types';

@Component({
    selector: 'app-variables-page',
    templateUrl: './variables-page.component.html',
    styleUrls: ['./variables-page.component.scss']
})
export class VariablesPageComponent implements OnInit {
    @Input() public campaignId: number;

    public variablesList: IVariable[];

    constructor (
        private variablesService: CampaignsVariablesService
    ) {}

    ngOnInit () {
        this.updateVariables();
    }

    public updateVariables (): void {
        this.variablesService.getList(this.campaignId, VariableAndConstantQueryTypes.VariablesTable)
            .subscribe(data => {
                this.variablesList = data;
            });
    }
}
