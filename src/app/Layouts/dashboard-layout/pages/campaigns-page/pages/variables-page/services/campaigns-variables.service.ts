import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";
import { IVariable } from "@Interfaces/variable.interface";
import { IAssociationModel } from "../components/variables-association/shared/association-model";
import { map } from "rxjs/operators";
import { SaveMode } from "@Types/save-mode.type";
import { IConditionVariables } from "../../../interfaces/condition-variables.interface";
import { VariableAndConstantQueryTypes } from "@Shared/variable-and-constant-query-types";

@Injectable()
export class CampaignsVariablesService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getList (campaignId: number, variablesType: VariableAndConstantQueryTypes): Observable<IVariable[]> {
        return this.httpService.get(`/Campaigns/${campaignId}/VariablesQuery?queryType=${variablesType}`);
    }

    public getConditionVariables (campaignId: number): Observable<IConditionVariables[]> {
        return this.httpService.get(`/Campaigns/${campaignId}/ConditionVariables`);
    }

    public create (campaignId: number, variable: IVariable): Observable<any> {
        return this.httpService.post(`/Campaigns/${campaignId}/Variables`, variable);
    }

    public edit (campaignId: number, variable: IVariable): Observable<any> {
        return this.httpService.post(`/Campaigns/${campaignId}/Variables`, variable);
    }

    public saveVariable (
        campaignId: number,
        variable: IVariable,
        mode: SaveMode
    ): Observable<any> {
        return (mode === 'create') ?
            this.create(campaignId, variable) :
            this.edit(campaignId, variable);
    }

    public delete (campaignId: number, variableId: number): Observable<any> {
        return this.httpService.delete(`/Campaigns/${campaignId}/Variables/${variableId}`);
    }

    public groupDelete (campaignId: number, variableIds: number[]): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/MassDeleteVariables`, { value: variableIds });
    }

    public uploadVariablesCsv (campaignId: number, variablesCsv: Blob): Observable<any> {
        return this.httpService.uploadFile(`/Campaigns/${campaignId}/Csv`, variablesCsv);
    }

    public getAssociations (campaignId: number): Observable<IAssociationModel> {
        return this.httpService.get(`/Campaigns/${campaignId}/Constructors`)
            .pipe(
                map((model: IAssociationModel) => {
                    for (const [key, value] of Object.entries(model)) {
                        model[key] = JSON.parse(value);
                    }

                    return model;
                })
            );
    }

    public saveAssociations (campaignId: number, model: IAssociationModel): Observable<any> {
        return this.httpService.post(`/Campaigns/${campaignId}/Constructors`, model);
    }
}
