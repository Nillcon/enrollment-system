import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VariablesPageComponent } from './variables-page.component';
import { SharedModule } from '@App/app-shared.module';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { VariablesTableComponent } from './components/variables-table/variables-table.component';
import { VariableWindowComponent } from './components/variables-table/components/variable-window/variable-window.component';
import { VariablesAssociationComponent } from './components/variables-association/variables-association.component';
import { ChipInputModule } from '@Components/chip-input/chip-input.module';

@NgModule({
    declarations: [
        VariablesPageComponent,

        VariablesTableComponent,
        VariablesAssociationComponent,
        VariableWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule,

        ChipInputModule
    ],
    entryComponents: [
        VariableWindowComponent
    ],
    exports: [
        VariablesPageComponent
    ]
})
export class VariablesPageModule { }
