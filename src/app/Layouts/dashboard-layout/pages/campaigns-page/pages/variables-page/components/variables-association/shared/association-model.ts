export interface IAssociationModel {
    hash: string[];
    tag: string[];
    alternativeHash: string[];
    name: string[];
    primaryKey: string[];
    blocksTags: string[];
}
