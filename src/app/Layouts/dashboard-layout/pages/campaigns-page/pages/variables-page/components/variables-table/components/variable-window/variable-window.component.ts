import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { IUserVariable } from '@Interfaces/user-variable.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NotificationService } from '@Services/notification.service';
// tslint:disable-next-line: max-line-length
import { CampaignsVariablesService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/variables-page/services/campaigns-variables.service';
import { WindowMode } from '@Types/window-mode.type';
import { SaveMode } from '@Types/save-mode.type';

interface IData {
    invitedUserId: number;
    variable?: IUserVariable;
    mode: WindowMode;
}

@Component({
    selector: 'app-variable-window',
    templateUrl: './variable-window.component.html',
    styleUrls: ['./variable-window.component.scss']
})
export class VariableWindowComponent implements OnInit {
    public variableFormGroup: FormGroup;
    public idFormControl: FormControl;
    public nameFormControl: FormControl;

    public isLoading: boolean = false;

    constructor (
        private dialogRef: MatDialogRef<VariableWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IData,
        private variablesService: CampaignsVariablesService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        if (!this.data) {
            throw new Error('Component can\'t resolve required @Input parameters!');
        }

        this.initForm();
    }

    public save (): void {
        if (this.variableFormGroup.valid) {
            const savingMode: SaveMode = (this.data.mode === 'addition') ? 'create' : 'edit';
            this.isLoading = true;

            this.variablesService.saveVariable(
                this.data.invitedUserId,
                this.variableFormGroup.value,
                savingMode
            )
                .subscribe(
                    () => {
                        this.isLoading = false;
                        this.dialogRef.close();
                    },
                    () => {
                        this.isLoading = false;
                    }
                );
        } else {
            this.notificationService.error('Oops', 'An error occurred while trying to save variable.');
        }
    }

    public cancel (): void {
        this.dialogRef.close();
    }

    private initForm (): void {
        const variable: Partial<IUserVariable> = this.data.variable || {};
        this.idFormControl    = new FormControl(variable.id    || 0);
        this.nameFormControl  = new FormControl(variable.name  || '');

        this.variableFormGroup = new FormGroup({
            id: this.idFormControl,
            name: this.nameFormControl
        });
    }
}
