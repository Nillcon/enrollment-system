import { Component, OnInit, Input, ViewChild, Injector, OnDestroy, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { VariableWindowComponent } from './components/variable-window/variable-window.component';
import { Roles } from '@Shared/roles';
import { IUser } from '@Interfaces/users';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { UserService } from '@Services/user.service';
import { IVariable } from '@Interfaces/variable.interface';
import { CampaignsVariablesService } from '../../services/campaigns-variables.service';
import { tap } from 'rxjs/operators';

@AutoUnsubscribe()
@Component({
    selector: 'app-variables-table',
    templateUrl: './variables-table.component.html',
    styleUrls: ['./variables-table.component.scss']
})
export class VariablesTableComponent implements OnInit, OnDestroy {

    @ViewChild('VariablesCsvUploader') private VariablesCsvUploader: { nativeElement: { click: () => void; }; };

    @Input() private campaignId: number;
    @Input() public variables: IVariable[];

    @Output() public OnSelection: EventEmitter<IVariable[]> = new EventEmitter();
    @Output() public OnSomethingChange: EventEmitter<boolean> = new EventEmitter();

    public readonly Roles: typeof Roles = Roles;

    public userData: IUser;

    public isCsvLoading: boolean = false;

    public selectedVariables: IVariable[] = [];

    private userDataSubscription: Subscription;

    constructor (
        public injector: Injector,
        private variablesService: CampaignsVariablesService,
        private dialog: MatDialog,
        private userService: UserService
    ) {}

    ngOnInit () {
        if (!this.campaignId) {
            throw new Error('Component can\'t resolve required @Input parameters!');
        }

        this.userDataObserver();
    }

    ngOnDestroy () {}

    public update (): void {
        this.OnSomethingChange.emit(true);
    }

    public onSelection (e: any): void {
        this.selectedVariables = e.selectedRowsData;
        this.OnSelection.emit(this.selectedVariables);
    }

    @NeedsConfirmation('Do you really want to delete variable?', 'Delete variable')
    public delete (variable: IVariable) {
        this.variablesService.delete(this.campaignId, variable.id)
            .subscribe(() => {
                this.update();
            });
    }

    @NeedsConfirmation('Please confirm deleting selected variables', 'Delete selected variables')
    public groupDeleting (): void {
        const selectedVariableIds: number[] = this.selectedVariables.map(variable => {
            return variable.id;
        });

        this.variablesService.groupDelete(this.campaignId, selectedVariableIds)
            .pipe(
                tap(() => {
                    this.selectedVariables = [];
                    this.update();
                })
            )
            .subscribe();
    }

    public uploadCsv (e: any): void {
        const csvFile: Blob = e.target.files[0];
        this.isCsvLoading   = true;

        this.variablesService.uploadVariablesCsv(this.campaignId, csvFile)
            .subscribe(
                () => {
                    this.isCsvLoading = false;
                    this.update();
                    this.OnSomethingChange.emit(true);
                    this.VariablesCsvUploader.nativeElement['value'] = null;
                },
                () => {
                    this.isCsvLoading = false;
                    this.VariablesCsvUploader.nativeElement['value'] = null;
                }
            );
    }

    public selectCsv (): void {
        this.VariablesCsvUploader.nativeElement.click();
    }

    public openAdditionWindow (): void {
        const additionWindow = this.dialog.open(VariableWindowComponent, {
            width: '450px',
            data: {
                invitedUserId: this.campaignId,
                mode: 'addition'
            }
        });

        additionWindow.afterClosed()
            .subscribe(() => this.update());
    }

    public openEditingWindow (variable: IVariable): void {
        const editingWindow = this.dialog.open(VariableWindowComponent, {
            width: '450px',
            data: {
                invitedUserId: this.campaignId,
                variable: variable,
                mode: 'editing'
            }
        });

        editingWindow.afterClosed()
            .subscribe(() => this.update());
    }

    private userDataObserver (): void {
        this.userDataSubscription = this.userService.Data$
            .subscribe(data => {
                this.userData = data;
            });
    }
}
