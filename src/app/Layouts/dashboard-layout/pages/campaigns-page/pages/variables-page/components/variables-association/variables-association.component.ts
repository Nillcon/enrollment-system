import { Component, OnInit, Input } from '@angular/core';
// tslint:disable-next-line: max-line-length
import { CampaignsVariablesService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/variables-page/services/campaigns-variables.service';
import { FormGroup, FormControl } from '@angular/forms';
import { IAssociationModel } from './shared/association-model';
import { NotificationService } from '@Services/notification.service';
import { IVariable } from '@Interfaces/variable.interface';

@Component({
    selector: 'app-variables-association',
    templateUrl: './variables-association.component.html',
    styleUrls: ['./variables-association.component.scss']
})
export class VariablesAssociationComponent implements OnInit {
    @Input() public campaignId: number;
    @Input() public variables: IVariable[] = [];

    public associationFormGroup: FormGroup;
    public nameFormControl: FormControl;
    public tagFormControl: FormControl;
    public hashFormControl: FormControl;
    public alternativeHashFormControl: FormControl;
    public primaryKeyFormControl: FormControl;
    public blocksTagsFormControl: FormControl;

    public isSaving: boolean = false;

    constructor (
        private variablesService: CampaignsVariablesService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        if (!this.campaignId) {
            throw new Error('Component can\'t resolve required @Input parameters!');
        }

        this.update();
    }

    public update (): void {
        this.variablesService.getAssociations(this.campaignId)
            .subscribe(data => {
                this.initForm(data);
            });
    }

    public save (): void {
        if (this.associationFormGroup.valid) {
            const data = Object.assign({}, this.associationFormGroup.value);
            for (const [key, value] of Object.entries(data)) {
                data[key] = JSON.stringify(value);
            }

            this.isSaving = true;

            this.variablesService.saveAssociations(this.campaignId, data)
                .subscribe(
                    () => {
                        this.isSaving = false;
                    },
                    () => {
                        this.isSaving = false;
                    }
                );
        } else {
            this.notificationService.error('Oops', 'An error occurred while trying to save the associations.');
        }
    }

    private initForm (model: IAssociationModel): void {
        this.nameFormControl            = new FormControl(model.name);
        this.tagFormControl             = new FormControl(model.tag);
        this.hashFormControl            = new FormControl(model.hash);
        this.alternativeHashFormControl = new FormControl(model.alternativeHash);
        this.primaryKeyFormControl      = new FormControl(model.primaryKey);
        this.blocksTagsFormControl      = new FormControl(model.blocksTags);

        this.associationFormGroup = new FormGroup({
            name: this.nameFormControl,
            tag: this.tagFormControl,
            hash: this.hashFormControl,
            alternativeHash: this.alternativeHashFormControl,
            primaryKey: this.primaryKeyFormControl,
            blocksTags: this.blocksTagsFormControl
        });
    }
}
