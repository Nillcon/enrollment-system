import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultGreetersPageComponent } from './default-greeters-page.component';
import { StaffSharedModule } from '@Layouts/dashboard-layout/pages/staff-page/staff-shared.module';

@NgModule({
    declarations: [
        DefaultGreetersPageComponent
    ],
    imports: [
        CommonModule,

        StaffSharedModule
    ],
    exports: [
        DefaultGreetersPageComponent
    ]
})
export class DefaultGreetersPageModule { }
