import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";

@Injectable()
export class CampaignsDefaultGreeters {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getSelectedIdsList (campaignId: number): Observable<number[]> {
        return this.httpService.get(`/Campaigns/${campaignId}/DefaultGreeters`);
    }

    public save (campaignId: number, greeterIds: number[]): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/DefaultGreeters`, greeterIds);
    }
}
