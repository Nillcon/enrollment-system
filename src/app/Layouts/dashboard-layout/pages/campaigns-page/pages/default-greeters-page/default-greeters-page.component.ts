import { Component, OnInit, Input } from '@angular/core';
import { CampaignsDefaultGreeters } from './services/campaigns-default-greeters.service';
import { IStaffUser } from '@Interfaces/users';
import { Roles } from '@Shared/roles';
import { NotificationService } from '@Services/notification.service';

@Component({
    selector: 'app-default-greeters-page',
    templateUrl: './default-greeters-page.component.html',
    styleUrls: ['./default-greeters-page.component.scss'],
    providers: [
        CampaignsDefaultGreeters
    ]
})
export class DefaultGreetersPageComponent implements OnInit {
    @Input() public campaignId: number;

    public readonly Roles: typeof Roles = Roles;

    public selectedGreetersIds: number[];
    public newSelectedGreetersIds: number[];

    public isSaving: boolean = false;

    constructor (
        private defaultGreetersService: CampaignsDefaultGreeters,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.defaultGreetersService.getSelectedIdsList(this.campaignId)
            .subscribe((greeterIds) => {
                this.selectedGreetersIds    = greeterIds;
                this.newSelectedGreetersIds = greeterIds;
            });
    }

    public prepareSelectedGreeters (selectedGreeters: IStaffUser[]): void {
        this.newSelectedGreetersIds = selectedGreeters.map(greeter => {
            return greeter.id;
        });
    }

    public save (): void {
        this.isSaving = true;

        this.defaultGreetersService.save(this.campaignId, this.newSelectedGreetersIds)
            .subscribe(
                () => {
                    this.isSaving = false;
                    this.notificationService.success();
                },
                () => {
                    this.isSaving = false;
                }
            );
    }
}
