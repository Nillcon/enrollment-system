import { Component, OnInit, Input } from '@angular/core';
import { Roles } from '@Shared/roles';
import { CampaignsManagersService } from './services/campaigns-managers.service';
import { IStaffUser } from '@Interfaces/users';

@Component({
    selector: 'app-managers-page',
    templateUrl: './managers-page.component.html',
    styleUrls: ['./managers-page.component.scss'],
    providers: [
        CampaignsManagersService
    ]
})
export class ManagersPageComponent implements OnInit {
    @Input() public campaignId: number;

    public readonly Roles: typeof Roles = Roles;

    public selectedManagerIds: number[];
    public newSelectedManagerIds: number[];

    public isSaving: boolean = false;

    constructor (
        private managersService: CampaignsManagersService
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.managersService.getSelectedIdsList(this.campaignId)
            .subscribe(managerIds => {
                this.selectedManagerIds    = managerIds;
                this.newSelectedManagerIds = managerIds;
            });
    }

    public save (): void {
        this.isSaving = true;

        this.managersService.save(this.campaignId, this.newSelectedManagerIds)
            .subscribe(
                () => {
                    this.isSaving = false;
                },
                () => {
                    this.isSaving = false;
                }
            );
    }

    public prepareSelectedGreeters (selectedManagers: IStaffUser[]): void {
        this.newSelectedManagerIds = selectedManagers.map(manager => {
            return manager.id;
        });
    }
}
