import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";

@Injectable()
export class CampaignsManagersService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getSelectedIdsList (campaignId): Observable<number[]> {
        return this.httpService.get(`/Campaigns/${campaignId}/Managers`);
    }

    public save (campaignId: number, managerIds: number[]): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Managers`, managerIds);
    }
}
