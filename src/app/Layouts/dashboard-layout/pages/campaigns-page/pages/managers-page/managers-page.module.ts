import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagersPageComponent } from './managers-page.component';
import { StaffSharedModule } from '@Layouts/dashboard-layout/pages/staff-page/staff-shared.module';

@NgModule({
    declarations: [
        ManagersPageComponent
    ],
    imports: [
        CommonModule,

        StaffSharedModule
    ],
    exports: [
        ManagersPageComponent
    ]
})
export class ManagersPageModule { }
