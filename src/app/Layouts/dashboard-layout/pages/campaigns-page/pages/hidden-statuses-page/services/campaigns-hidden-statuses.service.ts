import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";
import { IHiddenStatus } from "../interfaces/hidden-status.interface";
import { SaveMode } from "@Types/save-mode.type";

@Injectable()
export class CampaignsHiddenStatusesService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public get (campaignId: number, statusId: number): Observable<IHiddenStatus> {
        return this.httpService.get(`/Campaigns/${campaignId}/HiddenStatuses/${statusId}`);
    }

    public getList (campaignId: number): Observable<IHiddenStatus[]> {
        return this.httpService.get(`/Campaigns/${campaignId}/HiddenStatuses`);
    }

    public create (campaignId: number, model: IHiddenStatus): Observable<any> {
        return this.httpService.post(`/Campaigns/${campaignId}/HiddenStatuses`, model);
    }

    public edit (campaignId: number, model: IHiddenStatus): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/HiddenStatuses/${model.id}`, model);
    }

    public save (
        campaignId: number,
        model: IHiddenStatus,
        saveMode: SaveMode
    ): Observable<any> {
        return (saveMode === 'create') ?
            this.create(campaignId, model) :
            this.edit(campaignId, model);
    }

    public delete (campaignId: number, hiddenStatusId: number): Observable<any> {
        return this.httpService.delete(`/Campaigns/${campaignId}/HiddenStatuses/${hiddenStatusId}`);
    }
}
