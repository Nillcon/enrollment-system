import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HiddenStatusesPageComponent } from './hidden-statuses-page.component';
import { SharedModule } from '@App/app-shared.module';
import { CampaignsHiddenStatusesService } from './services/campaigns-hidden-statuses.service';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { HiddenStatusesTableComponent } from './components/hidden-statuses-table/hidden-statuses-table.component';
import { HiddenStatusWindowModule } from './components/hidden-status-window/hidden-status-window.module';

@NgModule({
    declarations: [
        HiddenStatusesPageComponent,
        HiddenStatusesTableComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule,

        HiddenStatusWindowModule
    ],
    exports: [
        HiddenStatusesPageComponent
    ],
    providers: [
        CampaignsHiddenStatusesService
    ]
})
export class HiddenStatusesPageModule { }
