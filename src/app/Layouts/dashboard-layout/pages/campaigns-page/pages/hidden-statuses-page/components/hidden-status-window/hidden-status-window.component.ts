import { Component, OnInit, Inject } from '@angular/core';
import { WindowMode } from '@Types/window-mode.type';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { CampaignsHiddenStatusesService } from '../../services/campaigns-hidden-statuses.service';
import { IHiddenStatus } from '../../interfaces/hidden-status.interface';
import { NotificationService } from '@Services/notification.service';
import { SaveMode } from '@Types/save-mode.type';

interface IData {
    mode: WindowMode;
    statusId: number;
    campaignId: number;
}

@Component({
    selector: 'app-hidden-status-window',
    templateUrl: './hidden-status-window.component.html',
    styleUrls: ['./hidden-status-window.component.scss']
})
export class HiddenStatusWindowComponent implements OnInit {
    public statusFormGroup: FormGroup;
    public idFormControl: FormControl;
    public nameFormControl: FormControl;

    public isSaving: boolean = false;

    constructor (
        private dialogRef: MatDialogRef<HiddenStatusWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IData,
        private statusesService: CampaignsHiddenStatusesService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        if (this.data.mode === 'editing') {
            this.statusesService.get(this.data.campaignId, this.data.statusId)
                .subscribe(data => {
                    this.initForm(data);
                });
        } else {
            this.initForm();
        }
    }

    public close (result?: boolean): void {
        this.dialogRef.close(result);
    }

    public save (): void {
        if (this.statusFormGroup.valid) {
            const saveMode: SaveMode = (this.data.mode === 'addition') ? 'create' : 'edit';
            this.isSaving            = true;

            this.statusesService.save(
                this.data.campaignId,
                this.statusFormGroup.value,
                saveMode
            )
                .subscribe(
                    () => {
                        this.isSaving = false;
                        this.close(true);
                    },
                    () => {
                        this.isSaving = false;
                    }
                );
        } else {
            this.notificationService.error('Oops', 'An error occurred while trying to save hidden status.');
        }
    }

    private initForm (model: Partial<IHiddenStatus> = {}): void {
        this.idFormControl    = new FormControl(model.id || 0);
        this.nameFormControl  = new FormControl(model.name || '');

        this.statusFormGroup = new FormGroup({
            id: this.idFormControl,
            name: this.nameFormControl
        });
    }
}
