import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { HiddenStatusWindowComponent } from './components/hidden-status-window/hidden-status-window.component';
import { filter } from 'rxjs/operators';
import { HiddenStatusesTableComponent } from './components/hidden-statuses-table/hidden-statuses-table.component';

@Component({
    selector: 'app-hidden-statuses-page',
    templateUrl: './hidden-statuses-page.component.html',
    styleUrls: ['./hidden-statuses-page.component.scss']
})
export class HiddenStatusesPageComponent implements OnInit {
    @ViewChild('StatusesTable') private StatusesTable: HiddenStatusesTableComponent;

    @Input() public campaignId: number;

    constructor (
        private dialog: MatDialog
    ) {}

    ngOnInit () {}

    public openAdditionWindow (): void {
        const additionWindow = this.dialog.open(HiddenStatusWindowComponent, {
            width: '450px',
            data: {
                mode: 'addition',
                campaignId: this.campaignId
            }
        });

        additionWindow.afterClosed()
            .pipe(
                filter(result => result === true)
            )
            .subscribe(() => {
                this.StatusesTable.update();
            });
    }
}
