import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HiddenStatusWindowComponent } from './hidden-status-window.component';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        HiddenStatusWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    exports: [
        HiddenStatusWindowComponent
    ],
    entryComponents: [
        HiddenStatusWindowComponent
    ]
})
export class HiddenStatusWindowModule { }
