import { Component, OnInit, ViewChild, Input, Injector, AfterViewInit } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { IHiddenStatus } from '../../interfaces/hidden-status.interface';
import { CampaignsHiddenStatusesService } from '../../services/campaigns-hidden-statuses.service';
import { HiddenStatusWindowComponent } from '../hidden-status-window/hidden-status-window.component';
import { filter } from 'rxjs/operators';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';

@Component({
  selector: 'app-hidden-statuses-table',
  templateUrl: './hidden-statuses-table.component.html',
  styleUrls: ['./hidden-statuses-table.component.scss']
})
export class HiddenStatusesTableComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort)      private sort: MatSort;

    @Input() public campaignId: number;

    public readonly DisplayedColumns: Array<string> = [
        'name', 'edit', 'delete'
    ];

    public dataSource: MatTableDataSource<IHiddenStatus> = new MatTableDataSource();

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private statusService: CampaignsHiddenStatusesService
    ) {}

    ngOnInit () {
        if (!this.campaignId) {
            throw new Error('Component can\'t resolve required @Input parameters!');
        }

        this.update();
    }

    ngAfterViewInit () {
        this.dataSource.paginator   = this.paginator;
        this.dataSource.sort        = this.sort;
    }

    public update (): void {
        this.statusService.getList(this.campaignId)
            .subscribe(data => {
                this.dataSource.data = data;
            });
    }

    public edit (status: IHiddenStatus): void {
        const additionWindow = this.dialog.open(HiddenStatusWindowComponent, {
            width: '450px',
            data: {
                mode: 'editing',
                statusId: status.id,
                campaignId: this.campaignId
            }
        });

        additionWindow.afterClosed()
            .pipe(
                filter(result => result === true)
            )
            .subscribe(() => {
                this.update();
            });
    }

    @NeedsConfirmation('Do you really want to delete hidden status?', 'Confirm Deleting')
    public delete (status: IHiddenStatus): void {
        this.statusService.delete(this.campaignId, status.id)
            .subscribe(() => {
                this.update();
            });
    }
}
