export interface IHiddenStatus {
    id: number;
    name: string;
}
