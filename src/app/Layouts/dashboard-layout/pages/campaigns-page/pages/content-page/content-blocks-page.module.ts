import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentBlocksPageComponent } from './content-blocks-page.component';
import { ContentBlocksTableComponent } from './components/content-blocks-table/content-blocks-table.component';
import { ContentBlockWindowModule } from './components/content-block-window/content-block-window.module';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        ContentBlocksPageComponent,

        ContentBlocksTableComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule,

        ContentBlockWindowModule
    ],
    exports: [
        ContentBlocksPageComponent
    ]
})
export class ContentBlocksPageModule { }
