import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FormControl } from '@angular/forms';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';

@AutoUnsubscribe()
@Component({
    selector: 'app-video-content',
    templateUrl: './video-content.component.html',
    styleUrls: ['./video-content.component.scss']
})
export class VideoContentComponent implements OnInit, OnDestroy {

    @Input()
    public videoSrc: string;

    @Output()
    public OnChange: EventEmitter<string> = new EventEmitter();

    public videoLinkControl: FormControl = new FormControl(
        '',
        [this.validateVideoPath.bind(this)]
    );

    private readonly youtubeVideoPath: string = environment.youtubeVideoPath;

    private videoPathChangesSubscription: Subscription;

    constructor () {}

    ngOnInit () {
        this.initForm();
        this.videoPathChangesObserver();
    }

    ngOnDestroy () {}

    private initForm (): void {
        const link: string = (this.videoSrc) ? `${this.youtubeVideoPath}${this.videoSrc}` : `${this.youtubeVideoPath}`;
        this.videoLinkControl.setValue(link);
        this.OnChange.emit(this.videoLinkControl.value);
    }

    private videoPathChangesObserver (): void {
        this.videoPathChangesSubscription = this.videoLinkControl.valueChanges
            .subscribe((value) => {
                this.OnChange.emit(value);
            });
    }

    private validateVideoPath (control: FormControl): { isVideoLinkIncorrect } {
        const reg = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/gm;

        if (!reg.test(control.value)) {
            return { isVideoLinkIncorrect: true };
        }

        return null;
    }

}
