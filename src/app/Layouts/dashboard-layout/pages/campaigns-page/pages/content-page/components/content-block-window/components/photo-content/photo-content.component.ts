import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';

@Component({
    selector: 'app-photo-content',
    templateUrl: './photo-content.component.html',
    styleUrls: ['./photo-content.component.scss']
})
export class PhotoContentComponent implements OnInit {
    @ViewChild('PhotoInput') public PhotoInput: { nativeElement: { click: () => void; }; };
    @Input() public photoSrc: string = '';

    @Output() public OnChange: EventEmitter<FormData> = new EventEmitter();

    public photo: Blob;
    public photoName: string;

    constructor () {}

    ngOnInit () {}

    public selectPhoto (): void {
        this.PhotoInput.nativeElement.click();
    }

    public emitPhoto (): void {
        const photoFormData: FormData = new FormData();
        photoFormData.append('file', this.photo);
        this.OnChange.emit(photoFormData);
    }
}
