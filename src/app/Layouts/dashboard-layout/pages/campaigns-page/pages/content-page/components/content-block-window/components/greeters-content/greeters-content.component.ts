import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IStaffUser } from '@Interfaces/users';
import { Roles } from '@Shared/roles';

@Component({
    selector: 'app-greeters-content',
    templateUrl: './greeters-content.component.html',
    styleUrls: ['./greeters-content.component.scss']
})
export class GreetersContentComponent implements OnInit {
    private _greeterIds: number[] = [];
    @Input() public set greeterIds (greeterIds: number[]) {
        this._greeterIds       = greeterIds;
        this.isManualSelection = Boolean(greeterIds.length);
    }
    public get greeterIds (): number[] {
        return this._greeterIds;
    }

    @Output() public OnChange: EventEmitter<number[]> = new EventEmitter();

    public Roles: typeof Roles = Roles;

    public isManualSelection: boolean = false;

    constructor () {}

    ngOnInit () {}

    public onGreetersSelectionChaged (greeters: IStaffUser[]): void {
        const greeterIds: number[] = greeters.map(greeter => {
            return greeter.id;
        });

        this.OnChange.emit(greeterIds);
    }
}
