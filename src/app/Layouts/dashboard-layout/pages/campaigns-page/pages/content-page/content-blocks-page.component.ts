import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-content-blocks-page',
    templateUrl: './content-blocks-page.component.html',
    styleUrls: ['./content-blocks-page.component.scss']
})
export class ContentBlocksPageComponent implements OnInit {
    @Input() public campaignId: number;

    constructor () {}

    ngOnInit () {}
}
