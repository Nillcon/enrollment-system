import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { WindowMode } from '@Types/window-mode.type';
import { CampaignsVariablesService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/variables-page/services/campaigns-variables.service';
import { Observable } from 'rxjs';
import { VariableAndConstantQueryTypes } from '@Shared/variable-and-constant-query-types';
import { IVariable } from '@Interfaces/variable.interface';
import { map, flatMap, toArray, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-html-content',
    templateUrl: './html-content.component.html',
    styleUrls: ['./html-content.component.scss']
})
export class HtmlContentComponent implements OnInit {
    @Input() public mode: WindowMode;

    private _content: string = '';
    @Input() public set content (newContent: string) {
        if (this.mode === 'addition') {
            this._content = newContent || this.defaultContent;
        } else {
            this._content = newContent;
        }

        setTimeout(() => this.OnChange.emit(this._content));
    }

    @Input()
    public campaignId: number;
    public get content (): string {
        return this._content;
    }

    @Output()
    public OnChange: EventEmitter<string> = new EventEmitter();

    public readonly apiKey: string = environment.tinyEditorApiKey;

    public editorOptions: object;

    private _variables: IVariable[];

    // tslint:disable-next-line: max-line-length
    private readonly defaultContent: string = `<span style="font-size: 36px;">Lorem&nbsp;ipsum&nbsp;dolor</span><img src="/assets/images/Joe-Lion.jpg" style="width: 300px; float: left;"><p><span style="font-size: 12px;">sit&nbsp;amet,&nbsp;consectetur&nbsp;adipiscing&nbsp;elit,&nbsp;sed&nbsp;do&nbsp;eiusmod&nbsp;tempor&nbsp;incididunt&nbsp;ut&nbsp;labore&nbsp;et&nbsp;dolore&nbsp;magna&nbsp;&nbsp;&nbsp;&nbsp;aliqua.&nbsp;Ut&nbsp;enim&nbsp;ad&nbsp;minim&nbsp;veniam,&nbsp;quis&nbsp;nostrud&nbsp;exercitation&nbsp;ullamco&nbsp;laboris&nbsp;nisi&nbsp;ut&nbsp;aliquip&nbsp;ex&nbsp;ea&nbsp;commodo&nbsp;consequat.&nbsp;Duis&nbsp;&nbsp;&nbsp;&nbsp;aute&nbsp;irure&nbsp;dolor&nbsp;in&nbsp;reprehenderit&nbsp;in&nbsp;voluptate&nbsp;velit&nbsp;esse&nbsp;cillum&nbsp;dolore&nbsp;eu&nbsp;fugiat&nbsp;nulla&nbsp;pariatur.&nbsp;Excepteur&nbsp;sint&nbsp;occaecat&nbsp;cupidatat&nbsp;non&nbsp;proident,&nbsp;sunt&nbsp;in&nbsp;culpa&nbsp;qui&nbsp;officia&nbsp;deserunt&nbsp;mollit&nbsp;anim&nbsp;id&nbsp;est&nbsp;laborum.</span><br></p>`;

    constructor (
        private campaignsVariablesService: CampaignsVariablesService
    ) {}

    public ngOnInit (): void {
        this.updateVariables()
            .pipe(
                tap(() => this.initEditorOptions())
            )
            .subscribe();
    }

    public updateVariables (): Observable<IVariable[]> {
        return this.campaignsVariablesService.getList(this.campaignId, VariableAndConstantQueryTypes.WelcomeMessageAndTitle)
            .pipe(
                flatMap(variables => variables),
                map(variable => {
                    variable.name = `#${variable.name}`;

                    return variable;
                }),
                toArray(),
                tap((variables) => this._variables = variables)
            );
    }

    private initEditorOptions (): void {
        this.editorOptions = {
            height: '400',
            plugins: [
                'link image paste table lists'
            ],
            toolbar: `  undo redo | h1 h2 h3 | bold italic |
                        alignleft aligncenter alignright alignjustify |
                        forecolor backcolor | link image table |
                        numlist bullist | variablesButton
            `,
            menubar: false,
            paste_data_images: true,
            images_upload_handler: function (blobInfo: any, success: any, failure: any) {
                success(`data:${blobInfo.blob().type};base64,${blobInfo.base64()}`);
            },
            setup: (editor: any) => {
                editor.ui.registry.addMenuButton('variablesButton', {
                    icon: 'sourcecode',
                    tooltip: 'Variables',
                    fetch: (callback: any) => {
                        const items: object[] = this._variables.map((currVariable) => {
                            return {
                                type: 'menuitem',
                                text: currVariable.name,
                                onAction: () => {
                                    editor.selection.setContent(` ${currVariable.name} `);
                                }
                            };
                        });

                        callback(items);
                    }
                });
            }
        };
    }

}
