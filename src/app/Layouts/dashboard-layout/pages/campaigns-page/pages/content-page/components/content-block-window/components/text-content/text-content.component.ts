import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IVariable } from '@Interfaces/variable.interface';
import { InputWithVariablesComponent } from '@Components/input-with-variables/input-with-variables.component';

@Component({
    selector: 'app-text-content',
    templateUrl: './text-content.component.html',
    styleUrls: ['./text-content.component.scss']
})
export class TextContentComponent implements OnInit {
    @ViewChild('TextInput') private TextInput: InputWithVariablesComponent;

    @Input() public text: string = '';
    @Input() public campaignId: number;
    @Input() public variablesList: IVariable[] = [];

    @Output() public OnChange: EventEmitter<string> = new EventEmitter();

    constructor () {}

    ngOnInit () {
        this.TextInput.value = this.text;
    }
}
