import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";
import { IBaseContentBlockModel, IContentBlockModel } from "@Interfaces/content-block-model.interface";
import { SaveMode } from "@Types/save-mode.type";

@Injectable()
export class CampaignsContentService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getList (campaignId: number): Observable<IBaseContentBlockModel[]> {
        return this.httpService.get(`/Campaigns/${campaignId}/Blocks`);
    }

    public get (campaignId: number, blockId: number): Observable<IBaseContentBlockModel> {
        return this.httpService.get(`/Campaigns/${campaignId}/Blocks/${blockId}`);
    }

    public create (campaignId: number, block: IContentBlockModel): Observable<any> {
        return this.httpService.post(`/Campaigns/${campaignId}/Blocks`, block);
    }

    public edit (campaignId: number, block: IContentBlockModel): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Blocks/${block.id}`, block);
    }

    public save (
        campaignId: number,
        block: IContentBlockModel,
        mode: SaveMode
    ): Observable<any> {
        return (mode === 'create') ? this.create(campaignId, block) : this.edit(campaignId, block);
    }

    public delete (campaignId: number, blockId: number): Observable<any> {
        return this.httpService.delete(`/Campaigns/${campaignId}/Blocks/${blockId}`);
    }

    public moveUp (campaignId: number, blockId: number): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Blocks/${blockId}/MoveUp`, {});
    }

    public moveDown (campaignId: number, blockId: number): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Blocks/${blockId}/MoveDown`, {});
    }

    public changeState (campaignId: number, blockId: number, state: boolean): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Blocks/${blockId}/State`, { value: state });
    }
}
