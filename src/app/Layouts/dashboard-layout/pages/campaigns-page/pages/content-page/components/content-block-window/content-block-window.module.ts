import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentBlockWindowComponent } from './content-block-window.component';
import { TextContentComponent } from './components/text-content/text-content.component';
import { HtmlContentComponent } from './components/html-content/html-content.component';
import { PhotoContentComponent } from './components/photo-content/photo-content.component';
import { VideoContentComponent } from './components/video-content/video-content.component';
import { GreetersContentComponent } from './components/greeters-content/greeters-content.component';
import { StaffSharedModule } from '@Layouts/dashboard-layout/pages/staff-page/staff-shared.module';
import { InputWithVariablesModule } from '@Components/input-with-variables/input-with-variables.module';
import { SharedModule } from '@App/app-shared.module';
import { DxColorBoxModule } from 'devextreme-angular';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ChipInputModule } from '@Components/chip-input/chip-input.module';
import { EditorModule } from '@tinymce/tinymce-angular';

@NgModule({
    declarations: [
        ContentBlockWindowComponent,
        TextContentComponent,
        HtmlContentComponent,
        PhotoContentComponent,
        VideoContentComponent,
        GreetersContentComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        EditorModule,

        ChipInputModule,
        StaffSharedModule,
        InputWithVariablesModule,
        ImageCropperModule,
        DxColorBoxModule
    ],
    exports: [
        ContentBlockWindowComponent
    ],
    entryComponents: [
        ContentBlockWindowComponent
    ]
})
export class ContentBlockWindowModule { }
