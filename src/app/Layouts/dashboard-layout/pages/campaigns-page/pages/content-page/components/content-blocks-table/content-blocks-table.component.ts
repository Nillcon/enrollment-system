import { Component, OnInit, ViewChild, Input, Injector, AfterViewInit } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { ContentBlockWindowComponent } from '../content-block-window/content-block-window.component';
import { filter, tap, switchMap } from 'rxjs/operators';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { IBaseContentBlockModel } from '@Interfaces/content-block-model.interface';
import { IContentType, ContentTypeList } from '@Shared/content-type';
import { CampaignsContentService } from '../../services/campaigns-content.service';
import { ConfirmationWindowService } from '@Components/confirmation-window/confirmation-window.service';

type ConditionMoveDirection = 'up' | 'down';

@Component({
    selector: 'app-content-blocks-table',
    templateUrl: './content-blocks-table.component.html',
    styleUrls: ['./content-blocks-table.component.scss']
})
export class ContentBlocksTableComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort)      private sort: MatSort;

    @Input() public campaignId: number;

    public readonly ContentTypeList: { [key: number]: IContentType } = ContentTypeList;

    public readonly DisplayedColumns: Array<string> = [
        'name', 'type', 'isActive', 'position', 'edit', 'delete'
    ];

    public dataSource: MatTableDataSource<any> = new MatTableDataSource();

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private contentService: CampaignsContentService,
        private confirmWindowService: ConfirmationWindowService
    ) {}

    ngOnInit () {
        if (!this.campaignId) {
            throw new Error('Component can\'t resolve required @Input parameters!');
        }

        this.update();
    }

    ngAfterViewInit () {
        this.dataSource.paginator   = this.paginator;
        this.dataSource.sort        = this.sort;
    }

    public update (): void {
        this.contentService.getList(this.campaignId)
            .subscribe(data => {
                this.dataSource.data = data;
            });
    }

    public add (): void {
        const additionWindow = this.dialog.open(ContentBlockWindowComponent, {
            width: '95%',
            height: '95%',
            disableClose: true,
            data: {
                mode: 'addition',
                campaignId: this.campaignId
            }
        });

        additionWindow.afterClosed()
            .pipe(
                filter(result => result === true)
            )
            .subscribe(() => {
                this.update();
            });
    }

    public edit (contentBlock: IBaseContentBlockModel): void {
        const editingWindow = this.dialog.open(ContentBlockWindowComponent, {
            width: '95%',
            height: '95%',
            disableClose: true,
            data: {
                mode: 'editing',
                campaignId: this.campaignId,
                contentBlockId: contentBlock.id
            }
        });

        editingWindow.afterClosed()
            .pipe(
                filter(result => result === true)
            )
            .subscribe(() => {
                this.update();
            });
    }

    public changeState (block: IBaseContentBlockModel, state: boolean): void {
        this.confirmWindowService.show('Do you really want to change block state?', 'Change block state')
            .pipe(
                tap(confirmationResult => {
                    if (!confirmationResult) {
                        block.isActive = !block.isActive;
                    }
                }),
                filter(confirmationResult => confirmationResult === true),
                switchMap(() => {
                    return this.contentService.changeState(this.campaignId, block.id, state);
                })
            )
            .subscribe();
    }

    @NeedsConfirmation('Do you really want to delete block?', 'Confirm Deleting')
    public delete (block: IBaseContentBlockModel): void {
        this.contentService.delete(this.campaignId, block.id)
            .subscribe(() => {
                this.update();
            });
    }

    public moveUp (block: IBaseContentBlockModel): void {
        this.contentService.moveUp(this.campaignId, block.id)
            .pipe(
                tap(() => this.moveBlock(block, 'up'))
            )
            .subscribe();
    }

    public moveDown (block: IBaseContentBlockModel): void {
        this.contentService.moveDown(this.campaignId, block.id)
            .pipe(
                tap(() => this.moveBlock(block, 'down'))
            )
            .subscribe();
    }

    private moveBlock (block: IBaseContentBlockModel, direction: ConditionMoveDirection): void {
        const data: IBaseContentBlockModel[] = this.dataSource.data;
        const blockIndex: number = data.findIndex((currBlock) => {
            return block.id === currBlock.id;
        });

        if (blockIndex !== -1) {
            if (
                direction === 'up' && blockIndex === 0
                ||
                direction === 'down' && blockIndex === (data.length - 1)
            ) {
                return;
            }

            const insertIndex: number = (direction === 'up') ? blockIndex - 1 : blockIndex + 1;
            const splicedCondition = data.splice(blockIndex, 1)[0];
            data.splice(insertIndex, 0, splicedCondition);

            this.dataSource.data = data;
        }
    }
}
