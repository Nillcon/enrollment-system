import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IContentBlockModel } from '@Interfaces/content-block-model.interface';
import { FormGroup, FormControl } from '@angular/forms';
import { NotificationService } from '@Services/notification.service';
import { IContentType, getContentTypeListAsArray, ContentTypeEnum, ContentTypeList } from '@Shared/content-type';
import { getJoeLionFaceListAsArray, IJoeLionFace, JoeLionFaceEnum } from '@Layouts/main-layout/components/joe-lion/shared/joe-lion-face';
// tslint:disable-next-line: max-line-length
import { CampaignsContentService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/content-page/services/campaigns-content.service';
import { WindowMode } from '@Types/window-mode.type';
import { SaveMode } from '@Types/save-mode.type';
import { CampaignsVariablesService } from '../../../variables-page/services/campaigns-variables.service';
import { IVariable } from '@Interfaces/variable.interface';
import { VariableAndConstantQueryTypes } from '@Shared/variable-and-constant-query-types';
import { CustomFontList } from '@Shared/custom-fonts';

interface IData {
    mode: WindowMode;
    campaignId: number;
    contentBlockId?: number;
}

@Component({
    selector: 'app-content-block-window',
    templateUrl: './content-block-window.component.html',
    styleUrls: ['./content-block-window.component.scss']
})
export class ContentBlockWindowComponent implements OnInit {
    @ViewChild('BlockBackgroundUploader') private BlockBackgroundUploader: { nativeElement: { click: () => void; }; };

    public readonly ContentType: typeof ContentTypeEnum    = ContentTypeEnum;
    public readonly JoeLionFace: typeof JoeLionFaceEnum    = JoeLionFaceEnum;
    public readonly JoeLionFaceListAsArray: IJoeLionFace[] = getJoeLionFaceListAsArray();
    public readonly ContentTypeListAsArray: IContentType[] = getContentTypeListAsArray();
    public readonly ContentTypeDictionary: { [key: number]: IContentType } = ContentTypeList;

    public readonly fontList: string[] = CustomFontList;

    public blockFormGroup: FormGroup;
    public idFormControl: FormControl;
    public nameFormControl: FormControl;
    public typeFormControl: FormControl;
    public isActiveFormControl: FormControl;
    public leoMessageFormControl: FormControl;
    public leoPhotoFormControl: FormControl;
    public contentFormControl: FormControl;
    public isChatButtonEnabledFormControl: FormControl;
    public chatButtonNameFormControl: FormControl;
    public positionFormControl: FormControl;

    public titleFontFormControl: FormControl;
    public titleColorFormControl: FormControl;
    public titleIconEnabledFormControl: FormControl;
    public blockBackgroundColorFormControl: FormControl;
    public blockBackgroundFormControl: FormControl;

    public tagsFormControl: FormControl;

    public textVariablesList: IVariable[];
    public joeLionVariablesList: IVariable[];

    public isLoading: boolean = false;

    constructor (
        private dialogRef: MatDialogRef<ContentBlockWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IData,
        private contentService: CampaignsContentService,
        private notificationService: NotificationService,
        private variablesService: CampaignsVariablesService
    ) {}

    ngOnInit () {
        if (!this.data) {
            throw new Error('Component can\'t resolve required @Input parameters!');
        }

        this.update();
        this.updateTextVariables();
        this.updateJoeLionVariables();
    }

    public update (): void {
        if (this.data.mode === 'editing') {
            this.contentService.get(this.data.campaignId, this.data.contentBlockId)
                .subscribe((data) => {
                    this.initForm(data);
                });
        } else {
            this.initForm();
        }
    }

    public save (): void {
        if (this.blockFormGroup.valid) {
            const saveMode: SaveMode = (this.data.mode === 'addition') ? 'create' : 'edit';
            this.isLoading = true;

            this.contentService.save(
                this.data.campaignId,
                this.blockFormGroup.value,
                saveMode
            )
                .subscribe(
                    () => {
                        this.isLoading = false;
                        this.close(true);
                    },
                    () => {
                        this.isLoading = false;
                    }
                );
        } else {
            this.notificationService.error('Oops', 'An error occurred while trying to save block.');
        }
    }

    public updateTextVariables (): void {
        this.variablesService.getList(this.data.campaignId, VariableAndConstantQueryTypes.TextContent)
            .subscribe(variables => {
                this.textVariablesList = variables;
            });
    }

    public updateJoeLionVariables (): void {
        this.variablesService.getList(this.data.campaignId, VariableAndConstantQueryTypes.JoeLionMessage)
            .subscribe(variables => {
                this.joeLionVariablesList = variables;
            });
    }

    public close (result: boolean = false): void {
        this.dialogRef.close(result);
    }

    public changeContent (content: any): void {
        this.contentFormControl.setValue(content);
    }

    public changeLeoPhoto (leo: IJoeLionFace): void {
        this.leoPhotoFormControl.setValue(leo.id);
    }

    public selectBg (): void {
        this.BlockBackgroundUploader.nativeElement.click();
    }

    public clearCustomization (): void {
        this.titleFontFormControl.reset();
        this.titleColorFormControl.reset();
        this.titleIconEnabledFormControl.setValue(true);
        this.blockBackgroundColorFormControl.reset();
        this.blockBackgroundFormControl.reset();
    }

    public makeBase64FromSelectedBg (event: any): void {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = () => {
            const base64output: string = (reader.result as string);
            this.blockBackgroundFormControl.setValue(base64output);
        };
    }

    private initForm (blockData: Partial<IContentBlockModel> = {}): void {
        this.idFormControl = new FormControl(blockData.id || 0);
        this.nameFormControl = new FormControl(blockData.name || '');
        this.contentFormControl = new FormControl(blockData.content || '');
        this.typeFormControl = new FormControl(blockData.type || this.ContentType.Text);
        this.leoMessageFormControl = new FormControl(blockData.leoMessage || '');
        this.leoPhotoFormControl = new FormControl(blockData.leoPhoto || JoeLionFaceEnum.Smiling);
        this.isChatButtonEnabledFormControl = new FormControl(blockData.chatButtonEnabled || false);
        this.chatButtonNameFormControl = new FormControl(blockData.chatButtonName || '');
        this.positionFormControl = new FormControl(blockData.position || 0);

        this.titleFontFormControl               = new FormControl(blockData.titleFont || '');
        this.titleColorFormControl              = new FormControl(blockData.titleColor || '');
        // tslint:disable-next-line: max-line-length
        this.titleIconEnabledFormControl        = new FormControl((typeof(blockData.titleIconEnabled) === 'boolean') ? blockData.titleIconEnabled : true);
        this.blockBackgroundColorFormControl    = new FormControl(blockData.backgroundColor || '');
        this.blockBackgroundFormControl         = new FormControl(blockData.backgroundImage || '');
        this.tagsFormControl                    = new FormControl(blockData.tags || '');

        this.blockFormGroup = new FormGroup({
            id: this.idFormControl,
            name: this.nameFormControl,
            content: this.contentFormControl,
            type: this.typeFormControl,
            titleFont: this.titleFontFormControl,
            titleColor: this.titleColorFormControl,
            titleIconEnabled: this.titleIconEnabledFormControl,
            backgroundColor: this.blockBackgroundColorFormControl,
            tags: this.tagsFormControl,
            leoMessage: this.leoMessageFormControl,
            leoPhoto: this.leoPhotoFormControl,
            chatButtonEnabled: this.isChatButtonEnabledFormControl,
            chatButtonName: this.chatButtonNameFormControl,
            position: this.positionFormControl
        });
    }
}
