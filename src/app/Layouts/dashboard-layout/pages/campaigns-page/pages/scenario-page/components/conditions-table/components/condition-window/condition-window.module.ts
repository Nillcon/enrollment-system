import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConditionWindowComponent } from './condition-window.component';
import { SharedModule } from '@App/app-shared.module';
import { DxScrollViewModule } from 'devextreme-angular';
import { MatExpansionModule, MatSlideToggleModule } from '@angular/material';
import { StipulationsComponent } from './expansion-tabs/stipulations/stipulations.component';
import { ThenDoComponent } from './expansion-tabs/then-do/then-do.component';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { SelectActionsComponent } from './components/select-actions/select-actions.component';
import { ChipInputModule } from '@Components/chip-input/chip-input.module';
import { ElseComponent } from './expansion-tabs/else/else.component';
import { SelectConditionsComponent } from './components/select-conditions/select-conditions.component';

@NgModule({
    declarations: [
        ConditionWindowComponent,
        StipulationsComponent,
        ThenDoComponent,
        SelectActionsComponent,
        ElseComponent,
        SelectConditionsComponent
    ],
    imports: [
        CommonModule,
        SharedModule,

        DxScrollViewModule,

        MatExpansionModule,
        MatSlideToggleModule,
        DragDropModule,

        ChipInputModule
    ],
    entryComponents: [
        ConditionWindowComponent,
        SelectActionsComponent
    ],
    exports: [
        ConditionWindowComponent
    ]
})
export class ConditionWindowModule { }
