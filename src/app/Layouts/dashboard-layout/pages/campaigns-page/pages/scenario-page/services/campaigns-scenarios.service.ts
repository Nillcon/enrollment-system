import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";
import { IBaseCondition } from '../../../interfaces/condition.interface';
import { IBaseAction } from '../../../interfaces/action.interface';
import { IConditionVariables } from "../../../interfaces/condition-variables.interface";
import { ConditionResultEnum } from "../../../shared/condition-result-list";
import { IResultAction } from "../../../interfaces/result-action.interface";

@Injectable()
export class CampaignsScenariosService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getConditionsList (campaignId: number): Observable<IBaseCondition[]> {
        return this.httpService.get(`/Campaigns/${campaignId}/Conditions`);
    }

    public getCondition (campaignId: number, conditionId: number): Observable<IBaseCondition> {
        return this.httpService.get<IBaseCondition>(`/Campaigns/${campaignId}/Conditions/${conditionId}`);
    }

    public createCondition(campaignId: number, condition: IBaseCondition): Observable<any> {
        return this.httpService.post<any>(`/Campaigns/${campaignId}/Conditions`, condition);
    }

    public saveCondition(campaignId: number, condition: IBaseCondition): Observable<any> {
        return this.httpService.put<any>(`/Campaigns/${campaignId}/Conditions/${condition.id}`, condition);
    }

    public deleteCondition (campaignId: number, conditionId: number): Observable<any> {
        return this.httpService.delete(`/Campaigns/${campaignId}/Conditions/${conditionId}`);
    }

    public conditionMoveUp (campaignId: number, conditionId: number): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Conditions/${conditionId}/MoveUp`, {});
    }

    public conditionMoveDown (campaignId: number, conditionId: number): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Conditions/${conditionId}/MoveDown`, {});
    }

    public getConditionVariables(compainId: number): Observable<IConditionVariables[]> {
        return this.httpService.get<IConditionVariables[]>(`/Campaigns/${compainId}/ConditionVariables`);
    }

    public getConditionResultActions(campaignId: number, conditionId: number) {
        return this.httpService.get<IResultAction[]>(`/Campaigns/${campaignId}/Conditions/${conditionId}/ResultActions`);
    }

    public saveChangedActionsOrder(campaignId: number, conditionId: number, resultActions: IResultAction[]) {
        return this.httpService.put(`/Campaigns/${campaignId}/Conditions/${conditionId}/ResultActions`, resultActions);
    }


    public getActionsList (campaignId: number): Observable<IBaseAction[]> {
        return this.httpService.get(`/Campaigns/${campaignId}/Actions`);
    }

    public getAction (campaignId: number, actionId: number): Observable<IBaseAction> {
        return this.httpService.get(`/Campaigns/${campaignId}/Actions/${actionId}`);
    }

    public createAction (campaignId: number, action: IBaseAction): Observable<any> {
        return this.httpService.post(`/Campaigns/${campaignId}/Actions`, action);
    }

    public saveAction (campaignId: number, action: IBaseAction): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Actions/${action.id}`, action);
    }

    public deleteAction (campaignId: number, actionId: number): Observable<any> {
        return this.httpService.delete(`/Campaigns/${campaignId}/Actions/${actionId}`);
    }

    public assignActionToCondition(campaignId: number, conditionId: number, actionId: number) {
        return this.httpService.post(`/Campaigns/${campaignId}/Conditions/${conditionId}/Action`, {
            value: actionId
        });
    }

    public deassignActionToCondition(campaignId: number, conditionId: number, resultActionId: number) {
        return this.httpService.delete(`/Campaigns/${campaignId}/Conditions/${conditionId}/Action/${resultActionId}`);
    }

    public activateCondition (campaignId: number, conditionId: number): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Conditions/${conditionId}/ActivateCondition`, {});
    }

    public deactivateCondition (campaignId: number, conditionId: number): Observable<any> {
        return this.httpService.put(`/Campaigns/${campaignId}/Conditions/${conditionId}/DeactivateCondition`, {});
    }
}
