import { Component, OnInit, forwardRef, Input, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
// tslint:disable-next-line: max-line-length
import { CampaignsVariablesService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/variables-page/services/campaigns-variables.service';
import { IVariable } from '@Interfaces/variable.interface';
import { InputWithVariablesComponent } from '@Components/input-with-variables/input-with-variables.component';

@Component({
    selector: 'app-text-box',
    templateUrl: './text-box.component.html',
    styleUrls: ['./text-box.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TextBoxComponent),
            multi: true
        },
        CampaignsVariablesService
    ]
})
export class TextBoxComponent implements OnInit, ControlValueAccessor {
    @ViewChild('TextInput') private TextInput: InputWithVariablesComponent;

    @Input() public campaignId: number;
    @Input() variables: IVariable[];

    @Input() placeholder: string = 'Type the text...';

    public text: string;

    constructor (
    ) {}

    ngOnInit () {
    }

    registerOnChange(fn: Function) {
        this.initTextInputValue();

        this.onChange = fn;
    }

    registerOnTouched () {}

    writeValue(value: string) {
        this.text = value;
        this.onChange(value);
    }

    private initTextInputValue() {
        this.TextInput.value = this.text;
    }

    private onChange: any = () => {};
}
