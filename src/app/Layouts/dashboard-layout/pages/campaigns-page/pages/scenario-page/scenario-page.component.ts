import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-scenario-page',
    templateUrl: './scenario-page.component.html',
    styleUrls: ['./scenario-page.component.scss']
})
export class ScenarioPageComponent implements OnInit {
    @Input() public campaignId: number;

    constructor () {}

    ngOnInit () {}
}
