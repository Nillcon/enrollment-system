import { Component, OnInit, Input } from '@angular/core';
import { IVariable } from '@Interfaces/variable.interface';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-variable-selection',
    templateUrl: './variable-selection.component.html',
    styleUrls: ['./variable-selection.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: VariableSelectionComponent,
            multi: true
        }
    ]
})
export class VariableSelectionComponent implements OnInit, ControlValueAccessor {

    public variableId: number;

    @Input() variablesList: IVariable[];

    constructor() { }

    ngOnInit() {
    }

    registerOnChange(fn: Function) {

        this.onChange = fn;
    }

    registerOnTouched() {}

    writeValue(value: number) {
        this.variableId = value || 0;

        this.onChange(this.variableId);
    }

    public onChange: any = () => {};
}
