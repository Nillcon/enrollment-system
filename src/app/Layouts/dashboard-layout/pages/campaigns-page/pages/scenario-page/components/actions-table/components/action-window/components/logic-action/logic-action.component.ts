import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { FormGroup, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { IVariable } from '@Interfaces/variable.interface';
import { ILogicAction } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/actions-type.interface';

@Component({
    selector: 'app-logic-action',
    templateUrl: './logic-action.component.html',
    styleUrls: ['./logic-action.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => LogicActionComponent),
            multi: true
        }
    ]
})
export class LogicActionComponent implements OnInit, ControlValueAccessor {

    @Input() selectBoxVariables: IVariable[];

    public logicFormGroup: FormGroup;
    private formAction: ILogicAction;

    constructor() { }

    ngOnInit() {
    }

    private initLogicFormGroup() {
        if (this.formAction) {
            this.logicFormGroup = new FormGroup({
                variable: new FormControl(this.formAction.variable || {}),
                value: new FormControl(this.formAction.value || '')
            });
        } else {
            this.logicFormGroup = new FormGroup({
                variable: new FormControl(),
                value: new FormControl()
            });
        }

        this.changeFormObserver();
    }

    private changeFormObserver() {
        this.logicFormGroup.valueChanges
            .subscribe(res => {
                this.writeValue(res);
            });
    }

    registerOnChange(fn: Function) {
        this.initLogicFormGroup();
        this.onChange = fn;
    }

    registerOnTouched() {
    }

    writeValue(value: ILogicAction) {
        this.formAction = value;
        this.onChange(value);
    }

    private onChange: any = () => {};

}
