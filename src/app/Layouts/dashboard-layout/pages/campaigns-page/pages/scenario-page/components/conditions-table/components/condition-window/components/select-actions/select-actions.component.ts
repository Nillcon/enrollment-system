import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IBaseAction } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/action.interface';
import { FormControl } from '@angular/forms';

@Component({
    selector: 'app-select-actions',
    templateUrl: './select-actions.component.html',
    styleUrls: ['./select-actions.component.scss']
})
export class SelectActionsComponent implements OnInit {

    public actionControl: FormControl = new FormControl();

    constructor(
        @Inject(MAT_DIALOG_DATA) public actionsList: IBaseAction[],
        public dialogRef: MatDialogRef<SelectActionsComponent>,
    ) { }

    ngOnInit() {
    }

    public addAction() {
        this.dialogRef.close(this.actionControl.value);
    }

    public closeWindow() {
        this.dialogRef.close();
    }

}
