import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { IBaseAction } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/action.interface';
import { ActionTypeList, ActionTypeEnum } from '@Layouts/dashboard-layout/pages/campaigns-page/shared/action';
// tslint:disable-next-line: max-line-length
import { CampaignsScenariosService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/scenario-page/services/campaigns-scenarios.service';
// tslint:disable-next-line: max-line-length
import { CampaignsVariablesService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/variables-page/services/campaigns-variables.service';
import { IVariable } from '@Interfaces/variable.interface';
import { WindowMode } from '@Types/window-mode.type';
import { map } from 'rxjs/operators';
import { IConditionVariables } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/condition-variables.interface';
import { VariableAndConstantQueryTypes } from '@Shared/variable-and-constant-query-types';

interface IActionWindowData {
    campaignId: number;
    actionId?: number;
}

@Component({
    selector: 'app-action-window',
    templateUrl: './action-window.component.html',
    styleUrls: ['./action-window.component.scss']
})
export class ActionWindowComponent implements OnInit {

    public windowMode: WindowMode;

    public readonly additionModeTitle: string = 'Add action';
    public readonly editionModeTitle: string = 'Edit action';

    public readonly additionModeSaveButton: string = 'Create';
    public readonly editionModeSaveButton: string = 'Save';

    public action: IBaseAction;

    public actionFormGroup: FormGroup;

    public ActionTypeList = ActionTypeList;
    public ActionTypeEnum = ActionTypeEnum;

    public variablesList: IVariable[];
    public selectBoxVariables: IVariable[];
    public markupVariables: IVariable[];

    constructor(
        public dialogRef: MatDialogRef<ActionWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IActionWindowData,

        public scenariosService: CampaignsScenariosService,
        public variablesService: CampaignsVariablesService
    ) { }

    ngOnInit() {
        this.initWindowMode();

        this.updateVariablesList();
        this.updateSelectBoxVariables();

        this.initActionFormGroup();
    }

    private initActionFormGroup() {
        if (this.windowMode === 'editing') {
            this.scenariosService.getAction(this.data.campaignId, this.data.actionId)
                .subscribe(res => {
                    this.action = res;
                    this.actionFormGroup = new FormGroup({
                        name: new FormControl(res.name),
                        type: new FormControl(res.type),
                        text: new FormControl(res.text),
                        data: new FormControl(res.data || {}),
                        skipButton: new FormControl(res.skipButton),
                        callSupportButton: new FormControl(res.callSupportButton),
                        variableId: new FormControl(res.variableId),
                        delay: new FormControl(res.delay),
                        submitButton: new FormControl(res.submitButton || 'Submit'),
                    });
                });
        } else {
            this.actionFormGroup = new FormGroup({
                name: new FormControl(''),
                type: new FormControl(ActionTypeEnum.Text),
                text: new FormControl(''),
                data: new FormControl({}),
                skipButton: new FormControl(null),
                callSupportButton: new FormControl(null),
                variableId: new FormControl(),
                delay: new FormControl(0),
                submitButton: new FormControl('Submit'),
            });
        }
    }

    private initWindowMode() {
        this.windowMode = (this.data.actionId) ? 'editing' : 'addition';
    }

    private updateVariablesList() {
        this.variablesService.getList(this.data.campaignId, VariableAndConstantQueryTypes.ScenariosActionsTextboxes)
            .subscribe(variables => {
                const cloneVariables = JSON.parse(JSON.stringify(variables));

                this.variablesList = variables;
                this.markupVariables = this.getMarkupVariables(cloneVariables);
            });
    }

    private updateSelectBoxVariables() {
        this.variablesService.getList(this.data.campaignId, VariableAndConstantQueryTypes.ScenariosActionsSelectboxes)
            .subscribe(variables => this.selectBoxVariables = variables);
    }

    public saveAction() {
        const data: IBaseAction = this.getDataForSaving();

        if (this.windowMode === 'addition') {
            this.scenariosService.createAction(this.data.campaignId, data)
                .subscribe(res => {
                    this.windowMode = 'editing';
                    this.data.actionId = res;
                });
        } else if (this.windowMode === 'editing') {
            this.scenariosService.saveAction(this.data.campaignId, data)
                .subscribe(() => this.dialogRef.close());
        }
    }

    private getDataForSaving() {
        const data: IBaseAction = {
            name: this.actionFormGroup.controls['name'].value,
            type: this.actionFormGroup.controls['type'].value,
            text: this.actionFormGroup.controls['text'].value,
            data: this.actionFormGroup.controls['data'].value,
            variableId: this.actionFormGroup.controls['variableId'].value || 0,
            skipButton: this.actionFormGroup.controls['skipButton'].value,
            callSupportButton: this.actionFormGroup.controls['callSupportButton'].value,
            delay: this.actionFormGroup.controls['delay'].value || 0,
            submitButton: this.actionFormGroup.controls['submitButton'].value || 'Submit'
        };

        data.id = this.data.actionId;

        return data;
    }

    public changeData (data: any): void {
        this.actionFormGroup.controls['data'].setValue(data);
    }

    public closeActionWindow() {
        this.dialogRef.close();
    }

    private getMarkupVariables(variables_: IConditionVariables[]): IConditionVariables[] {
        const variables = [...variables_];
        variables
            .map(res => {
                res.name = `#${res.name}`;

                return res;
            });

        return variables;
    }
}
