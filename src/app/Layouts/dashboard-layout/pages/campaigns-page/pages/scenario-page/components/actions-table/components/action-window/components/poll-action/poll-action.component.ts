import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormGroup } from '@angular/forms';
import { IPollMessage, IMessageButton } from '@Components/chat/interfaces/message.interface';
import { DxScrollViewComponent } from 'devextreme-angular';
import { IVariable } from '@Interfaces/variable.interface';

@Component({
    selector: 'app-poll-action',
    templateUrl: './poll-action.component.html',
    styleUrls: ['./poll-action.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: PollActionComponent,
            multi: true
        }
    ]
})
export class PollActionComponent implements OnInit, ControlValueAccessor {

    public pollAction: IPollMessage;

    public buttons: IMessageButton[];

    @Input() variablesList: IVariable[];

    @ViewChild(DxScrollViewComponent) buttonsListScroll: DxScrollViewComponent;

    constructor() { }

    ngOnInit() {
    }

    private initPollActionButtons() {
        if (this.pollAction) {
            if (this.pollAction.buttons) {
                this.buttons = this.pollAction.buttons || [];

                this.writeChanges();
            } else {
                this.createDefaultForm();
            }
        } else {
           this.createDefaultForm();
        }
    }

    registerOnChange(fn: Function) {
        this.initPollActionButtons();

        this.onChange = fn;

        this.writeChanges();
    }

    registerOnTouched() {}

    writeValue(value: IPollMessage) {
        this.pollAction = value;

        this.onChange(value);
    }

    private onChange: any = () => {};

    private createDefaultForm() {
        this.buttons = [];

        for (let i = 0; i < 2; i++) {
            this.addButton();
        }
    }

    public addButton() {
        this.buttons.push({
            text: '',
            value: null
        });

        this.writeChanges();

        this.scrollButtonsBlock();
    }

    public deleteButton(index: number) {
        this.buttons.splice(index, 1);

        this.writeChanges();
    }

    public writeChanges() {
        const pollData: IPollMessage = {
            buttons: this.buttons
        };

        this.writeValue(pollData);
    }

    public scrollButtonsBlock() {
    }

    public chipInputOnChange(value: any[], index: number) {
        this.buttons[index].value = value[0];

        this.writeChanges();
    }
}
