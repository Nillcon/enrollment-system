import { Component, OnInit, forwardRef, Input, OnDestroy } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormGroup, FormControl } from '@angular/forms';
import { IFormMessage, IMessageFormField, IMessage } from '@Components/chat/interfaces/message.interface';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { IVariable } from '@Interfaces/variable.interface';

@AutoUnsubscribe()
@Component({
    selector: 'app-form-action',
    templateUrl: './form-action.component.html',
    styleUrls: ['./form-action.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FormActionComponent),
            multi: true
        }
    ]
})
export class FormActionComponent implements OnInit, OnDestroy, ControlValueAccessor {

    @Input() windowMode: 'addition' | 'editing' = 'addition';
    @Input() variablesList: IVariable[];
    @Input() selectBoxVariables: IVariable[];

    public formAction: IFormMessage;
    public fields: IMessageFormField[];

    constructor() { }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    registerOnChange(fn: Function) {
        this.initFormActionFields();
        this.onChange = fn;
    }

    registerOnTouched() {
    }

    writeValue(value: IFormMessage) {
        this.formAction = value;
        this.onChange(value);
    }

    private onChange: any = () => {};

    private initFormActionFields() {
        if (this.formAction) {
            if (this.formAction.fields) {
                this.fields = this.formAction.fields || [];
                this.writeChanges();
            } else {
                this.createDefaultForm();
            }
        } else {
           this.createDefaultForm();
        }
    }

    public deleteField(index: number) {
        this.fields.splice(index, 1);
    }

    private createDefaultForm() {
        this.fields = [];

        this.addField();
    }

    public callOnChange() {
        this.writeValue(this.formAction);
    }

    public writeChanges() {
        const formData: IFormMessage = {
            fields: this.fields
        };

        this.writeValue(formData);
    }

    public addField() {
        this.fields.push({
            placeholder: '',
            variableId: null
        });

        this.writeChanges();
    }
}
