import { Component, Input, OnInit } from "@angular/core";
import { IBaseAction } from "@Layouts/dashboard-layout/pages/campaigns-page/interfaces/action.interface";
import { IMyAction } from "@Layouts/dashboard-layout/pages/campaigns-page/shared/action";
import { MatDialog } from "@angular/material";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { SelectActionsComponent } from "../../components/select-actions/select-actions.component";
import { filter } from "rxjs/operators";
import { ConditionWindowComponent } from "../../condition-window.component";

@Component({
  selector: 'app-else',
  templateUrl: './else.component.html',
  styleUrls: ['./else.component.scss']
})
export class ElseComponent implements OnInit {

    @Input() campaignId: number;
    @Input() actionsList: IBaseAction[];
    @Input() myActions: IMyAction[];

    public myActionsList: IBaseAction[];

    constructor(
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.updateMyActionsList();
    }

    private updateMyActionsList() {
        this.myActionsList = [];

        this.myActions.forEach(value => {
            const myAction: IBaseAction = this.actionsList
                .find(res => res.id === value.actionId);

            if (myAction) {
                this.myActionsList.push(myAction);
            }
        });
    }

    public drop(event: CdkDragDrop<IBaseAction[]>) {
        moveItemInArray(this.myActionsList, event.previousIndex, event.currentIndex);

        moveItemInArray(this.myActions, event.previousIndex, event.currentIndex);
    }

    public addAction() {
        const window = this.dialog.open(SelectActionsComponent, {
            data: this.actionsList
        });

        window.afterClosed()
            .pipe(
                filter(res => !!res)
            )
            .subscribe((res: IBaseAction) => {
                this.myActions.push({
                    actionId: res.id
                });

                this.myActionsList.push(res);
            });
    }

    public addCondition() {
        const dialog = this.dialog.open(ConditionWindowComponent, {
            data: {
                windowMode: 'addition',
                campaignId: this.campaignId,
            },
            width: '725px'
        });
    }

    public deleteAction(index: number) {
        this.myActionsList.splice(index, 1);
        this.myActions.splice(index, 1);
    }

}
