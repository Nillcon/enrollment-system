import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { IBaseCondition } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/condition.interface';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-select-conditions',
  templateUrl: './select-conditions.component.html',
  styleUrls: ['./select-conditions.component.scss']
})
export class SelectConditionsComponent implements OnInit {

    public conditionControl: FormControl = new FormControl();

    constructor(
        @Inject(MAT_DIALOG_DATA) public conditionsList: IBaseCondition[],
        public dialogRef: MatDialogRef<SelectConditionsComponent>,
    ) { }

    ngOnInit() {
    }

    public addCondition() {
        this.dialogRef.close(this.conditionControl.value);
    }

    public closeWindow() {
        this.dialogRef.close();
    }

}
