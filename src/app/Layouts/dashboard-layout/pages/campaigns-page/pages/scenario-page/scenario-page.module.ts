import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScenarioPageComponent } from './scenario-page.component';
import { ActionsTableComponent } from './components/actions-table/actions-table.component';
import { ConditionsTableComponent } from './components/conditions-table/conditions-table.component';
import { ActionWindowModule } from './components/actions-table/components/action-window/action-window.module';
import { SharedModule } from '@App/app-shared.module';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { ConditionWindowModule } from './components/conditions-table/components/condition-window/condition-window.module';

@NgModule({
    declarations: [
        ScenarioPageComponent,

        ActionsTableComponent,
        ConditionsTableComponent,
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule,

        ConditionWindowModule,

        ActionWindowModule
    ],
    entryComponents: [
    ],
    exports: [
        ScenarioPageComponent
    ]
})
export class ScenarioPageModule { }
