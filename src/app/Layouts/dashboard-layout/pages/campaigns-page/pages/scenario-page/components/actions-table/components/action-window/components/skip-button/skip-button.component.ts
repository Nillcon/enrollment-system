import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormGroup, FormControl } from '@angular/forms';
import { IHiddenButton } from '@Components/chat/interfaces/message.interface';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';

@AutoUnsubscribe()
@Component({
    selector: 'app-skip-button',
    templateUrl: './skip-button.component.html',
    styleUrls: ['./skip-button.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: SkipButtonComponent,
            multi: true,
        }
    ]
})
export class SkipButtonComponent implements OnInit, OnDestroy, ControlValueAccessor {

    public skipButtonText: string;

    private readonly defaultSkipButtonText: string = 'Skip';

    constructor() { }

    ngOnInit() {
    }

    ngOnDestroy() {}

    registerOnChange(fn: Function) {
        this.onChange = fn;
    }

    registerOnTouched() {}

    writeValue(value?: string) {
        if (value) {
            this.skipButtonText = value;
        }

        this.onChange(this.skipButtonText);
    }

    public onChange: any = () => {};

}
