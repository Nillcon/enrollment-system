import { Component, OnInit, Input } from '@angular/core';
import { IStipulation } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/stipulation.interface';
import { LogicaOperatorsList, LogicalOperatorsEnum } from '@Layouts/dashboard-layout/pages/campaigns-page/shared/logical-operator';
import { ConnectorsList, ConnectorsEnum } from '@Layouts/dashboard-layout/pages/campaigns-page/shared/connector';
import { IVariable } from '@Interfaces/variable.interface';
import { IConditionVariables } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/condition-variables.interface';

@Component({
  selector: 'app-stipulations',
  templateUrl: './stipulations.component.html',
  styleUrls: ['./stipulations.component.scss']
})
export class StipulationsComponent implements OnInit {

    @Input() stipulations: IStipulation[];
    @Input() variablesList: IVariable[];
    @Input() conditionVariables: IConditionVariables[];

    public LogicalOperatorsList = LogicaOperatorsList;
    public ConnectorsList = ConnectorsList;

    constructor(
    ) { }

    ngOnInit() {
        this.markupVariables();
    }

    public markupVariables() {
        this.variablesList
            .map(res => {
                res.name = `#${res.name}`;

                return res;
            });
    }

    public addStipulations() {
        this.stipulations.push({
            connectorType: ConnectorsEnum.And,
            condition: LogicalOperatorsEnum.Equal,
            variable: null,
            value: null
        });
    }

    public deleteStipulations(index: number) {
        this.stipulations.splice(index, 1);
    }

    public chipInputOnChange(e: string[], index: number) {
        this.stipulations[index].value = e[0];
    }

}
