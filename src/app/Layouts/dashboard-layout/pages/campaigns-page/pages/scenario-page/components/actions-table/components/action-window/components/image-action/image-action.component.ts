import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-image-action',
    templateUrl: './image-action.component.html',
    styleUrls: ['./image-action.component.scss']
})
export class ImageActionComponent implements OnInit {
    @ViewChild('PhotoInput') public PhotoInput: { nativeElement: { click: () => void; }; };
    @Input() public photoSrc: string = '';

    @Output() public OnChange: EventEmitter<Blob> = new EventEmitter();

    public photo: Blob;
    public photoName: string;

    constructor () {}

    ngOnInit () {}

    public selectPhoto (): void {
        this.PhotoInput.nativeElement.click();
    }

    public emitPhoto (e: any): void {
        const image: Blob = e.target.files[0];
        this.OnChange.emit(image);
    }
}
