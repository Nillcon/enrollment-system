import { Component, OnInit, Inject } from '@angular/core';
import { IBaseCondition } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/condition.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { LogicaOperatorsList, LogicalOperatorsEnum } from '@Layouts/dashboard-layout/pages/campaigns-page/shared/logical-operator';
import { ConnectorsEnum, ConnectorsList } from '@Layouts/dashboard-layout/pages/campaigns-page/shared/connector';
import { IVariable } from '@Interfaces/variable.interface';
import { IBaseAction } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/action.interface';
// tslint:disable-next-line: max-line-length
import { CampaignsScenariosService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/scenario-page/services/campaigns-scenarios.service';
// tslint:disable-next-line: max-line-length
import { CampaignsVariablesService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/variables-page/services/campaigns-variables.service';
import { NotificationService } from '@Services/notification.service';
import { WindowMode } from '@Types/window-mode.type';
import { VariableAndConstantQueryTypes } from '@Shared/variable-and-constant-query-types';

interface IConditionWindowData {
    conditionId?: number;
    campaignId: number;
    parentId?: number;
}

@Component({
  selector: 'app-condition-window',
  templateUrl: './condition-window.component.html',
  styleUrls: ['./condition-window.component.scss']
})
export class ConditionWindowComponent implements OnInit {

    public windowMode: WindowMode;

    public LogicalOperatorsList = LogicaOperatorsList;
    public ConnectorsList = ConnectorsList;

    public readonly additionModeTitle: string = 'Add condition';
    public readonly editionModeTitle: string = 'Edit condition';

    public readonly additionModeSaveButton: string = 'Create';
    public readonly editionModeSaveButton: string = 'Save';

    public conditionFormGroup: FormGroup;

    public variablesList: IVariable[];
    public conditionVariables: IVariable[];

    public actionsList: IBaseAction[];
    public condition: IBaseCondition;

    constructor(
        public dialogRef: MatDialogRef<ConditionWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IConditionWindowData,
        private scenariosService: CampaignsScenariosService,
        private campaignsVariablesService: CampaignsVariablesService,
        private notifyService: NotificationService
    ) { }

    ngOnInit() {
        this.initWindowMode();

        this.updateActionsList();
        this.updateVariablesList();

        this.getConditionVariables();

        this.initCondition();
    }

    private initCondition() {
        if (this.windowMode === 'addition') {
            this.condition = {
                conditions: [
                    {
                        condition: LogicalOperatorsEnum.Equal,
                        connectorType: ConnectorsEnum.If,
                        value: null,
                        variable: null
                    }
                ],
                resultActions: []
            };
        } else {
            this.scenariosService.getCondition(this.data.campaignId, this.data.conditionId)
                .subscribe(res => {
                    this.condition = res;
                });
        }
    }

    public addStipulations() {
        this.condition.conditions.push({
            connectorType: ConnectorsEnum.And,
            condition: LogicalOperatorsEnum.Equal,
            variable: null
        });
    }

    public deleteStipulations(index: number) {
        this.condition.conditions.splice(index, 1);
    }

    private updateActionsList() {
        this.scenariosService.getActionsList(this.data.campaignId)
            .subscribe(res => {
                this.actionsList = res;
            });
    }

    private getConditionVariables() {
        this.campaignsVariablesService.getList(this.data.campaignId, VariableAndConstantQueryTypes.ScenariosConditionsVariables)
            .subscribe(res => this.conditionVariables = res);
    }

    private updateVariablesList() {
        this.campaignsVariablesService.getList(this.data.campaignId, VariableAndConstantQueryTypes.ScenariosConditions)
            .subscribe(res => {
                this.variablesList = res;
            });
    }

    public saveCondition() {
        this.condition.id = this.data.conditionId;
        this.condition.parentId = this.data.parentId;

        if (this.windowMode === 'addition') {
            this.scenariosService.createCondition(this.data.campaignId, this.condition)
                .subscribe((res: number) => {
                    this.windowMode = 'editing';
                    this.data.conditionId = res;

                    // TODO: Add updating condition
                    this.initCondition();
                });

        } else if (this.windowMode === 'editing') {
            this.scenariosService.saveCondition(this.data.campaignId, this.condition)
                .subscribe(() => {
                    this.dialogRef.close();
                });

        }
    }

    public closeConditionWindow() {
        this.dialogRef.close();
    }

    public thenDoDisableNotification() {
        this.notifyService.info('Wait', 'First you need create this condition!');
    }

    private initWindowMode() {
        this.windowMode = (this.data.conditionId) ? 'editing' : 'addition';
    }

}
