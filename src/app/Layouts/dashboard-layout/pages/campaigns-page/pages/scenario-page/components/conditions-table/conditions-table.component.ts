import { Component, OnInit, ViewChild, AfterViewInit, Injector, Input } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { CampaignsScenariosService } from '../../services/campaigns-scenarios.service';
import { IBaseCondition } from '../../../../interfaces/condition.interface';
import { tap, filter, switchMap } from 'rxjs/operators';
import { ConditionWindowComponent } from './components/condition-window/condition-window.component';
import { Observable } from 'rxjs';
import { NotificationService } from '@Services/notification.service';

type ConditionMoveDirection = 'up' | 'down';

@Component({
    selector: 'app-conditions-table',
    templateUrl: './conditions-table.component.html',
    styleUrls: ['./conditions-table.component.scss']
})
export class ConditionsTableComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort)      private sort: MatSort;

    @Input() public campaignId: number;

    public readonly DisplayedColumns: Array<string> = [
        'name', 'conditionText', 'position', 'isActive', 'play', 'edit', 'delete'
    ];

    public dataSource: MatTableDataSource<any> = new MatTableDataSource();

    constructor (
        public injector: Injector,
        private scenariosService: CampaignsScenariosService,
        private dialog: MatDialog,
        private notifyService: NotificationService
    ) {}

    ngOnInit () {
        this.update();
    }

    ngAfterViewInit () {
        this.dataSource.paginator   = this.paginator;
        this.dataSource.sort        = this.sort;
    }

    public update (): void {
        this.scenariosService.getConditionsList(this.campaignId)
            .subscribe(data => {
                this.dataSource.data = data;
            });
    }

    public add (): void {
        const dialog = this.dialog.open(ConditionWindowComponent, {
            data: {
                windowMode: 'addition',
                campaignId: this.campaignId,
            },
            width: '725px'
        });


        dialog.afterClosed()
            .subscribe(res => {
                this.update();
            });
    }

    public edit (condition: IBaseCondition): void {
        const dialog = this.dialog.open(ConditionWindowComponent, {
            data: {
                windowMode: 'editing',
                conditionId: condition.id,
                campaignId: this.campaignId,
            },
            width: '725px',
        });

        dialog.afterClosed()
            .subscribe(res => {
                this.update();
            });
    }

    public activateCondition (condition: IBaseCondition): Observable<any> {
        return this.scenariosService.activateCondition(this.campaignId, condition.id);
    }

    public deactivateCondition (condition: IBaseCondition): Observable<any> {
        return this.scenariosService.deactivateCondition(this.campaignId, condition.id);
    }

    @NeedsConfirmation('Do you really want to delete condition?', 'Delete condition')
    public delete (conditionId: number): void {
        this.scenariosService.deleteCondition(this.campaignId, conditionId)
            .subscribe(() => this.update());
    }

    public moveUp (condition: IBaseCondition): void {
        this.scenariosService.conditionMoveUp(this.campaignId, condition.id)
            .pipe(
                tap(() => this.moveCondition(condition, 'up'))
            )
            .subscribe();
    }

    public moveDown (condition: IBaseCondition): void {
        this.scenariosService.conditionMoveDown(this.campaignId, condition.id)
            .pipe(
                tap(() => this.moveCondition(condition, 'down'))
            )
            .subscribe();
    }

    public onPlayScenario (): void {
        this.notifyService.info('Here will be chat', '');
    }

    public onPlayCondition (): void {
        this.notifyService.info('Here will be chat', '');
    }

    public onIsActivateClick (condition: IBaseCondition): void {
        if (condition.isActive) {
            this.deactivateCondition(condition)
                .subscribe();
        } else {
            this.activateCondition(condition)
                .subscribe();
        }
    }

    private moveCondition (condition: IBaseCondition, direction: ConditionMoveDirection): void {
        const data: IBaseCondition[] = this.dataSource.data;
        const conditionIndex: number = data.findIndex((currCondition) => {
            return condition.id === currCondition.id;
        });

        if (conditionIndex !== -1) {
            if (
                direction === 'up' && conditionIndex === 0
                ||
                direction === 'down' && conditionIndex === (data.length - 1)
            ) {
                return;
            }

            const insertIndex: number = (direction === 'up') ? conditionIndex - 1 : conditionIndex + 1;
            const splicedCondition = data.splice(conditionIndex, 1)[0];
            data.splice(insertIndex, 0, splicedCondition);

            this.dataSource.data = data;
        }
    }
}
