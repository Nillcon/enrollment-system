import { Component, OnInit, Input, ViewChild, Injector, AfterViewInit } from '@angular/core';
import { CampaignsScenariosService } from '../../services/campaigns-scenarios.service';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { IActionType, ActionTypeList } from '../../../../shared/action';
import { ActionWindowComponent } from './components/action-window/action-window.component';
import { IBaseAction } from '../../../../interfaces/action.interface';
// tslint:disable-next-line: max-line-length
import { CampaignsVariablesService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/variables-page/services/campaigns-variables.service';
import { IVariable } from '@Interfaces/variable.interface';

@Component({
    selector: 'app-actions-table',
    templateUrl: './actions-table.component.html',
    styleUrls: ['./actions-table.component.scss']
})
export class ActionsTableComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort)      private sort: MatSort;

    @Input() public campaignId: number;

    public readonly ActionTypeList: { [key: number]: IActionType } = ActionTypeList;

    public readonly DisplayedColumns: Array<string> = [
        'name', 'type', 'edit', 'delete'
    ];

    public dataSource: MatTableDataSource<any> = new MatTableDataSource();

    public variablesList: IVariable[];

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private scenariosService: CampaignsScenariosService,
        public variablesService: CampaignsVariablesService
    ) {}

    ngOnInit () {
        this.update();
    }

    ngAfterViewInit () {
        this.dataSource.sort        = this.sort;
        this.dataSource.paginator   = this.paginator;
    }

    public update (): void {
        this.scenariosService.getActionsList(this.campaignId)
            .subscribe(data => {
                this.dataSource.data = data;
            });
    }

    public add (): void {
        const dialog = this.dialog.open(ActionWindowComponent, {
            data: {
                campaignId: this.campaignId
            },
            width: '632px'
        });

        dialog.afterClosed()
            .subscribe(res => {
                this.update();
            });
    }

    public edit (action: IBaseAction): void {
        const dialog = this.dialog.open(ActionWindowComponent, {
            data: {
                campaignId: this.campaignId,
                actionId: action.id
            },
            width: '632px',
        });

        dialog.afterClosed()
            .subscribe(res => {
                this.update();
            });
    }

    @NeedsConfirmation('Do you really want to delete action?', 'Delete action')
    public delete (conditionId: number): void {
        this.scenariosService.deleteAction(this.campaignId, conditionId)
            .subscribe(() => this.update());
    }
}
