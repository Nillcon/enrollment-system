import { Component, OnInit, Input } from '@angular/core';
import { IBaseAction } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/action.interface';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { MatDialog } from '@angular/material';
import { SelectActionsComponent } from '../../components/select-actions/select-actions.component';
import { filter, switchMap } from 'rxjs/operators';
import { ConditionWindowComponent } from '../../condition-window.component';
import { ActionWindowComponent } from '../../../../../actions-table/components/action-window/action-window.component';
// tslint:disable-next-line: max-line-length
import { CampaignsScenariosService } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/scenario-page/services/campaigns-scenarios.service';
import { IResultAction } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/result-action.interface';
import { ResultActionTypeEnum } from '@Layouts/dashboard-layout/pages/campaigns-page/shared/result-action-type';
import { IBaseCondition } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/condition.interface';

@Component({
  selector: 'app-then-do',
  templateUrl: './then-do.component.html',
  styleUrls: ['./then-do.component.scss']
})
export class ThenDoComponent implements OnInit {

    @Input() campaignId: number;
    @Input() conditionId: number;
    @Input() actionsList: IBaseAction[];

    public resultActions: IResultAction[];
    public ResultActionTypeEnum = ResultActionTypeEnum;

    constructor(
        private dialog: MatDialog,
        private scenariosService: CampaignsScenariosService
    ) { }

    ngOnInit() {
        this.updateResultActions();
    }

    private updateResultActions() {
        this.scenariosService.getConditionResultActions(this.campaignId, this.conditionId)
            .subscribe(res => {
                this.resultActions = res;
            });
    }

    public drop(event: CdkDragDrop<IBaseAction[]>) {
        moveItemInArray(this.resultActions, event.previousIndex, event.currentIndex);

        this.saveChangedActionsOrder();
    }

    public addAction() {
        const window = this.dialog.open(SelectActionsComponent, {
            data: this.actionsList
        });

        window.afterClosed()
            .pipe(
                filter(res => !!res),
                switchMap((res: IBaseAction) =>
                    this.scenariosService.assignActionToCondition(this.campaignId, this.conditionId, res.id)
                )
            )
            .subscribe(res => {
                this.updateResultActions();
            });
    }

    public addCondition() {
        const window = this.dialog.open(ConditionWindowComponent, {
            data: {
                campaignId: this.campaignId,
                parentId: this.conditionId
            },
            width: '725px'
        });

        window.afterClosed()
            .subscribe(res =>  this.updateResultActions());
    }

    public openResultAction(action: IResultAction) {
        if (action.type === ResultActionTypeEnum.Action) {
            this.openAction(action.actionId);
        } else {
            this.openCondition(action.actionId);
        }
    }

    public deleteAction(action: IResultAction) {
        this.scenariosService.deassignActionToCondition(this.campaignId, this.conditionId, action.id)
            .subscribe(res => this.updateResultActions());
    }

    private openAction(actionId: number) {
        const dialog = this.dialog.open(ActionWindowComponent, {
            data: {
                campaignId: this.campaignId,
                actionId: actionId
            },
            width: '632px'
        });

        dialog.afterClosed()
            .pipe(
                filter(res => !!res),
                switchMap((res: IBaseAction) => this.scenariosService.saveAction(this.campaignId, res))
            )
            .subscribe(res => {});
    }

    private openCondition(conditionId: number) {
        const window = this.dialog.open(ConditionWindowComponent, {
            data: {
                campaignId: this.campaignId,
                conditionId: conditionId
            },
            width: '725px'
        });

        window.afterClosed()
            .subscribe(res => this.updateResultActions());
    }

    private saveChangedActionsOrder() {
        this.scenariosService.saveChangedActionsOrder(this.campaignId, this.conditionId, this.resultActions)
            .subscribe();
    }

}
