import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionWindowComponent } from './action-window.component';
import { SharedModule } from '@App/app-shared.module';
import { FormActionComponent } from './components/form-action/form-action.component';
import { TextBoxComponent } from './components/text-box/text-box.component';
import { SkipButtonComponent } from './components/skip-button/skip-button.component';
import { PollActionComponent } from './components/poll-action/poll-action.component';
import { DxScrollViewModule } from 'devextreme-angular';
import { VariableSelectionComponent } from './components/variable-selection/variable-selection.component';
import { LogicActionComponent } from './components/logic-action/logic-action.component';
import { DelayMessageComponent } from './components/delay-message/delay-message.component';
import { CallSupportButtonComponent } from './components/call-support-button/call-support-button.component';
import { AppointmentActionComponent } from './components/appointment-action/appointment-action.component';
import { ChipInputModule } from '@Components/chip-input/chip-input.module';
import { ImageActionComponent } from './components/image-action/image-action.component';
import { InputWithVariablesModule } from '@Components/input-with-variables/input-with-variables.module';
import { CheckActionComponent } from './components/check-action/check-action.component';

@NgModule({
    declarations: [
        ActionWindowComponent,
        TextBoxComponent,
        FormActionComponent,
        SkipButtonComponent,
        PollActionComponent,
        VariableSelectionComponent,
        LogicActionComponent,
        DelayMessageComponent,
        CallSupportButtonComponent,
        AppointmentActionComponent,
        ImageActionComponent,
        CheckActionComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        ChipInputModule,

        DxScrollViewModule,
        InputWithVariablesModule
    ],
    entryComponents: [
        ActionWindowComponent
    ],
    exports: [
        ActionWindowComponent
    ]
})
export class ActionWindowModule { }
