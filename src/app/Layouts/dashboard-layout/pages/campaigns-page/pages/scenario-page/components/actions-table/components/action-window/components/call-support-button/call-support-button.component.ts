import { Component, OnInit, OnDestroy } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormGroup, FormControl, ControlValueAccessor } from '@angular/forms';
import { IHiddenButton } from '@Components/chat/interfaces/message.interface';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
    selector: 'app-call-support-button',
    templateUrl: './call-support-button.component.html',
    styleUrls: ['./call-support-button.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: CallSupportButtonComponent,
            multi: true,
        }
    ]
})
export class CallSupportButtonComponent implements OnInit, OnDestroy, ControlValueAccessor {

    public callSupportButtonGroup: FormGroup;
    public callSupportButtonText: string;

    private readonly defaultCallSupportText: string = 'Call support';

    private callSupportButtonGroupSubscription: Subscription;

    constructor() { }

    ngOnInit() {
    }

    ngOnDestroy() {}

    registerOnChange(fn: Function) {
        this.onChange = fn;
    }

    registerOnTouched() {}

    writeValue(value?: string) {
        if (value) {
            this.callSupportButtonText = value;
        }

        this.onChange(this.callSupportButtonText);
    }

    public onChange: any = () => {};

}
