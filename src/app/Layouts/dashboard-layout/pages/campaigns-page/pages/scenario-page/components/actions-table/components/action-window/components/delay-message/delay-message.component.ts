import { Component, OnInit, forwardRef, OnDestroy } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormGroup, ControlValueAccessor, FormControl } from '@angular/forms';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';

@AutoUnsubscribe()
@Component({
    selector: 'app-delay-message',
    templateUrl: './delay-message.component.html',
    styleUrls: ['./delay-message.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DelayMessageComponent),
            multi: true
        }
    ]
})
export class DelayMessageComponent implements OnInit, OnDestroy, ControlValueAccessor {

    public delayMessageFormGroup: FormGroup;
    private delay: number;

    private delayMessageSubscription: Subscription;

    constructor() { }

    ngOnInit() {
    }

    ngOnDestroy() {}

    private initDelayMessageFormGroup() {
        this.delayMessageFormGroup = new FormGroup({
            delay: new FormControl(this.delay)
        });

        this.delayMessageFormObserver();
    }

    private delayMessageFormObserver() {
        this.delayMessageFormGroup.valueChanges
            .subscribe((res) => {
                this.writeValue(res.delay);
            });
    }

    registerOnChange(fn: Function) {
        this.initDelayMessageFormGroup();
        this.onChange = fn;
    }

    registerOnTouched() {
    }

    writeValue(value: number) {
        this.delay = value || 0;
        this.onChange(value);
    }

    private onChange: any = () => {};

}
