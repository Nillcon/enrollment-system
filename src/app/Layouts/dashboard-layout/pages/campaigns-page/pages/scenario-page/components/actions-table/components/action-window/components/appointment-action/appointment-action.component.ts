import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormControl, FormGroup } from '@angular/forms';
import { IVariable } from '@Interfaces/variable.interface';
import { IAppointmentAction } from '@Layouts/dashboard-layout/pages/campaigns-page/interfaces/actions-type.interface';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
    selector: 'app-appointment-action',
    templateUrl: './appointment-action.component.html',
    styleUrls: ['./appointment-action.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: AppointmentActionComponent,
            multi: true,
        }
    ]
})
export class AppointmentActionComponent implements OnInit, OnDestroy, ControlValueAccessor {

    @Input() appointmentAction: IAppointmentAction;
    @Input() variablesList: IVariable[];

    public appointmentActionForm: FormGroup;

    private appointmentActionSubscription: Subscription;

    constructor() { }

    ngOnInit() {
    }
    ngOnDestroy() {}

    registerOnChange(fn: Function) {
        this.initAppointmentActionForm();
        this.onChange = fn;
    }

    registerOnTouched() {}

    writeValue(value?: IAppointmentAction) {
        this.appointmentAction = value;

        this.onChange(this.appointmentAction);
    }

    public onChange: any = () => {};

    public initAppointmentAction() {

    }

    private initAppointmentActionForm() {
        this.appointmentActionForm = new FormGroup({
            selectDateText: new FormControl(this.appointmentAction.selectDateText || ''),
            selectTimeText: new FormControl(this.appointmentAction.selectTimeText || ''),
            selectGreeterText: new FormControl(this.appointmentAction.selectGreeterText || ''),
        })

        this.textInputOnChangeObserver();
    }

    public textInputOnChangeObserver() {
        this.appointmentActionSubscription = this.appointmentActionForm.valueChanges
            .subscribe(res => {
                this.writeValue(res);
            });
    }

}
