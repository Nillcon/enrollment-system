import { Component, OnInit, Injector } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CampaignWindowComponent } from './components/campaign-window/campaign-window.component';
import { ICampaign } from './interfaces/campaign.interface';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { CampaignsService } from './services/campaigns.service';
import { filter, switchMap, tap } from 'rxjs/operators';
import { Roles } from '@Shared/roles';

@Component({
    selector: 'app-campaigns-page',
    templateUrl: './campaigns-page.component.html',
    styleUrls: ['./campaigns-page.component.scss']
})
export class CampaignsPageComponent implements OnInit {
    public campaigns: ICampaign[];

    public readonly Roles: typeof Roles = Roles;

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private campaignsService: CampaignsService
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.campaignsService.getList()
            .subscribe(data => {
                this.campaigns = data;
            });
    }

    public openAdditionWindow (): void {
        const additionWindow = this.dialog.open(CampaignWindowComponent, {
            width: '450px',
            data: {
                mode: 'addition'
            }
        });

        additionWindow.afterClosed()
            .pipe(
                filter(result => result === true)
            )
            .subscribe(() => {
                this.update();
            });
    }

    public openEditingWindow (campaign: ICampaign): void {
        this.campaignsService.get(campaign.id)
            .pipe(
                switchMap((campaignData) => {
                    return this.dialog.open(CampaignWindowComponent, {
                        width: '450px',
                        data: {
                            mode: 'editing',
                            campaign: campaignData
                        }
                    }).afterClosed();
                }),
                filter(result => result === true),
                tap(() => this.update())
            )
            .subscribe();
    }

    @NeedsConfirmation(
        'Do you really want to delete this campaign? <b style="color: red;">This will lead to irreparable consequences.</b>',
        'Confirm deleting'
    )
    public delete (campaign: ICampaign): void {
        this.campaignsService.delete(campaign.id)
            .subscribe(() => {
                this.update();
            });
    }
}
