import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CampaignsPageComponent } from './campaigns-page.component';
import { CampaignsPageRoutingModule } from './campaigns-page.routing';
import { SharedModule } from '@App/app-shared.module';
import { MatExpansionModule } from '@angular/material';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { CampaignWindowModule } from './components/campaign-window/campaign-window.module';
import { CampaignsService } from './services/campaigns.service';
import { CampaignsVariablesService } from './pages/variables-page/services/campaigns-variables.service';
import { CampaignsContentService } from './pages/content-page/services/campaigns-content.service';
import { CampaignsScenariosService } from './pages/scenario-page/services/campaigns-scenarios.service';
import { UserVariablesTableModule } from '@Components/user-variables-table/user-variables-table.module';
import { VariablesPageModule } from './pages/variables-page/variables-page.module';
import { ContentBlocksPageModule } from './pages/content-page/content-blocks-page.module';
import { ScenarioPageModule } from './pages/scenario-page/scenario-page.module';
import { DefaultGreetersPageModule } from './pages/default-greeters-page/default-greeters-page.module';
import { ManagersPageModule } from './pages/managers-page/managers-page.module';
import { HiddenStatusesPageModule } from './pages/hidden-statuses-page/hidden-statuses-page.module';
import { MailingPageModule } from './pages/mailing-page/mailing-page.module';
import { StylingPageModule } from './pages/styling-page/styling-page.module';

@NgModule({
    declarations: [
        CampaignsPageComponent,
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule,

        CampaignsPageRoutingModule,

        MatExpansionModule,

        CampaignWindowModule,
        UserVariablesTableModule,

        VariablesPageModule,
        ContentBlocksPageModule,
        ScenarioPageModule,
        DefaultGreetersPageModule,
        ManagersPageModule,
        HiddenStatusesPageModule,
        MailingPageModule,
        StylingPageModule,
    ],
    providers: [
        CampaignsService,
        CampaignsVariablesService,
        CampaignsContentService,
        CampaignsScenariosService
    ]
})
export class CampaignsPageModule { }
