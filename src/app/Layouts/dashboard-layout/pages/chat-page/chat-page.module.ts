import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatPageComponent } from './chat-page.component';
import { ChatPageRoutingModule } from './chat-page.routing';
import { ChatModule } from '@Components/chat/chat.module';

@NgModule({
    declarations: [
        ChatPageComponent
    ],
    imports: [
        CommonModule,

        ChatModule,

        ChatPageRoutingModule
    ]
})
export class ChatPageModule { }
