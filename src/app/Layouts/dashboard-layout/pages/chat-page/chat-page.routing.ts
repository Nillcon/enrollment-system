import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ChatPageComponent } from './chat-page.component';

export const routes: Routes = [
    {
        path: '',
        component: ChatPageComponent
    },
    {
        path: ':id',
        component: ChatPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ChatPageRoutingModule {}
