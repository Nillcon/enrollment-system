import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { InvitedUserService } from '@Services/invited-user.service';
import { FormGroup, FormControl } from '@angular/forms';
import { NotificationService } from '@Services/notification.service';

@Component({
    selector: 'app-users-addition-window',
    templateUrl: './users-addition-window.component.html',
    styleUrls: ['./users-addition-window.component.scss']
})
export class UsersAdditionWindowComponent implements OnInit {
    public invitedUserFormGroup: FormGroup;
    public nameFormControl: FormControl = new FormControl('');

    constructor (
        private dialogRef: MatDialogRef<UsersAdditionWindowComponent>,
        private invitedUserService: InvitedUserService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        this.initFormGroup();
    }

    public save (): void {
        if (this.invitedUserFormGroup.valid) {
            this.invitedUserService.create(this.invitedUserFormGroup.value)
                .subscribe(() => this.close());
        } else {
            this.notificationService.error('Oops', 'Please check if all the data is entered correctly.');
        }
    }

    public close (): void {
        this.dialogRef.close();
    }

    private initFormGroup (): void {
        this.invitedUserFormGroup = new FormGroup({
            name: this.nameFormControl
        });
    }
}
