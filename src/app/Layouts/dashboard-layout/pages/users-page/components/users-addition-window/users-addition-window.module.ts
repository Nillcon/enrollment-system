import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersAdditionWindowComponent } from './users-addition-window.component';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        UsersAdditionWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    exports: [
        UsersAdditionWindowComponent
    ],
    entryComponents: [
        UsersAdditionWindowComponent
    ]
})
export class UsersAdditionWindowModule { }
