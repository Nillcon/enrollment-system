import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoPageComponent } from './info-page.component';
import { UserVariablesTableModule } from '@Components/user-variables-table/user-variables-table.module';

@NgModule({
    declarations: [
        InfoPageComponent
    ],
    imports: [
        CommonModule,

        UserVariablesTableModule
    ],
    exports: [
        InfoPageComponent
    ]
})
export class InfoPageModule { }
