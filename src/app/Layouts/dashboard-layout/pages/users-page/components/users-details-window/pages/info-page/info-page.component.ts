import { Component, OnInit, Input } from '@angular/core';
import { IInvitedUser } from '@Interfaces/users';

@Component({
    selector: 'app-info-page',
    templateUrl: './info-page.component.html',
    styleUrls: ['./info-page.component.scss']
})
export class InfoPageComponent implements OnInit {
    @Input() public openedInvitedUser: IInvitedUser;

    constructor () {}

    ngOnInit () {}
}
