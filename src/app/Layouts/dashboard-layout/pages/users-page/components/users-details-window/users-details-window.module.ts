import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersDetailsWindowComponent } from './users-details-window.component';
import { SharedModule } from '@App/app-shared.module';
import { InfoPageModule } from './pages/info-page/info-page.module';
import { LogsPageModule } from './pages/logs-page/logs-page.module';
@NgModule({
    declarations: [
        UsersDetailsWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        InfoPageModule,
        LogsPageModule
    ],
    exports: [
        UsersDetailsWindowComponent
    ],
    entryComponents: [
        UsersDetailsWindowComponent
    ]
})
export class UsersDetailsWindowModule { }
