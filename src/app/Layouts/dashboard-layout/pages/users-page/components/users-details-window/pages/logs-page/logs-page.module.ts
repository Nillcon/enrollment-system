import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsPageComponent } from './logs-page.component';
import { LogsService } from '../../../../../../components/global/logs/services/logs.service';
import { LogsSharedModule } from '@Layouts/dashboard-layout/components/global/logs/logs-shared.module';

@NgModule({
    declarations: [
        LogsPageComponent
    ],
    imports: [
        CommonModule,

        LogsSharedModule
    ],
    exports: [
        LogsPageComponent
    ],
    providers: [
        LogsService
    ]
})
export class LogsPageModule { }
