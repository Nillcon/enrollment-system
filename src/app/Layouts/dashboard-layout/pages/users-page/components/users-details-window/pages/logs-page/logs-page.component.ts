import { Component, OnInit, Input } from '@angular/core';
import { IInvitedUser } from '@Interfaces/users';

@Component({
    selector: 'app-logs-page',
    templateUrl: './logs-page.component.html',
    styleUrls: ['./logs-page.component.scss']
})
export class LogsPageComponent implements OnInit {
    @Input() public openedInvitedUser: IInvitedUser;

    constructor () {}

    ngOnInit () {}
}
