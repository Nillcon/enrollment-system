import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IInvitedUser } from '@Interfaces/users';
import { InvitedUserService } from '@Services/invited-user.service';

@Component({
    selector: 'app-users-details-window',
    templateUrl: './users-details-window.component.html',
    styleUrls: ['./users-details-window.component.scss']
})
export class UsersDetailsWindowComponent implements OnInit {
    public readonly withoutNameText: string = '<No Name>';

    constructor (
        private dialogRef: MatDialogRef<UsersDetailsWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public openedInvitedUser: IInvitedUser,
        private invitedUserService: InvitedUserService
    ) {}

    ngOnInit () {
        if (!this.openedInvitedUser) {
            throw new Error('StaffDetailsWindow component can\'t get required @Input parameter');
        }
    }
}
