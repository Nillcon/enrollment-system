import { Component, OnInit, Injector, Output, EventEmitter } from '@angular/core';
import { InvitedUserService } from '@Services/invited-user.service';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { IInvitedUser } from '@Interfaces/users';
import { MatDialog } from '@angular/material';
import { UsersDetailsWindowComponent } from '../users-details-window/users-details-window.component';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-users-table',
    templateUrl: './users-table.component.html',
    styleUrls: ['./users-table.component.scss']
})
export class UsersTableComponent implements OnInit {

    @Output() public OnSelection: EventEmitter<IInvitedUser[]> = new EventEmitter();

    public readonly withoutNameText: string = '<No Name>';

    public users: IInvitedUser[];

    public selectedUsers: IInvitedUser[] = [];

    constructor (
        public injector: Injector,
        private invitedUserService: InvitedUserService,
        private dialog: MatDialog
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.invitedUserService.getList()
            .subscribe(invitedUsers => {
                this.users = invitedUsers;
            });
    }

    public onSelection (e: any): void {
        this.selectedUsers = e.selectedRowsData;
        this.OnSelection.emit(this.selectedUsers);
    }

    public edit (invitedUser: IInvitedUser): void {
        this.dialog.open(UsersDetailsWindowComponent, {
            width: '90%',
            height: '500px',
            data: invitedUser
        })
        .afterClosed()
            .pipe(
                tap(() => this.update())
            )
            .subscribe();
    }

    @NeedsConfirmation('Please confirm deleting selected visitors', 'Delete selected visitors')
    public groupDeleting (): void {
        const selectedUserIds: number[] = this.selectedUsers.map(user => {
            return user.id;
        });

        this.invitedUserService.groupDelete(selectedUserIds)
            .pipe(
                tap(() => {
                    this.selectedUsers = [];
                    this.update();
                })
            )
            .subscribe();
    }

    @NeedsConfirmation('Please confirm visitor deleting', 'Delete visitor')
    public delete (invitedUser: IInvitedUser): void {
        this.invitedUserService.delete(invitedUser.id)
            .pipe(
                tap(() => this.update())
            )
            .subscribe();
    }
}
