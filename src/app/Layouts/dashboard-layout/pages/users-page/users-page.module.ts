import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersPageComponent } from './users-page.component';
import { UsersPageRoutingModule } from './users-page.routing';
import { UsersTableComponent } from './components/users-table/users-table.component';
import { SharedModule } from '@App/app-shared.module';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { UsersAdditionWindowModule } from './components/users-addition-window/users-addition-window.module';
import { UsersDetailsWindowModule } from './components/users-details-window/users-details-window.module';

@NgModule({
    declarations: [
        UsersPageComponent,
        UsersTableComponent
    ],
    imports: [
        CommonModule,

        UsersPageRoutingModule,

        SharedModule,
        DashboardLayoutSharedModule,

        UsersAdditionWindowModule,
        UsersDetailsWindowModule
    ]
})
export class UsersPageModule { }
