import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersAdditionWindowComponent } from './components/users-addition-window/users-addition-window.component';
import { MatDialog } from '@angular/material';
import { UsersTableComponent } from './components/users-table/users-table.component';

@Component({
    selector: 'app-users-page',
    templateUrl: './users-page.component.html',
    styleUrls: ['./users-page.component.scss']
})
export class UsersPageComponent implements OnInit {
    @ViewChild('UsersTable') private usersTable: UsersTableComponent;

    constructor (
        private dialog: MatDialog
    ) {}

    ngOnInit () {}

    public openAdditionWindow (): void {
        const additionWindow = this.dialog.open(UsersAdditionWindowComponent, {
            width: '450px'
        });

        additionWindow.afterClosed()
            .subscribe(() => {
                this.usersTable.update();
            });
    }
}
