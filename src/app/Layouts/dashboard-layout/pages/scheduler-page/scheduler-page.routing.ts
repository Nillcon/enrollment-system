import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SchedulerPageComponent } from './scheduler-page.component';

export const routes: Routes = [
    {
        path: '',
        component: SchedulerPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SchedulerPageRoutingModule {}
