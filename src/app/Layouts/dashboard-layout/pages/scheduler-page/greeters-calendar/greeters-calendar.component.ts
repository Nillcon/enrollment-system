import { Component, OnInit } from '@angular/core';
import { IGreeterCalendarInfo } from '@Components/greeter-calendar/interface/info.interface';
import { SchedulerService } from '../services/scheduler.service';
import { UserDetailsWindowService } from '@Components/user-details-window/services/user-details-window.service';
import { IStaffUser } from '@Interfaces/users';

@Component({
    selector: 'app-greeters-calendar',
    templateUrl: './greeters-calendar.component.html',
    styleUrls: ['./greeters-calendar.component.scss']
})
export class GreetersCalendarComponent implements OnInit {

    public calendarInfo: IGreeterCalendarInfo;

    public currentDate: Date = new Date();

    constructor (
        private schedulerService: SchedulerService,
        private userDetailsWindowService: UserDetailsWindowService
    ) {}

    public ngOnInit () {
        this.updateCalendarInfo(this.currentDate);
    }

    public updateCalendarInfo (date: Date) {
        this.schedulerService.getCalendarInfo(date)
            .subscribe((calendarInfo) => {
                this.calendarInfo = calendarInfo;
            });
    }

    public openUserDetailsWindow (greeter: IStaffUser): void {
        this.userDetailsWindowService.open(greeter, { isMeetButtonVisible: false });
    }

}
