import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";
import { IGreeterCalendarInfo } from "@Components/greeter-calendar/interface/info.interface";
import { DateMethod } from "@Methods/date.method";

@Injectable()
export class SchedulerService {

    constructor (
        private httpService: HttpRequestService
    ) {}

    public getCalendarInfo (dateFrom: Date): Observable<IGreeterCalendarInfo> {
        const dateFromAsStr: string = DateMethod.getLocalJsonTimeString(dateFrom);

        return this.httpService.get<IGreeterCalendarInfo>(`/Appointments/Calendar?from=${dateFromAsStr}`);
    }

}
