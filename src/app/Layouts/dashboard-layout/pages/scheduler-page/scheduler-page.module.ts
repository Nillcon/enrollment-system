import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchedulerPageComponent } from './scheduler-page.component';
import { SchedulerPageRoutingModule } from './scheduler-page.routing';
import { GreeterCalendarModule } from '@Components/greeter-calendar/greeter-calendar.module';
import { GreetersCalendarComponent } from './greeters-calendar/greeters-calendar.component';
import { SchedulerService } from './services/scheduler.service';

@NgModule({
    declarations: [
        SchedulerPageComponent,
        GreetersCalendarComponent
    ],
    imports: [
        CommonModule,

        SchedulerPageRoutingModule,

        GreeterCalendarModule
    ],
    providers: [
        SchedulerService
    ]
})
export class SchedulerPageModule {}
