import { Component, OnInit, ViewChild, AfterViewInit, Injector, Output, EventEmitter, Input } from '@angular/core';
import { StaffService } from '@Services/staff.service';
import { IStaffUser } from '@Interfaces/users/staff.interface';
import { IRole } from '@Interfaces/role.interface';
import { RolesList, Roles } from '@Shared/roles';
import { MatPaginator, MatTableDataSource, MatSort, MatDialog } from '@angular/material';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { StaffDetailsWindowComponent } from '../staff-details-window/staff-details-window.component';
import { UserDetailsWindowService } from '@Components/user-details-window/services/user-details-window.service';
import { SelectionModel } from '@angular/cdk/collections';
import { ILog } from '@Layouts/dashboard-layout/components/global/logs/interfaces/log.interface';

@Component({
    selector: 'app-staff-table',
    templateUrl: './staff-table.component.html',
    styleUrls: ['./staff-table.component.scss']
})
export class StaffTableComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort)      private sort: MatSort;

    @Input() public isSelectionEnabled: boolean = false;
    @Input() public isActionColumnsVisible: boolean = false;
    @Input() public selectedGreeterIds: number[] = [];
    @Input() public filterByRoles: Roles[] = [];

    @Output() public OnSelection: EventEmitter<IStaffUser[]> = new EventEmitter();

    public readonly RolesList: { [key: number]: IRole } = RolesList;

    public DisplayedColumns: string[] = [];

    public dataSource: MatTableDataSource<IStaffUser> = new MatTableDataSource();
    public selection = new SelectionModel<IStaffUser>(true, []);

    public isLoading: boolean = false;

    constructor (
        public injector: Injector,
        private staffService: StaffService,
        private dialog: MatDialog,
        private userDetailsWindowService: UserDetailsWindowService
    ) {}

    ngOnInit () {
        this.initColumns();
        this.update();
    }

    ngAfterViewInit () {
        this.dataSource.paginator   = this.paginator;
        this.dataSource.sort        = this.sort;
    }

    public update () {
        this.isLoading = true;

        this.staffService.getList(this.filterByRoles)
            .subscribe(greeters => {
                this.dataSource.data = greeters;

                if (this.isSelectionEnabled && this.selectedGreeterIds.length) {
                    const greetersToSelect: IStaffUser[] = [];

                    this.selectedGreeterIds.forEach(id => {
                        const findedGreeter: IStaffUser  = greeters.find((currentGreeter) => {
                            return currentGreeter.id === id;
                        });

                        if (findedGreeter) {
                            greetersToSelect.push(findedGreeter);
                        }
                    });

                    this.selection.select(...greetersToSelect);
                }

                this.isLoading = false;
            });
    }

    public search (phrase: string): void {
        this.dataSource.filter = phrase.trim().toLowerCase();
    }

    public openUserInfoWindow (staffUser: IStaffUser): void {
        this.userDetailsWindowService.open(staffUser, {
            isMeetButtonVisible: false
        });
    }

    public openDetailsWindow (user: IStaffUser): void {
        const window = this.dialog.open(StaffDetailsWindowComponent, {
            width: '95%',
            height: '95%',
            data: user
        });

        window.afterClosed()
            .subscribe(() => {
                this.update();
            });
    }

    @NeedsConfirmation('Do you really want to delete user?', 'Delete user')
    public deleteStaffUser (user: IStaffUser) {
        this.staffService.delete(user.id)
            .subscribe(() => {
                this.update();
            });
    }

    public isAllSelected (): boolean {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }

    public masterToggle (): void {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));

        this.OnSelection.emit(this.selection.selected);
    }

    public toggle (row: IStaffUser): void {
        this.selection.toggle(row);
        this.OnSelection.emit(this.selection.selected);
    }

    private initColumns (): void {
        const columns: string[]        = [];
        const defaultColumns: string[] = ['photo', 'name', 'login', 'role'];

        if (this.isSelectionEnabled) {
            columns.push('select');
        }

        columns.push(...defaultColumns);

        if (this.isActionColumnsVisible) {
            columns.push('edit');
            columns.push('delete');
        }

        this.DisplayedColumns = columns;
    }
}
