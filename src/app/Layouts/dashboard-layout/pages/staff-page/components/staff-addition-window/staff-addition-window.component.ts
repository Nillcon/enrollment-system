import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatDialogRef, MatRadioGroup } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StaffService } from '@Services/staff.service';
import { IRole } from '@Interfaces/role.interface';
import { GetRolesListAvailableForUserCreating, Roles } from '@Shared/roles';
import { NotificationService } from '@Services/notification.service';
import { StaffAdditionTypeEnum } from './shared/staff-addition-type.enum';
import { BehaviorSubject, Subscription, throwError } from 'rxjs';
import { IStaffUser } from '@Interfaces/users';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { switchMap, filter } from 'rxjs/operators';

@AutoUnsubscribe()
@Component({
    selector: 'app-staff-addition-window',
    templateUrl: './staff-addition-window.component.html',
    styleUrls: ['./staff-addition-window.component.scss']
})
export class StaffAdditionWindowComponent implements OnInit, OnDestroy {
    public readonly Roles: typeof Roles = Roles;
    public readonly AdditionType: typeof StaffAdditionTypeEnum = StaffAdditionTypeEnum;

    public rolesList: IRole[] = GetRolesListAvailableForUserCreating();

    public isPasswordHidden: boolean = true;

    public staffUserFormGroup: FormGroup;
    public activeDirectoryUserFormGroup: FormGroup;

    public activeDirectorySearchFilter: FormControl = new FormControl('');
    public activeDirectoryFilteredUsers: BehaviorSubject<IStaffUser[]> = new BehaviorSubject([]);

    @ViewChild(MatRadioGroup)
    private additionTypeBlock: MatRadioGroup;

    private activeDirectoryUsersFilteringSubscription: Subscription;

    constructor (
        public dialogRef: MatDialogRef<StaffAdditionWindowComponent>,
        private notificationService: NotificationService,
        private staffService: StaffService
    ) {}

    public ngOnInit (): void {
        this.initStaffUserFormGroup();
        this.initActiveDirectoryUserFormGroup();

        this.activeDirectoryUserFilterObserver();
    }

    public ngOnDestroy (): void {}

    public onSaveButtonClick (): void {
        if (this.additionTypeBlock.value === StaffAdditionTypeEnum.NewUser) {
            this.createStaffUser();
        } else {
            this.createUserFromActiveDirectory();
        }
    }

    public cancel (): void {
        this.dialogRef.close();
    }

    private createStaffUser (): void {
        if (this.staffUserFormGroup.valid) {
            this.staffService.create(this.staffUserFormGroup.value)
                .subscribe(() => {
                    this.dialogRef.close();
                });
        } else {
            this.notificationService.error('Oops', 'Please check if all the data is entered correctly.');
        }
    }

    private createUserFromActiveDirectory (): void {
        if (this.activeDirectoryUserFormGroup.valid) {
            this.staffService.create(this.activeDirectoryUserFormGroup.value)
                .subscribe(() => {
                    this.dialogRef.close();
                });
        } else {
            this.notificationService.error('Oops', 'Please check if all the data is entered correctly.');
        }
    }

    private initStaffUserFormGroup (): void {
        this.staffUserFormGroup = new FormGroup({
            name: new FormControl('', [Validators.required]),
            login: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
            role: new FormControl(Roles.Support, [Validators.required]),
            capacity: new FormControl(0, [Validators.required]),
            isADUser: new FormControl(false)
        });
    }

    private initActiveDirectoryUserFormGroup (): void {
        this.activeDirectoryUserFormGroup = new FormGroup({
            login: new FormControl(''),
            role: new FormControl(Roles.Support, [Validators.required]),
            capacity: new FormControl(0, [Validators.required]),
            isADUser: new FormControl(true)
        });
    }

    private activeDirectoryUserFilterObserver (): void {
        this.activeDirectoryUsersFilteringSubscription = this.activeDirectorySearchFilter.valueChanges
            .pipe(
                filter((searchString) => !!searchString),
                switchMap((searchString: string) => this.staffService.search(searchString))
            )
            .subscribe((activeDirectoryUsers) => {
                this.activeDirectoryFilteredUsers.next(activeDirectoryUsers);
            });
    }

}
