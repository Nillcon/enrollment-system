import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaffAdditionWindowComponent } from './staff-addition-window.component';
import { SharedModule } from '@App/app-shared.module';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';

@NgModule({
    declarations: [
        StaffAdditionWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule
    ],
    exports: [
        StaffAdditionWindowComponent
    ],
    entryComponents: [
        StaffAdditionWindowComponent
    ]
})
export class StaffAdditionWindowModule { }
