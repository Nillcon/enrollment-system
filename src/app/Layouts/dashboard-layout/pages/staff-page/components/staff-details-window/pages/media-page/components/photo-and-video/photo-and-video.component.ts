import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NotificationService } from '@Services/notification.service';
import { StaffService } from '@Services/staff.service';
import { IStaffUser } from '@Interfaces/users';
import { tap, catchError } from 'rxjs/operators';
import { MatDialog, MatBottomSheet } from '@angular/material';
import { VideoPathWindowComponent } from './components/video-path-window/video-path-window.component';
import { IMediaContent } from '@Components/media-preview/interfaces/media-content.interface';
import { MediaPreviewContentTypeEnum } from '@Components/media-preview/shared/content-type';
import { MediaPreviewComponent } from '@Components/media-preview/media-preview.component';
import { AvatarTypeSheetComponent } from './components/avatar-type-sheet/avatar-type-sheet.component';
import { AvatarUploadingTypeEnum } from './components/avatar-type-sheet/shared/avatar-uploading-type';
import { PhotoCropperWindowComponent } from '@Components/photo-cropper-window/photo-cropper-window.component';
import { throwError } from 'rxjs';

interface IStaffUserMedia {
    photo: string;
    photoPreview: string;
    video: string;
    videoId: string;
}

@Component({
    selector: 'app-photo-and-video',
    templateUrl: './photo-and-video.component.html',
    styleUrls: ['./photo-and-video.component.scss']
})
export class PhotoAndVideoComponent implements OnInit {
    @ViewChild('AvatarFileUploader')    private AvatarUploadButton: { nativeElement: { click: () => void; }; };
    @ViewChild('AvatarsMediaPreview')   private AvatarsMediaPreview: MediaPreviewComponent;
    @ViewChild('VideosMediaPreview')    private VideosMediaPreview: MediaPreviewComponent;

    @Input() public openedStaffUser: IStaffUser;

    public mediaAvatars: IMediaContent[] = [];
    public mediaVideos: IMediaContent[]  = [];

    public media: IStaffUserMedia;

    public isAvatarUploading: boolean = false;

    constructor (
        private notificationService: NotificationService,
        private bottomSheet: MatBottomSheet,
        private staffService: StaffService,
        private dialogRef: MatDialog
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.staffService.getMedia(this.openedStaffUser.id)
            .subscribe((media: IStaffUserMedia) => {
                this.media = media;
                this.mediaAvatars = [];
                this.mediaVideos  = [];

                if (media.photo && media.photoPreview) {
                    this.mediaAvatars.push({
                        content: media.photo,
                        type: MediaPreviewContentTypeEnum.Image
                    });
                }

                if (media.videoId) {
                    this.mediaVideos.push({
                        content: media.videoId,
                        type: MediaPreviewContentTypeEnum.YoutubeVideo
                    });
                }
            });
    }

    public viewAvatar (): void {
        if (this.AvatarsMediaPreview) {
            this.AvatarsMediaPreview.open();
        }
    }

    public viewVideo (): void {
        if (this.VideosMediaPreview) {
            this.VideosMediaPreview.open();
        }
    }

    public selectAvatar (): void {
        this.AvatarUploadButton.nativeElement.click();
    }

    public openAvatarSelectorWindow (): void {
        this.AvatarUploadButton.nativeElement.click();
    }

    public openAvatarCropWindow (image: Blob): void {
        const reader = new FileReader();
        reader.readAsDataURL(image);

        reader.onloadend = () => {
            const base64data = reader.result;

            const cropWindow = this.dialogRef.open(PhotoCropperWindowComponent, {
                width: '450px',
                data: base64data
            });

            cropWindow.afterClosed()
                .pipe(
                    tap((outputBase64: string) => {
                        this.isAvatarUploading = true;
                        this.staffService.changeAvatarAsBase64(this.openedStaffUser.id, outputBase64)
                            .pipe(
                                tap(() => this.update())
                            )
                            .subscribe(() => {
                                this.isAvatarUploading = false;
                            });
                    })
                )
                .subscribe();
        };
    }

    public changeAvatar (event: any): void {
        const files: Blob[] = event.target.files;

        if (files.length) {
            const avatarTypeSheet = this.bottomSheet.open(AvatarTypeSheetComponent);

            avatarTypeSheet.afterDismissed()
                .pipe(
                    tap((type: AvatarUploadingTypeEnum) => {
                        switch (type) {
                            case AvatarUploadingTypeEnum.Automatically:
                                this.isAvatarUploading = true;
                                this.staffService.changeAvatarAsFile(this.openedStaffUser.id, files[0])
                                    .pipe(
                                        catchError((error) => {
                                            this.openAvatarCropWindow(files[0]);
                                            return throwError(error);
                                        }),
                                        tap(() => this.update())
                                    )
                                    .subscribe(() => {
                                        this.isAvatarUploading = false;
                                    });
                                break;
                            case AvatarUploadingTypeEnum.Manual:
                                this.openAvatarCropWindow(files[0]);
                                break;
                        }
                    })
                )
                .subscribe();
        }
    }

    public changeVideo (): void {
        const pathWindow = this.dialogRef.open(VideoPathWindowComponent, {
            width: '500px',
            data: {
                video: this.media.video,
                videoId: this.media.videoId,
                openedStaffUserId: this.openedStaffUser.id
            }
        });

        pathWindow.afterClosed()
            .subscribe(() => this.update());
    }
}
