import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { StaffService } from '@Services/staff.service';
import { NotificationService } from '@Services/notification.service';
import { FormControl } from '@angular/forms';
import { environment } from 'src/environments/environment';

interface IData {
    video: string;
    videoId: string;
    openedStaffUserId: number;
}

@Component({
    selector: 'app-video-path-window',
    templateUrl: './video-path-window.component.html',
    styleUrls: ['./video-path-window.component.scss']
})
export class VideoPathWindowComponent implements OnInit {
    @ViewChild('VideoFileUploader') private VideoUploadButton: { nativeElement: { click: () => void; }; };

    private readonly youtubeVideoPath: string = environment.youtubeVideoPath;

    public videoLinkControl: FormControl = new FormControl(
        '',
        [this.validateVideoPath.bind(this)]
    );

    constructor (
        public dialogRef: MatDialogRef<VideoPathWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IData,
        private staffService: StaffService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        if (!this.data) {
            throw new Error('Component can\'t resolve required @Input params!');
        }

        this.initForm();
    }

    public cancel (): void {
        this.dialogRef.close();
    }

    public changeVideoLink (): void {
        if (this.videoLinkControl.valid) {
            this.staffService.changeVideoLink(this.data.openedStaffUserId, this.videoLinkControl.value)
                .subscribe(() => {
                    this.dialogRef.close();
                });
        }
    }

    public uploadVideo (event: any): void {
        const files: Blob[] = event.target.files;

        if (files.length) {
            this.staffService.changeVideo(this.data.openedStaffUserId, files[0])
                .subscribe();
        } else {
            this.notificationService.error('Oops', 'An error occurred while trying to upload a video.');
        }
    }

    public selectVideo (): void {
        this.VideoUploadButton.nativeElement.click();
    }

    private initForm (): void {
        this.videoLinkControl.setValue(`${this.youtubeVideoPath}${this.data.videoId}`);
    }

    private validateVideoPath (control: FormControl): { isVideoLinkIncorrect } {
        const reg = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/gm;

        if (!reg.test(control.value)) {
            return { isVideoLinkIncorrect: true };
        }

        return null;
    }
}
