export enum AvatarUploadingTypeEnum {
    Automatically = 1,
    Manual        = 2
}
