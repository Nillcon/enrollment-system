import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SchedulePageComponent } from './schedule-page.component';
import { ScheduleModule } from '@Components/schedule/schedule.module';

@NgModule({
    declarations: [
        SchedulePageComponent
    ],
    imports: [
        CommonModule,

        ScheduleModule
    ],
    exports: [
        SchedulePageComponent
    ]
})
export class SchedulePageModule { }
