import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoPageComponent } from './info-page.component';
import { SharedModule } from '@App/app-shared.module';
import { ChipInputModule } from '@Components/chip-input/chip-input.module';

@NgModule({
    declarations: [
        InfoPageComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        ChipInputModule
    ],
    exports: [
        InfoPageComponent
    ]
})
export class InfoPageModule { }
