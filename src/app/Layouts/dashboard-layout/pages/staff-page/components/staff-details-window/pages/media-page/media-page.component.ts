import { Component, OnInit, Input } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { StaffDetailsWindowComponent } from '../../staff-details-window.component';
import { IStaffUser } from '@Interfaces/users';

@Component({
    selector: 'app-media-page',
    templateUrl: './media-page.component.html',
    styleUrls: ['./media-page.component.scss']
})
export class MediaPageComponent implements OnInit {
    @Input() public dialogRef: MatDialogRef<StaffDetailsWindowComponent>;
    @Input() public openedStaffUser: IStaffUser;

    constructor () {}

    ngOnInit () {
        if (!this.dialogRef || !this.openedStaffUser) {
            throw new Error('MediaPage component can\'t resolve required @Input parameters!');
        }
    }
}
