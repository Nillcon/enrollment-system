import { Component, OnInit, ViewChild, Injector, Input } from '@angular/core';
import { NotificationService } from '@Services/notification.service';
import { StaffService } from '@Services/staff.service';
import { IStaffUser } from '@Interfaces/users';
import { IHobbie } from '@Interfaces/hobby.interface';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { tap, filter } from 'rxjs/operators';
import { MediaPreviewComponent } from '@Components/media-preview/media-preview.component';
import { IMediaContent } from '@Components/media-preview/interfaces/media-content.interface';
import { MediaPreviewContentTypeEnum } from '@Components/media-preview/shared/content-type';
import { HobbyVideoPathWindowComponent } from './components/hobby-video-path-window/hobby-video-path-window.component';
import { MatDialog } from '@angular/material';

@Component({
    selector: 'app-hobbies',
    templateUrl: './hobbies.component.html',
    styleUrls: ['./hobbies.component.scss']
})
export class HobbiesComponent implements OnInit {
    @ViewChild('HobbieAdditionFileUploader')    private HobbieAdditionUploadButton: { nativeElement: { click: () => void; }; };
    @ViewChild('MediaPreview')                  private MediaPreview: MediaPreviewComponent;

    @Input() public openedStaffUser: IStaffUser;

    public readonly MediaType: typeof MediaPreviewContentTypeEnum = MediaPreviewContentTypeEnum;

    public hobbiesMedia: IMediaContent[] = [];

    public hobbies: IHobbie[];

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private notificationService: NotificationService,
        private staffService: StaffService
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.staffService.getHobbies(this.openedStaffUser.id)
            .pipe(
                tap(hobbies => {
                    this.hobbiesMedia = [];

                    hobbies.forEach(hobby => {
                        this.hobbiesMedia.push({
                            content: hobby.content,
                            description: hobby.description,
                            type: hobby.type
                        });
                    });
                })
            )
            .subscribe(hobbies => {
                this.hobbies = hobbies;
            });
    }

    public viewHobbie (hobbieIndex: number): void {
        if (this.MediaPreview) {
            this.MediaPreview.open(hobbieIndex);
        }
    }

    public editHobbieDescription (hobby: IHobbie): void {
        this.staffService.editHobbie(this.openedStaffUser.id, hobby)
            .subscribe();
    }

    @NeedsConfirmation('Do you really want to delete gallery item?', 'Confirm deleting')
    public deleteHobbie (hobby: IHobbie): void {
        this.staffService.deleteHobbie(this.openedStaffUser.id, hobby.id)
            .pipe(
                tap(() => this.update())
            )
            .subscribe();
    }

    public uploadAndAddHobbie (event: any): void {
        const files: Blob[] = event.target.files;

        if (files.length) {
            this.staffService.addHobbieAsImage(this.openedStaffUser.id, files[0])
                .pipe(
                    tap(() => this.update())
                )
                .subscribe();
        } else {
            this.notificationService.error('Oops', 'An error occurred while trying to add a gallery item.');
        }
    }

    public addHobbieAsImage (): void {
        this.HobbieAdditionUploadButton.nativeElement.click();
    }

    public addHobbieAsVideo (): void {
        const videoWindow = this.dialog.open(HobbyVideoPathWindowComponent, {
            width: '450px',
            data: this.openedStaffUser.id
        });

        videoWindow.afterClosed()
            .pipe(
                filter(result => result === true)
            )
            .subscribe(() => this.update());
    }
}
