import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { StaffService } from '@Services/staff.service';
import { FormControl } from '@angular/forms';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'app-hobby-video-path-window',
    templateUrl: './hobby-video-path-window.component.html',
    styleUrls: ['./hobby-video-path-window.component.scss']
})
export class HobbyVideoPathWindowComponent implements OnInit {

    public videoLinkControl: FormControl = new FormControl(
        '',
        [this.validateVideoPath.bind(this)]
    );

    public isLoading: boolean = false;

    private readonly youtubeVideoPath: string = environment.youtubeVideoPath;

    constructor (
        public dialogRef: MatDialogRef<HobbyVideoPathWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public openedStaffUserId: number,
        private staffService: StaffService
    ) {}

    ngOnInit () {
        if (!this.openedStaffUserId) {
            throw new Error('Component can\'t resolve required @Input params!');
        }

        this.initForm();
    }

    public cancel (result: boolean = false): void {
        this.dialogRef.close(result);
    }

    public changeVideoLink (): void {
        if (this.videoLinkControl.valid) {
            this.isLoading = true;

            this.staffService.addHobbieAsVideo(this.openedStaffUserId, this.videoLinkControl.value)
                .subscribe(
                    () => {
                        this.isLoading = false;
                        this.dialogRef.close(true);
                    },
                    () => {
                        this.isLoading = false;
                    }
                );
        }
    }

    private initForm (): void {
        this.videoLinkControl.setValue(`${this.youtubeVideoPath}`);
    }

    private validateVideoPath (control: FormControl): { isVideoLinkIncorrect } {
        const reg = /^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$/gm;

        if (!reg.test(control.value)) {
            return { isVideoLinkIncorrect: true };
        }

        return null;
    }

}
