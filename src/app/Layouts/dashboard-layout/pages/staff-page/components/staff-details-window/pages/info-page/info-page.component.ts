import { Component, OnInit, Input } from '@angular/core';
import { IRole } from '@Interfaces/role.interface';
import { FormGroup, FormControl } from '@angular/forms';
import { StaffDetailsWindowComponent } from '../../staff-details-window.component';
import { MatDialogRef } from '@angular/material';
import { Roles, GetRolesListAvailableForUserCreating } from '@Shared/roles';
import { StaffService } from '@Services/staff.service';
import { IStaffUser } from '@Interfaces/users';
import { NotificationService } from '@Services/notification.service';

@Component({
    selector: 'app-info-page',
    templateUrl: './info-page.component.html',
    styleUrls: ['./info-page.component.scss']
})
export class InfoPageComponent implements OnInit {
    @Input() private dialogRef: MatDialogRef<StaffDetailsWindowComponent>;
    @Input() public openedStaffUserId: number;

    public readonly Roles: typeof Roles = Roles;

    public info: Partial<IStaffUser>;
    public rolesList: IRole[] = GetRolesListAvailableForUserCreating();

    public isPasswordHidden: boolean = true;

    public staffUserFormGroup: FormGroup;
    public nameFormControl: FormControl;
    public loginFormControl: FormControl;
    public phoneFormControl: FormControl;
    public mailFormControl: FormControl;
    public descriptionFormControl: FormControl;
    public passwordFormControl: FormControl;
    public roleFormControl: FormControl;
    public tagsFormControl: FormControl;
    public capacityFormControl: FormControl;

    public isSaving: boolean = false;

    constructor (
        private staffService: StaffService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        if (!this.dialogRef || !this.openedStaffUserId) {
            throw new Error('InfoPage component can\'t resolve required @Input parameters!');
        }

        this.update();
    }

    public update (): void {
        this.staffService.getInfo(this.openedStaffUserId)
            .subscribe(info => {
                this.initStaffUserFormGroup(info);
                this.info = info;
            });
    }

    public save (): void {
        if (this.staffUserFormGroup.valid) {
            this.isSaving = true;

            this.staffService.saveInfo(this.openedStaffUserId, this.staffUserFormGroup.value)
                .subscribe(
                    () => {
                        this.isSaving = false;
                        this.notificationService.success('Saved', 'User information was saved successfully!');
                    },
                    () => {
                        this.isSaving = false;
                    }
                );
        } else {
            this.notificationService.info('Oops', 'Please check if all the data is entered correctly.');
        }
    }

    public cancel (): void {
        this.dialogRef.close();
    }

    private initStaffUserFormGroup (info: Partial<IStaffUser>): void {
        this.nameFormControl = new FormControl(info.name || '');

        this.loginFormControl = new FormControl({
            value: info.login || '',
            disabled: info.isADUser || false
        });

        this.passwordFormControl = new FormControl({
            value: info.password || '',
            disabled: info.isADUser || false
        });

        this.phoneFormControl       = new FormControl(info.phone || '');
        this.mailFormControl        = new FormControl(info.email || '');
        this.descriptionFormControl = new FormControl(info.about || '');
        this.roleFormControl        = new FormControl(info.role || Roles.Support);
        this.tagsFormControl        = new FormControl(info.tags || []);
        this.capacityFormControl    = new FormControl(info.capacity || 0);

        this.staffUserFormGroup = new FormGroup({
            name: this.nameFormControl,
            login: this.loginFormControl,
            phone: this.phoneFormControl,
            email: this.mailFormControl,
            about: this.descriptionFormControl,
            password: this.passwordFormControl,
            role: this.roleFormControl,
            tags: this.tagsFormControl,
            capacity: this.capacityFormControl
        });
    }
}
