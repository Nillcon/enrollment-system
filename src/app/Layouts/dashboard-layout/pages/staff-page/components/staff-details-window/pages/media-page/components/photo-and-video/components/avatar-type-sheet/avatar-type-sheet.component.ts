import { Component, OnInit } from '@angular/core';
import { AvatarUploadingTypeEnum } from './shared/avatar-uploading-type';
import { MatBottomSheetRef } from '@angular/material';

@Component({
    selector: 'app-avatar-type-sheet',
    templateUrl: './avatar-type-sheet.component.html',
    styleUrls: ['./avatar-type-sheet.component.scss']
})
export class AvatarTypeSheetComponent implements OnInit {
    public readonly AvatarUploadingType: typeof AvatarUploadingTypeEnum = AvatarUploadingTypeEnum;

    constructor (
        private bottomSheetRef: MatBottomSheetRef<AvatarTypeSheetComponent>
    ) {}

    ngOnInit () {}

    public selectType (type: AvatarUploadingTypeEnum): void {
        this.bottomSheetRef.dismiss(type);
    }
}
