import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IStaffUser } from '@Interfaces/users/staff.interface';
import { Roles } from '@Shared/roles';

@Component({
    selector: 'app-staff-details-window',
    templateUrl: './staff-details-window.component.html',
    styleUrls: ['./staff-details-window.component.scss']
})
export class StaffDetailsWindowComponent implements OnInit {
    public readonly Roles: typeof Roles = Roles;

    constructor (
        public dialogRef: MatDialogRef<StaffDetailsWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IStaffUser
    ) {}

    ngOnInit () {
        if (!this.data) {
            throw new Error('StaffDetailsWindow component can\'t get required @Input parameter "data"');
        }
    }

    public close (): void {
        this.dialogRef.close();
    }
}
