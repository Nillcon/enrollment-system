import { Component, OnInit, Input } from '@angular/core';
import { StaffService } from '@Services/staff.service';
import { IScheduleView } from '@Components/schedule/interfaces/schedule-view.interface';

@Component({
  selector: 'app-schedule-page',
  templateUrl: './schedule-page.component.html',
  styleUrls: ['./schedule-page.component.scss']
})
export class SchedulePageComponent implements OnInit {

    @Input() public openedStaffUserId: number;

    constructor(
    ) { }

    ngOnInit() {
    }
}
