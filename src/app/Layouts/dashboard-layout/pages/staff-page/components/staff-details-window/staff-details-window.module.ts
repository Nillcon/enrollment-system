import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaffDetailsWindowComponent } from './staff-details-window.component';
import { SharedModule } from '@App/app-shared.module';
import { InfoPageModule } from './pages/info-page/info-page.module';
import { MediaPageModule } from './pages/media-page/media-page.module';
import { SchedulePageModule } from './pages/schedule-page/schedule-page.module';

@NgModule({
    declarations: [
        StaffDetailsWindowComponent,
    ],
    imports: [
        CommonModule,

        SharedModule,

        InfoPageModule,
        MediaPageModule,
        SchedulePageModule
    ],
    exports: [
        StaffDetailsWindowComponent
    ],
    entryComponents: [
        StaffDetailsWindowComponent
    ]
})
export class StaffDetailsWindowModule { }
