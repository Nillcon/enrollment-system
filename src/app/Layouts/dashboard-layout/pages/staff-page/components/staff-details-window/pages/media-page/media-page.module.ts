import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaPageComponent } from './media-page.component';
import { SharedModule } from '@App/app-shared.module';
import { PhotoAndVideoComponent } from './components/photo-and-video/photo-and-video.component';
import { HobbiesComponent } from './components/hobbies/hobbies.component';
import { VideoPathWindowComponent } from './components/photo-and-video/components/video-path-window/video-path-window.component';
import { HobbyVideoPathWindowComponent } from './components/hobbies/components/hobby-video-path-window/hobby-video-path-window.component';
import { MatBottomSheetModule, MatListModule } from '@angular/material';
import { AvatarTypeSheetComponent } from './components/photo-and-video/components/avatar-type-sheet/avatar-type-sheet.component';
import { PhotoCropperWindowComponent } from '@Components/photo-cropper-window/photo-cropper-window.component';

@NgModule({
    declarations: [
        MediaPageComponent,
        PhotoAndVideoComponent,
        HobbiesComponent,
        VideoPathWindowComponent,
        HobbyVideoPathWindowComponent,
        AvatarTypeSheetComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        MatBottomSheetModule,
        MatListModule
    ],
    exports: [
        MediaPageComponent
    ],
    entryComponents: [
        VideoPathWindowComponent,
        HobbyVideoPathWindowComponent,
        PhotoCropperWindowComponent,
        AvatarTypeSheetComponent
    ]
})
export class MediaPageModule { }
