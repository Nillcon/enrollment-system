import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaffTableComponent } from './components/staff-table/staff-table.component';
import { StaffDetailsWindowModule } from './components/staff-details-window/staff-details-window.module';
import { StaffAdditionWindowModule } from './components/staff-addition-window/staff-addition-window.module';
import { SharedModule } from '@App/app-shared.module';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';

@NgModule({
    declarations: [
        StaffTableComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule,

        StaffAdditionWindowModule,
        StaffDetailsWindowModule
    ],
    exports: [
        StaffTableComponent,

        SharedModule,
        DashboardLayoutSharedModule,

        StaffAdditionWindowModule,
        StaffDetailsWindowModule
    ]
})
export class StaffSharedModule { }
