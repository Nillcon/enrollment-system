import { Component, OnInit, ViewChild } from '@angular/core';
import { StaffAdditionWindowComponent } from './components/staff-addition-window/staff-addition-window.component';
import { MatDialog } from '@angular/material';
import { StaffTableComponent } from './components/staff-table/staff-table.component';

@Component({
    selector: 'app-staff-page',
    templateUrl: './staff-page.component.html',
    styleUrls: ['./staff-page.component.scss']
})
export class StaffPageComponent implements OnInit {
    @ViewChild('StaffTable') private staffTable: StaffTableComponent;

    constructor (
        private dialog: MatDialog
    ) {}

    ngOnInit () {}

    public openAdditionWindow (): void {
        const additionWindow = this.dialog.open(StaffAdditionWindowComponent, {
            width: '450px'
        });

        additionWindow.afterClosed()
            .subscribe(() => {
                this.staffTable.update();
            });
    }
}
