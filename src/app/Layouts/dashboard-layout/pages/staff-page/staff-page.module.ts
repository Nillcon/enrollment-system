import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StaffPageComponent } from './staff-page.component';
import { StaffPageRoutingModule } from './staff-page.routing';
import { StaffSharedModule } from './staff-shared.module';

@NgModule({
    declarations: [
        StaffPageComponent
    ],
    imports: [
        CommonModule,

        StaffSharedModule,
        StaffPageRoutingModule
    ]
})
export class StaffPageModule { }
