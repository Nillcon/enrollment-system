import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LogsPageComponent } from './logs-page.component';

export const routes: Routes = [
    {
        path: '',
        component: LogsPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class LogsPageRoutingModule {}
