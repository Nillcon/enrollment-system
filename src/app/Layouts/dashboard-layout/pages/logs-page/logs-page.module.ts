import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsPageComponent } from './logs-page.component';
import { LogsPageRoutingModule } from './logs-page.routing';
import { LogsSharedModule } from '@Layouts/dashboard-layout/components/global/logs/logs-shared.module';

@NgModule({
    declarations: [
        LogsPageComponent
    ],
    imports: [
        CommonModule,

        LogsSharedModule,

        LogsPageRoutingModule
    ],
    exports: [
        LogsPageComponent
    ]
})
export class LogsPageModule { }
