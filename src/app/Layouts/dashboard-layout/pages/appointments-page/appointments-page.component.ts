import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AppointmentsWindowComponent } from './components/appointments-window/appointments-window.component';
import { AppointmentsTableComponent } from './components/appointments-table/appointments-table.component';

@Component({
    selector: 'app-appointments-page',
    templateUrl: './appointments-page.component.html',
    styleUrls: ['./appointments-page.component.scss']
})
export class AppointmentsPageComponent implements OnInit {
    @ViewChild('AppointmentsTable') private AppointmentsTable: AppointmentsTableComponent;

    constructor (
        private dialog: MatDialog
    ) {}

    ngOnInit () {}

    public openAdditionWindow (): void {
        const addtionWindow = this.dialog.open(AppointmentsWindowComponent, {
            width: '95%',
            height: '95%',
            data: {
                mode: 'addition'
            }
        });

        addtionWindow.afterClosed()
            .subscribe(() => {
                this.AppointmentsTable.update();
            });
    }
}
