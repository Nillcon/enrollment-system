import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentsPageComponent } from './appointments-page.component';
import { AppointmentsPageRoutingModule } from './appointments-page.routing';
import { AppointmentsTableComponent } from './components/appointments-table/appointments-table.component';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { SharedModule } from '@App/app-shared.module';
import { AppointmentModule } from '@Components/appointment/appointment.module';
import { UserDetailsWindowModule } from '@Components/user-details-window/user-details-window.module';
import { AppointmentsWindowModule } from './components/appointments-window/appointments-window.module';
import { StatusesChangerWindowModule } from './components/statuses-changer-window/statuses-changer-window.module';

@NgModule({
    declarations: [
        AppointmentsPageComponent,
        AppointmentsTableComponent
    ],
    imports: [
        CommonModule,

        AppointmentsPageRoutingModule,

        SharedModule,
        DashboardLayoutSharedModule,

        StatusesChangerWindowModule,

        AppointmentModule,
        UserDetailsWindowModule,
        AppointmentsWindowModule
    ]
})
export class AppointmentsPageModule { }
