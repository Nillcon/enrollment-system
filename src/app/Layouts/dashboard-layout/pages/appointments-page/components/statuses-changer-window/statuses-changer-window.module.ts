import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatusesChangerWindowComponent } from './statuses-changer-window.component';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        StatusesChangerWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    exports: [
        StatusesChangerWindowComponent
    ],
    entryComponents: [
        StatusesChangerWindowComponent
    ]
})
export class StatusesChangerWindowModule { }
