import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppointmentService } from '@Components/appointment/services/appointment.service';
import { IAppointmentStatus, getAppointmentStatusListAsArray } from '@Components/appointment/shared/schedule-status';
import { IAppointment } from '@Components/appointment/interfaces/appointment.interface';

@Component({
    selector: 'app-statuses-changer-window',
    templateUrl: './statuses-changer-window.component.html',
    styleUrls: ['./statuses-changer-window.component.scss'],
    providers: [
        AppointmentService
    ]
})
export class StatusesChangerWindowComponent implements OnInit {
    public selectedStatus: number;
    public statusesList: IAppointmentStatus[] = getAppointmentStatusListAsArray();

    public isLoading: boolean = false;

    constructor (
        private dialogRef: MatDialogRef<StatusesChangerWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public selectedAppointments: IAppointment[],
        private appointmentsService: AppointmentService
    ) {}

    ngOnInit () {}

    public close (result: boolean = false): void {
        this.dialogRef.close(result);
    }

    public save (): void {
        this.isLoading = true;

        this.appointmentsService.changeStatuses(this.getSelectedAppointmentIds(), this.selectedStatus)
            .subscribe(
                () => {
                    this.isLoading = false;
                    this.dialogRef.close(true);
                },
                () => {
                    this.isLoading = false;
                }
            );
    }

    private getSelectedAppointmentIds (): number[] {
        return this.selectedAppointments.map(appointment => {
            return appointment.id;
        });
    }
}
