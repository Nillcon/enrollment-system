import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WindowMode } from '@Types/window-mode.type';

interface IData {
    mode: WindowMode;
    appointmentId: number;
}

@Component({
    selector: 'app-appointments-window',
    templateUrl: './appointments-window.component.html',
    styleUrls: ['./appointments-window.component.scss']
})
export class AppointmentsWindowComponent implements OnInit {
    constructor (
        public dialogRef: MatDialogRef<AppointmentsWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IData
    ) {}

    ngOnInit () {}
}
