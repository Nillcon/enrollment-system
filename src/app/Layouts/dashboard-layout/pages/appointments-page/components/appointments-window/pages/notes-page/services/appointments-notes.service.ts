import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";
import { IAppointmentNote } from "../interfaces/appointment-note.interface";

@Injectable()
export class AppointmentsNotesService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getList (appointmentId: number): Observable<IAppointmentNote[]> {
        return this.httpService.get(`/Appointments/${appointmentId}/Notes`);
    }

    public create (appointmentId: number, model: Partial<IAppointmentNote>): Observable<any> {
        return this.httpService.post(`/Appointments/${appointmentId}/Notes`, model);
    }

    public delete (appointmentId: number, noteId: number): Observable<any> {
        return this.httpService.delete(`/Appointments/${appointmentId}/Notes/${noteId}`);
    }
}
