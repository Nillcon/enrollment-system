import { AppointmentRepeatTypeEnum } from "../components/repeating-appointment-block/shared/repeating-type";

export interface IRepeatingAppointmentSettings {
    type: AppointmentRepeatTypeEnum;
    rate: number;
    untilDate: Date;
    amount: number;
    greeterId: number;
    visitorId: number;
    firstDate: string;
}
