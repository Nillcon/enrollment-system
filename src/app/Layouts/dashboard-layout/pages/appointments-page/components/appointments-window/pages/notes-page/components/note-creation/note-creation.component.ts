import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { AppointmentsNotesService } from '../../services/appointments-notes.service';
import { NotificationService } from '@Services/notification.service';

@Component({
    selector: 'app-note-creation',
    templateUrl: './note-creation.component.html',
    styleUrls: ['./note-creation.component.scss']
})
export class NoteCreationComponent implements OnInit {
    @Input() public appointmentId: number;

    @Output() public OnAdditionCanceled: EventEmitter<boolean> = new EventEmitter();
    @Output() public OnNoteSaved: EventEmitter<boolean>        = new EventEmitter();

    public text: string = '';

    public isSaving: boolean = false;

    constructor (
        private notesService: AppointmentsNotesService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {}

    public save (): void {
        if (!this.text) {
            this.notificationService.error('Oops', 'You can\'t create empty note!');
            return;
        }

        this.isSaving = true;

        this.notesService.create(this.appointmentId, {
            note: this.text
        })
            .subscribe(
                () => {
                    this.isSaving = false;
                    this.notificationService.success('Note created', 'New note was saved successfully!');
                    this.OnNoteSaved.emit(true);
                    this.cancel();
                },
                () => {
                    this.isSaving = false;
                }
            );
    }

    public cancel (): void {
        this.OnAdditionCanceled.emit(true);
    }
}
