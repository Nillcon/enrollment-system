import { Component, OnInit, Input, OnDestroy, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { AppointmentsWindowComponent } from '../../appointments-window.component';
import { WindowMode } from '@Types/window-mode.type';
import { AppointmentsInfoService } from './services/appointments-info.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IInvitedUser, IStaffUser } from '@Interfaces/users';
import { IAppointmentInfoModel } from './interfaces/appointments-info-model.interface';
import { NotificationService } from '@Services/notification.service';
import { SaveMode } from '@Types/save-mode.type';
import { DateMethod } from '@Methods/date.method';
import { Subscription, ReplaySubject } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { filter } from 'rxjs/operators';
import { AppointmentTypeEnum } from '@Components/appointment/shared/appointment-type';
import { RepeatingAppointmentBlockComponent } from './components/repeating-appointment-block/repeating-appointment-block.component';

@AutoUnsubscribe()
@Component({
    selector: 'app-info-page',
    templateUrl: './info-page.component.html',
    styleUrls: ['./info-page.component.scss']
})
export class InfoPageComponent implements OnInit, OnDestroy {
    @Input() public dialogRef: MatDialogRef<AppointmentsWindowComponent>;
    @Input() public appointmentId: number;
    @Input() public mode: WindowMode;

    @ViewChild('RepeatingAppointmentBlock') public repeatingAppointmentBlock: RepeatingAppointmentBlockComponent;

    public readonly appointmentType: typeof AppointmentTypeEnum = AppointmentTypeEnum;
    public readonly noNameText: string = '<No Name>';

    public infoFormGroup: FormGroup;
    public visitorIdFormControl: FormControl;
    public greeterIdFormControl: FormControl;
    public greeterDateFormControl: FormControl;
    public greeterDateTimeFormControl: FormControl;
    public isRepeatedFormControl: FormControl;
    public appointmentTypeFormControl: FormControl;
    public plannedAppointmentsFormControl: FormControl;

    public visitorFilterFormControl: FormControl = new FormControl('');
    public filteredVisitors: ReplaySubject<IInvitedUser[]> = new ReplaySubject<IInvitedUser[]>(1);

    public visitorList: IInvitedUser[]      = [];
    public greeterList: IStaffUser[]        = [];
    public greeterDatesList: string[]       = [];
    public greeterDatesListAsDates: Date[]  = [];

    // List of visitors which includes in appointment by selected date&time and with selected greeter
    public appointmentVisitors: IInvitedUser[] = [];

    public isDateChangingInEditingMode: boolean = false;

    public isSaving: boolean = false;

    private greeterTimeChangesSubscription: Subscription;
    private visitorFilteringSubscription: Subscription;

    constructor (
        private infoService: AppointmentsInfoService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        this.initPage();
        this.visitorFilterObserver();
    }

    ngOnDestroy () {}

    public updateVisitorList (clearDependentForms: boolean = true): void {
        this.visitorList = null;

        this.infoService.getVisitorList()
            .subscribe((visitors) => {
                this.visitorList = visitors;

                if (clearDependentForms) {
                    this.greeterIdFormControl.setValue(null);
                    this.greeterDateFormControl.setValue(null);
                    this.greeterDateTimeFormControl.setValue(null);
                }

                this.filteredVisitors.next(visitors.slice());

                this.updateAppointmentVisitors();
            });
    }

    public updateGreeterList (clearDependentForms: boolean = true): void {
        this.greeterList = null;

        const selectedVisitorId: number = this.visitorIdFormControl.value;

        this.infoService.getGreeterList(selectedVisitorId)
            .subscribe((greeters) => {
                this.greeterList = greeters;

                if (clearDependentForms) {
                    this.greeterIdFormControl.setValue(null);
                    this.greeterDateFormControl.setValue(null);
                    this.greeterDateTimeFormControl.setValue(null);
                }

                this.updateAppointmentVisitors();
            });
    }

    public updateGreeterDates (clearDependentForms: boolean = true): void {
        this.greeterDatesList = null;

        const selectedVisitorId: number = this.visitorIdFormControl.value;
        const selectedGreeterId: number = this.greeterIdFormControl.value;

        this.infoService.getGreeterDates(selectedVisitorId, selectedGreeterId)
            .subscribe((greeterDates) => {
                this.greeterDatesList        = greeterDates;
                this.greeterDatesListAsDates = greeterDates.map(date => {
                    return new Date(date);
                });

                if (clearDependentForms) {
                    this.greeterDateFormControl.setValue(null);
                    this.greeterDateTimeFormControl.setValue(null);
                }

                this.updateAppointmentVisitors();
            });
    }

    public updateGreeterTimes (): void {
        this.greeterDateTimeFormControl.setValue(
            (this.greeterDateFormControl.value as Date).toJSON()
        );
    }

    public updateAppointmentVisitors (): void {
        if (
            this.visitorIdFormControl.value &&
            this.greeterIdFormControl.value &&
            this.greeterDateTimeFormControl.value
        ) {
            this.appointmentVisitors = null;
        } else {
            this.appointmentVisitors = [];
            return;
        }

        this.infoService.getVisitorsByAppointmentDate({
            visitor: this.visitorIdFormControl.value,
            greeter: this.greeterIdFormControl.value,
            date: this.greeterDateTimeFormControl.value
        })
            .subscribe(visitors => {
                this.appointmentVisitors = visitors;
            });
    }

    public prepeareDatesClasses = (currentDate: Date): string => {
        const currentCalendarTime: number = currentDate.getTime();
        const currentDateIndex: number    = this.greeterDatesListAsDates.findIndex(currDate => {
            return currDate.getTime()   === currentCalendarTime;
        });

        return (currentDateIndex !== -1) ? 'greeter_date' : undefined;
    }

    public showDatesInEditingMode (): void {
        this.isDateChangingInEditingMode = true;
        this.updateGreeterDates();
    }

    public close (result?: boolean): void {
        this.dialogRef.close(result);
    }

    public save (): void {
        const saveMode: SaveMode = (this.mode === 'addition') ? 'create' : 'edit';
        this.isSaving = true;

        if (this.infoFormGroup.valid) {
            // Prepearing some data for backend
            const data   = Object.assign(this.infoFormGroup.value);
            // Greenwich Date Conversion
            data['date'] = DateMethod.getLocalTimeString(new Date(this.greeterDateTimeFormControl.value));
            // Appointment type
            // tslint:disable-next-line: max-line-length
            data['type'] = (this.repeatingAppointmentBlock && this.repeatingAppointmentBlock.isRepeatingAppointment) ? AppointmentTypeEnum.Repeated : AppointmentTypeEnum.Single;

            this.infoService.saveInfo(this.appointmentId, data, saveMode)
                .subscribe(
                    () => {
                        this.isSaving = false;
                        this.close(true);
                    },
                    () => {
                        this.isSaving = false;
                    }
                );
        } else {
            this.notificationService.error(
                'Oops',
                'An error occurred while trying to save appointment info. All sections is required!'
            );
        }
    }

    private greeterTimeChangesObserver (): void {
        this.greeterTimeChangesSubscription = this.greeterDateTimeFormControl.valueChanges
            .pipe(
                filter((time: string) => time !== null)
            )
            .subscribe(() => {
                this.updateAppointmentVisitors();
            });
    }

    private initPage (): void {
        if (this.mode === 'addition') {
            this.updateVisitorList();
            this.initForm();
        } else if (this.mode === 'editing') {
            this.infoService.getInfo(this.appointmentId)
                .subscribe(info => {
                    this.initForm(info);

                    this.updateVisitorList(false);
                    this.updateGreeterList(false);
                });
        }
    }

    private visitorFilterObserver (): void {
        this.visitorFilteringSubscription = this.visitorFilterFormControl.valueChanges
            .subscribe(() => {
                this.filterVisitors();
            });
    }

    protected filterVisitors (): void {
        if (!this.visitorList) {
            return;
        }

        let search = this.visitorFilterFormControl.value;
        if (!search) {
            this.filteredVisitors.next(this.visitorList.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        this.filteredVisitors.next(
            this.visitorList.filter(visitor => visitor.name.toLowerCase().indexOf(search) > -1)
        );
      }

    private initForm (model: Partial<IAppointmentInfoModel> = {}): void {
        this.visitorIdFormControl               = new FormControl(model.visitor, [Validators.required]);
        this.greeterIdFormControl               = new FormControl(model.greeter, [Validators.required]);
        this.greeterDateFormControl             = new FormControl();
        this.greeterDateTimeFormControl         = new FormControl(model.date, [Validators.required]);
        this.appointmentTypeFormControl         = new FormControl(model.type);
        this.plannedAppointmentsFormControl     = new FormControl([]);

        this.infoFormGroup = new FormGroup({
            visitor: this.visitorIdFormControl,
            greeter: this.greeterIdFormControl,
            date: this.greeterDateTimeFormControl,
            repeatedAppointments: this.plannedAppointmentsFormControl
        });

        this.greeterTimeChangesObserver();
    }
}
