import { IStaffUser, IInvitedUser } from "@Interfaces/users";
import { IPlannedAppointment } from "./planned-appointment-model.interface";
import { AppointmentTypeEnum } from "@Components/appointment/shared/appointment-type";

export interface IAppointmentInfoModel {
    id: number;
    greeter: number;
    greeterModel: IStaffUser;
    visitor: number;
    visitorModel: IInvitedUser;
    date: string;
    type: AppointmentTypeEnum;
    repeatedAppointments: IPlannedAppointment[];
}
