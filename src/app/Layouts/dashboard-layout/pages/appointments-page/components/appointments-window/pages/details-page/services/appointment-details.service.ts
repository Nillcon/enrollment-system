import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";
import { IAppointmentDetailsModel } from "../interfaces/appointment-details-model.interface";
// tslint:disable-next-line: max-line-length
import { IHiddenStatus } from "@Layouts/dashboard-layout/pages/campaigns-page/pages/hidden-statuses-page/interfaces/hidden-status.interface";

@Injectable()
export class AppointmentDetailsService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public get (appointmentId: number): Observable<IAppointmentDetailsModel> {
        return this.httpService.get(`/Appointments/${appointmentId}/Details`);
    }

    public edit (appointmentId: number, model: IAppointmentDetailsModel): Observable<any> {
        return this.httpService.put(`/Appointments/${appointmentId}/Details`, model);
    }

    public getHiddenStatusList (appointmentId: number): Observable<IHiddenStatus[]> {
        return this.httpService.get(`/Appointments/${appointmentId}/HiddenStatuses`);
    }
}
