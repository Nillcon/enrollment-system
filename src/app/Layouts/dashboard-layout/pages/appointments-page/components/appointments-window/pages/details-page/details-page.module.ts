import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsPageComponent } from './details-page.component';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        DetailsPageComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    exports: [
        DetailsPageComponent
    ]
})
export class DetailsPageModule { }
