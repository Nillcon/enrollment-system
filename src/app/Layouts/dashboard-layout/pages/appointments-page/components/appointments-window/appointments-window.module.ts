import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentsWindowComponent } from './appointments-window.component';
import { InfoPageModule } from './pages/info-page/info-page.module';
import { NotesPageModule } from './pages/notes-page/notes-page.module';
import { DetailsPageModule } from './pages/details-page/details-page.module';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        AppointmentsWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        InfoPageModule,
        NotesPageModule,
        DetailsPageModule
    ],
    exports: [
        AppointmentsWindowComponent
    ],
    entryComponents: [
        AppointmentsWindowComponent
    ]
})
export class AppointmentsWindowModule { }
