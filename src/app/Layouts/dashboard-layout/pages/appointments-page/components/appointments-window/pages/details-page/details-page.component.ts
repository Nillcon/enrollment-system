import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { IAppointmentStatus, AppointmentStatusList, getAppointmentStatusListAsArray } from '@Components/appointment/shared/schedule-status';
import { AppointmentDetailsService } from './services/appointment-details.service';
import { NotificationService } from '@Services/notification.service';
import { IAppointmentDetailsModel } from './interfaces/appointment-details-model.interface';
import { MatDialogRef } from '@angular/material';
import { AppointmentsWindowComponent } from '../../appointments-window.component';
// tslint:disable-next-line: max-line-length
import { IHiddenStatus } from '@Layouts/dashboard-layout/pages/campaigns-page/pages/hidden-statuses-page/interfaces/hidden-status.interface';

@Component({
    selector: 'app-details-page',
    templateUrl: './details-page.component.html',
    styleUrls: ['./details-page.component.scss'],
    providers: [
        AppointmentDetailsService
    ]
})
export class DetailsPageComponent implements OnInit {
    @Input() public dialogRef: MatDialogRef<AppointmentsWindowComponent>;
    @Input() public appointmentId: number;

    public detailsFormGroup: FormGroup;
    public statusFormControl: FormControl;
    public hiddenStatusIdFormControl: FormControl;

    public readonly StatusList: { [key: number]: IAppointmentStatus } = AppointmentStatusList;
    public readonly StatusListAsArray: IAppointmentStatus[] = getAppointmentStatusListAsArray();

    public hiddenStatusList: IHiddenStatus[];

    public isSaving: boolean = false;

    constructor (
        private detailsService: AppointmentDetailsService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        this.update();
        this.updateStatusList();
    }

    public update (): void {
        this.detailsService.get(this.appointmentId)
            .subscribe(data => {
                this.initForm(data);
            });
    }

    public updateStatusList (): void {
        this.detailsService.getHiddenStatusList(this.appointmentId)
            .subscribe((data) => {
                this.hiddenStatusList = data;
            });
    }

    public save (): void {
        if (this.detailsFormGroup.valid) {
            this.isSaving = true;

            this.detailsService.edit(this.appointmentId, this.detailsFormGroup.value)
                .subscribe(
                    () => {
                        this.isSaving = false;
                        this.notificationService.success('Saved', 'Details information was saved successfully!');
                    },
                    () => {
                        this.isSaving = false;
                    }
                );
        } else {
            this.notificationService.error('Oops', 'An error occurred while trying to save appointment details.');
        }
    }

    public close (result?: boolean): void {
        this.dialogRef.close(result);
    }

    private initForm (data: IAppointmentDetailsModel): void {
        this.statusFormControl       = new FormControl(data.status);
        this.hiddenStatusIdFormControl = new FormControl(data.hiddenStatusId);

        this.detailsFormGroup        = new FormGroup({
            status: this.statusFormControl,
            hiddenStatusId: this.hiddenStatusIdFormControl
        });
    }
}
