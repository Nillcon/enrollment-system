import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoPageComponent } from './info-page.component';
import { SharedModule } from '@App/app-shared.module';
import { MatListModule } from '@angular/material';
import { DxDateBoxModule } from 'devextreme-angular';
import { RepeatingAppointmentBlockComponent } from './components/repeating-appointment-block/repeating-appointment-block.component';
import { AppointmentsInfoService } from './services/appointments-info.service';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';

@NgModule({
    declarations: [
        InfoPageComponent,
        RepeatingAppointmentBlockComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        DashboardLayoutSharedModule,
        DxDateBoxModule,
        MatListModule
    ],
    exports: [
        InfoPageComponent
    ],
    providers: [
        AppointmentsInfoService
    ]
})
export class InfoPageModule { }
