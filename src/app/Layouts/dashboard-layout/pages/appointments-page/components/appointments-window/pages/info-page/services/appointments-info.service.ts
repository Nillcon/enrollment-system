import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { IInvitedUser, IStaffUser } from "@Interfaces/users";
import { Observable } from "rxjs";
import { IAppointmentInfoModel } from '../interfaces/appointments-info-model.interface';
import { SaveMode } from "@Types/save-mode.type";
import { IRepeatingAppointmentSettings } from "../interfaces/repeating-appointment-settings-model.interface";
import { IPlannedAppointment } from "../interfaces/planned-appointment-model.interface";
import { flatMap, map, toArray, tap } from "rxjs/operators";
import { DateMethod } from "@Methods/date.method";

@Injectable()
export class AppointmentsInfoService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getInfo (appointmentId: number): Observable<IAppointmentInfoModel> {
        return this.httpService.get(`/Appointments/${appointmentId}/Info`);
    }

    public createInfo (model: IAppointmentInfoModel): Observable<any> {
        return this.httpService.post(`/Appointments/Info`, model);
    }

    public editInfo (appointmentId: number, model: IAppointmentInfoModel): Observable<any> {
        return this.httpService.put(`/Appointments/${appointmentId}/Info`, model);
    }

    public saveInfo (
        appointmentId: number,
        model: IAppointmentInfoModel,
        saveMode: SaveMode
    ): Observable<any> {
        return (saveMode === 'create') ?
            this.createInfo(model) :
            this.editInfo(appointmentId, model);
    }

    public getVisitorList (): Observable<IInvitedUser[]> {
        return this.httpService.get(`/Users/UsersForAppointment`);
    }

    public getGreeterList (visitorId: number): Observable<IStaffUser[]> {
        return this.httpService.get(`/Accounts/GreetersForAppointment?userId=${visitorId}`);
    }

    public getGreeterDates (visitorId: number, greeterId: number): Observable<string[]> {
        return this.httpService.get(
            `/Schedules/AvailableDates?userId=${visitorId}&greeterId=${greeterId}`
        );
    }

    public getVisitorsByAppointmentDate (info: Partial<IAppointmentInfoModel>): Observable<IInvitedUser[]> {
        return this.httpService.get<IInvitedUser[]>(
            `/Appointments/OtherVisitors?visitor=${info.visitor}&greeter=${info.greeter}&date=${info.date}`
        );
    }

    public getGreeterTimes (
        visitorId: number,
        greeterId: number,
        appointmentDate: string
    ): Observable<string[]> {
        return this.httpService.get(
            `/Schedules/AvailablePeriods?userId=${visitorId}&greeterId=${greeterId}&appointmentDate=${appointmentDate}`
        );
    }

    public getScheduleWithRepeatingAppointments (data: IRepeatingAppointmentSettings): Observable<IPlannedAppointment[]> {
        return this.httpService.get<IPlannedAppointment[]>(
            // tslint:disable-next-line: max-line-length
            `/Schedules/DatesForRepeatingAppointments?value=${JSON.stringify(data)}`
        )
        .pipe(
            flatMap(plannedAppointments => plannedAppointments),
            map(appointment => {
                appointment.date = DateMethod.getLocalJsonTimeString(new Date(appointment.date));
                return appointment;
            }),
            toArray()
        );
    }

    // tslint:disable-next-line: max-line-length
    public getUpdatedInfoAboutRepeatingAppointment (visitorId: number, greeterId: number, repeatingAppointmentDate: string): Observable<IPlannedAppointment> {
        return this.httpService.get(
            `/Schedules/IsOnSchedule?visitorId=${visitorId}&greeterId=${greeterId}&date=${repeatingAppointmentDate}`
        );
    }
}
