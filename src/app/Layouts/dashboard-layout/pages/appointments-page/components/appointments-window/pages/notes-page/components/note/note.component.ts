import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IAppointmentNote } from '../../interfaces/appointment-note.interface';
import { Roles } from '@Shared/roles';
import { NoteTypeEnum, INoteType, NoteTypeList } from '../../shared/note-type';

@Component({
    selector: 'app-note',
    templateUrl: './note.component.html',
    styleUrls: ['./note.component.scss']
})
export class NoteComponent implements OnInit {
    @Input() public data: IAppointmentNote;

    @Output() public OnDeleting: EventEmitter<IAppointmentNote> = new EventEmitter();

    public readonly Roles: typeof Roles = Roles;
    public readonly NoteType: typeof NoteTypeEnum = NoteTypeEnum;
    public readonly NoteTypeList: { [key: number]: INoteType } = NoteTypeList;

    constructor () {}

    ngOnInit () {}

    public delete (): void {
        if (this.data.noteType !== NoteTypeEnum.Comment) {
            return;
        }

        this.OnDeleting.emit(this.data);
    }
}
