export interface IAppointmentDetailsModel {
    status: number;
    hiddenStatusId: number;
    hiddenStatus: string;
}
