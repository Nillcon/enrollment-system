import { Component, OnInit, Input, forwardRef, Output, EventEmitter, Injector } from '@angular/core';
import { FormGroup, FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { IAppointmentRepeatType, getAppointmentRepeatTypesAsArray, AppointmentRepeatTypeEnum } from './shared/repeating-type';
import { DateMethod } from '@Methods/date.method';
import { AppointmentsInfoService } from '../../services/appointments-info.service';
import { IPlannedAppointment } from '../../interfaces/planned-appointment-model.interface';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';

@Component({
    selector: 'app-repeating-appointment-block',
    templateUrl: './repeating-appointment-block.component.html',
    styleUrls: ['./repeating-appointment-block.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RepeatingAppointmentBlockComponent),
            multi: true
        }
    ]
})
export class RepeatingAppointmentBlockComponent implements OnInit, ControlValueAccessor {
    public repeatTypes: IAppointmentRepeatType[] = getAppointmentRepeatTypesAsArray();

    @Input() public greeterDatesListAsDates: Date[];

    @Input() public set appointmentVisitorId (visitorId: number) {
        this.appointmentVisitorIdFormControl.setValue(visitorId);
    }
    @Input() public set appointmentGreeterId (greeterId: number) {
        this.appointmentGreeterIdFormControl.setValue(greeterId);
    }
    @Input() public set appointmentDate (date: string) {
        this.appointmentDateFormControl.setValue(date);

        if (this.untilDateFormControl) {
            this.untilDateFormControl.setValue(DateMethod.plusDays(new Date(date), 7));
        }
    }

    @Output() public change: EventEmitter<IPlannedAppointment[]> = new EventEmitter();

    public recurringAppointmentFormGroup: FormGroup;
    public typeFormControl: FormControl;
    public rateFormControl: FormControl;
    public untilDateFormControl: FormControl;
    public amountFormControl: FormControl;

    public appointmentVisitorIdFormControl: FormControl = new FormControl();
    public appointmentGreeterIdFormControl: FormControl = new FormControl();
    public appointmentDateFormControl: FormControl      = new FormControl();

    public isScheduleLoading: boolean      = false;
    public isRepeatingAppointment: boolean = false;

    public plannedAppointments: IPlannedAppointment[];

    constructor (
        public injector: Injector,
        private infoService: AppointmentsInfoService
    ) {}

    ngOnInit () {
        this.initForm();
    }

    public getDaysCountInputPlaceholder (): string {
        const baseText: string = 'Every ';

        switch (this.typeFormControl.value) {
            case AppointmentRepeatTypeEnum.Daily:
                return baseText + 'day(s)';
            case AppointmentRepeatTypeEnum.Weekly:
                return baseText + 'week(s)';
            case AppointmentRepeatTypeEnum.Monthly:
                return baseText + 'month(s)';
        }
    }

    public loadSchedule (): void {
        this.isScheduleLoading = true;

        this.infoService.getScheduleWithRepeatingAppointments(this.recurringAppointmentFormGroup.value)
            .subscribe(
                (plannedAppointments) => {
                    this.plannedAppointments = plannedAppointments;
                    this.isScheduleLoading   = false;
                    this.writeValue();
                    console.log(this.plannedAppointments);
                },
                () => {
                    this.isScheduleLoading = false;
                }
            );
    }

    public prepeareDatesClasses = (currentDate: Date): string => {
        const currentCalendarTime: number = currentDate.getTime();
        const currentDateIndex: number    = this.greeterDatesListAsDates.findIndex(currDate => {
            return currDate.getTime()   === currentCalendarTime;
        });

        return (currentDateIndex !== -1) ? 'greeter_date' : undefined;
    }

    @NeedsConfirmation('Do you really want to delete this appointment?', 'Confirm deleting')
    public removePlannedAppointment (appointmentIndex: number): void {
        this.plannedAppointments.splice(appointmentIndex, 1);
    }

    public onPlannedAppointmentDateTimeChange (appointmentIndex: number, date: Date): void {
        const currDate: Date = (typeof date === 'string') ? new Date(date) : date;
        const currAppointment: IPlannedAppointment = this.plannedAppointments[appointmentIndex];
        currAppointment.date = DateMethod.getLocalJsonTimeString(currDate);

        currAppointment.isInfoUpdating = true;

        this.infoService.getUpdatedInfoAboutRepeatingAppointment(
            this.appointmentVisitorIdFormControl.value,
            this.appointmentGreeterIdFormControl.value,
            currAppointment.date
        )
            .subscribe(updatedAppointment => {
                this.plannedAppointments[appointmentIndex] = updatedAppointment;
            });
    }

    public registerOnChange (fn: Function): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (plannedAppointments?: IPlannedAppointment[]): void {
        if (plannedAppointments) {
            this.plannedAppointments = plannedAppointments;
        }

        this.onChange(this.plannedAppointments);
        this.change.emit(this.plannedAppointments);
    }

    public onChange: Function = () => {};

    private initForm (): void {
        this.typeFormControl      = new FormControl(AppointmentRepeatTypeEnum.Daily);
        this.rateFormControl      = new FormControl(1);
        this.untilDateFormControl = new FormControl(DateMethod.plusDays(new Date(), 7));
        this.amountFormControl    = new FormControl(3);

        this.recurringAppointmentFormGroup = new FormGroup({
            type: this.typeFormControl,
            rate: this.rateFormControl,
            untilDate: this.untilDateFormControl,
            amount: this.amountFormControl,
            visitorId: this.appointmentVisitorIdFormControl,
            greeterId: this.appointmentGreeterIdFormControl,
            firstDate: this.appointmentDateFormControl
        });
    }
}
