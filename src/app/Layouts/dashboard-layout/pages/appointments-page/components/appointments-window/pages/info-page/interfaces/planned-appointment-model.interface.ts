import { IInvitedUser } from "@Interfaces/users";

export interface IPlannedAppointment {
    date: string;
    onSchedule: boolean;
    visitors: IInvitedUser[];
    isInfoUpdating?: boolean;
}
