import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotesPageComponent } from './notes-page.component';
import { NoteComponent } from './components/note/note.component';
import { SharedModule } from '@App/app-shared.module';
import { NoteCreationComponent } from './components/note-creation/note-creation.component';
import { MatChipsModule } from '@angular/material';

@NgModule({
    declarations: [
        NotesPageComponent,
        NoteComponent,
        NoteCreationComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        MatChipsModule
    ],
    exports: [
        NotesPageComponent
    ]
})
export class NotesPageModule { }
