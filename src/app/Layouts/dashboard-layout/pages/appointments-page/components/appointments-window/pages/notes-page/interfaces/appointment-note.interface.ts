import { IBaseUser } from "@Interfaces/users";
import { NoteTypeEnum } from "../shared/note-type";

export interface IAppointmentNote {
    noteId: number;
    note: string;
    noteDate: string;
    noteType: NoteTypeEnum;
    author: IBaseUser;
}
