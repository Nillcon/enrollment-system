export enum AppointmentRepeatTypeEnum {
    Daily   = 1,
    Weekly  = 2,
    Monthly = 3
}

export interface IAppointmentRepeatType {
    id: number;
    name: string;
}

export const AppointmentRepeatTypes: { [key: number]: IAppointmentRepeatType } = {
    [AppointmentRepeatTypeEnum.Daily]: {
        id: AppointmentRepeatTypeEnum.Daily,
        name: 'Daily'
    },
    [AppointmentRepeatTypeEnum.Weekly]: {
        id: AppointmentRepeatTypeEnum.Weekly,
        name: 'Weekly'
    },
    [AppointmentRepeatTypeEnum.Monthly]: {
        id: AppointmentRepeatTypeEnum.Monthly,
        name: 'Monthly'
    }
};

export function getAppointmentRepeatTypesAsArray (): IAppointmentRepeatType[] {
    return Object.values(AppointmentRepeatTypes);
}
