export enum NoteTypeEnum {
    Comment = 0,
    CancelReason = 1,
    Feedback = 2
}

export interface INoteType {
    id: number;
    name: string;
    iconClass: string;
}

export const NoteTypeList: { [key: number]: INoteType } = {
    [NoteTypeEnum.Comment]: {
        id: NoteTypeEnum.Comment,
        name: 'Comment',
        iconClass: 'far fa-comment'
    },
    [NoteTypeEnum.CancelReason]: {
        id: NoteTypeEnum.CancelReason,
        name: 'Cancel Reason',
        iconClass: 'fas fa-ban'
    },
    [NoteTypeEnum.Feedback]: {
        id: NoteTypeEnum.Feedback,
        name: 'Feedback',
        iconClass: 'far fa-comment-alt'
    }
};
