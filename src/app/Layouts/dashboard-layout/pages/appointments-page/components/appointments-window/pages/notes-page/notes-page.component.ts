import { Component, OnInit, Input, Injector } from '@angular/core';
import { AppointmentsNotesService } from './services/appointments-notes.service';
import { MatDialogRef } from '@angular/material';
import { AppointmentsWindowComponent } from '../../appointments-window.component';
import { IAppointmentNote } from './interfaces/appointment-note.interface';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-notes-page',
    templateUrl: './notes-page.component.html',
    styleUrls: ['./notes-page.component.scss'],
    providers: [
        AppointmentsNotesService
    ]
})
export class NotesPageComponent implements OnInit {
    @Input() public dialogRef: MatDialogRef<AppointmentsWindowComponent>;
    @Input() public appointmentId: number;

    public notesList: IAppointmentNote[];

    public isCreationBlockVisible: boolean = false;

    constructor (
        public injector: Injector,
        private notesService: AppointmentsNotesService
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.notesService.getList(this.appointmentId)
            .subscribe(notes => {
                this.notesList = notes;
            });
    }

    public create (note: Partial<IAppointmentNote>): void {
        this.notesService.create(this.appointmentId, note)
            .pipe(
                tap(() => this.update())
            )
            .subscribe();
    }

    @NeedsConfirmation('Do you really want to delete note?', 'Delete note')
    public delete (note: IAppointmentNote): void {
        this.notesService.delete(this.appointmentId, note.noteId)
            .pipe(
                tap(() => this.update())
            )
            .subscribe();
    }

    public openCreationBlock (): void {
        this.isCreationBlockVisible = true;
    }

    public closeCreationBlock (): void {
        this.isCreationBlockVisible = false;
    }
}
