import { Component, OnInit, Injector } from '@angular/core';
import { MatDialog } from '@angular/material';
import { IStaffUser } from '@Interfaces/users';
import { UserDetailsWindowService } from '@Components/user-details-window/services/user-details-window.service';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { AppointmentService } from '@Components/appointment/services/appointment.service';
import { IAppointment } from '@Components/appointment/interfaces/appointment.interface';
import { IAppointmentStatus, AppointmentStatusList } from '@Components/appointment/shared/schedule-status';
import { AppointmentsWindowComponent } from '../appointments-window/appointments-window.component';
// tslint:disable-next-line: max-line-length
import { AppointmentCancelWindowComponent } from '@Components/appointment/components/appointment-cancel-window/appointment-cancel-window.component';
import { StatusesChangerWindowComponent } from '../statuses-changer-window/statuses-changer-window.component';
import { filter, tap } from 'rxjs/operators';
import { AppointmentTypeEnum } from '@Components/appointment/shared/appointment-type';
import { Roles, RolesList } from '@Shared/roles';
import { IRole } from '@Interfaces/role.interface';

@Component({
    selector: 'app-appointments-table',
    templateUrl: './appointments-table.component.html',
    styleUrls: ['./appointments-table.component.scss']
})
export class AppointmentsTableComponent implements OnInit {
    public readonly RolesList: { [key: number]: IRole } = RolesList;
    public readonly Roles: typeof Roles = Roles;
    public readonly AppointmentType: typeof AppointmentTypeEnum = AppointmentTypeEnum;
    public readonly StatusList: { [key: number]: IAppointmentStatus } = AppointmentStatusList;

    public selectedAppointments: IAppointment[] = [];
    public appointments: IAppointment[];

    public isSomeAppointmentSelected: boolean = false;

    private currentDate: Date = new Date();

    constructor (
        public injector: Injector,
        private dialog: MatDialog,
        private userInfoWindowService: UserDetailsWindowService,
        private appointmentService: AppointmentService
    ) {}

    ngOnInit () {
        this.update();
    }

    public update (): void {
        this.appointmentService.getList()
            .subscribe(data => {
                this.appointments = data;
            });
    }

    public openStatusesChangerWindow (): void {
        const statusesWindow = this.dialog.open(StatusesChangerWindowComponent, {
            width: '500px',
            data: this.selectedAppointments
        });

        statusesWindow.afterClosed()
            .pipe(
                filter(result => result === true)
            )
            .subscribe(() => this.update());
    }

    public openUserInfoWindow (staffUser: IStaffUser): void {
        this.userInfoWindowService.open(staffUser, {
            isMeetButtonVisible: false
        });
    }

    public downloadChat (appointment: IAppointment): void {
        this.appointmentService.downloadChat(appointment)
            .subscribe();
    }

    public edit (appointment: IAppointment): void {
        const appointmentWindow = this.dialog.open(AppointmentsWindowComponent, {
            width: '95%',
            height: '95%',
            data: {
                mode: 'editing',
                appointmentId: appointment.id
            }
        });

        appointmentWindow.afterClosed()
            .subscribe(() => {
                this.update();
            });
    }

    public onSelection (e: any): void {
        this.selectedAppointments = e.selectedRowsData;
    }

    public prepareRow (event: any): void {
        const appointmentData: IAppointment   = event.data;
        const rowElement: HTMLTableRowElement = event.rowElement;

        if (appointmentData) {
            if ((this.currentDate.getTime() - new Date(appointmentData.date).getTime()) >= 0) {
                rowElement.classList.add('passed_appointment');
            }
        }
    }

    @NeedsConfirmation('Are you really want to cancel this appointment?', 'Cancel appointment')
    public cancel (appointment: any): void {
        const cancelWindow = this.dialog.open(AppointmentCancelWindowComponent, {
            width: '450px',
            data: appointment.id
        });
    }

    @NeedsConfirmation('Are you really want to delete this appointment?', 'Delete appointment')
    public delete (appointment: IAppointment): void {
        this.appointmentService.delete(appointment.id)
            .pipe(
                tap(() => {
                    this.update();
                })
            )
            .subscribe();
    }
}
