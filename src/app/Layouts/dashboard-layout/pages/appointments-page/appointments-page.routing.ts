import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppointmentsPageComponent } from './appointments-page.component';

export const routes: Routes = [
    {
        path: '',
        component: AppointmentsPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppointmentsPageRoutingModule {}
