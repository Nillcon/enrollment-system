import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AudioDescriptionPageComponent } from './audio-description-page.component';
import { AudioDescriptionRoutingModule } from './audio-description-page.routing';
import { BlindPeopleAdaptationModule } from '@Components/blind-people-adaptation/blind-people-adaptation.module';

@NgModule({
  declarations: [
    AudioDescriptionPageComponent
  ],
  imports: [
    CommonModule,

    AudioDescriptionRoutingModule,
    BlindPeopleAdaptationModule
  ]
})
export class AudioDescriptionPageModule { }
