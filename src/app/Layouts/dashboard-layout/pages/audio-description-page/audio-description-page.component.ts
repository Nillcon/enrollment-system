import { Component, OnInit } from '@angular/core';
import { BlindPeopleAdaptationService } from '@Components/blind-people-adaptation/services/blind-people-adaptation.service';

@Component({
    selector: 'app-audio-description-page',
    templateUrl: './audio-description-page.component.html',
    styleUrls: ['./audio-description-page.component.scss']
})
export class AudioDescriptionPageComponent implements OnInit {

    constructor(
        private blindAdaptationService: BlindPeopleAdaptationService
    ) { }

    ngOnInit() {
        this.blindAdaptationService.Data$
            .subscribe(res => console.log(res));
        // this.blindAdaptationService.getList();
    }

}
