import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AudioDescriptionPageComponent } from './audio-description-page.component';

export const routes: Routes = [
    {
        path: '',
        component: AudioDescriptionPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AudioDescriptionRoutingModule {}
