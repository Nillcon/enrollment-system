import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTableModule, MatSortModule, MatPaginatorModule, MatSlideToggleModule, MatToolbarModule } from '@angular/material';
import { DxTreeListModule, DxCheckBoxModule, DxDataGridModule } from 'devextreme-angular';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';

@NgModule({
    imports: [
        CommonModule,

        DxDataGridModule,
        DxTreeListModule,
        DxCheckBoxModule,

        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatSlideToggleModule,
        MatToolbarModule,
        NgxMatSelectSearchModule
    ],
    exports: [
        DxDataGridModule,
        DxTreeListModule,
        DxCheckBoxModule,

        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatSlideToggleModule,
        MatToolbarModule,
        NgxMatSelectSearchModule
    ]
})
export class DashboardLayoutSharedModule { }
