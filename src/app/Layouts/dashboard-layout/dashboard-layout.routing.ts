import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardLayoutGuard } from './guards/dashboard-layout.guard';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'Chat',
        pathMatch: 'full'
    },
    {
        path: 'Index',
        loadChildren: '@Layouts/dashboard-layout/pages/index-page/index-page.module#IndexPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    },
    {
        path: 'Users',
        loadChildren: '@Layouts/dashboard-layout/pages/staff-page/staff-page.module#StaffPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    },
    {
        path: 'Visitors',
        loadChildren: '@Layouts/dashboard-layout/pages/users-page/users-page.module#UsersPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    },
    {
        path: 'Campaigns',
        loadChildren: '@Layouts/dashboard-layout/pages/campaigns-page/campaigns-page.module#CampaignsPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    },
    {
        path: 'Appointments',
        loadChildren: '@Layouts/dashboard-layout/pages/appointments-page/appointments-page.module#AppointmentsPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    },
    {
        path: 'Scheduler',
        loadChildren: '@Layouts/dashboard-layout/pages/scheduler-page/scheduler-page.module#SchedulerPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    },
    {
        path: 'Logs',
        loadChildren: '@Layouts/dashboard-layout/pages/logs-page/logs-page.module#LogsPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    },
    {
        path: 'Reports',
        loadChildren: '@Layouts/dashboard-layout/pages/reports-page/reports-page.module#ReportsPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    },
    {
        path: 'Chat',
        loadChildren: '@Layouts/dashboard-layout/pages/chat-page/chat-page.module#ChatPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    },
    {
        path: 'AudioDescription',
        loadChildren: '@Layouts/dashboard-layout/pages/audio-description-page/audio-description-page.module#AudioDescriptionPageModule',
        canActivate: [
            DashboardLayoutGuard
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class DashboardLayoutRoutingModule {}
