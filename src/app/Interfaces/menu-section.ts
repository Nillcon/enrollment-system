import { Roles } from "@Shared/roles";

export interface IMenuSection {
    text: string;
    href: string;
    permissions: Roles[];
    iconClass?: string;
}
