import { Roles } from '@Shared/roles';

export interface IBaseUser {
    id: number;
    name: string;
    role: Roles;
    userType?: Roles;
    login: string;
}
