import { IBaseUser } from './base-user.interface';
import { IHobbie } from '@Interfaces/hobby.interface';

export interface IStudent extends IBaseUser {
    hobbies: IHobbie[];
    phone: string;
    photo: string;
    video: string;
    videoId: string;
    about: string;
}
