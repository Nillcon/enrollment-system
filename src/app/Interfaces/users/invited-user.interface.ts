export interface IInvitedUser {
    id: number;
    name: string;
    campaignName: string;
    campaignId: number;
    token: string;
    url: string;
}
