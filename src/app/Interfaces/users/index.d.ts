export * from './base-user.interface';
export * from './student.interface';
export * from './teacher.interface';
export * from './staff.interface';
export * from './user.interface';
export * from './invited-user.interface';
