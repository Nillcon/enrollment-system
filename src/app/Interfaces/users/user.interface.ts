import { IBaseUser } from './base-user.interface';

export interface IUser extends IBaseUser {
    id: number;
    isAllowAnonymous: boolean;
    chatLink: string;
    welcomeMessage: string;
    browserTitleText: string;
    backgroundColor: string;
}
