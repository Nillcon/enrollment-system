import { ITeacher } from './teacher.interface';
import { IStudent } from './student.interface';

export interface IStaffUser extends ITeacher, IStudent {
    password?: string;
    email?: string;
    tags?: string[];
    capacity?: number;
    color?: string;
    isADUser?: boolean;
}
