import { IBaseUser } from './base-user.interface';
import { IHobbie } from '@Interfaces/hobby.interface';

export interface ITeacher extends IBaseUser {
    hobbies: IHobbie[];
    phone: string;
    photo: string;
    video: string;
    videoId: string;
    about: string;
}
