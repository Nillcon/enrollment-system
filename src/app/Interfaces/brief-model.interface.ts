export interface IBriefModel {
    id: number;
    name: string;
}
