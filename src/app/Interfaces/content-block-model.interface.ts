import { IStaffUser } from "@Interfaces/users";
import { ContentTypeEnum } from "@Shared/content-type";

interface ChatButtonModel {
    id: number;
    chatId: string;
    name: string;
}

export interface IBaseContentBlockModel {
    id: number;
    name: string;
    type: ContentTypeEnum;
    isActive: boolean;
}

export interface IContentBlockModel extends IBaseContentBlockModel {
    content: any | any[];
    position: number;
    titleFont: string;
    titleColor: string;
    titleIconEnabled: boolean;
    backgroundColor: string;
    backgroundImage: string;
    tags: string[];
    leoMessage: string;
    leoPhoto: string;
    chatButton: ChatButtonModel;
    chatButtonEnabled: boolean;
    chatButtonName: string;
}
