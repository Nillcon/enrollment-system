import { ValueTypeEnum } from "@Layouts/dashboard-layout/pages/campaigns-page/shared/value-type";

export interface IVariable {
    id: number;
    name?: string;
    type?: ValueTypeEnum;
}
