import { MediaPreviewContentTypeEnum } from "@Components/media-preview/shared/content-type";

export interface IHobbie {
    id: number;
    type: MediaPreviewContentTypeEnum;
    content: string;
    preview: string;
    description: string;
}
