import { IBriefModel } from './brief-model.interface';

export interface IBriefValueModel extends IBriefModel {
    value: string;
}
