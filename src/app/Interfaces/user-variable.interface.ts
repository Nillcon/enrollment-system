import { IVariable } from './variable.interface';

export interface IUserVariable extends IVariable {
    value: string;
}
