export interface IRole {
    id: number;
    name: string;
    signInRedirectLink?: string;
}
