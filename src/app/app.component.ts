import { Component } from '@angular/core';
import { LocalStorageService } from '@Services/local-storage.service';
import { UrlService } from '@Services/url.service';
import { NavigationService } from '@Services/navigation.service';
import { ScrollService } from '@Services/scroll.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: [
        './app.component.scss'
    ]
})
export class AppComponent {
    constructor (
        private storageService: LocalStorageService,
        private urlService: UrlService,
        private navigationService: NavigationService,
        private scrollService: ScrollService
    ) {
    }
}
