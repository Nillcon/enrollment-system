import { ElementRef, Directive, TemplateRef, ViewContainerRef, OnDestroy, Input, OnInit } from "@angular/core";
import { ElementRenderableFeatures } from "@Directives/element-renderable-features";
import { Subscription } from "rxjs";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { UserService } from "@App/Services/user.service";
import { IUser } from "@Interfaces/users";
import { Roles, RolesList } from "@Shared/roles";

@AutoUnsubscribe()
@Directive({
    selector: "[AvailableFor]"
})
export class AvailableForDirective extends ElementRenderableFeatures implements OnInit, OnDestroy {
    private userData: IUser;
    private userInfoSubscription: Subscription;

    @Input('AvailableFor') private rolesList: Roles[];

    constructor (
        private element: ElementRef,
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private userService: UserService
    ) {
        super(element, templateRef, viewContainer);
    }

    ngOnInit () {
        this.userInfoSubscription = this.userService.Data$.subscribe(data => {
            this.userData = data;

            this.processElement();
        });
    }
    ngOnDestroy () {}

    private processElement () {
        const userRole: Roles           = this.userData.role;
        let isElementAvailable: boolean = false;

        try {
            if (!this.rolesList.length || this.rolesList.includes(userRole) || this.checkSuperAdminPermission(userRole)) {
                isElementAvailable = true;
            }
        } catch (err) {}

        (isElementAvailable) ? this.showElement() : this.hideElement();
    }

    private checkSuperAdminPermission(userRole: Roles) {
        return userRole === Roles.SuperUser;
    }
}
