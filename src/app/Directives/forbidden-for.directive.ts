import { ElementRef, Directive, TemplateRef, ViewContainerRef, OnDestroy, Input, OnInit } from "@angular/core";
import { ElementRenderableFeatures } from "@Directives/element-renderable-features";
import { Subscription } from "rxjs";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { UserService } from "@App/Services/user.service";
import { IUser } from "@Interfaces/users";
import { Roles } from "@Shared/roles";

@AutoUnsubscribe()
@Directive({
    selector: "[ForbiddenFor]"
})
export class ForbiddenForDirective extends ElementRenderableFeatures implements OnInit, OnDestroy {
    private userData: IUser;
    private userInfoSubscription: Subscription;

    @Input('ForbiddenFor') private rolesList: Roles[];

    constructor (
        private element: ElementRef,
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private userService: UserService
    ) {
        super(element, templateRef, viewContainer);
    }

    ngOnInit () {
        this.userInfoSubscription = this.userService.Data$.subscribe(data => {
            this.userData = data;

            this.processElement();
        });
    }
    ngOnDestroy () {}

    private processElement () {
        const userRole: Roles           = this.userData.role;
        let isElementAvailable: boolean = true;

        try {
            if (this.rolesList.includes(userRole)) {
                isElementAvailable = false;
            }
        } catch (err) {}

        (isElementAvailable) ? this.showElement() : this.hideElement();
    }
}
