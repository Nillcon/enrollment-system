import { Injectable } from '@angular/core';
import { UserService } from '@Services/user.service';

@Injectable()
export class AppConfig {
    constructor (
        private userService: UserService
    ) {}

    public load () {
        return this.userService.update()
            .toPromise();
    }
}
