import { BaseClassForCustomDecorator } from '@Interfaces/base-class-for-custom-decorator.interface';
import { ConfirmationWindowService } from '@Components/confirmation-window/confirmation-window.service';

export function NeedsConfirmation (description?: string, title?: string) {
    return function (targetClass: BaseClassForCustomDecorator, method: string, decorator: PropertyDescriptor) {
        if (!targetClass['_maPatched']) {
            targetClass['_maPatched']   = true;
            targetClass['_maEventsMap'] = [...(targetClass['_maEventsMap'] || [])];
        }

        let confirmationWindowService: ConfirmationWindowService;

        const ngOnInitUnpatched = targetClass.ngOnInit;
        const originalMethod    = decorator.value;

        targetClass.ngOnInit = function (this: BaseClassForCustomDecorator) {
            confirmationWindowService = this.injector.get(ConfirmationWindowService);

            if (ngOnInitUnpatched) {
                return ngOnInitUnpatched.call(this);
            }
        };

        decorator.value = function (...args) {
            confirmationWindowService.show(description, title)
                .subscribe(confirmationResult => {
                    if (confirmationResult) {
                        return originalMethod.apply(this, args);
                    } else {
                        return;
                    }
                });
        };

        return decorator;
    };
}
