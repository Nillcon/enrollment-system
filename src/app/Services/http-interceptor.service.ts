import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { throwError, Observable, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {

    public get OnError$ (): Observable<HttpErrorResponse> {
        return this._onError$.asObservable();
    }

    private _onError$: Subject<HttpErrorResponse> = new Subject();

    constructor () {}

    public intercept (query: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(query)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    this._onError$.next(error);

                    return throwError(error);
                }
            )
        );
    }

}
