import { Injectable } from "@angular/core";
import { HttpRequestService } from "./http-request.service";
import { Observable } from "rxjs";
import { IInvitedUser } from "@Interfaces/users";
import { IUserVariable } from "@Interfaces/user-variable.interface";
import { SaveMode } from "@Types/save-mode.type";

@Injectable({
    providedIn: 'root'
})
export class InvitedUserService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public get (invitedUserId: number): Observable<IInvitedUser> {
        return this.httpService.get<IInvitedUser>(`/Users/${invitedUserId}`);
    }

    public getList (): Observable<IInvitedUser[]> {
        return this.httpService.get<IInvitedUser[]>(`/Users`);
    }

    public create (data: Partial<IInvitedUser>): Observable<any> {
        return this.httpService.post(`/Users`, data);
    }

    public delete (invitedUserId: number): Observable<any> {
        return this.httpService.delete(`/Users/${invitedUserId}`);
    }

    public groupDelete (invitedUserIds: number[]): Observable<any> {
        return this.httpService.put(`/Users/MassRemoval`, { value: invitedUserIds });
    }

    public getVariables (invitedUserId: number): Observable<IUserVariable[]> {
        return this.httpService.get<IUserVariable[]>(`/Users/${invitedUserId}/Variables`);
    }

    public createVariable (invitedUserId: number, variable: Partial<IUserVariable>): Observable<any> {
        return this.httpService.post<any>(`/Users/${invitedUserId}/Variables`, variable);
    }

    public editVariable (invitedUserId: number, variable: IUserVariable): Observable<any> {
        return this.httpService.put<any>(`/Users/${invitedUserId}/Variables`, variable);
    }

    public saveVariable (
        invitedUserId: number,
        variable: IUserVariable,
        mode: SaveMode
    ): Observable<any> {
        return (mode === 'create') ?
            this.createVariable(invitedUserId, variable) :
            this.editVariable(invitedUserId, variable);
    }

    public deleteVariable (invitedUserId: number, variableId: number): Observable<any> {
        return this.httpService.delete(`/Users/${invitedUserId}/Variables/${variableId}`);
    }

    public uploadVariablesCsv (invitedUserId: number, variablesFile: Blob): Observable<any> {
        return this.httpService.uploadFile(`/Users/${invitedUserId}/Csv`, variablesFile);
    }
}
