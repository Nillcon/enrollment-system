import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UrlService {
    constructor(
        private router: Router
    ) { }

    public getParameters() {
        const urlParams = this.router.parseUrl(this.router.url);

        return urlParams.queryParams;
    }

    public getParameter(nameVariable: string) {
        const parameters = this.getParameters();

        return parameters[nameVariable];
    }
    public setParameter(nameParameter: string, value: string) {
        const queryParams = {};
        queryParams[nameParameter] = value;

        this.router.navigate([], {
            queryParams: queryParams,
            queryParamsHandling: 'merge'
        });
    }

    public pushValueInParameter(nameParameter: string, value: string) {
        let parametersVariable: string = this.getParameter(nameParameter);

        parametersVariable += ',' + value;

        this.setParameter(nameParameter, parametersVariable);
    }
    public removeValueInParameter(nameParameter: string, value: string) {
        const parametersVariable: string[] = this.getParameter(nameParameter).split(',');

        parametersVariable
            .filter(val => value !== val);

        this.setParameter(nameParameter, parametersVariable.join(','));
    }

    public removeParameter(nameParameter: string) {
        const queryParams = {};
        queryParams[nameParameter] = null;

        this.router.navigate([], {
            queryParams: queryParams,
            queryParamsHandling: 'merge',
        });
    }

    public clearParameters() {
        this.router.navigate([], {
            queryParams: {}
        });
    }

    public isExistParameter(nameParameter: string) {
        const valueVariable = this.getParameter(nameParameter);

        return !!valueVariable;
    }
}
