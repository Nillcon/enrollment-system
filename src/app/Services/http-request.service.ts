import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from './local-storage.service';
import { StorageKey } from '@Shared/storage-key';
import { environment } from 'src/environments/environment';
import { NotificationService } from './notification.service';

interface HttpOption {
    headers: object;
}

@Injectable({
    providedIn: 'root'
})
export class HttpRequestService {
    constructor (
        private httpService: HttpClient,
        private notifyService: NotificationService,
        private storageService: LocalStorageService
    ) {
        console.log(`HOST: ${this.getHost()}`);
    }

    public get<T> (way: string, data?: any): Observable<T> {
        return this.httpService.get<T>(this.getUrl(way), <{}>this.getHeaders());
    }

    public post<T> (way: string, data: {}): Observable<T> {
        return this.httpService.post<T>(this.getUrl(way), data, <{}>this.getHeaders());
    }

    public delete (way: string): Observable<boolean> {
        return this.httpService.delete<boolean>(this.getUrl(way), <{}>this.getHeaders());
    }

    public put<T> (way: string, data: {}): Observable<T> {
        return this.httpService.put<T>(this.getUrl(way), data, <{}>this.getHeaders());
    }

    public patch<T> (way: string, data: {}): Observable<T> {
        return this.httpService.patch<T>(this.getUrl(way), data, <{}>this.getHeaders());
    }

    public uploadFile<T> (way: string, files: Blob | Blob[]): Observable<T> {
        const formData: FormData = new FormData();

        if (Array.isArray(files)) {
            files.forEach((file, index) => {
                formData.append(`file-${index}`, file);
            });
        } else {
            formData.append('file-0', files);
        }

        return this.httpService.post<T>(this.getUrl(way), formData, <{}>this.getHeaders());
    }

    public downloadFile (
        way: string,
        params = {
            name: 'File',
            type: '.csv'
        }
    ): Observable<Blob> {
        return this.httpService.get(this.getUrl(way), {
            headers: {
                'Authorization': this.getToken()
            },
            responseType: 'blob',
            observe: 'response'
        })
            .pipe(
                tap((response) => {
                    if (typeof(window.navigator.msSaveOrOpenBlob) === 'function') {
                        /** File download in Edge (Maybe also in IE) */
                        window.navigator.msSaveOrOpenBlob(response.body, params.name + params.type);
                    } else {
                        /** File download in Chrome, Firefox and other browsers */
                        const urlAddress = window.URL.createObjectURL(response.body);
                        const a = document.createElement('a');
                        document.body.appendChild(a);
                        a.setAttribute('style', 'display: none');
                        a.href = urlAddress;
                        a.download = params.name + params.type;
                        a.click();
                        window.URL.revokeObjectURL(urlAddress);
                        a.remove();
                    }
                }),
                map(response => response.body)
            );
    }

    public getHost (): string {
        const debugHost: string = this.storageService.get<string>(StorageKey.Debug_Host);
        const host: string   = debugHost || environment.host || location.origin;

        return host;
    }

    public getHeaders (): HttpOption {
        const httpOptions: HttpOption = {
            headers: {}
        };

        const access_key = this.storageService.get(StorageKey.Access_Token);
        if (access_key) {
            httpOptions.headers['Authorization'] = this.getToken();
        }

        return httpOptions;
    }

    public getToken (): string {
        return `Bearer ${this.storageService.get(StorageKey.Access_Token)}`;
    }

    private getUrl (way: string): string {
        const baseUrl: string = this.getHost();
        let keyword: string[] = ['http://', 'https://'];

        keyword = keyword
        .filter(elem => way.includes(elem));

        way = `/api${way}`;

        return (keyword.length) ? way : `${baseUrl}${way}`;
    }

}
