import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularBootstrapToastsService } from 'angular-bootstrap-toasts';
import { ToastMessageParams } from 'angular-bootstrap-toasts/lib/Models/toast-message.models';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    private readonly defaultParametes: {} = {
        pauseDurationOnMouseEnter: true,
    };

    constructor (
        private toasts: AngularBootstrapToastsService
    ) {
        this.toasts.changeDefaultDuration(2000);
    }

    public info (
        title: string,
        text: string,
        duration?: number
    ): Observable<boolean> {
        return this.makeNotification({
            title: title,
            text: text,
            duration: duration,
            toastClass: 'advanced-info-toast'
        });
    }

    public success (
        title?: string,
        text?: string,
        duration?: number
    ): Observable<boolean> {
        return this.makeNotification({
            title: title || 'Success',
            text: text || 'Data successfully updated!',
            duration: duration,
            toastClass: 'advanced-success-toast'
        });
    }

    public warning (
        title: string,
        text: string,
        duration?: number
    ): Observable<boolean> {
        return this.makeNotification({
            title: title,
            text: text,
            duration: duration,
            toastClass: 'advanced-warning-toast'
        });
    }

    public error (
        title: string,
        text: string,
        duration?: number
    ): Observable<boolean> {
        return this.makeNotification({
            title: title,
            text: text,
            duration: duration,
            toastClass: 'advanced-error-toast'
        });
    }

    public confirm (
        title: string,
        text: string,
        duration: number = Infinity
    ): Observable<boolean> {
        return this.makeNotification({
            title: title,
            text: text,
            duration: duration,
            closeButtonClass: 'info-close',
            toolbarClass: 'confirm-toolbar',
            toastClass: 'advanced-base-toast advanced-confirm-toast',
            toolbarItems: {
                actionButton: {
                text: 'Подтвердить',
                visible: true,
                class: 'yes-confirm-button'
                },
                cancelButton: {
                text: 'Отмена',
                visible: true,
                class: 'no-confirm-button'
                }
            }
        });
    }

    private makeNotification (params: ToastMessageParams): Observable<boolean> {
        const parameters = Object.assign(params, this.defaultParametes);
        const toast = this.toasts.showConfirmToast(parameters);

        return toast.ConfirmationResult$;
    }
}
