import { Injectable, OnDestroy, NgZone } from "@angular/core";
import { Title } from "@angular/platform-browser";
import { Router, NavigationEnd, ActivatedRoute, Params } from "@angular/router";
import { Subscription, BehaviorSubject, Observable } from "rxjs";
import { filter, map, tap } from "rxjs/operators";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { UrlService } from "./url.service";
import { IUrlData } from '@Interfaces/url-data.interface';

@AutoUnsubscribe()
@Injectable({
    providedIn: 'root'
})
export class NavigationService implements OnDestroy {
    private navigationSubscription: Subscription;

    private _currentPageData: BehaviorSubject<IUrlData> = new BehaviorSubject({});
    public get CurrentPageData (): IUrlData {
        return this._currentPageData.value;
    }
    public get CurrentPageData$ (): Observable<IUrlData> {
        return this._currentPageData.asObservable();
    }

    constructor (
        private zone: NgZone,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private title: Title,
        private urlService: UrlService
    ) {
        this.navigationObserver();
    }

    ngOnDestroy () {}

    public navigate (
        subUrl: string,
        relativeToCurrentPath: boolean = true,
        params: Params = null
    ): Promise<boolean> {
        return this.zone.run(() => {
            return this.router.navigate(
                [subUrl],
                {
                    relativeTo: (relativeToCurrentPath) ? this.activatedRoute : null,
                    queryParams: params || this.urlService.getParameters()
                }
            );
        });
    }

    public async navigateWithFullReload (
        subUrl: string,
        params: Params = null
    ): Promise<boolean> {
        await this.navigate('Reload', false);
        return this.navigate(subUrl, false, params);
    }

    private navigationObserver (): void {
        this.navigationSubscription = this.router.events.pipe(
            filter((event) => event instanceof NavigationEnd),
            tap(() => {
                this._currentPageData.next({});
            }),
            map(() => this.activatedRoute),
            map((route) => {
                const pageData: IUrlData = {};

                while (route.firstChild) {
                    const routeData: IUrlData = route.firstChild.snapshot.data || {};

                    if (routeData && Object.keys(routeData).length) {
                        if (routeData.title) {
                            pageData.title = routeData.title;
                        }
                    }

                    route = route.firstChild;
                }

                return pageData;
            })
        ).subscribe((urlData: IUrlData) => {
            if (urlData.title) {
                this.title.setTitle(urlData.title);
            }

            this._currentPageData.next(urlData);
        });
    }
}
