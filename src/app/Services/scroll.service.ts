import { Injectable } from '@angular/core';
import { filter, debounce } from 'rxjs/operators';
import { Observable, timer, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ScrollService {
    private scrollEvents: Subject<Event> = new Subject();

    public get NewScrollEvent$ (): Observable<Event> {
        return this.scrollEvents.asObservable().pipe(
            filter(event => event !== null)
        );
    }

    public get LastScrollEventEnd$ (): Observable<Event> {
        return this.NewScrollEvent$.pipe(
            debounce(() => timer(500))
        );
    }

    constructor () {
        window.addEventListener('scroll', (e) => this.onScroll(e));
    }

    private onScroll (event: any): void {
        this.scrollEvents.next(event);
    }
}
