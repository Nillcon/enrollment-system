import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ViewportService {
    constructor () {}

    public isElementInViewport (elem: HTMLElement): boolean {
        const elementRect = elem.getBoundingClientRect();
        return (
            elementRect.top >= 0 &&
            elementRect.left >= 0 &&
            elementRect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            elementRect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    public isAnyPartOfElementInViewport (elem: HTMLElement): boolean {
        const elementRect  = elem.getBoundingClientRect();
        const windowHeight = (window.innerHeight || document.documentElement.clientHeight);
        const windowWidth  = (window.innerWidth || document.documentElement.clientWidth);
        const vertInView   = (elementRect.top <= windowHeight) && ((elementRect.top + elementRect.height) >= 0);
        const horInView    = (elementRect.left <= windowWidth) && ((elementRect.left + elementRect.width) >= 0);

        return (vertInView && horInView);
    }
}
