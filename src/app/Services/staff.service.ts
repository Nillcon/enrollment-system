import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestService } from './http-request.service';
import { IStaffUser } from '@Interfaces/users/staff.interface';
import { IBaseUser } from '@Interfaces/users';
import { IHobbie } from '@Interfaces/hobby.interface';
import { Roles } from '@Shared/roles';
import { MediaPreviewContentTypeEnum } from '@Components/media-preview/shared/content-type';

@Injectable({
    providedIn: 'root'
})
export class StaffService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    /** Get info about staff user (Teacher or Student) */
    public get (staffUserId: number): Observable<IStaffUser> {
        return this.httpService.get<IStaffUser>(`/Greeters/${staffUserId}`);
    }

    /** Get staff list for main page */
    public getStaffList (): Observable<any> {
        return this.httpService.get<any>(`/Greeters`);
    }

    /** Get list of staff users (Teachers/Students/Support/Super admins and other...) */
    public getList (filterByRoles?: Roles[]): Observable<IStaffUser[]> {
        let variables: string = '';

        if (filterByRoles && filterByRoles.length) {
            variables += `?filterByRoles=${JSON.stringify(filterByRoles)}`;
        }

        return this.httpService.get<IStaffUser[]>(`/Accounts${variables}`);
    }

    /** Get personal list of teachers for current visitor on index page [Legacy] */
    public getTeachersList (): Observable<IStaffUser[]> {
        return this.httpService.get<IStaffUser[]>(`/Greeters/Staff`);
    }

    /** Get personal list of students for current visitor on index page [Legacy] */
    public getStudentsList (): Observable<IStaffUser[]> {
        return this.httpService.get<IStaffUser[]>(`/Greeters/Students`);
    }

    /** Get staff users by part of his name/login */
    public search (searchString: string): Observable<IStaffUser[]> {
        return this.httpService.get<IStaffUser[]>(`/Accounts/Search?searchString=${searchString}`);
    }

    /** Create staff user with any role instead "Super Admin" */
    public create (data: IBaseUser): Observable<any> {
        return this.httpService.post(`/Accounts`, data);
    }

    /** Get info about staff user for admin details window */
    public getInfo (staffUserId: number): Observable<Partial<IStaffUser>> {
        return this.httpService.get<Partial<IStaffUser>>(`/Accounts/${staffUserId}/Info`);
    }

    /** Get media about staff user for admin details window */
    public getMedia (staffUserId: number): Observable<Partial<IStaffUser>> {
        return this.httpService.get<Partial<IStaffUser>>(`/Accounts/${staffUserId}/Media`);
    }

    /** Save info about staff user for admin details window */
    public saveInfo (staffUserId: number, data: Partial<IStaffUser>): Observable<any> {
        return this.httpService.put(`/Accounts/${staffUserId}/Info`, data);
    }

    /** Delete staff user (Available for admin only) */
    public delete (staffUserId: number): Observable<any> {
        return this.httpService.delete(`/Accounts/${staffUserId}`);
    }

    /** Change avatar for staff user as file (Available for admin only) */
    public changeAvatarAsFile (staffUserId: number, file: Blob): Observable<any> {
        return this.httpService.uploadFile(`/Accounts/${staffUserId}/Photo`, file);
    }

    /** Change avatar for staff user as base64 (Available for admin only) */
    public changeAvatarAsBase64 (staffUserId: number, base64: string): Observable<any> {
        return this.httpService.post(`/Accounts/${staffUserId}/PhotoManually`, { base64image: base64 });
    }

    /** Change video for staff user (Available for admin only) */
    public changeVideo (staffUserId: number, file: Blob): Observable<any> {
        return this.httpService.uploadFile(`/Accounts/${staffUserId}/Video`, file);
    }

    /** Change video link for staff user (Available for admin only) */
    public changeVideoLink (staffUserId: number, url: string): Observable<any> {
        return this.httpService.put(`/Accounts/${staffUserId}/VideoLink`, { value: url });
    }

    /** Get hobbies about staff user for admin details window */
    public getHobbies (staffUserId: number): Observable<IHobbie[]> {
        return this.httpService.get<IHobbie[]>(`/Accounts/${staffUserId}/Hobbies`);
    }

    /** Add hobby of staff user for admin details window as image */
    public addHobbieAsImage (staffUserId: number, file: Blob): Observable<any> {
        return this.httpService.uploadFile(`/Accounts/${staffUserId}/Hobbies/Image`, file);
    }

    /** Add hobby of staff user for admin details window as video */
    public addHobbieAsVideo (staffUserId: number, link: string): Observable<any> {
        return this.httpService.post(`/Accounts/${staffUserId}/Hobbies/Youtube`, {
            content: link,
            type: MediaPreviewContentTypeEnum.YoutubeVideo
        });
    }

    /** Edit hobby of staff user for admin details window */
    public editHobbie (staffUserId: number, hobby: IHobbie): Observable<any> {
        return this.httpService.put(`/Accounts/${staffUserId}/Hobbies/${hobby.id}/Description`, hobby);
    }

    /** Delete hobby of staff user for admin details window */
    public deleteHobbie (staffUserId: number, hobbieId: number): Observable<any> {
        return this.httpService.delete(`/Accounts/${staffUserId}/Hobbies/${hobbieId}`);
    }
}
