import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { HttpRequestService } from './http-request.service';
import { tap, map, switchMap, filter } from 'rxjs/operators';
import { IUser } from '@Interfaces/users/user.interface';
import { Roles } from '@Shared/roles';
import { AuthorizeService } from './authorize.service';
import { SessionStorageService } from './session-storage.service';
import { SessionKey } from '@Shared/session-key';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private _data: BehaviorSubject<IUser> = new BehaviorSubject(null);
    private _chatLink$: BehaviorSubject<string> = new BehaviorSubject(null);

    public get Data$ (): Observable<IUser> {
        return this._data.asObservable();
    }

    public get Data (): IUser {
        return this._data.value;
    }

    public get ChatLink$ (): Observable<string> {
        return this._chatLink$.asObservable()
            .pipe(
                filter(chatLink => chatLink !== null)
            );
    }

    public get ChatLink (): string {
        return this._chatLink$.value;
    }

    constructor (
        private httpService: HttpRequestService,
        private authorizeService: AuthorizeService,
        private sessionService: SessionStorageService
    ) {}

    public update (): Observable<IUser> {
        return this.httpService.get<IUser>(`/Accounts/Info`)
        .pipe(
            switchMap((user) => {
                switch (user.role) {
                    case Roles.Anonymous:
                        // Authorizing anonymous user and setting they token into localStorage
                        const isNeedAuthorize: boolean = this.sessionService.get(SessionKey.Is_Need_Authorize);

                        this.sessionService.remove(SessionKey.Is_Need_Authorize);

                        if (
                            (typeof(isNeedAuthorize) === 'boolean' && isNeedAuthorize)
                            ||
                            (typeof(isNeedAuthorize) !== 'boolean')
                        ) {
                            return this.authorizeService.authorize(user.id)
                            .pipe(
                                map(() => user)
                            );
                        } else {
                            return of(user);
                        }
                    default:
                        return of(user);
                }
            }),
            tap(user => {
                this._data.next(user);
            })
        );
    }

    public updateChatLink (): Observable<string> {
        return this.httpService.get<string>(`/Accounts/ChatLink`)
            .pipe(
                tap((chatLink) => this._chatLink$.next(chatLink))
            );
    }

}
