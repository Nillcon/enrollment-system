import { Injectable } from '@angular/core';
import { SessionKey } from '@Shared/session-key';

@Injectable({
    providedIn: 'root'
})
export class SessionStorageService {
    constructor () {}

    public get<T> (key: SessionKey): T {
        const data = sessionStorage.getItem(key);

        return (data) ? JSON.parse(data) : null;
    }

    public set (key: SessionKey, data: any) {
        const body = JSON.stringify(data);
        sessionStorage.setItem(key, body);
    }

    public remove (key: SessionKey) {
        sessionStorage.removeItem(key);
    }

    public clear () {
        sessionStorage.clear();
    }
}
