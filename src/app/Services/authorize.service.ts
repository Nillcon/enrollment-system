import { Injectable } from '@angular/core';
import { HttpRequestService } from './http-request.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LocalStorageService } from './local-storage.service';
import { StorageKey } from '@Shared/storage-key';
import { ISignInData } from '@Interfaces/authorization/sign-in-data.interface';
import { SessionStorageService } from './session-storage.service';
import { SessionKey } from '@Shared/session-key';

@Injectable({
    providedIn: 'root'
})
export class AuthorizeService {
    constructor (
        private httpService: HttpRequestService,
        private storageService: LocalStorageService,
        private sessionService: SessionStorageService
    ) {}

    public authorize (userId: number): Observable<string> {
        return this.httpService.get<string>(`/Users/Authorize?userId=${userId}`)
            .pipe(
                tap(token => {
                    this.storageService.set(StorageKey.Access_Token, token);
                })
            );
    }

    public signOut ({
        redirectUrl = location.origin,
        isNeedAuthorizeAfterRedirect = true
    } = {}): void {
        this.storageService.remove(StorageKey.Access_Token);
        location.href = redirectUrl;

        if (typeof(isNeedAuthorizeAfterRedirect) === 'boolean') {
            this.sessionService.set(SessionKey.Is_Need_Authorize, isNeedAuthorizeAfterRedirect);
        }
    }

    public signIn (data: ISignInData) {
        return this.httpService.post<string>(`/Accounts/SignIn`, data).pipe(
            tap(token => {
                this.storageService.set(StorageKey.Access_Token, token);
            })
        );
    }
}
