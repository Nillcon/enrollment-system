import { DomSanitizer } from '@angular/platform-browser';
import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'safeUrl'
})
export class SafeUrlPipe implements PipeTransform  {
    constructor (
        private sanitizer: DomSanitizer
    ) {}

    transform (url: string) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}
