import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SharedModule } from './app-shared.module';
import { AppRoutingModule } from './app.routing';
import { ConfirmationWindowModule } from '@Components/confirmation-window/confirmation-window.module';
import { AppConfig } from './app.config';
import { AngularBootstrapToastsModule } from 'angular-bootstrap-toasts';
import { UserDetailsWindowModule } from '@Components/user-details-window/user-details-window.module';
import { MainLayoutModule } from '@Layouts/main-layout/main-layout.module';
import { HttpInterceptorService } from '@Services/http-interceptor.service';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        AngularBootstrapToastsModule,

        AppRoutingModule,

        SharedModule,

        ConfirmationWindowModule,
        UserDetailsWindowModule,

        // FIXME: Remove (But the chat on the main page will break)
        MainLayoutModule
    ],
    providers: [
        AppConfig,
        HttpInterceptorService,
        { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => config.load(), deps: [AppConfig], multi: true },
        {
            provide: HTTP_INTERCEPTORS,
            useExisting: HttpInterceptorService,
            multi: true
        }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
