import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { AuthorizeService } from '@Services/authorize.service';
import { UserService } from '@Services/user.service';
import { Roles, RolesList } from '@Shared/roles';
import { NavigationService } from '@Services/navigation.service';
import { HttpInterceptorService } from '@Services/http-interceptor.service';
import { NotificationService } from '@Services/notification.service';
import { Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-sign-in-page',
    templateUrl: './sign-in-page.component.html',
    styleUrls: ['./sign-in-page.component.scss']
})
export class SignInPageComponent implements OnInit {
    public login: FormControl = new FormControl('', [
        Validators.required,
        Validators.maxLength(25)
    ]);

    public password: FormControl = new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(127)
    ]);

    public signInForm: FormGroup = new FormGroup({
        login: this.login,
        password: this.password
    });

    public isShowPassword: boolean = false;

    public httpErrorSubscription: Subscription;
    public isLoad: boolean = false;

    constructor (
        private userService: UserService,
        private navigationService: NavigationService,
        private authorizationService: AuthorizeService,
        private httpInterceptorService: HttpInterceptorService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        this.httpErrorObserver();
    }

    public signIn() {
        if (this.signInForm.valid) {
            this.isLoad = true;

            this.authorizationService.signIn({
                login: this.login.value,
                password: this.password.value
            })
            .subscribe(
                () => {
                    this.userService.update()
                        .subscribe((userData) => {
                            switch (userData.role) {
                                case Roles.SuperUser:
                                case Roles.Student:
                                case Roles.Support:
                                case Roles.Staff:
                                    const redirectLink: string = RolesList[userData.role].signInRedirectLink;
                                    this.navigationService.navigate(`/Dashboard/${redirectLink}`);
                                    break;
                                default:
                                    this.navigationService.navigate('/');
                                    break;
                            }
                        });
                },
                () => {
                    this.hideLoader();
                }
            );
        } else {
            this.login.markAsTouched();
            this.password.markAsTouched();
        }
    }

    private httpErrorObserver (): void {
        const errorNotifyDuration: number = 7000;

        this.httpErrorSubscription = this.httpInterceptorService.OnError$
            .pipe(
                tap((error) => {
                    if (error.status === 500) {
                        this.notificationService.error(
                            'Error',
                            error.error || error.message,
                            errorNotifyDuration
                        );
                    } else if (error.status === 422) {
                        this.notificationService.warning(
                            'Warning',
                            error.error || error.message,
                            errorNotifyDuration
                        );
                    }
                })
            )
            .subscribe();
    }

    public showPassword() {
        this.isShowPassword = !this.isShowPassword;
    }

    private hideLoader() {
        this.isLoad = false;
    }
}
