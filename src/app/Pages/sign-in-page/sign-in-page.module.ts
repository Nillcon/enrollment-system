import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInPageComponent } from './sign-in-page.component';
import { SignInPageRoutingModule } from './sign-in-page.routing';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        SignInPageComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        SignInPageRoutingModule
    ]
})
export class SignInPageModule { }
