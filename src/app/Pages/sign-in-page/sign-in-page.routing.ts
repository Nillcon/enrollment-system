import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { SignInPageComponent } from './sign-in-page.component';

export const routes: Routes = [
    {
        path: '',
        component: SignInPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SignInPageRoutingModule {}
