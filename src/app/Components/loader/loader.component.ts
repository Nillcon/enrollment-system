import { Component, OnInit, Input } from '@angular/core';

export type LoaderTypes = 'Circular' | 'LineDots';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

    @Input() loaderType: LoaderTypes = 'Circular';

    constructor() { }

    ngOnInit() {
    }

}
