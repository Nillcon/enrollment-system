import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@App/app-shared.module';

import { RandgeDateComponent } from './randge-date.component';

@NgModule({
    declarations: [
        RandgeDateComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
    ],
    exports: [
        RandgeDateComponent
    ]
})
export class RangeDateModule { }
