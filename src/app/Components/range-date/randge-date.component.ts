import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

import { IDateRange } from './interfaces/date-range.interface';

@Component({
  selector: 'app-randge-date',
  templateUrl: './randge-date.component.html',
  styleUrls: ['./randge-date.component.scss']
})
export class RandgeDateComponent implements OnInit {

    @Input() inputDate: IDateRange = {
        from: null,
        to: null
    };

    @Output() OnChange: EventEmitter<IDateRange> = new EventEmitter();

    public dateFrom: FormControl;
    public dateTo: FormControl;

    constructor() { }

    ngOnInit() {
        this.dateFrom = new FormControl(this.inputDate.from, []);
        this.dateTo = new FormControl(this.inputDate.to, []);
    }

    public changeDate() {
        this.OnChange.emit(<IDateRange>{
            from: this.dateFrom.value,
            to: this.dateTo.value
        });
    }

}
