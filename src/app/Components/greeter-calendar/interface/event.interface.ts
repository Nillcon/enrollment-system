export interface IGreeterCalendarEvent {
    text: string;
    greeterId: number;
    startDate: Date | string;
    endDate: Date | string;
}
