import { IStaffUser } from "@Interfaces/users";
import { IGreeterCalendarEvent } from "./event.interface";

export interface IGreeterCalendarInfo {
    greeters: Partial<IStaffUser>[];
    data: IGreeterCalendarEvent[];
}
