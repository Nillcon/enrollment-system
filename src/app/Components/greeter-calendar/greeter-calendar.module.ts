import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@App/app-shared.module';
import { DxSchedulerModule } from 'devextreme-angular';
import { GreeterCalendarComponent } from './greeter-calendar.component';

@NgModule({
    declarations: [
        GreeterCalendarComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DxSchedulerModule
    ],
    exports: [
        GreeterCalendarComponent
    ]
})
export class GreeterCalendarModule {}
