import { Component, OnInit, Input, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import DataSource from 'devextreme/data/data_source';
import { IStaffUser } from '@Interfaces/users';
import { IGreeterCalendarInfo } from './interface/info.interface';
import { IGreeterCalendarEvent } from './interface/event.interface';
import { DxSchedulerComponent } from 'devextreme-angular';

@Component({
    selector: 'app-greeter-calendar',
    templateUrl: './greeter-calendar.component.html',
    styleUrls: ['./greeter-calendar.component.scss']
})
export class GreeterCalendarComponent implements OnInit, AfterViewInit {

    @Input()
    public currentDate: Date = new Date();

    @Input()
    public set data (data: IGreeterCalendarInfo) {
        this.initGreeters(data.greeters);
        this.initDataSource(data.data);
    }

    @Output()
    public OnCurrentDateChanged: EventEmitter<Date> = new EventEmitter();

    @Output()
    public OnGreeterBlockClick: EventEmitter<IStaffUser> = new EventEmitter();

    public dataSource: DataSource;
    public greeters: Partial<IStaffUser>[] = [];

    private _currentDate: Date = new Date();

    @ViewChild(DxSchedulerComponent)
    private _schedulerComponent: DxSchedulerComponent;

    constructor () {}

    public ngOnInit (): void {}

    public ngAfterViewInit (): void {
        this.initCalendarScrollByWheel();
    }

    public onOptionChanged (event: any): void {
        if (event.name === 'currentDate') {
            const currDate: Date = event.value;
            const prevDate: Date = event.previousValue;

            if (!prevDate || (currDate.toLocaleDateString() !== prevDate.toLocaleDateString())) {
                const currDatePrepeared: Date = currDate;

                setTimeout(() => this.OnCurrentDateChanged.emit(currDatePrepeared));
            }
        }
    }

    private initCalendarScrollByWheel (): void {
        const calendarBlock: HTMLElement = (this._schedulerComponent as any).element.nativeElement;

        calendarBlock.addEventListener(
            "mousewheel",
            (e) => {
                const calendarScrollBlock: HTMLElement = calendarBlock.querySelector('.dx-scrollable-container');
                this.scrollCalendarHorizontaly(e, calendarScrollBlock);
            },
            false
        );

        calendarBlock.addEventListener(
            "DOMMouseScroll",
            (e) => {
                const calendarScrollBlock: HTMLElement = calendarBlock.querySelector('.dx-scrollable-container');
                this.scrollCalendarHorizontaly(e, calendarScrollBlock);
            },
            false
        );
    }

    private scrollCalendarHorizontaly (event: any, calendarBlock: HTMLElement): void {
        const delta = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));

        calendarBlock.scrollLeft -= (delta * 40);
        event.preventDefault();
    }

    private initGreeters (greeters: Partial<IStaffUser>[]): void {
        this.greeters = greeters;
    }

    private initDataSource (calendarEvents: IGreeterCalendarEvent[]): void {
        this.dataSource = new DataSource({
            store: calendarEvents
        });
    }

}
