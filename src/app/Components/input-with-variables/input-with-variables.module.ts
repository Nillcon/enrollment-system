import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputWithVariablesComponent } from './input-with-variables.component';
import { SharedModule } from '@App/app-shared.module';
import { DxHtmlEditorModule } from 'devextreme-angular';

@NgModule({
    declarations: [
        InputWithVariablesComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DxHtmlEditorModule
    ],
    exports: [
        InputWithVariablesComponent
    ]
})
export class InputWithVariablesModule { }
