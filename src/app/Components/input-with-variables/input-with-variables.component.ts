import { Component, OnInit, Output, EventEmitter, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { IVariable } from '@Interfaces/variable.interface';

@Component({
    selector: 'app-input-with-variables',
    templateUrl: './input-with-variables.component.html',
    styleUrls: ['./input-with-variables.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => InputWithVariablesComponent),
            multi: true
        }
    ]
})
export class InputWithVariablesComponent implements OnInit, ControlValueAccessor {
    @Input() public variables: IVariable[];
    @Input() public placeholder: string = '';
    @Input() public value: string = '';

    @Output() public change: EventEmitter<string> = new EventEmitter();

    public readonly VariableSymbol: string = '#';

    constructor () {}

    ngOnInit () {}

    public processValue (value: string): void {
        const targetValue: string = this.parseValue(value);

        this.onChange(targetValue);
        this.change.emit(targetValue);
    }

    public registerOnChange (fn: Function): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (value?: string): void {
        if (value) {
            this.value = this.parseValue(value);
        }

        this.onChange(this.value);
        this.change.emit(this.value);
    }

    public onChange: Function = () => {};

    private parseValue (value: string): string {
        const contentAsElem: HTMLElement = document.createElement('div');
        contentAsElem.innerHTML = value;

        return contentAsElem.innerText;
    }

    private prepareValue (value: string): void {
        // tslint:disable-next-line: max-line-length
        const template: string = `<span class="dx-mention" spellcheck="false" data-marker="${this.VariableSymbol}" data-mention-value="Kevin Carter"><span contenteditable="false"><span>@</span>Kevin Carter</span></span>`;
        const variablesInValue = value.split(this.VariableSymbol);
    }
}
