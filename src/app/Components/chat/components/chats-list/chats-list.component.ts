import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { IRoom } from '@Components/chat/interfaces/room.interface';
import { RoomHubService } from '@Components/chat/services/room-hub.service';
import { Observable } from 'rxjs';
import { MatMenuTrigger } from '@angular/material';
import { tap } from 'rxjs/operators';
import { DateMethod } from '@Methods/date.method';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'chats-list',
    templateUrl: './chats-list.component.html',
    styleUrls: ['./chats-list.component.scss'],
    providers: [

    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatsListComponent implements OnInit {

    @Input() selectedChatLink: string;
    @Input() isShowCloseButton: boolean;

    public nonameText: string = '<noname>';

    public chatContactsList$: Observable<IRoom[]> = new Observable(null);
    public searchingText: string = '';

    @ViewChild(MatMenuTrigger) public contextMenu: MatMenuTrigger;
    public contextMenuPosition = { x: '0px', y: '0px' };

    @Output() OnSelectedChat: EventEmitter<string> = new EventEmitter();
    @Output() OnCloseChat: EventEmitter<IRoom> = new EventEmitter();
    @Output() OnClose: EventEmitter<boolean> = new EventEmitter();

    public dateMethod = DateMethod;

    constructor(
        private roomService: RoomHubService,
        private changeDetector: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.initChatContactsUpdating();
    }

    private initChatContactsUpdating() {
        this.chatContactsList$ = this.roomService.roomsList$;
    }

    public onChangeSearching(searchingText: string) {
        this.searchingText = searchingText;
    }

    public selectChat(chatLink: string) {
        this.OnSelectedChat.emit(chatLink);
        this.selectedChatLink = chatLink;
    }

    public deleteChat (chatLink: string) {
        this.roomService.invokeDeleteChat(chatLink);
   }

    public onRightClick(event: MouseEvent, chatContact: IRoom) {

        this.contextMenuPosition.x = event.clientX + 'px';
        this.contextMenuPosition.y = event.clientY + 'px';
        this.contextMenu.menuData = { 'item': chatContact };
        this.contextMenu.openMenu();

        return false;
    }

    public onChatDelete (item: IRoom) {
        this.deleteChat(item.chatLink);
        this.closeChat(item);
    }

    public close() {
        this.OnClose.emit(true);
    }

    public closeChat (item: IRoom) {
        this.OnCloseChat.emit(item);
    }
}
