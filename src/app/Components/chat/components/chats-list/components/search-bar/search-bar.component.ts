import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

    @Input() isShowCloseButton: boolean;

    @Output() OnClose: EventEmitter<boolean> = new EventEmitter();

    @Output() OnChange: EventEmitter<string> = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    public searchOnChange(event: {}) {
        const value: string = event['target'].value;

        this.OnChange.emit(value);
    }

    public close() {
        this.OnClose.emit(true);
    }
}
