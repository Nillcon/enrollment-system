import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatsListComponent } from './chats-list.component';

import {
    MatListModule,
    MatRippleModule,
    MatMenuModule
} from '@angular/material';
import { SharedModule } from '@App/app-shared.module';
import { SearchBarComponent } from './components/search-bar/search-bar.component';

@NgModule({
    declarations: [
        ChatsListComponent,
        SearchBarComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,

        MatListModule,
        MatRippleModule,
        MatMenuModule
    ],
    exports: [
        ChatsListComponent
    ]
})
export class ChatsListModule { }
