import { Component, OnInit, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { WindowedChatService } from '@Components/chat/services/windowed-chat.service';
import { Subscription } from 'rxjs';
import { IChatAnaliticsData } from '@Components/chat/interfaces/chat-analitics-data.interface';

@AutoUnsubscribe()
@Component({
    // tslint:disable-next-line: component-selector
    selector: 'windowed-chat',
    templateUrl: './windowed-chat.component.html',
    styleUrls: ['./windowed-chat.component.scss']
})
export class WindowedChatComponent implements OnInit, OnDestroy {
    public state: boolean = false;
    public chatLink: string;
    public isShowContacts: boolean;
    public analiticsData: IChatAnaliticsData;

    private stateSubscription: Subscription;

    constructor (
        private windowedChatService: WindowedChatService
    ) {}

    ngOnInit () {
        this.stateObserver();
    }

    ngOnDestroy () {}

    public close (): void {
        this.windowedChatService.close();
    }

    private stateObserver (): void {
        this.stateSubscription = this.windowedChatService.State$
            .subscribe(state => {
                if (state) {
                    this.isShowContacts = this.windowedChatService.IsShowContacts;
                    this.chatLink       = this.windowedChatService.ChatLink;
                    this.analiticsData  = this.windowedChatService.AnaliticsData;
                }

                this.state = state;
            });
    }
}
