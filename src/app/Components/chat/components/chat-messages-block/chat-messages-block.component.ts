// tslint:disable-next-line:max-line-length
import { Component, OnInit, Input, ElementRef, ViewChild, EventEmitter, Output, OnDestroy, AfterViewInit, ViewChildren, QueryList } from '@angular/core';
import { IMessage } from '@Components/chat/interfaces/message.interface';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';
import { Observable } from 'rxjs';
import { IChatInfo } from '@Components/chat/interfaces/chat-info.interface';
import { IUser } from '@Interfaces/users';
import { IChatAnaliticsData } from '@Components/chat/interfaces/chat-analitics-data.interface';
import { map, tap, filter } from 'rxjs/operators';
import { MessageBlockTypesEnum } from '@Components/chat/shared/message-block-types';
import { MediaPreviewComponent } from '@Components/media-preview/media-preview.component';
import { ITyping } from '@Components/chat/interfaces/typing.interface';

type needScrollTo = 'down' | 'current' | 'top';
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'chat-messages-block',
    templateUrl: './chat-messages-block.component.html',
    styleUrls: ['./chat-messages-block.component.scss'],
})
export class ChatMessagesBlockComponent implements OnInit, OnDestroy, AfterViewInit {

    @Input() roomLink: string;
    @Input() userInfo: IUser;

    @Input() isShowContacts: boolean;
    @Input() isShowCloseButton: boolean;

    @Input() analiticsData: IChatAnaliticsData = null;

    @Output() OnCloseMessages: EventEmitter<boolean> = new EventEmitter();
    @Output() OnClose: EventEmitter<boolean> = new EventEmitter();

    public chatInfo$: Observable<IChatInfo>;
    public messagesList$: Observable<IMessage[]>;
    public typing$: Observable<ITyping>;

    public lastMessage: IMessage;
    public currentScrollHeight: number;

    public needScrollTo: needScrollTo;

    public isScrolled: boolean = false;

    @ViewChild('scrollContainer') private scrollContainer: ElementRef;
    @ViewChild('mediaViewer') public mediaViewer: MediaPreviewComponent;
    @ViewChildren('messageBlocks') messageBlocks: QueryList<any>;

    constructor(
        private chatService: ChatHubService
    ) { }

    ngOnInit() {
        this.enterTheChat();
    }
    ngAfterViewInit() {
        this.checkMessageBlocksChanges();
    }

    ngOnDestroy() {
        this.exitFromChat();
    }

    private enterTheChat() {
        this.chatService.enterTheChat(this.roomLink, this.analiticsData)
            .subscribe(() => {
                this.initChatInfoUpdating();
                this.initMessagesUpdating();
                this.initTypingInfoUpdating();

                this.readMessages();

              this.chatService.startScenario(this.roomLink);
            });
    }

    public scrollDown() {
        if (this.scrollContainer) {
            this.scrollContainer.nativeElement.scrollTop = this.scrollContainer.nativeElement.scrollHeight;

            this.isScrolled = true;
        }
    }

    public scrollTo(height: number) {
        if (this.scrollContainer) {
            this.scrollContainer.nativeElement.scrollTop = height;
        }
    }

    public initChatInfoUpdating() {
        this.chatInfo$ = this.chatService.chatInfo$;
    }

    public initTypingInfoUpdating() {
        this.typing$ = this.chatService.typing$;
    }

    public initMessagesUpdating() {
        this.messagesList$ = this.chatService.messagesList$
            .pipe(
                filter(messages => !!messages),
                tap (messages => {
                    if (this.isScrolled) {
                        this.currentScrollHeight = this.scrollContainer.nativeElement.scrollHeight;

                        this.needScrollTo = (this.chatService.lastAction === 'load') ? 'current' : 'down';
                    } else {
                        this.needScrollTo = 'down';
                    }
                }),
                tap(messages => {
                    this.lastMessage = messages[0];
                }),
                map(messages => this.markupDatesInMessages(messages)),
                map(messages => this.markupLinksInMessages(messages)),
                tap((messages: IMessage[]) => {
                    this.readMessages();
                })
            );
    }

    public onScrollTop() {
        if (this.lastMessage) {
            this.chatService.loadChatHistory(this.roomLink, this.lastMessage.id);
        }
    }

    public readMessages() {
        this.chatService.readMessages(this.roomLink)
            .subscribe();
    }

    public closeMessages() {
        this.OnCloseMessages.emit(true);
    }

    public closeChat() {
        this.OnClose.emit(true);
    }

    public messagesHasEqualAuthor(prevMessage: IMessage, currentMessage: IMessage) {
        return (prevMessage.chatUserId === currentMessage.chatUserId);
    }

    public sendMessage(messageText: string) {
        this.chatService.sendMessage(this.roomLink, messageText);
    }

    public trackByFn(index: number, item: IMessage) {
        return item.id;
    }

    public checkMessageBlocksChanges() {
        this.messageBlocks.changes.subscribe(t => {
            if (this.needScrollTo === 'current') {
                let scrollHeight: number = this.scrollContainer.nativeElement.scrollHeight - this.currentScrollHeight;
                scrollHeight += 53; // it is height of date block

                this.scrollTo(scrollHeight);
            } else if (this.needScrollTo === 'down') {
                this.scrollDown();
            }
        });
    }

    public deleteMessage(message: IMessage) {
        this.chatService.deleteMessageByIndex(message.id);
    }

    public exitFromChat() {
        this.chatService.exitFromChat(this.roomLink)
            .subscribe();
    }

    private markupDatesInMessages(messages_: IMessage[]) {
        const messages = JSON.parse(JSON.stringify(messages_));

        messages.unshift({
            type: MessageBlockTypesEnum.NewDate,
            data: {
                date: (messages.length) ? messages[0].sendDate : new Date().toJSON()
            }
        });

        for (let i = 1; i < messages.length; i++) {
            if (messages[i].type !== MessageBlockTypesEnum.NewDate && messages[i - 1].type !== MessageBlockTypesEnum.NewDate) {
                const firstDate = new Date(messages[i].sendDate).toDateString();
                const secondDate = new Date(messages[i - 1].sendDate).toDateString();

                if (firstDate !== secondDate) {
                    messages.splice(i, 0, {
                        type: MessageBlockTypesEnum.NewDate,
                        data: {
                            date: messages[i].sendDate
                        }
                    });
                }
            }
        }

        return messages;
    }
    private markupLinksInMessages(messages_: IMessage[]) {
        const urlRegex: RegExp = /(\b(https?|ftp|file|http):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;

        messages_ = messages_.map(message => {
            if (message.text) {
                message.text = message.text.replace(urlRegex, (url) => {
                    return '<a target="_blank" href="' + url + '">' + url + '</a>';
                });
            }

            return message;
        });

        return messages_;
    }
}
