import { Component, OnInit, EventEmitter, Output, HostListener, ViewChild, ElementRef, Input, OnDestroy, AfterViewInit } from '@angular/core';
import { IEmoji } from '@Components/chat-emoji-popup/interfaces/emoji.interface';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';
import { UserService } from '@Services/user.service';
import { filter, switchMap, first, mergeMap, delay, takeUntil, skipUntil } from 'rxjs/operators';
import { Roles } from '@Shared/roles';
import { fromEvent, Subscription, of } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { MatDialog } from '@angular/material';
import { UploadImageWindowComponent } from './components/upload-image-window/upload-image-window.component';

@AutoUnsubscribe()
@Component({
    // tslint:disable-next-line: component-selector
    selector: 'message-textarea',
    templateUrl: './message-textarea.component.html',
    styleUrls: ['./message-textarea.component.scss'],
})
export class MessageTextareaComponent implements OnInit, OnDestroy, AfterViewInit {

    public messageText: string = '';

    @Input() isDisable: boolean = false;
    @Input() chatLink: string;

    @ViewChild('textarea') public textarea: ElementRef;

    @Output() OnSendMessage: EventEmitter<string> = new EventEmitter();

    @ViewChild('PhotoInput') public PhotoInput: { nativeElement: { click: () => void; }; };

    private isTyping: boolean;
    private userDataSubscription: Subscription;

    public photo: Blob;

    constructor(
        private chatService: ChatHubService,
        private userService: UserService,
        private dialog: MatDialog
    ) { }

    ngOnInit() {
        this.takeInProcessing();
    }

    ngOnDestroy() {}

    ngAfterViewInit() {
        this.listenWriting();
    }

    // @HostListener('document:keypress', ['$event'])
    // public keyHandler(event: KeyboardEvent) {
    //     // tslint:disable-next-line: deprecation
    //     switch (event.charCode) {
    //     case 13:
    //         this.send();
    //         break;
    //     }
    // }

    public send() {
        if (this.messageText) {
        this.OnSendMessage.emit(this.messageText);
        }

        this.clearTextarea();
    }

    public keyDown (event: any) {
        if (event.which === 13) {
            this.send();
            return false;
        }
    }

    public clearTextarea() {
        this.messageText = '';
    }

    public addEmojiToTextArea(emoji: IEmoji) {
        const startPosition: number = this.textarea.nativeElement.selectionStart;
        this.messageText =  this.messageText.slice(0, startPosition) + emoji.char + this.messageText.slice(startPosition);

        this.textarea.nativeElement.setRangeText(emoji.char, startPosition, startPosition + emoji.char.length, 'select');

        this.textarea.nativeElement.focus();
        const newPosition = startPosition + emoji.char.length;

        setTimeout(() => {
            this.textarea.nativeElement.setSelectionRange(newPosition, newPosition);
        });
    }

    public takeInProcessing() {
        const textarea = document.querySelector('#textarea');

        this.userDataSubscription = this.userService.Data$
            .pipe(
                switchMap(() => fromEvent(textarea, 'focus')),
                first()
            )
            .subscribe(() => {
                this.chatService.takeInProcessing(this.chatLink);
            });
    }

    public selectFile() {
        this.PhotoInput.nativeElement.click();
    }

    public uploadFile(event: any) {
        const file = event.target.files[0];

        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            const encoded = reader.result['replace'](/^data:(.*,)?/, '');

            const dialog = this.dialog.open(UploadImageWindowComponent, {
                data: {
                    chatLink: this.chatLink,
                    imageBase64: reader.result,
                    messageText: this.messageText
                }
            });

            dialog.afterClosed()
                .subscribe((res: boolean) => {
                    this.photo = null;

                    if (res) {
                        this.messageText = '';
                    }
                });
        };
    }

    private listenWriting() {
        const textareaEvent = fromEvent(this.textarea.nativeElement, 'keydown');
 
        textareaEvent
            .pipe(
                mergeMap(event =>
                    of(event).pipe(
                        delay(1000),
                        skipUntil(textareaEvent)
                    )
                )
            )
            .subscribe(res => {
               this.startTyping();
            });
            
        textareaEvent
            .pipe(
                mergeMap(event =>
                    of(event).pipe(
                        delay(1000),
                        takeUntil(textareaEvent)
                    )
                )
            )
            .subscribe(res => {
                this.stopTyping();
            }); 
    }

    public startTyping() {
        if (!this.isTyping) {
            this.chatService.startTyping(this.chatLink);
            this.isTyping = true;
        } else {
            return;
        }
    }

    public stopTyping() {
        if (this.isTyping) {
            this.chatService.stopTyping(this.chatLink);
            this.isTyping = false;
        } else {
            return;
        }
    }
}
