import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageTextareaComponent } from './message-textarea.component';
import { SharedModule } from '@App/app-shared.module';
import { ChatEmojiPopupModule } from '@Components/chat-emoji-popup/chat-emoji-popup.module';
import { UploadImageWindowComponent } from './components/upload-image-window/upload-image-window.component';

@NgModule({
    declarations: [
        MessageTextareaComponent,
        UploadImageWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        ChatEmojiPopupModule
    ],
    exports: [
        MessageTextareaComponent
    ],
    entryComponents: [
        UploadImageWindowComponent
    ]
})
export class MessageTextareaModule { }
