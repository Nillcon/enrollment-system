import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';
import { ChatService } from '@Components/chat/services/chat.service';
import { Base64Worker } from '@Methods/base64Worker.method';

interface IImageComponentData {
    chatLink: string;
    messageText: string;
    imageBase64: string;
}

@Component({
    selector: 'app-upload-image-window',
    templateUrl: './upload-image-window.component.html',
    styleUrls: ['./upload-image-window.component.scss']
})
export class UploadImageWindowComponent implements OnInit {

    public isLoad: boolean = false;
    public messageText: string;

    constructor(
        public dialogRef: MatDialogRef<UploadImageWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IImageComponentData,
        private chatService: ChatService
    ) { }

    ngOnInit() {
        this.initMessageText();
    }

    private initMessageText() {
        this.messageText = this.data.messageText;
    }

    public send() {
        this.isLoad = true;

        const encodedImage = Base64Worker.encodeToBytes(this.data.imageBase64);

        this.chatService.uploadPhoto(this.data.chatLink, this.messageText || null, encodedImage)
            .subscribe(res => {
                this.isLoad = false;

                this.dialogRef.close(true);
            });
    }

    public close() {
        this.dialogRef.close(false);
    }
}
