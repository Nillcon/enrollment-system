import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { IChatInfo } from '@Components/chat/interfaces/chat-info.interface';
import { MatDialog } from '@angular/material';
import { IUser } from '@Interfaces/users';
import { Roles } from '@Shared/roles';
import { ScheduleComponent } from '@Components/schedule/schedule.component';
// tslint:disable-next-line: max-line-length
import { AppointmentsWindowComponent } from '@Layouts/dashboard-layout/pages/appointments-page/components/appointments-window/appointments-window.component';
import { UserVariablesWindowComponent } from '@Components/user-variables-table/components/user-variables-window/user-variables-window.component';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'messages-block-toolbar',
    templateUrl: './messages-block-toolbar.component.html',
    styleUrls: ['./messages-block-toolbar.component.scss']
})
export class MessagesBlockToolbarComponent implements OnInit {

    @Input() chatInfo: IChatInfo;
    @Input() userInfo: IUser;

    @Input() isShowContacts: boolean;
    @Input() isShowCloseButton: boolean;

    public Roles = Roles;

    @Output() OnBack: EventEmitter<boolean> = new EventEmitter();
    @Output() OnClose: EventEmitter<boolean> = new EventEmitter();

    public nonameText: string = '<noname>';

    constructor(
        private dialog: MatDialog
    ) {}

    ngOnInit() {}

    public emitOnBack() {
        this.OnBack.emit(true);
    }

    public openVariablesTable() {
        this.dialog.open(UserVariablesWindowComponent, {
            data: this.chatInfo.visitorId
        });
    }

    public openSchedule() {
        this.dialog.open(ScheduleComponent);
    }

    public openAppointmens() {
        this.dialog.open(AppointmentsWindowComponent, {
            data: {
                mode: 'addition'
            }
        });
    }

    public close() {
        this.OnClose.emit(true);
    }
}
