import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessagesBlockToolbarComponent } from './messages-block-toolbar.component';

import { SharedModule } from '@App/app-shared.module';
import { MatMenuModule, MatToolbarModule } from '@angular/material';
import { ScheduleModule } from '@Components/schedule/schedule.module';
import { UserVariablesTableModule } from '@Components/user-variables-table/user-variables-table.module';
// tslint:disable-next-line: max-line-length
import { AppointmentsWindowModule } from '@Layouts/dashboard-layout/pages/appointments-page/components/appointments-window/appointments-window.module';

@NgModule({
    declarations: [
        MessagesBlockToolbarComponent
    ],
    imports: [
        CommonModule,

        MatMenuModule,

        MatToolbarModule,
        UserVariablesTableModule,
        ScheduleModule,
        AppointmentsWindowModule,

        SharedModule
    ],
    exports: [
        MessagesBlockToolbarComponent
    ],
})
export class MessagesBlockToolbarModule { }
