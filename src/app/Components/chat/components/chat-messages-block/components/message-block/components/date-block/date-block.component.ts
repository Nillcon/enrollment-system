import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMessage } from '@Components/chat/interfaces/message.interface';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'date-block',
    templateUrl: './date-block.component.html',
    styleUrls: ['./date-block.component.scss']
})
export class DateBlockComponent implements OnInit {

    @Input() chatLink: string;
    @Input() isMyself: boolean;
    @Input() message: IMessage;
    @Input() showName: boolean;

    @Output() OnDelete: EventEmitter<boolean> = new EventEmitter();

    private myDates: Date[] = [];

    public selectedDate: Date;

    constructor(
        private chatService: ChatHubService
    ) { }

    ngOnInit() {
        this.updateMyDates();
    }

    public deleteBlock() {
        this.OnDelete.emit(true);
    }

    public onSkipMessage() {
        this.deleteBlock();
    }

    public send() {
        this.chatService.sendPollData(this.chatLink, this.selectedDate.toDateString(), this.message);

        this.deleteBlock();
    }

    private updateMyDates() {
        (this.message.data['dates'] as string[])
            .forEach(elem => {
                this.myDates.push(new Date(elem));
            });
    }

    filterFreeDates = (date: Date) => {
        const dateIsExist = !!this.myDates.filter(myDate => {
            if (myDate.toDateString() === date.toDateString()) {
                return true;
            }

            return false;
        }).length;

        return dateIsExist;
    }

}
