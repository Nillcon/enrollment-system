import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMessage } from '@Components/chat/interfaces/message.interface';
import { MessageBlockTypesEnum } from '@Components/chat/shared/message-block-types';
import { MediaPreviewComponent } from '@Components/media-preview/media-preview.component';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'message-block',
    templateUrl: './message-block.component.html',
    styleUrls: ['./message-block.component.scss']
})
export class MessageBlockComponent implements OnInit {

    @Input() chatLink: string;
    @Input() isMyself: boolean;
    @Input() message: IMessage;
    @Input() showName: boolean;
    @Input() showPhoto: boolean;

    @Input() mediaViewer?: MediaPreviewComponent;

    @Input() newDate: string = null;

    @Output() OnDelete: EventEmitter<boolean> = new EventEmitter();

    public MessageBlockTypes = MessageBlockTypesEnum;

    constructor() { }

    ngOnInit() {
    }

    public selfDestroy() {
        this.OnDelete.emit(true);
    }

}
