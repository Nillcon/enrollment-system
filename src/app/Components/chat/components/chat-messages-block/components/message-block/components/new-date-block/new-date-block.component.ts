import { Component, OnInit, Input } from '@angular/core';
import { IMessage } from '@Components/chat/interfaces/message.interface';

@Component({
    // tslint:disable-next-line: component-selector
    selector: 'new-date-block',
    templateUrl: './new-date-block.component.html',
    styleUrls: ['./new-date-block.component.scss']
})
export class NewDateBlockComponent implements OnInit {

    @Input() message: IMessage;

    constructor() { }

    ngOnInit() {
    }

}
