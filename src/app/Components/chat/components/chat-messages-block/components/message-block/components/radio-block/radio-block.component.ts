import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMessage, IMessageRadioItem } from '@Components/chat/interfaces/message.interface';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'radio-block',
    templateUrl: './radio-block.component.html',
    styleUrls: ['./radio-block.component.scss']
})
export class RadioBlockComponent implements OnInit {

    @Input() chatLink: string;
    @Input() isMyself: boolean;
    @Input() message: IMessage;
    @Input() showName: boolean;

    @Output() OnDelete: EventEmitter<boolean> = new EventEmitter();

    public selectedItem: IMessageRadioItem;

    constructor(
        private chatService: ChatHubService
    ) { }

    ngOnInit() {

    }

    public send() {

        this.message.data['radiobuttons']
            .map((radio: IMessageRadioItem) => {

                if (radio.id === this.selectedItem.id) {
                    radio.checked = true;
                }

                return radio;
            });

        this.chatService.sendRadioData(this.chatLink, this.message);

        this.deleteBlock();
    }

    public deleteBlock() {
        this.OnDelete.emit(true);
    }

    public onSkipMessage() {
        this.deleteBlock();
    }
}
