import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMessage } from '@Components/chat/interfaces/message.interface';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';

@Component({
  selector: 'app-question-message-toolbar',
  templateUrl: './question-message-toolbar.component.html',
  styleUrls: ['./question-message-toolbar.component.scss']
})
export class QuestionMessageToolbarComponent implements OnInit {

    @Input() chatLink: string;
    @Input() message: IMessage;

    @Output() OnSkipButtonClick: EventEmitter<boolean> = new EventEmitter();
    @Output() OnCallSupporButtonClick: EventEmitter<boolean> = new EventEmitter();

    constructor(
        public chatService: ChatHubService
    ) { }

    ngOnInit() {
    }

    public skipButtonClick() {
        this.OnSkipButtonClick.emit(true);

        this.chatService.skipMessage(this.message);
    }

    public callSupporButtonClick() {
        this.OnCallSupporButtonClick.emit(true);

        this.chatService.callSupport(this.chatLink);
    }

}
