import { Component, OnInit, Input, ɵConsole } from '@angular/core';
import { IMessage } from '@Components/chat/interfaces/message.interface';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'text-block',
    templateUrl: './text-block.component.html',
     styleUrls: ['./text-block.component.scss']
})
export class TextBlockComponent implements OnInit {

    @Input() chatLink: string;
    @Input() isMyself: boolean;
    @Input() message: IMessage;
    @Input() showName: boolean;

    constructor(
        public chatService: ChatHubService
    ) { }

    ngOnInit() {
    }

}
