import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageBlockComponent } from './message-block.component';
import { ImageBlockComponent } from './components/image-block/image-block.component';
import { TextBlockComponent } from './components/text-block/text-block.component';
import { FormBlockComponent } from './components/form-block/form-block.component';
import { SharedModule } from '@App/app-shared.module';
import { PollBlockComponent } from './components/poll-block/poll-block.component';
import { RadioBlockComponent } from './components/radio-block/radio-block.component';
import { QuestionMessageToolbarComponent } from './components/question-message-toolbar/question-message-toolbar.component';
import { DateBlockComponent } from './components/date-block/date-block.component';
import { NewDateBlockComponent } from './components/new-date-block/new-date-block.component';
import { CheckBlockComponent } from './components/check-block/check-block.component';

@NgModule({
    declarations: [
        MessageBlockComponent,
        ImageBlockComponent,
        TextBlockComponent,
        FormBlockComponent,
        PollBlockComponent,
        RadioBlockComponent,
        QuestionMessageToolbarComponent,
        DateBlockComponent,
        NewDateBlockComponent,
        CheckBlockComponent
    ],
    imports: [
        CommonModule,
        SharedModule
    ],
    exports: [
        MessageBlockComponent,
    ]
})
export class MessageBlockModule { }
