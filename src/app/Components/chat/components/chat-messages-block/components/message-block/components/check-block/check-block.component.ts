import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IMessage, IMessageRadioItem } from '@Components/chat/interfaces/message.interface';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'check-block',
    templateUrl: './check-block.component.html',
    styleUrls: ['./check-block.component.scss']
})
export class CheckBlockComponent implements OnInit {

    @Input() chatLink: string;
    @Input() isMyself: boolean;
    @Input() message: IMessage;
    @Input() showName: boolean;

    @Output() OnDelete: EventEmitter<boolean> = new EventEmitter();

    public selectedItem: IMessageRadioItem;

    constructor(
        private chatService: ChatHubService
    ) { }

    ngOnInit() {

    }

    public send() {
        this.chatService.sendCheckData(this.chatLink, this.message);

        this.deleteBlock();
    }

    public deleteBlock() {
        this.OnDelete.emit(true);
    }

    public onSkipMessage() {
        this.deleteBlock();
    }
}
