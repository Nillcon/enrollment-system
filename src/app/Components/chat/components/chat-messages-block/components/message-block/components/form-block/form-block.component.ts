import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { IMessage, IMessageFormField } from '@Components/chat/interfaces/message.interface';
import { FormControl, Validators } from '@angular/forms';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';
import { IFormBlockResponse } from '@Components/chat/interfaces/form-block-response';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'form-block',
    templateUrl: './form-block.component.html',
    styleUrls: ['./form-block.component.scss']
})
export class FormBlockComponent implements OnInit {

    @Input() chatLink: string;
    @Input() isMyself: boolean;
    @Input() message: IMessage;
    @Input() showName: boolean;

    @Output() OnDelete: EventEmitter<boolean> = new EventEmitter();

    public formResponse: IFormBlockResponse[];

    constructor(
        private chatService: ChatHubService
    ) { }

    ngOnInit() {
        this.initFormResponse();
    }

    public send() {
        const isValid = this.formResponse
            .find(field => !!field.value);

        if (isValid) {
            this.chatService.sendFormData(
                this.chatLink,
                this.formResponse,
                this.message
            )
            .then(() => {});

            this.deleteBlock();
        }
    }

    public initFormResponse() {
        this.formResponse = [];

        (this.message.data['fields'] as IMessageFormField[])
            .forEach(value => {
                this.formResponse.push({
                    value: '',
                    varId: value.variableId
                });
            });
    }

    @HostListener('document:keypress', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {
        // tslint:disable-next-line: deprecation
        switch (event.charCode) {
            case 13:
                this.send();
                break;
            }
    }

    public deleteBlock() {
        this.OnDelete.emit(true);
    }

    public onSkipMessage() {
        this.deleteBlock();
    }
}
