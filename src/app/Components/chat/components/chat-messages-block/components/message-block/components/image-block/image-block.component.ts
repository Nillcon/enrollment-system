import { Component, OnInit, Input } from '@angular/core';
import { IMessage } from '@Components/chat/interfaces/message.interface';
import { environment } from 'src/environments/environment';
import { MediaPreviewComponent } from '@Components/media-preview/media-preview.component';
import { MediaPreviewContentTypeEnum } from '@Components/media-preview/shared/content-type';


@Component({
    // tslint:disable-next-line:component-selector
    selector: 'image-block',
    templateUrl: './image-block.component.html',
    styleUrls: ['./image-block.component.scss']
})
export class ImageBlockComponent implements OnInit {

    @Input() isMyself: boolean;
    @Input() message: IMessage;

    @Input() mediaViewer?: MediaPreviewComponent;

    public readonly host = environment.host;

    public largeImageUrl: string;

    constructor() { }

    ngOnInit() {
        this.largeImageUrl = this.getLargeImageUrl();
    }

    public openImage() {
        this.mediaViewer.media = [
            {
                type: MediaPreviewContentTypeEnum.Image,
                content: this.largeImageUrl,
                description: this.message.text
            }
        ];

        this.mediaViewer.open();
    }

    private getLargeImageUrl() {
        const largeImageUrl = environment.host + this.message.data['imageUrl']
            .replace('_small.jpg', '.jpg');

        return largeImageUrl;
    }

}
