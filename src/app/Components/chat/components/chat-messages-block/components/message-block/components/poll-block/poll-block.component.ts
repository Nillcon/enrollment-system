import { Component, OnInit, Input, EventEmitter, Output, HostListener } from '@angular/core';
import { IMessage } from '@Components/chat/interfaces/message.interface';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'poll-block',
  templateUrl: './poll-block.component.html',
  styleUrls: ['./poll-block.component.scss']
})
export class PollBlockComponent implements OnInit {

    @Input() chatLink: string;
    @Input() isMyself: boolean;
    @Input() message: IMessage;
    @Input() showName: boolean;

    @Output() OnDelete: EventEmitter<boolean> = new EventEmitter();

    constructor(
        private chatService: ChatHubService
    ) { }

    ngOnInit() {
    }

    public send(value: string) {
        this.chatService.sendPollData(this.chatLink, value, this.message);

        this.deleteBlock();
    }

    public deleteBlock() {
        this.OnDelete.emit(true);
    }

    public onSkipMessage() {
        this.deleteBlock();
    }

}
