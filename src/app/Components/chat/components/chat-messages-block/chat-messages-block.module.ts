import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatMessagesBlockComponent } from './chat-messages-block.component';
import { MessageTextareaModule } from './components/message-textarea/message-textarea.module';
import { MessagesBlockToolbarModule } from './components/messages-block-toolbar/messages-block-toolbar.module';
import { MessageBlockModule } from './components/message-block/message-block.module';
import { SharedModule } from '@App/app-shared.module';
import { ChatHubService } from '@Components/chat/services/chat-hub.service';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
    declarations: [
        ChatMessagesBlockComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,

        InfiniteScrollModule,

        MessageTextareaModule,
        MessagesBlockToolbarModule,
        MessageBlockModule
    ],
    exports: [
        ChatMessagesBlockComponent
    ],
    providers: [
        ChatHubService
    ],
    entryComponents: [
        ChatMessagesBlockComponent
    ]
})
export class ChatMessagesBlockModule { }
