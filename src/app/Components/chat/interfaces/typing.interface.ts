export interface ITyping {
    isTyping: boolean;
    whoWrites: string;   
}
