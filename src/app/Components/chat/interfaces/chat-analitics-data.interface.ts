export interface IChatAnaliticsData {
    blockId?: number;
    userId?: number;
    buttonName?: string;
}
