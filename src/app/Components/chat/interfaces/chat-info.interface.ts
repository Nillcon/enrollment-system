import { ISender } from "./sender.interface";

export interface IChatInfo {
    chatName: string;
    inProcessing: boolean;
    myChatId: number;
    users: ISender[];
    visitorId: number;
    isTextInputEnabled: boolean;
    isOnline: boolean;
}
