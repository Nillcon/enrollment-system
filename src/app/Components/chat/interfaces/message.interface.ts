import { MessageBlockTypesEnum } from "../shared/message-block-types";

export interface IMessage {
    conditionId?: number;
    acionId?: number;
    chatUserId?: number;
    chatUserName?: string;
    chatUserPhotoUrl?: string;
    data?: Partial<IImageMessage | IFormMessage | IPollMessage | IRadioMessage | IDateMessage | INewDateMessage>;
    id?: number;
    sendDate?: string;
    status?: number;
    delay?: number;
    text?: string;
    type?: MessageBlockTypesEnum;
    appointmentStage?: number;
    context?: any;
    skipButton?: IHiddenButton;
    callSupportButton?: IHiddenButton;
    variableId?: number;
}

export interface IImageMessage {
    imageUrl?: string;
}

export interface IFormMessage {
    fields: IMessageFormField[];
}

export interface IPollMessage {
    buttons: IMessageButton[];

    varId?: number;
}

export interface IRadioMessage {
    radiobuttons: IMessageRadioItem[];
    submitButtonText: string;

    varId?: number;
}

export interface IDateMessage {
    dates: string[];
}

export interface INewDateMessage {
    date: string;
}

export interface IMessageButton {
    text: string;
    value?: any;
}

export interface IMessageFormField {
    placeholder?: string;
    variableId?: number;
}

export interface IMessageRadioItem {
    id: number;
    name: string;
    photoUrl?: string;

    checked: boolean;
}

export interface IHiddenButton {
    isShow: boolean;
    data?: ISkipButtonData;
}
interface ISkipButtonData {
    text: string;
}

