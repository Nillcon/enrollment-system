export interface ISender {
    id?: number;
    name: string;
    photo?: string;
}
