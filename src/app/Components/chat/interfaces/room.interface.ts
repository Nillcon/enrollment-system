import { MessageBlockTypesEnum } from "../shared/message-block-types";

export interface IRoom {
    chatLink?: string;
    chatUserId: number;
    inProcessing: number;
    lastMessage: string;
    lastMessageAuthor: string;
    messageType: MessageBlockTypesEnum;
    name: string;
    photo?: string;
    roomId: number;
    sendDate: string;
    unreadMessageCount?: number;
    userId: number;
    userType?: number;
    isOnline?: boolean;
}
