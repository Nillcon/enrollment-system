export interface IFormBlockResponse {
    varId: number;
    value: string;
}
