import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpRequestService } from "@Services/http-request.service";
import { UserService } from "@Services/user.service";

@Injectable()
export class ChatService {
    constructor (
        private httpService: HttpRequestService,
        private userService: UserService
    ) {}

    public createRoom (): Observable<string> {
        return this.httpService.post(`/Chat/CreateRoom`, {});
    }

    public uploadPhoto(chatLink: string, messageText: string, imageBase64: string): Observable<any> {
        return this.httpService.post('/Chat/UploadChatImage', {
            ChatLink: chatLink,
            Text: messageText,
            Base64image: imageBase64
        });
    }
}
