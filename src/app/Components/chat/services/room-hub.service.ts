import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { LocalStorageService } from "@Services/local-storage.service";
import { StorageKey } from "@Shared/storage-key";
import { HubConnectionBuilder, HubConnection } from "@aspnet/signalr";
import { BehaviorSubject, Observable } from "rxjs";
import { IRoom } from "../interfaces/room.interface";

@Injectable()
export class RoomHubService {

    private stream: HubConnection;

    private _roomsList: BehaviorSubject<IRoom[]> = new BehaviorSubject(null);

    private readonly hourInMilliseconds = 3600000;

    constructor(
        private httpService: HttpRequestService,
        private storageService: LocalStorageService
    ) {
        this.connectToRoomHub();
    }

    private async connectToRoomHub() {
        const host = this.httpService.getHost();
        const token = this.storageService.get<string>(StorageKey.Access_Token);

        this.stream = new HubConnectionBuilder()
            .withUrl(`${host}/roomhub`,
                {
                    accessTokenFactory: () => token
                }
            )
            .build();

        this.stream.start()
            .then((res) => {
                console.log('RoomHub connection started...');

                this.invokeUpdateRoomsList();

                // Call observer functions
                this.updateRoomObserver();
                this.newRoomObserver();
            })
            .catch(err => {
                console.log('Error while starting connection: ' + err);
            });

        this.stream.serverTimeoutInMilliseconds = this.hourInMilliseconds;
    }

    /** Update array with rooms */
    public async invokeUpdateRoomsList() {
        this.updateRoomsListObserver();

        await this.stream.invoke('getmyrooms');
    }

    public async invokeDeleteChat (chatLink: string) {
        console.log(chatLink);
        await this.stream.invoke('DeleteChat', chatLink);
    }

    private updateRoomsListObserver() {
        this.stream.on('Info', res => {
            this._roomsList.next(res);
        });
    }

    /** Observer to rooms changes */
    private updateRoomObserver(): void {
        this.stream.on('UpdateRoom',
            (res: IRoom) => {
                let rooms: IRoom[] = this._roomsList.value;
                const room: IRoom[] = rooms.filter(elem => elem.roomId === res.roomId);

                if (room.length) {
                    rooms = rooms.map(elem => {
                        if (elem.roomId === res.roomId) {
                            elem = res;
                        }

                        return elem;
                    });
                } else {
                    rooms.unshift(res);
                }

                this._roomsList.next(rooms);
            }
        );
    }

    /** Observer to new rooms changes */
    private newRoomObserver(): void {
        this.stream.on('NewRoom',
            (res: IRoom) => {
                const rooms = this._roomsList.value;
                rooms.unshift(res);

                this._roomsList.next(rooms);
            }
        );
    }

    public get roomsList$(): Observable<IRoom[]> {
        return this._roomsList.asObservable();
    }
}
