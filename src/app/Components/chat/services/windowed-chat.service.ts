import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { IChatAnaliticsData } from "../interfaces/chat-analitics-data.interface";
import { UserService } from "@Services/user.service";

// FIXME: Add to providers array of chat.module, and fix bugs on main layout with opening
// via content blocks chat buttons and "Meet" button in user details window
@Injectable({
    providedIn: 'root'
})
export class WindowedChatService {
    private _state: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public get State (): boolean {
        return this._state.getValue();
    }
    public get State$ (): Observable<boolean> {
        return this._state.asObservable();
    }

    private _isShowContacts: boolean = false;
    public get IsShowContacts (): boolean {
        return this._isShowContacts;
    }

    private _chatLink: string;
    public get ChatLink (): string {
        return this._chatLink;
    }

    private _analiticsData: IChatAnaliticsData;
    public get AnaliticsData (): IChatAnaliticsData {
        return this._analiticsData;
    }

    constructor (
        private userService: UserService
    ) {}

    /** Open chat in windowed mode */
    public open ({
        isShowContacts = false,
        chatLink = null,
        analiticsData  = {} as IChatAnaliticsData
    }): void {
        this._chatLink       = chatLink;
        this._isShowContacts = chatLink ? isShowContacts : true;
        this._analiticsData  = analiticsData;

        this._state.next(true);
    }

    /** Close windowed chat */
    public close (): void {
        this._state.next(false);

        this._isShowContacts    = null;
        this._chatLink          = null;
    }
}
