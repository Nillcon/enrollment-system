import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { LocalStorageService } from "@Services/local-storage.service";
import { StorageKey } from "@Shared/storage-key";
import { HubConnectionBuilder, HubConnection } from "@aspnet/signalr";
import { BehaviorSubject, Observable, ReplaySubject, of } from "rxjs";
import { ISender } from "../interfaces/sender.interface";
import { IMessage } from "../interfaces/message.interface";
import { switchMap, tap, filter, first } from "rxjs/operators";
import { IChatInfo } from "../interfaces/chat-info.interface";
import { IChatAnaliticsData } from "../interfaces/chat-analitics-data.interface";
import { IFormBlockResponse } from "../interfaces/form-block-response";
import { ITyping } from "../interfaces/typing.interface";

type action = 'load' | 'new';
@Injectable()
export class ChatHubService {

    public lastAction: action;

    private _chatInfo$: BehaviorSubject<IChatInfo> = new BehaviorSubject(null);
    private _messagesList$: BehaviorSubject<IMessage[]> = new BehaviorSubject(null);

    private _typing$: ReplaySubject<ITyping> = new ReplaySubject();

    private isLoad: ReplaySubject<boolean> = new ReplaySubject();

    private stream: HubConnection;

    private readonly hourInMilliseconds = 3600000;

    constructor(
        private httpService: HttpRequestService,
        private storageService: LocalStorageService
    ) {
        this.connectToChatHub();
    }

    public connectToChatHub(): void {
        const host = this.httpService.getHost();
        const token = this.storageService.get<string>(StorageKey.Access_Token);

        this.stream = new HubConnectionBuilder()
            .withUrl(`${host}/chathub`,
                {
                    accessTokenFactory: () => token
                }
            )
            .build();

        this.stream.start()
            .then(() => {
                console.log('Chat connection started');

                this.chatInfoObserver();
                this.messagesObserver();
                this.newMessagesObserver();
                this.typingObserver();

                this.isLoad.next(true);
            })
            .catch(err => console.log('Error while starting connection:', err));

            this.stream.serverTimeoutInMilliseconds = this.hourInMilliseconds;
    }

    /** Update messages */
    private messagesObserver(): void {
        this.stream.on('Messages',
            (res: IMessage[]) => {
                of(res)
                    .pipe(
                        switchMap(() => this._chatInfo$),
                        filter(chatInfo => !!chatInfo),
                        first(),
                        tap(chatInfo => {
                            const users: ISender[] = chatInfo.users;

                            res.map(message => {
                                const chatUser: ISender = this.getUserById(users, message.chatUserId);

                                try {
                                    message.chatUserName = chatUser.name;
                                    message.chatUserPhotoUrl = chatUser.photo;
                                } catch (e) {
                                    message.chatUserName = 'anonymous';
                                    message.chatUserPhotoUrl = null;
                                }

                                return message;
                            });

                            const newMessages: IMessage[] = res;

                            if (this._messagesList$.value) {
                                newMessages.push(...this._messagesList$.value);
                            }

                            this.lastAction = 'load';

                            this._messagesList$.next(newMessages);
                        })
                    )
                    .subscribe();
            }
        );
    }

    /** Update chat users */
    private chatInfoObserver(): void {
        this.stream.on('Info',
            (res: any) => {
                this._chatInfo$.next(res);
            }
        );
    }

    private newMessagesObserver(): void {
      this.stream.on('NewMessage', (res: IMessage) => {
            const messages = this._messagesList$.value;

            try {
                const users: ISender[] = this._chatInfo$.value.users || [];

                const chatUser: ISender = this.getUserById(users, res.chatUserId);


                res.chatUserName = chatUser.name;
                res.chatUserPhotoUrl = chatUser.photo;
            } catch (e) {
                res.chatUserName = 'anonymous';
                res.chatUserPhotoUrl = null;
            }

            messages.push(res);

            this.lastAction = 'new';

            this._messagesList$.next(messages);
        });
    }

    private typingObserver() {
        this.stream.on('Typing', (res: ITyping) => {
            this._typing$.next(res);
        });
    }

    public deleteMessageByIndex(idOfMessage: number) {
        let messages = this._messagesList$.value;

        messages = messages
            .filter(elem => elem.id !== idOfMessage);

        this._messagesList$.next(messages);
    }

    public enterTheChat(chatId: string, analiticsData?: IChatAnaliticsData): Observable<any> {
        return this.isLoad
            .pipe(
                tap(() => {
                    this._messagesList$.next(null);
                    this._chatInfo$.next(null);
                }),
                switchMap(() => this.stream.invoke('enter', chatId, analiticsData)),
            );
    }

    public async loadChatHistory(chatId: string, lastMessageId: number) {
        await this.stream.invoke('GetHistory', chatId, lastMessageId);
    }

    public async startScenario(chatId: string) {
        await this.stream.invoke('startScenario', chatId);
    }

    public exitFromChat(chatId: string) {
        return of(true)
            .pipe(
                tap(() => {
                    this._messagesList$.next(null);
                    this._chatInfo$.next(null);
                }),
                switchMap(() => this.stream.invoke('exit', chatId))
            );
    }

    public uploadPhoto(formData: FormData): Observable<any> {
        return this.httpService.post('/Chat/UploadPhoto', formData);
    }

    public readMessages(chatId: string) {
        return of(true)
            .pipe(
                switchMap(() => this.isLoad),
                switchMap(() => this.stream.invoke('Read', chatId))
            );
    }

    public async sendMessage(chatId: string, messageText: string) {
        await this.stream.invoke('Send', chatId, messageText);
    }

    public async sendRadioData(chatId: string, message: IMessage) {
        await this.stream.invoke('sendRadioData', chatId, message);
    }

    public async sendCheckData(chatId: string, message: IMessage) {
        await this.stream.invoke('sendMultiplePollData', chatId, message);
    }

    public async sendPollData(chatId: string, value: string, message: IMessage) {
        await this.stream.invoke('sendPollData', chatId, value, message);
    }

    public async sendFormData(chatLink: string, value: IFormBlockResponse[], message: IMessage) {
        await this.stream.invoke('sendFormData', chatLink, value, message);
    }

    public async sendImage(chatLink: string, imageBase64: string, text: string) {
        await this.stream.invoke('sendImage', chatLink, imageBase64, text);
    }

    public async takeInProcessing(chatLink: string) {
        await this.stream.invoke('TakeInProcessing', chatLink);
    }

    public async skipMessage(message: IMessage) {
        await this.stream.invoke('skip', message);
    }

    public async callSupport(chatLink: string) {
        await this.stream.invoke('callsupport', chatLink);
    }

    public async startTyping(chantLink: string) {
        await this.stream.invoke('StartTyping', chantLink);
    }

    public async stopTyping(chantLink: string) {
        await this.stream.invoke('StopTyping', chantLink);
    }

    public get chatInfo$(): Observable<IChatInfo> {
        return this._chatInfo$.asObservable();
    }

    public get messagesList$(): Observable<IMessage[]> {
        return this._messagesList$.asObservable();
    }

    public get typing$(): Observable<ITyping> {
        return this._typing$.asObservable();
    }

    private getUserById(users: ISender[], userId: number) {
        const selectUser: ISender = users
            .filter(user => user.id === userId)[0];

        return selectUser;
    }
}
