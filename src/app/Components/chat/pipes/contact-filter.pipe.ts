import { Pipe, PipeTransform } from '@angular/core';
import { IRoom } from '../interfaces/room.interface';

@Pipe({
    name: 'contactsFilter'
})
export class ContactsFilterPipe implements PipeTransform {
    transform(value: IRoom[], args: string = ''): IRoom[] {
        const keyword = args.toLowerCase();

        if (args) {
            value = value.filter(elem => {
                try {
                    const name = elem.name.toLocaleLowerCase();
                    return name.includes(keyword);
                } catch {
                    return true;
                }
            });
        }

        return value;
    }
}
