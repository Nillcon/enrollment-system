import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatComponent } from './chat.component';
import { ChatsListModule } from './components/chats-list/chats-list.module';
import { ChatMessagesBlockModule } from './components/chat-messages-block/chat-messages-block.module';
import { SharedModule } from '@App/app-shared.module';
import { ChatHubService } from './services/chat-hub.service';
import { WindowedChatComponent } from './components/windowed-chat/windowed-chat.component';
import { ChatService } from './services/chat.service';
import { RoomHubService } from './services/room-hub.service';

@NgModule({
    declarations: [
        ChatComponent,
        WindowedChatComponent,
    ],
    imports: [
        CommonModule,
        SharedModule,
        ChatsListModule,
        ChatMessagesBlockModule
    ],
    exports: [
        ChatComponent,
        WindowedChatComponent
    ],
    providers: [
        ChatService,
        ChatHubService,
        RoomHubService
    ]
})
export class ChatModule { }
