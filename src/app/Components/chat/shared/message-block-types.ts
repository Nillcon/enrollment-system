export enum MessageBlockTypesEnum {
    Text = 0,
    Image = 1,
    Video = 2,
    Poll = 3,
    Form = 4,
    Date = 5,
    Radio = 6,
    NewDate = 7,
    Checkbox = 8
}
