import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { IUser } from '@Interfaces/users';
import { UserService } from '@Services/user.service';
import { Observable, Subscription } from 'rxjs';
import { IChatAnaliticsData } from './interfaces/chat-analitics-data.interface';
import { Roles } from '@Shared/roles';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
    // tslint:disable-next-line:component-selector
    selector: 'chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, OnDestroy {

    @Input() isSmallBlock: boolean = false;
    @Input() isShowContacts: boolean = true;
    @Input() isShowCloseButton: boolean = false;

    @Input() selectedChatLink: string = null;

    @Input() analiticsData: IChatAnaliticsData = null;

    @Output() OnSelectedChatLink: EventEmitter<string> = new EventEmitter();
    @Output() OnBackToContacts: EventEmitter<boolean> = new EventEmitter();
    @Output() OnClose: EventEmitter<boolean> = new EventEmitter();

    private readonly LimitedRoles: Roles[] = [Roles.Anonymous, Roles.InvitedUser];

    public userInfo$: Observable<IUser>;
    public chatIsOpened: boolean;

    private userInfoSubscription: Subscription;

    public Roles = Roles;

    constructor(
        private router: Router,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.userInfoObserver();
        this.initChatIsOpened();

        this.userDataObserver();
    }

    ngOnDestroy() {}

    private userInfoObserver() {
        this.userInfo$ = this.userService.Data$;
    }

    public openChat(chatLink: string): void {
        this.selectedChatLink = chatLink;

        this.reloadMessagesBlock();

        this.OnSelectedChatLink.emit(chatLink);
    }

    private userDataObserver() {
        this.userInfoSubscription = this.userService.Data$.subscribe(data => {
            if (this.LimitedRoles.includes(data.role)) {
                this.isShowContacts = false;
            }
        });
    }

    public closeMessages(): void {
        this.selectedChatLink = null;
        this.backToContacts();
        this.chatIsOpened = false;
    }
    public closeChat(): void {
        this.OnClose.emit(true);
    }

    private initChatIsOpened() {
        this.chatIsOpened = (this.selectedChatLink) ? true : false;
    }

    private reloadMessagesBlock() {
        this.chatIsOpened = false;

        setTimeout(() => {
            this.chatIsOpened = true;
        });
    }

    public backToContacts() {
        this.OnBackToContacts.emit(true);
    }

}
