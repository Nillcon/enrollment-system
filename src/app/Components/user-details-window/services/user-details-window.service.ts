import { Injectable } from "@angular/core";
import { MatDialog, MatDialogRef } from "@angular/material";
import { UserDetailsWindowComponent } from "../user-details-window.component";
import { IStaffUser } from "@Interfaces/users";

@Injectable()
export class UserDetailsWindowService {
    constructor (
        private dialog: MatDialog
    ) {}

    public open (staffUser: IStaffUser, params?: {
        isMeetButtonVisible: boolean
    }): MatDialogRef<UserDetailsWindowComponent> {
        const windowWidth: string = (innerWidth < 600) ? '95%' : '460px';

        const infoWindow = this.dialog.open(UserDetailsWindowComponent, {
            width: windowWidth,
            data: staffUser
        });

        if (params) {
            infoWindow.componentInstance.isMeetButtonVisible = params.isMeetButtonVisible;
        }

        return infoWindow;
    }
}
