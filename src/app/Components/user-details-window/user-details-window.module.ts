import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailsWindowComponent } from './user-details-window.component';
import { SharedModule } from '@App/app-shared.module';
import { UserDetailsWindowService } from './services/user-details-window.service';

@NgModule({
    declarations: [
        UserDetailsWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    exports: [
        UserDetailsWindowComponent
    ],
    entryComponents: [
        UserDetailsWindowComponent
    ],
    providers: [
        UserDetailsWindowService
    ]
})
export class UserDetailsWindowModule { }
