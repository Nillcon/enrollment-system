import { Component, OnInit, Inject, ViewChild, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IStaffUser } from '@Interfaces/users';
import { StaffService } from '@Services/staff.service';
import { tap } from 'rxjs/operators';
import { ActionResultEnum } from './shared/action-result-enum';
import { IMediaContent } from '@Components/media-preview/interfaces/media-content.interface';
import { MediaPreviewContentTypeEnum } from '@Components/media-preview/shared/content-type';
import { MediaPreviewComponent } from '@Components/media-preview/media-preview.component';

@Component({
    selector: 'app-user-details-window',
    templateUrl: './user-details-window.component.html',
    styleUrls: ['./user-details-window.component.scss']
})
export class UserDetailsWindowComponent implements OnInit {
    @ViewChild('UserVideoMediaPreview') public UserVideoMediaPreview: MediaPreviewComponent;
    @ViewChild('GalleryMediaPreview') public GalleryMediaPreview: MediaPreviewComponent;

    @Input() public isMeetButtonVisible: boolean = true;

    public readonly MediaType: typeof MediaPreviewContentTypeEnum = MediaPreviewContentTypeEnum;

    public info: IStaffUser;

    public userVideoMedia: IMediaContent[] = [];
    public galleryMedia: IMediaContent[]   = [];

    public isAboutSliced: boolean = true;

    constructor (
        public dialogRef: MatDialogRef<UserDetailsWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IStaffUser,
        private staffService: StaffService
    ) {}

    ngOnInit () {
        if (!this.data) {
            throw new Error('Component can\'t get data about user!');
        }

        this.update();
    }

    public update (): void {
        this.staffService.get(this.data.id)
            .pipe(
                tap(userData => {
                    // Create media array from user video for media preview component
                    if (userData.videoId) {
                        this.userVideoMedia.push({
                            content: userData.videoId,
                            type: MediaPreviewContentTypeEnum.YoutubeVideo
                        });
                    }

                    // Create hobbies (gallery) array for media preview component
                    userData.hobbies.forEach(hobbie => {
                        this.galleryMedia.push({
                            content: hobbie.content,
                            description: hobbie.description,
                            type: hobbie.type
                        });
                    });
                })
            )
            .subscribe(userData => {
                this.info = userData;
            });
    }

    public openVideo (): void {
        if (this.UserVideoMediaPreview) {
            this.UserVideoMediaPreview.open();
        }
    }

    public meet (): void {
        this.close(ActionResultEnum.Meet);
    }

    public close (result?: ActionResultEnum): void {
        this.dialogRef.close(result);
    }
}
