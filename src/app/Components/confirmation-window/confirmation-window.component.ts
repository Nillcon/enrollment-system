import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

interface ConfirmationData {
  title: string;
  message: string;
}

@Component({
    selector: 'app-confirmation-window',
    templateUrl: './confirmation-window.component.html',
    styleUrls: [
        './confirmation-window.component.scss'
    ]
})
export class ConfirmationWindowComponent implements OnInit {

    constructor (
        public dialogRef: MatDialogRef<ConfirmationWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ConfirmationData
    ) {}

    ngOnInit() {}

    closeWithResult (result: boolean) {
        this.dialogRef.close(result);
    }
}
