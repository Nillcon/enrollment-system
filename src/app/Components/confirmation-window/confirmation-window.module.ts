import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmationWindowComponent } from './confirmation-window.component';
import { ConfirmationWindowService } from './confirmation-window.service';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        ConfirmationWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    entryComponents: [
        ConfirmationWindowComponent
    ],
    providers: [
        ConfirmationWindowService
    ]
})
export class ConfirmationWindowModule { }
