import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material";
import { ConfirmationWindowComponent } from "./confirmation-window.component";
import { Observable } from "rxjs";

@Injectable()
export class ConfirmationWindowService {
    constructor (
        private dialogService: MatDialog
    ) {}

    public show (
        text: string = 'Are you sure want to do it?',
        title: string = 'Confirm action'
    ): Observable<boolean> {
        const window = this.dialogService.open(ConfirmationWindowComponent, {
            width: '350px',
            disableClose: true,
            data: {
                title: title,
                message: text
            }
        });

        return window.afterClosed();
    }
}
