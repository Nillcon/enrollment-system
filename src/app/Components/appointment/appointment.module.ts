import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentComponent } from './appointment.component';
import { SharedModule } from '@App/app-shared.module';
import { AppointmentCancelWindowModule } from './components/appointment-cancel-window/appointment-cancel-window.module';
import { FeedbackWindowModule } from './components/feedback-window/feedback-window.module';
import { AppointmentService } from './services/appointment.service';
import { AppointmentListComponent } from './components/appointment-list/appointment-list.component';
import { BlindPeopleAdaptationModule } from '@Components/blind-people-adaptation/blind-people-adaptation.module';

@NgModule({
    declarations: [
        AppointmentComponent,
        AppointmentListComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        AppointmentCancelWindowModule,

        FeedbackWindowModule,
        BlindPeopleAdaptationModule
    ],
    exports: [
        AppointmentComponent,
        AppointmentListComponent
    ],
    providers: [
        AppointmentService
    ]
})
export class AppointmentModule { }
