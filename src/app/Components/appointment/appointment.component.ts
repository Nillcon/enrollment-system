import { Component, OnInit, Input, Injector, Output, EventEmitter } from '@angular/core';
import { IAppointment } from './interfaces/appointment.interface';
import { IAppointmentStatus, AppointmentStatusList, AppointmentStatusEnum } from './shared/schedule-status';
import { MatDialog } from '@angular/material';
import { IStaffUser } from '@Interfaces/users';
import { UserDetailsWindowService } from '@Components/user-details-window/services/user-details-window.service';
import { AppointmentCancelWindowComponent } from './components/appointment-cancel-window/appointment-cancel-window.component';
import { filter } from 'rxjs/operators';
import { FeedbackWindowComponent } from './components/feedback-window/feedback-window.component';

@Component({
    selector: 'app-appointment',
    templateUrl: './appointment.component.html',
    styleUrls: ['./appointment.component.scss']
})
export class AppointmentComponent implements OnInit {
    @Input() public data: IAppointment;

    @Output() public OnCancel: EventEmitter<IAppointment>   = new EventEmitter();
    @Output() public OnFeedback: EventEmitter<IAppointment> = new EventEmitter();

    public readonly ScheduleStatus: typeof AppointmentStatusEnum      = AppointmentStatusEnum;
    public readonly StatusList: { [key: number]: IAppointmentStatus } = AppointmentStatusList;

    constructor (
        public injector: Injector,
        private userDetailsWindowService: UserDetailsWindowService,
        private dialog: MatDialog
    ) {}

    ngOnInit () {
        if (!this.data) {
            throw new Error('Component can\'t resolve required @Input parameters!');
        }
    }

    public openUserDetails (staffUser: IStaffUser): void {
        this.userDetailsWindowService.open(staffUser);
    }

    public feedback (): void {
        const windowMode: string = 'addition';

        const feedbackWindow = this.dialog.open(FeedbackWindowComponent, {
            width: '450px',
            data: {
                mode: windowMode,
                appointmentId: this.data.id
            }
        });

        feedbackWindow.afterClosed()
            .pipe(
                filter(result => result === true)
            )
            .subscribe(() => {
                this.OnFeedback.emit(this.data);
            });
    }

    public cancel (): void {
        const cancelWindow = this.dialog.open(AppointmentCancelWindowComponent, {
            width: '450px',
            data: this.data.id
        });

        cancelWindow.afterClosed()
            .pipe(
                filter(result => result === true)
            )
            .subscribe(() => {
                this.OnCancel.emit(this.data);
            });
    }
}
