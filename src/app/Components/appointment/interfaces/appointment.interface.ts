import { IStaffUser, IInvitedUser } from "../../../Interfaces/users";
// tslint:disable-next-line: max-line-length
import { IPlannedAppointment } from "@Layouts/dashboard-layout/pages/appointments-page/components/appointments-window/pages/info-page/interfaces/planned-appointment-model.interface";
import { AppointmentTypeEnum } from "../shared/appointment-type";

export interface IAppointment {
    id: number;
    greeter: number;
    greeterModel: IStaffUser;
    // Prepearing on Frontend and contains visitor name
    visitor: string;
    visitorModel: IInvitedUser;
    campaignName: string;
    status: any;
    date: string;
    comment: string;
    buttonName: string;
    type: AppointmentTypeEnum;
    parentId: number;
    repeatedAppointments: IPlannedAppointment[];
    whoCreatedId: number;
    whoCreatedType: number;
}
