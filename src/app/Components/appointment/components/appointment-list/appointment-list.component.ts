import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IAppointment } from '@Components/appointment/interfaces/appointment.interface';
import { AppAltObjectsEnum } from '@Components/blind-people-adaptation/shared/app-alt-objects';

@Component({
    selector: 'app-appointment-list',
    templateUrl: './appointment-list.component.html',
    styleUrls: ['./appointment-list.component.scss']
})
export class AppointmentListComponent implements OnInit {
    public readonly AltObjects: typeof AppAltObjectsEnum = AppAltObjectsEnum;

    @ViewChild('AppointmentsBlock')
    private AppointmentsBlock: any;

    @Input()
    public appointments: IAppointment[];

    @Output()
    public OnCancel: EventEmitter<IAppointment> = new EventEmitter();

    @Output()
    public OnFeedback: EventEmitter<boolean> = new EventEmitter();

    private scrollRange: number = 325;

    constructor () {}

    ngOnInit () {}

    public scrollLeft (): void {
        const currentScrollX: number = this.AppointmentsBlock.nativeElement.scrollLeft;
        this.AppointmentsBlock.nativeElement.scrollTo(currentScrollX - this.scrollRange, 0);
    }

    public scrollRight (): void {
        const currentScrollX: number = this.AppointmentsBlock.nativeElement.scrollLeft;
        this.AppointmentsBlock.nativeElement.scrollTo(currentScrollX + this.scrollRange, 0);
    }
}
