import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AppointmentService } from '@Components/appointment/services/appointment.service';
import { NotificationService } from '@Services/notification.service';
import { WindowMode } from '@Types/window-mode.type';
import { SaveMode } from '@Types/save-mode.type';

interface IData {
    mode: WindowMode;
    appointmentId: number;
}

@Component({
    selector: 'app-feedback-window',
    templateUrl: './feedback-window.component.html',
    styleUrls: ['./feedback-window.component.scss']
})
export class FeedbackWindowComponent implements OnInit {
    public feedbackFormGroup: FormGroup;
    public feedbackFormControl: FormControl;

    public isLoading: boolean = false;

    constructor (
        private dialogRef: MatDialogRef<FeedbackWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public data: IData,
        private appointmentService: AppointmentService,
        private notificationService: NotificationService
    ) {}

    ngOnInit () {
        this.initForm();
    }

    public save (): void {
        if (this.feedbackFormGroup.valid) {
            const savingMode: SaveMode = (this.data.mode === 'addition') ? 'create' : 'edit';
            this.isLoading = true;

            this.appointmentService.saveFeedback(
                this.data.appointmentId,
                this.feedbackFormControl.value,
                savingMode
            )
                .subscribe(
                    () => {
                        this.isLoading = false;
                        this.close(true);
                    },
                    () => {
                        this.isLoading = false;
                    }
                );
        } else {
            this.notificationService.error('Oops', 'An error occurred while trying to save feedback.');
        }
    }

    public close (result: boolean = false): void {
        this.dialogRef.close(result);
    }

    private initForm (): void {
        this.feedbackFormControl = new FormControl('');

        this.feedbackFormGroup = new FormGroup({
            feedback: this.feedbackFormControl
        });
    }
}
