import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeedbackWindowComponent } from './feedback-window.component';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        FeedbackWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    exports: [
        FeedbackWindowComponent
    ],
    entryComponents: [
        FeedbackWindowComponent
    ]
})
export class FeedbackWindowModule { }
