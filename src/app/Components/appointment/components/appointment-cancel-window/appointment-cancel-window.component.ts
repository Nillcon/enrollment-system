import { Component, OnInit, Inject } from '@angular/core';
import { AppointmentService } from '@Components/appointment/services/appointment.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
    selector: 'app-appointment-cancel-window',
    templateUrl: './appointment-cancel-window.component.html',
    styleUrls: ['./appointment-cancel-window.component.scss'],
    providers: [
        AppointmentService
    ]
})
export class AppointmentCancelWindowComponent implements OnInit {
    public commentFormGroup: FormGroup;
    public commentFormControl: FormControl;

    public isLoading: boolean = false;

    constructor (
        private dialogRef: MatDialogRef<AppointmentCancelWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public appointmentId: number,
        private appointmentService: AppointmentService
    ) {}

    ngOnInit () {
        this.initForm();
    }

    public save (): void {
        this.isLoading = true;

        this.appointmentService.cancel(this.appointmentId, this.commentFormControl.value)
            .subscribe(
                () => {
                    this.isLoading = false;
                    this.close(true);
                },
                () => {
                    this.isLoading = false;
                }
            );
    }

    public close (result: boolean = false): void {
        this.dialogRef.close(result);
    }

    private initForm (): void {
        this.commentFormControl = new FormControl('');

        this.commentFormGroup = new FormGroup({
            comment: this.commentFormControl
        });
    }
}
