import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppointmentCancelWindowComponent } from './appointment-cancel-window.component';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        AppointmentCancelWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    exports: [
        AppointmentCancelWindowComponent
    ],
    entryComponents: [
        AppointmentCancelWindowComponent
    ]
})
export class AppointmentCancelWindowModule {}
