export enum AppointmentTypeEnum {
    Single   = 0,
    Repeated = 1
}
