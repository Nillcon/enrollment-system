export enum AppointmentStatusEnum {
    Pending  = 1,
    Approved = 2,
    Canceled = 3,
    Modified = 4,
    Finished = 5
}

export interface IAppointmentStatus {
    id: AppointmentStatusEnum;
    name: string;
    colorClass: string;
    iconClass: string;
}

export const AppointmentStatusList: { [key: number]: IAppointmentStatus } = {
    [AppointmentStatusEnum.Pending]: {
        id: AppointmentStatusEnum.Pending,
        name: 'In pending',
        colorClass: 'text-primary',
        iconClass: 'fas fa-spinner'
    },
    [AppointmentStatusEnum.Approved]: {
        id: AppointmentStatusEnum.Approved,
        name: 'Approved',
        colorClass: 'text-success',
        iconClass: 'fas fa-check'
    },
    [AppointmentStatusEnum.Canceled]: {
        id: AppointmentStatusEnum.Canceled,
        name: 'Canceled',
        colorClass: 'text-danger',
        iconClass: 'fas fa-times'
    },
    [AppointmentStatusEnum.Modified]: {
        id: AppointmentStatusEnum.Modified,
        name: 'Modified',
        colorClass: 'text-warning',
        iconClass: 'fas fa-pencil-alt'
    },
    [AppointmentStatusEnum.Finished]: {
        id: AppointmentStatusEnum.Finished,
        name: 'Finished',
        colorClass: 'text-secondary',
        iconClass: 'fas fa-check-double'
    }
};

export function getAppointmentStatusListAsArray (): IAppointmentStatus[] {
    return Object.values(AppointmentStatusList);
}
