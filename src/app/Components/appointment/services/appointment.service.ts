import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable } from "rxjs";
import { IAppointment } from "../interfaces/appointment.interface";
import { AppointmentStatusEnum } from "../shared/schedule-status";
import { map, flatMap, toArray } from "rxjs/operators";

type SavingMode = 'create' | 'edit';

@Injectable()
export class AppointmentService {
    constructor (
        private httpService: HttpRequestService
    ) {}

    public getList (): Observable<IAppointment[]> {
        return this.httpService.get<IAppointment[]>(`/Appointments`, {})
            .pipe(
                flatMap(appointments => appointments),
                map(appointment => {
                    appointment.visitor = appointment.visitorModel.name;

                    return appointment;
                }),
                toArray()
            );
    }

    public get (appointmentId: number): Observable<IAppointment> {
        return this.httpService.get<IAppointment>(`/Appointments/${appointmentId}`, {})
            .pipe(
                map(appointment => {
                    appointment.visitor = appointment.visitorModel.name;

                    return appointment;
                })
            );
    }

    public downloadChat (appointment: IAppointment): Observable<any> {
        return this.httpService.downloadFile(`/Appointments/${appointment.id}/Chat`, {
            name: `${appointment.greeterModel.name} chat`,
            type: '.pdf'
        });
    }

    public create (): Observable<any> {
        return this.httpService.post(`/Appointments`, {});
    }

    public edit (): Observable<any> {
        return this.httpService.put(`/Appointments`, {});
    }

    public cancel (appointmentId: number, reason: string): Observable<any> {
        return this.httpService.put(`/Appointments/${appointmentId}/Cancel`, { value: reason });
    }

    public leaveFeedback (appointmentId: number, feedback: string): Observable<any> {
        // FIXME: Change on post in future
        return this.httpService.put(`/Appointments/${appointmentId}/Feedbacks`, { value: feedback });
    }

    public editFeedback (appointmentId: number, feedback: string): Observable<any> {
        return this.httpService.put(`/Appointments/${appointmentId}/Feedbacks`, { value: feedback });
    }

    public saveFeedback (appointmentId: number, feedback: string, mode: SavingMode): Observable<any> {
        return (mode === 'create') ?
            this.leaveFeedback(appointmentId, feedback) :
            this.editFeedback(appointmentId, feedback);
    }

    public changeStatuses (appointments: number[], status: AppointmentStatusEnum): Observable<any> {
        return this.httpService.put(`/Appointments/GroupChangeStatus`, {
            targetType: status,
            ids: appointments
        });
    }

    public delete (appointmentId: number): Observable<any> {
        return this.httpService.delete(`/Appointments/${appointmentId}`);
    }

}
