import { MediaPreviewContentTypeEnum } from "../shared/content-type";

export interface IMediaContent {
    /**
     * If type === YoutubeVideo, then content is videoId with type string;
     * else content is url with type string
     */
    content: string;
    type: MediaPreviewContentTypeEnum;
    description?: string;
}
