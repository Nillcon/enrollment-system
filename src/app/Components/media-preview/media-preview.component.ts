import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IMediaContent } from './interfaces/media-content.interface';
import { MediaPreviewContentTypeEnum } from './shared/content-type';

@Component({
    selector: 'app-media-preview',
    templateUrl: './media-preview.component.html',
    styleUrls: ['./media-preview.component.scss']
})
export class MediaPreviewComponent implements OnInit {
    @Output() public OnOpened: EventEmitter<any> = new EventEmitter();
    @Output() public OnClosed: EventEmitter<any> = new EventEmitter();

    @Input() public media: IMediaContent[] = [];

    public readonly ContentType: typeof MediaPreviewContentTypeEnum = MediaPreviewContentTypeEnum;

    private _openedMediaIndex: number = null;
    public get OpenedMediaIndex (): number {
        return this._openedMediaIndex;
    }

    public _isOpened: boolean = false;
    public get IsOpened (): boolean {
        return this._isOpened;
    }

    constructor () {}

    ngOnInit () {}

    /** Data as `number` is index of item from media array wich will be oppened
     * Data as `IMediaContent` is item wich will be opened
    */
    public open (mediaIndex: number = 0): void {
        if (!this.media[mediaIndex]) {
            throw new Error(`There is no media content with index "${mediaIndex}"`);
        } else {
            this._openedMediaIndex = mediaIndex;
        }

        this._isOpened = true;
        this.OnOpened.emit();
    }

    public close (): void {
        this._isOpened = false;
        this.OnClosed.emit();
    }

    public next (): void {
        if (this.media[this.OpenedMediaIndex + 1]) {
            this._openedMediaIndex++;
        }
    }

    public previous (): void {
        if (this.media[this.OpenedMediaIndex - 1]) {
            this._openedMediaIndex--;
        }
    }
}
