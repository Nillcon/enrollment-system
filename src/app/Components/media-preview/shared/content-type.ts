export enum MediaPreviewContentTypeEnum {
    Image           = 0,
    YoutubeVideo    = 1,
    Video           = 2
}
