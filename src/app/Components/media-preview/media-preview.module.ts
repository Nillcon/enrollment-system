import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatVideoModule } from 'mat-video';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { MediaPreviewComponent } from './media-preview.component';
import { MatTooltipModule } from '@angular/material';
import { LoaderModule } from '@Components/loader/loader.module';

@NgModule({
    declarations: [
        MediaPreviewComponent
    ],
    imports: [
        CommonModule,

        MatVideoModule,
        NgxYoutubePlayerModule,
        MatTooltipModule,
        LoaderModule
    ],
    exports: [
        MediaPreviewComponent
    ]
})
export class MediaPreviewModule { }
