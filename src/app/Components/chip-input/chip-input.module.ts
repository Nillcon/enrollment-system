import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChipInputComponent } from './chip-input.component';
import { MatInputModule, MatChipsModule, MatAutocompleteModule } from '@angular/material';
import { AutocompleteFilterPipe } from './filter.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        ChipInputComponent,
        AutocompleteFilterPipe
    ],
    imports: [
        CommonModule,
        FormsModule,

        MatChipsModule,
        MatInputModule,
        MatAutocompleteModule
    ],
    exports: [
        ChipInputComponent
    ]
})
export class ChipInputModule { }
