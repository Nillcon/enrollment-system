import { Component, OnInit, forwardRef, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatAutocomplete } from '@angular/material';

@Component({
    selector: 'app-chip-input',
    templateUrl: './chip-input.component.html',
    styleUrls: ['./chip-input.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ChipInputComponent),
            multi: true
        }
    ]
})
export class ChipInputComponent implements OnInit, ControlValueAccessor {
    @ViewChild('Autocompete') private Autocomplete: MatAutocomplete;
    @ViewChild('InputElem') private InputElem: { nativeElement: { value: string; }; };

    @Input() public allowCustomChipValue: boolean = false;
    @Input() public placeholder: string = '';
    @Input() public prefixClass: string = '';

    @Input() public maxCountOfChips: number = Infinity;

    private _chips: string[] = [];

    /** chips for each chip in input element */
    @Input() public set chips (newChips: any[]) {
        if (newChips) {
            this._chips = newChips.map(chip => {
                return String(chip);
            });
        }
    }

    /** chips for each chip in input element */
    public get chips (): any[] {
        return this._chips;
    }

    /** The key of object of autocomplete item, which contains needed value to display for user */
    @Input() public autocompleteDisplayExpr: string;

    /** The key of object of autocomplete item, which contains needed value to use it as value for some chip */
    @Input() public autocompleteValueExpr: string;

    private _autocompleteItems: string[] | object[] = [];
    @Input() public set autocompleteItems (items: string[] | object[]) {
        this._autocompleteItems = items;
        this.initTypeOfAutocompleteItems();
    }
    public get autocompleteItems (): string[] | object[] {
        return this._autocompleteItems;
    }

    @Output() public change: EventEmitter<any[]> = new EventEmitter();

    @Input() public set value(chips: any) {
        if (chips) {
            if (typeof chips === typeof Array) {
                this.chips = chips;
            } else {
                this.chips = [chips];
            }
        }
    }

    public isAutocompleteItemsObjects: boolean = false;
    public autocompleteItemsAsObject: object;

    public autocompleteText: string;

    public readonly separatorKeysCodes: number[] = [ENTER, COMMA];

    constructor () {}

    ngOnInit () {
        this.initTypeOfAutocompleteItems();
    }

    public addValue (value: any): void {
        const targetValue = this.getTargetValue(value);

        if (targetValue) {
            this.chips.push(targetValue);
        }

        this.InputElem.nativeElement.value = '';

        this.writeValue();
    }

    public removeValue (value: any): void {
        const targetValue   = this.getTargetValue(value);
        const index         = this.chips.indexOf(targetValue);

        if (index >= 0) {
            this.chips.splice(index, 1);
        }

        this.writeValue();
    }

    private getTargetValue (value: any = ''): string {
        if (value) {
            if (typeof(value) === 'object') {
                if (this.autocompleteValueExpr) {
                    return String(value[this.autocompleteValueExpr]).trim();
                } else {
                    return value;
                }
            } else {
                return String(value).trim();
            }
        }
    }

    public registerOnChange (fn: Function): void {
        this.onChange = fn;
    }

    public registerOnTouched (): void {}

    public writeValue (chips?: any): void {

        if (chips) {
            if (this.chips instanceof Array) {
                this.chips = chips;
            } else {
                this.chips = [chips];
            }
        }

        this.onChange(this.chips);
        this.change.emit(this.chips);
    }

    public onChange: Function = () => {};

    private initTypeOfAutocompleteItems (): void {
        if (this._autocompleteItems && this._autocompleteItems.length) {
            if (typeof(this._autocompleteItems[0]) === 'object') {
                this.isAutocompleteItemsObjects = true;
                this.autocompleteItemsAsObject  = {};

                if (!this.autocompleteValueExpr && !this.autocompleteDisplayExpr) {
                    // tslint:disable-next-line: max-line-length
                    throw new Error('Chip input component with autocomplete items as objects, require @Input params: autocompleteValueExpr & autocompleteDisplayExpr');
                }

                for (const item of this._autocompleteItems) {
                    this.autocompleteItemsAsObject[item[this.autocompleteValueExpr]] = item;
                }
            }
        }
    }
}
