import { PipeTransform, Pipe } from "@angular/core";

@Pipe({
    name: 'autocompleteFilter'
})
export class AutocompleteFilterPipe implements PipeTransform {
    transform(
        value: any[],
        isObject: boolean = false,
        autocompleteValueExpr: string = '',
        args: string
    ): string[] | object[] {
        if (args) {
            const keyword = args.toLowerCase();

            value = value.filter((elem: string | object) => {
                try {
                    let name: string = (isObject) ? elem[autocompleteValueExpr] : elem;
                    name = name.toLocaleLowerCase();

                    return name.includes(keyword);
                } catch {
                    return true;
                }
            });
        }

        return value;
    }
}
