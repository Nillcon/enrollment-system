import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScheduleComponent } from './schedule.component';
import { RangeDateModule } from '@Components/range-date/range-date.module';
import { DaysOffComponent } from './components/days-off/days-off.component';
import { SchedulePickerComponent } from './components/schedule-picker/schedule-picker.component';
import { DxDateBoxModule, DxScrollViewModule } from 'devextreme-angular';
import { SharedModule } from '@App/app-shared.module';
import { MatDividerModule, MatExpansionModule } from '@angular/material';
import { DatePickerWindowModule } from '@Components/date-picker-window/date-picker-window.module';

@NgModule({
    declarations: [
        ScheduleComponent,
        DaysOffComponent,
        SchedulePickerComponent
    ],
    imports: [
        CommonModule,
        SharedModule,

        RangeDateModule,
        DatePickerWindowModule,

        MatDividerModule,
        MatExpansionModule,

        DxDateBoxModule,
        DxScrollViewModule
    ],
    entryComponents: [
        ScheduleComponent
    ],
    exports: [
        ScheduleComponent
    ]
})
export class ScheduleModule { }
