import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { IScheduleView } from '../interfaces/schedule-view.interface';
import { map } from 'rxjs/operators';
import { HttpRequestService } from '@Services/http-request.service';
import { UserService } from '@Services/user.service';
import { ByteMaskWorker } from '@Methods/byteMaskWorker.methods';

@Injectable()
export class ScheduleService {

    public readonly numberDaysOfWeek = 7;

    constructor(
        private httpService: HttpRequestService,
        private userService: UserService
    ) { }

    public saveScheduleView(staffId: number, scheduleView: IScheduleView): Observable<any> {
        return this.httpService.post(`/Accounts/${staffId}/Schedules`, scheduleView);
    }

    public getScheduleView(staffId: number): Observable<IScheduleView> {
        return this.httpService.get<IScheduleView>(`/Accounts/${staffId}/Schedules`)
            .pipe(
                map(res => {
                    res.schedules
                        .map(elem => {
                            elem.daysOfWeekBinaryArray = ByteMaskWorker
                                .getBinaryArray(elem.daysOfWeek, this.numberDaysOfWeek)
                                .reverse();

                            return elem;
                        });
                    return res;
                })
            );
    }
}
