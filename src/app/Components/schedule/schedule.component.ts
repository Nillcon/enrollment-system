import { Component, OnInit, Input } from '@angular/core';
import { ScheduleService } from './services/schedule.service';
import { IScheduleView } from './interfaces/schedule-view.interface';
import { IDateRange } from '@Components/range-date/interfaces/date-range.interface';
import { map, tap } from 'rxjs/operators';
import { DateMethod } from '@Methods/date.method';
import { scheduled } from 'rxjs';
import { ISchedule } from './interfaces/schedule.interface';

@Component({
    selector: 'app-schedule',
    templateUrl: './schedule.component.html',
    styleUrls: ['./schedule.component.scss'],
    providers: [
        ScheduleService
    ]
})
export class ScheduleComponent implements OnInit {

    @Input() public staffUserId: number;

    public scheduleView: IScheduleView;

    public inputDate: IDateRange;

    constructor(
        private scheduleService: ScheduleService,
    ) { }

    ngOnInit() {
        this.updateScheduleView();
    }

    public updateScheduleView() {
        this.scheduleService.getScheduleView(this.staffUserId)
            .pipe(
                tap(res => {
                    this.inputDate = {
                        from: new Date(res.fromDay),
                        to: new Date(res.toDay)
                    };
                })
            )
            .subscribe(res => this.scheduleView = res);
    }

    public saveScheduleView() {
        console.log(this.scheduleView.schedules);
        // this.scheduleView.schedules = this.scheduleView.schedules
        //     .map(schedule => {
        //         console.log(schedule);
        //         schedule.fromTime = DateMethod.getLocalTimeString(new Date(schedule.fromTime));
        //         schedule.toTime = DateMethod.getLocalTimeString(new Date(schedule.toTime));

        //         console.log(schedule);

        //         return schedule;
        //     });
        this.scheduleService.saveScheduleView(this.staffUserId, this.scheduleView)
            .subscribe();
    }

    public onChange (schedules: ISchedule[]) {
        this.scheduleView.schedules = schedules;
        console.log(this.scheduleView.schedules);
    }

    public dateRangeOnChange(dateRange: IDateRange) {
        this.scheduleView.fromDay = DateMethod.getLocalTimeString(dateRange.from);
        this.scheduleView.toDay = DateMethod.getLocalTimeString(dateRange.to);
    }

}
