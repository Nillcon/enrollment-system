import { Component, OnInit, Input, Output, EventEmitter, Injector } from '@angular/core';
import { ISchedule } from '@Components/schedule/interfaces/schedule.interface';
import { ByteMaskWorker } from '@Methods/byteMaskWorker.methods';
import { DateMethod } from '@Methods/date.method';

@Component({
  selector: 'app-schedule-picker',
  templateUrl: './schedule-picker.component.html',
  styleUrls: ['./schedule-picker.component.scss']
})
export class SchedulePickerComponent implements OnInit {

    @Input() scheduleList: ISchedule[];
    @Input() height: number;

    @Output() OnChange: EventEmitter<ISchedule[]> = new EventEmitter();

    public readonly daysOfWeek: string[] = [
        'Su',
        'Mo',
        'Tu',
        'We',
        'Th',
        'Fr',
        'Sa'
    ];

    constructor(
        public injector: Injector
    ) { }

    ngOnInit() {
    }

    public addSchedule() {
        this.scheduleList.push({
            daysOfWeek: 0,
            fromTime: '2000-01-01T09:00:00',
            toTime: '2000-01-01T18:00:00',
            daysOfWeekBinaryArray: Array(7).fill(0)
        });

        this.callOnChange();
    }

    public changeDayStatus(scheduleIndex: number, dayIndex: number, value: number) {
        this.scheduleList[scheduleIndex].daysOfWeekBinaryArray[dayIndex] = value;

        const bufferBinaryArray = [...this.scheduleList[scheduleIndex].daysOfWeekBinaryArray];

        this.scheduleList[scheduleIndex].daysOfWeek = ByteMaskWorker.getIntFromBinaryArray(
            bufferBinaryArray.reverse()
        );

        this.callOnChange();
    }

    public callOnChange() {
        this.OnChange.emit(this.scheduleList);
    }

    public deleteSchedule(index: number) {
        this.scheduleList.splice(index, 1);
    }

    public dateRangeOnChange (date: any, index: number) {
        this.callOnChange();
    }

}
