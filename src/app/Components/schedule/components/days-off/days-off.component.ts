import { Component, OnInit, Input, Injector } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DatePickerWindowComponent } from '@Components/date-picker-window/date-picker-window.component';
import { NeedsConfirmation } from '@Decorators/needs-confirmation.decorator';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-days-off',
    templateUrl: './days-off.component.html',
    styleUrls: ['./days-off.component.scss']
})
export class DaysOffComponent implements OnInit {

    @Input() daysOff: string[];

    @Input() height: number;

    constructor(
        private dialog: MatDialog,
        public injector: Injector
    ) { }

    ngOnInit() {
    }

    public addDayOff() {
        const datePicker = this.dialog.open(DatePickerWindowComponent);

        datePicker.afterClosed()
            .pipe(
                filter(res => !!res)
            )
            .subscribe((res: string) => {
                this.daysOff.push(res);
            });

    }

    public deleteDayOff(index: number) {
        this.daysOff.splice(index, 1);
    }

}
