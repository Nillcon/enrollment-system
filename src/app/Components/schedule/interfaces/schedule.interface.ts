export interface ISchedule {
    id?: number;
    fromTime?: string;
    toTime?: string;
    daysOfWeek: number;
    daysOfWeekBinaryArray?: number[];
}

