import { ISchedule } from "./schedule.interface";

export interface IScheduleView {
    fromDay: string;
    toDay: string;
    schedules: ISchedule[];
    daysOff: string[];
}
