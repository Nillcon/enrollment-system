import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserVariablesTableComponent } from './user-variables-table.component';
import { SharedModule } from '@App/app-shared.module';
import { DashboardLayoutSharedModule } from '@Layouts/dashboard-layout/dashboard-layout-shared.module';
import { VariableWindowComponent } from './components/variable-window/variable-window.component';
import { UserVariablesWindowComponent } from './components/user-variables-window/user-variables-window.component';

@NgModule({
    declarations: [
        UserVariablesTableComponent,
        UserVariablesWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule,
        DashboardLayoutSharedModule
    ],
    exports: [
        UserVariablesTableComponent,
        UserVariablesWindowComponent
    ],
    entryComponents: [
        VariableWindowComponent,
        UserVariablesTableComponent,
        UserVariablesWindowComponent
    ]
})
export class UserVariablesTableModule { }
