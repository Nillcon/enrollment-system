import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { InvitedUserService } from '@Services/invited-user.service';
import { IUserVariable } from '@Interfaces/user-variable.interface';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '@angular/material';
import { VariableWindowComponent } from './components/variable-window/variable-window.component';
import { Roles } from '@Shared/roles';
import { IUser } from '@Interfaces/users';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { UserService } from '@Services/user.service';

@AutoUnsubscribe()
@Component({
    selector: 'app-user-variables-table',
    templateUrl: './user-variables-table.component.html',
    styleUrls: ['./user-variables-table.component.scss']
})
export class UserVariablesTableComponent implements OnInit, OnDestroy {
    @ViewChild(MatPaginator) private paginator: MatPaginator;
    @ViewChild(MatSort)      private sort: MatSort;

    @Input() private invitedUserId: number;

    public _invitedUserId: number;

    public readonly Roles: typeof Roles = Roles;

    public userData: IUser;

    public readonly DisplayedColumns: Array<string> = [
        'name', 'value', 'edit'
    ];

    public dataSource: MatTableDataSource<IUserVariable> = new MatTableDataSource();

    public isCsvLoading: boolean = false;

    private userDataSubscription: Subscription;

    constructor (
        private invitedUserService: InvitedUserService,
        private dialog: MatDialog,
        private userService: UserService
    ) {}

    ngOnInit () {
        this._invitedUserId = this.invitedUserId;

        if (!this._invitedUserId ) {
            throw new Error('Component can\'t resolve required @Input parameters!');
        }

        this.update();
        this.userDataObserver();
    }

    ngOnDestroy () {}

    public update (): void {
        this.invitedUserService.getVariables(this._invitedUserId)
            .subscribe(variables => {
                this.dataSource.data = variables;
            });
    }

    public search (phrase: string): void {
        this.dataSource.filter = phrase.trim().toLowerCase();
    }

    public openAdditionWindow (): void {
        const additionWindow = this.dialog.open(VariableWindowComponent, {
            width: '450px',
            data: {
                invitedUserId: this._invitedUserId,
                mode: 'addition'
            }
        });

        additionWindow.afterClosed()
            .subscribe(() => this.update());
    }

    public openEditingWindow (variable: IUserVariable): void {
        const editingWindow = this.dialog.open(VariableWindowComponent, {
            width: '450px',
            data: {
                invitedUserId: this._invitedUserId,
                variable: variable,
                mode: 'editing'
            }
        });

        editingWindow.afterClosed()
            .subscribe(() => this.update());
    }

    private userDataObserver (): void {
        this.userDataSubscription = this.userService.Data$
            .subscribe(data => {
                this.userData = data;
            });
    }
}
