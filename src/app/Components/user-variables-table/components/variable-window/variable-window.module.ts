import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VariableWindowComponent } from './variable-window.component';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        VariableWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule
    ],
    exports: [
        VariableWindowComponent
    ],
    entryComponents: [
        VariableWindowComponent
    ]
})
export class VariableWindowModule { }
