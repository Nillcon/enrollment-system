import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-user-variables-window',
    templateUrl: './user-variables-window.component.html',
    styleUrls: ['./user-variables-window.component.scss']
})
export class UserVariablesWindowComponent implements OnInit {

    constructor (
        @Inject(MAT_DIALOG_DATA) public invitedUserId: number
    ) {}

    ngOnInit () {}

}
