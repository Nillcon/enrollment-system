import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { ImageCropperComponent, ImageCroppedEvent } from 'ngx-image-cropper';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Base64Worker } from '@Methods/base64Worker.method';

@Component({
    selector: 'app-photo-cropper-window',
    templateUrl: './photo-cropper-window.component.html',
    styleUrls: ['./photo-cropper-window.component.scss']
})
export class PhotoCropperWindowComponent implements OnInit {
    public croppedImage: string   = null;
    public showCropper: boolean   = false;

    @ViewChild(ImageCropperComponent) imageCropper: ImageCropperComponent;

    constructor (
        private dialogRef: MatDialogRef<PhotoCropperWindowComponent>,
        @Inject(MAT_DIALOG_DATA) public imageAsBase64: string
    ) {}

    ngOnInit () {}

    public imageCropped (event: ImageCroppedEvent): void {
        this.croppedImage = event.base64;
    }

    public imageLoaded (): void {
        this.showCropper = true;
    }

    public close (result?: string): void {
        this.dialogRef.close(result);
    }

    public save (): void {
        if (this.croppedImage) {
            const encodedBase64: string = Base64Worker.encodeToBytes(this.croppedImage);
            this.close(encodedBase64);
        }
    }
}
