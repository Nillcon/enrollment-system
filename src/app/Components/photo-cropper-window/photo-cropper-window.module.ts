import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhotoCropperWindowComponent } from './photo-cropper-window.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { SharedModule } from '@App/app-shared.module';
@NgModule({
    declarations: [
        PhotoCropperWindowComponent
    ],
    imports: [
        CommonModule,

        SharedModule,

        ImageCropperModule
    ],
    exports: [
        PhotoCropperWindowComponent
    ]
})
export class PhotoCropperWindowModule { }
