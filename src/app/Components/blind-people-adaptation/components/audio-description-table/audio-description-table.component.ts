import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BlindPeopleAdaptationService } from '../../services/blind-people-adaptation.service';
import { IAdvancedAppAltObject } from '../../interfaces/advanced-app-alt-object.interface';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { first } from 'rxjs/operators';
import { AppAltObjectTypeEnum } from '@Components/blind-people-adaptation/shared/app-alt-object-type';
import { NotificationService } from '@Services/notification.service';

@AutoUnsubscribe()
@Component({
    selector: 'app-audio-description-table',
    templateUrl: './audio-description-table.component.html',
    styleUrls: ['./audio-description-table.component.scss']
})
export class AudioDescriptionTableComponent implements OnInit, OnDestroy {

    public AppAltObjectTypeEnum = AppAltObjectTypeEnum;

    public advancedAltObjectSubscription: Subscription;
    public advancedAltObject: IAdvancedAppAltObject[];

    public displayedColumns: string[] = ['image', 'value'];

    constructor(
        private blindPeopleAdaptationService: BlindPeopleAdaptationService,
        private notificationService: NotificationService
    ) { }

    ngOnInit() {
        this.initAdvancedAltObjectSubscription();
    }

    ngOnDestroy() {}

    private initAdvancedAltObjectSubscription() {
        this.advancedAltObjectSubscription = this.blindPeopleAdaptationService.Data$
            .pipe(
                first(),
                map(altObjectsAsDict => {
                    return Object.values(altObjectsAsDict);
                })
            )
            .subscribe(res => {
                this.advancedAltObject = <IAdvancedAppAltObject[]>res;
            });
    }

    public saveAltObjects() {
        this.blindPeopleAdaptationService.save(this.advancedAltObject)
            .subscribe(() =>
                this.notificationService.success('Successful save'));
    }
}
