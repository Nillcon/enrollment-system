import { Injectable } from "@angular/core";
import { HttpRequestService } from "@Services/http-request.service";
import { Observable, BehaviorSubject } from "rxjs";
import { IBaseAppAltObject } from "../interfaces/base-app-alt-object.interface";
import { filter, tap, map, flatMap, toArray } from "rxjs/operators";
import { IAdvancedAppAltObject } from "../interfaces/advanced-app-alt-object.interface";
import { AltObjectInfo } from "../shared/app-alt-objects";
import { AppAltObjectTypeEnum } from "../shared/app-alt-object-type";

@Injectable()
export class BlindPeopleAdaptationService {
    private _data: BehaviorSubject<{ [key: number]: IAdvancedAppAltObject }> = new BehaviorSubject(null);
    public get Data$ (): Observable<{ [key: number]: IAdvancedAppAltObject }> {
        return this._data.asObservable()
            .pipe(
                filter(data => data !== null)
            );
    }

    constructor (
        private httpService: HttpRequestService
    ) {
        this.update()
            .subscribe();
    }

    public update (): Observable<{ [key: number]: IAdvancedAppAltObject }> {
        const listOfIds = Object.keys(AltObjectInfo);

        return this.httpService.get<IBaseAppAltObject[]>(`/AltTags?value=${JSON.stringify(listOfIds)}`)
            .pipe(
                map(altObjectsAsArray => {
                    const baseAltObjects: { [key: number]: IBaseAppAltObject } = {};
                    const advancedAltObjects: { [key: number]: IAdvancedAppAltObject } = {};

                    // FIXME: Remove this foreach and change type of data in string 30
                    altObjectsAsArray.forEach(altObj => {
                        baseAltObjects[altObj.id] = altObj;
                    });

                    for (const [id] of Object.entries(AltObjectInfo)) {
                        advancedAltObjects[id] = (baseAltObjects[id]) ? (baseAltObjects[id]) : {};

                        for (const [currAltObjKey, currAltObjVal] of Object.entries(AltObjectInfo[id])) {
                            advancedAltObjects[id][currAltObjKey] = currAltObjVal;
                        }
                    }

                    this._data.next(advancedAltObjects);

                    return advancedAltObjects;
                })
            );
    }

    public save(altObjects: IAdvancedAppAltObject[]) {
        const data = {
            value: JSON.stringify(altObjects)
        };
        return this.httpService.put('/AltTags', data);
    }
}
