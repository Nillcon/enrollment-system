import { ElementRef, Directive, TemplateRef, ViewContainerRef, OnDestroy, Input, OnInit } from "@angular/core";
import { ElementRenderableFeatures } from "@Directives/element-renderable-features";
import { Subscription } from "rxjs";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { AppAltObjectsEnum } from "../shared/app-alt-objects";
import { BlindPeopleAdaptationService } from "../services/blind-people-adaptation.service";
import { IAdvancedAppAltObject } from "../interfaces/advanced-app-alt-object.interface";

@AutoUnsubscribe()
@Directive({
    selector: "[DescribeForBlindPeople]"
})
export class DescribeForBlindPeopleDirective extends ElementRenderableFeatures implements OnInit, OnDestroy {
    private altObjects: { [key: number]: IAdvancedAppAltObject };

    private altObjectsSubscription: Subscription;

    @Input('DescribeForBlindPeople') private altObject: AppAltObjectsEnum;

    constructor (
        private element: ElementRef,
        private templateRef: TemplateRef<any>,
        private viewContainer: ViewContainerRef,
        private blindPeopleService: BlindPeopleAdaptationService
    ) {
        super(element, templateRef, viewContainer);
    }

    ngOnInit () {
        this.altObjectsSubscription = this.blindPeopleService.Data$.subscribe(data => {
            this.altObjects = data;

            this.processElement();
        });
    }

    ngOnDestroy () {}

    private processElement () {
        try {
            this.viewContainer.createEmbeddedView(this.templateRef);

            const targetElem: HTMLElement = this.element.nativeElement.nextSibling;
            targetElem.setAttribute('alt', this.altObjects[this.altObject].value);
        } catch (error) {}
    }
}
