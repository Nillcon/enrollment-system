import { IAdvancedAppAltObject } from "../interfaces/advanced-app-alt-object.interface";
import { AppAltObjectTypeEnum } from "./app-alt-object-type";

export enum AppAltObjectsEnum {
    IconAppointmentsScrollLeft     = 1,
    IconAppointmentsScrollRight    = 2
}

export const AltObjectInfo: { [key: number]: Partial<IAdvancedAppAltObject> } = {
    [AppAltObjectsEnum.IconAppointmentsScrollLeft]: {
        id: AppAltObjectsEnum.IconAppointmentsScrollLeft,
        type: AppAltObjectTypeEnum.Icon,
        data: 'fas fa-chevron-left fa-2x',
        description: 'Appointments scroll left icon'
    },
    [AppAltObjectsEnum.IconAppointmentsScrollRight]: {
        id: AppAltObjectsEnum.IconAppointmentsScrollRight,
        type: AppAltObjectTypeEnum.Icon,
        data: 'fas fa-chevron-right fa-2x',
        description: 'Appointments scroll right icon'
    }
};
