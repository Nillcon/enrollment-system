import { IBaseAppAltObject } from "./base-app-alt-object.interface";
import { AppAltObjectTypeEnum } from "../shared/app-alt-object-type";

export interface IAdvancedAppAltObject extends IBaseAppAltObject {
    type: AppAltObjectTypeEnum;
    /** #### Data of AltObject.
     * - if @param type `icon`, @param data contains _class of icon_;
     * - if @param type `image`, @param data contains _link on image_;
     */
    data: string;
    description: string;
}
