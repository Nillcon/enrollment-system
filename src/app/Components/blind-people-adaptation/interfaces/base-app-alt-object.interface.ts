import { AppAltObjectsEnum } from "../shared/app-alt-objects";

export interface IBaseAppAltObject {
    id: AppAltObjectsEnum;
    value: string;
}
