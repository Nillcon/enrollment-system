import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DescribeForBlindPeopleDirective } from './directives/describe-for-blind-people.directive';
import { AudioDescriptionTableComponent } from './components/audio-description-table/audio-description-table.component';
import { SharedModule } from '@App/app-shared.module';
import { MatTableModule } from '@angular/material';
import { BlindPeopleAdaptationService } from './services/blind-people-adaptation.service';

@NgModule({
    declarations: [
        DescribeForBlindPeopleDirective,
        AudioDescriptionTableComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MatTableModule
    ],
    exports: [
        DescribeForBlindPeopleDirective,
        AudioDescriptionTableComponent
    ],
    providers: [
        BlindPeopleAdaptationService
    ]
})
export class BlindPeopleAdaptationModule { }
