import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DateMethod } from '@Methods/date.method';
import { MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-date-picker-window',
    templateUrl: './date-picker-window.component.html',
    styleUrls: ['./date-picker-window.component.scss']
})
export class DatePickerWindowComponent implements OnInit {

    public date: FormControl = new FormControl();

    constructor(
        public dialogRef: MatDialogRef<DatePickerWindowComponent>
    ) { }

    ngOnInit() {
    }

    public chooseDate() {
        if (this.date.value) {
            const selectedDate = DateMethod.getLocalTimeString(this.date.value);

            this.dialogRef.close(selectedDate);
        } else {
            this.dialogRef.close();
        }
    }

}
