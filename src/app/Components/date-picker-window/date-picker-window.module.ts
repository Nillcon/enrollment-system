import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatePickerWindowComponent } from './date-picker-window.component';
import { MatDatepickerModule } from '@angular/material';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        DatePickerWindowComponent
    ],
    imports: [
        CommonModule,
        SharedModule,
        MatDatepickerModule
    ],
    entryComponents: [
        DatePickerWindowComponent
    ],
    exports: [
        DatePickerWindowComponent
    ]
})
export class DatePickerWindowModule { }
