import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { IEmoji } from './interfaces/emoji.interface';

@Component({
    selector: 'app-chat-emoji-popup',
    templateUrl: './chat-emoji-popup.component.html',
    styleUrls: ['./chat-emoji-popup.component.scss']
})
export class ChatEmojiPopupComponent implements OnInit {

    @Input() isDisable: boolean = false;

    public isVisible: boolean = false;

    @Output() public OnEmojiPick: EventEmitter<IEmoji> = new EventEmitter();
    @Output() public OnDelete: EventEmitter<boolean> = new EventEmitter();

    constructor() { }

    ngOnInit() { }

    public toogleEmojiPopover() {
        this.isVisible = !this.isVisible;
    }

    public onEmojiSelect(event: IEmoji) {
        this.OnEmojiPick.emit(event);
    }

    public onDelete(event: any) {
        this.OnDelete.emit(true);
    }

}
