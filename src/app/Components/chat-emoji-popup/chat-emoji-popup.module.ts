import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatEmojiPopupComponent } from './chat-emoji-popup.component';
import { NgxEmojModule } from 'ngx-emoj';
import { DxPopoverModule, DxTemplateModule } from 'devextreme-angular';
import { SharedModule } from '@App/app-shared.module';

@NgModule({
    declarations: [
        ChatEmojiPopupComponent
    ],
    imports: [
        CommonModule,
        SharedModule,

        DxPopoverModule,
        DxTemplateModule,
        NgxEmojModule,
    ],
    exports: [
        ChatEmojiPopupComponent
    ]
})
export class ChatEmojiPopupModule { }
