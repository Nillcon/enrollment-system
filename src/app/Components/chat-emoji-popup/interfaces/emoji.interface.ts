export interface IEmoji {
    char: string;
    name: string;
}
