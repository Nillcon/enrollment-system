export module Base64Worker {
    /** Encodes js base64 to c# base64 (removes meta info, for ex.: "data:image/png;base64,") */
    export function encodeToBytes (base64: string): string {
        return base64.replace(/^data:(.*,)?/, '');
    }
}
