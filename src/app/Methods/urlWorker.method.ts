export module urlWorker {
    export function getVariablesListFromURL(url: string = null): object {
        let address: string;
        if (!url) {
            address = window.location.href;
        } else {
            address = url;
        }

        let variables: Array<string> = address.split('?');
        const variablesObject: object = {};

        if (variables.length > 1) {
            variables = variables[1].split('&');

            for (let i: number = 0; i < variables.length; i++) {
                const currVariable: Array<string> = variables[i].split('=');
                variablesObject[currVariable[0]] = currVariable[1];
            }
        }
        return variablesObject;
    }

    export function setValForVariable(variableName: string, value: string): object {
        const variablesList: object = this.getVariablesListFromURL();
        variablesList[variableName] = value;

        return variablesList;
    }

    export function pushValForVariable(variableName: string, value: string, urlObj: object = null): object {
        let variablesList: object;

        if (!urlObj) {
            variablesList = this.getVariablesListFromURL();
        } else {
            variablesList = urlObj;
        }

        if (variablesList[variableName]) {
            variablesList[variableName] = variablesList[variableName] + ',' + value;
        } else {
            variablesList[variableName] = value;
        }

        return variablesList;
    }

    export function removeValFromVariable(variableName: string, value: string, urlObj: object = null): object {
        let variablesList: object;
        let variableVals: Array<string>;

        if (!urlObj) {
            variablesList = this.getVariablesListFromURL();
        } else {
            variablesList = urlObj;
        }

        if (variablesList[variableName]) {
            variableVals = variablesList[variableName].split(',');
            for (let i = 0; i < variableVals.length; i++) { if (variableVals[i] === value) { variableVals.splice(i, 1); } }
            variablesList[variableName] = variableVals.join(',');
        }

        if (!variablesList[variableName] || variablesList[variableName] === '') { delete variablesList[variableName]; }

        return variablesList;
    }

    export function removeVariable(variableName: string) {
        const variablesList: object = this.getVariablesListFromURL();
        delete variablesList[variableName];

        return variablesList;
    }

    export function variableIssetInUrl(variableName: string): boolean {
        const variablesList: object = this.getVariablesListFromURL();
        return (typeof (variablesList[variableName]) !== 'undefined');
    }
}
