export module ByteMaskWorker {
    export function getBinaryArray(number: number, digit?: number): number[] {
        const byteString = number.toString(2);
        let byteArray = byteString
            .split('')
            .map(elem => Number(elem));

        if (digit) {
            if (byteArray.length < digit) {
                const missingDigits = Array(digit - byteArray.length).fill(0);

                byteArray = [...missingDigits, ...byteArray];
            }
        }

        return byteArray;
    }

    export function getIntFromBinaryArray(binaryArray: number[]): number {
        const binaryString = binaryArray.join('');

        return parseInt(binaryString, 2);
    }
}
