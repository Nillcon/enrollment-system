export module DateMethod {
    export function toStringFormat(date: Date | string) {
        switch (typeof date) {
            case 'string':
                return new Date(date);

            case 'object':
                return date;
        }
    }

    export function minusDays (date: Date, days: number) {
        return new Date(date.setDate(date.getDate() - days));
    }

    export function plusDays (date: Date, days: number) {
        return new Date(date.setDate(date.getDate() + days));
    }

    export function getLocalTimeString (date: Date): string {
        const targetDate = new Date(date);
        const minutes = targetDate.getMinutes();
        const offset = targetDate.getTimezoneOffset();

        targetDate.setMinutes(minutes - offset);
        return targetDate.toISOString();
    }

    export function getLocalJsonTimeString (date: Date): string {
        const targetDate = new Date(date);
        const minutes = targetDate.getMinutes();
        const offset = targetDate.getTimezoneOffset();

        targetDate.setMinutes(minutes - offset);
        return targetDate.toJSON().slice(0, -1);
    }

    export function isToday(date: Date | string): boolean {
        const _date = new Date(date);
        const dateNow = new Date();

        return (_date.toDateString() === dateNow.toDateString());
    }
}
